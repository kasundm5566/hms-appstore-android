## 1. Prerequisites
* Java 1.8 (set JAVA_HOME before building)
* Android SDK with platform support from 3 to above (set ANDROID_HOME before building)
* Gradle 2.14.1 (set GRADLE_HOME before building)

## 2. How to Change Address Configurations

1. Go to the following file;
        /appstore-android/src/<PROFILE_NAME>/res/raw/profile_property.xml

2. Change the IP Address and the Port in the following Properties;
        rest.host
        rest.host.http
        appstore.image.url
        iap.server.url

     For Live Connection
    ======================

    #### Vodafone
        https://apps.vodafone.com.fj

    #### Dialog
        https://www.allapps.lk

    #### Robi
        https://store.bdapps.com
        
    For Genymotion with Local Setup; Change the IP Address to 10.0.3.2    
    For Android Emulator with Local Setup; Change the IP Address to 10.0.2.2

    ##### Dialog Staging Configurations (as of 2015-June-15)

        <entry key="rest.host">https://stagingideamart.dialog.lk</entry> <!--ip:6578-->
        <entry key="rest.host.http">http://stagingideamart.dialog.lk</entry> <!--ip:6578-->
        <entry key="appstore.image.url">https://stagingallapps.dialog.lk/appstore/</entry> <!--ip:4287-->
        <entry key="iap.server.url">https://stagingallapps.dialog.lk/iap-api/v1/trx/endpoint</entry> <!--ip:7596-->
       
3. To change the Terms and Conditions link, Go to the following file;    
        /appstore-android/src/<PROFILE_NAME>/res/values/strings.xml
    
   Replace the 'terms_and_condition_caption' string property's <a href=""> with the correct URL;

## 3. How to Test
       
### 3.1. How to Test Google Analytics Integration
- Create a test anayltics account and get the tracking id
- Change the Analytics Tracking ID found in
        /appstore-android/src/<profile>/res/raw/profile_property.xml
            <entry key="google.analytics.tracking.id">UA-54233333-3</entry>
-  Change the 'ga_trackingId' found in 
        /appstore-android/src/<profile>/res/xml/app_tracker.xml
        /appstore-android/src/<profile>/res/xml/ecommerce_tracker.xml
        
### 3.2 How to Test 'Keep Me Logged In' Functionality
<integer name="login_session_expiry_time_seconds" translatable="false">2592000</integer>
Change the above line to a shorter period for testing purposes. 
    Location : /appstore-android/src/main/res/values/config.xml 
    Note: Value is in Seconds        

### 3.3 How to test 'Auto Login' flow
In http headers, we need to have specific headers for auto_login according to the profile.
Therefore, we need to change the following properties in '/appstore-android/src/<profile>/res/raw/profile_property.xml' as follows.

    <entry key="test.mode.enable">true</entry>
    <entry key="test.mode.rest.client.headers">msisdn:6799900000,X-Forwarded-For:192.168.1.101</entry>


## 4. How to Add a New Flavor/New Locale

Follow the profile-creation-checklist.md file in docs/checklists directory

[Creating new Flavors and Adding new Locales](https://gitlablive.hsenidmobile.com/appstore/appstore-android/blob/master/docs/checklists/profile-creation-checklist.md)

## 5. How to Build


<pre>Software Requirements</pre>

* Java Version - JDK 1.8
* Gradle Version - 2.14.1
* CompileSdkVersion - 23
* buildToolsVersion - 23.0.2
* Update the Android SDK to support latest android builds


<pre>
Latest Softwares and SDKs can be found at;
</pre>

* Android SDK - smb://172.16.0.13/softwares/Linux/Dev_tools/Android/android-sdk-lite/android-sdk-linux-20170112
* Gradle 2.14.1 - smb://172.16.0.13/softwares/Linux/Dev_tools/Gradle/gradle-2.14.1-bin.zip

<pre>Build Steps</pre>

* While being in the parent appstore-android folder, run the following command;

   ` gradle assemble<profile_name>`

   Eg: gradle assembleVodafone

* After the Build is Successful, go to the following path

    ` appstore-android/appstore-android/build/outputs/apk`

* APK files belonging to the _Dialog_, _Robi_, _Vodafone_, _MTN_, _Stream_, _Operator_ Product Flavors (Profiles) can be found here.

    <pre>
    For  *Dialog*, use _appstore-android-dialog-release.apk_
    For  *Vodafone*, use _appstore-android-vodafone-release.apk_
    For  *Robi*, use _appstore-android-robi_bd-release.apk_
    </pre>

## 6. In-app purchasing

For testing purposes, if the in-app purchase request type is secured(https) it has to set 'inapp_request_secure_bypass' to 'true' in config.xml.
In RestClient it has been bypassed through NewSSLSocketFactory. In real environment, it should be set to 'false'.

In Command Line execute the following 

<pre>
android list sdk --all

android update sdk -u -a -t <package no.>
</pre>

Select the required package numbers from the list and update the SDK accordingly

### 7. How to change Splash screen duration
Goto **/appstore-android/src/main/res/raw/property.xml** and change the follwing property value in Milliseconds.

     <entry key="splash.duration">4000</entry>

