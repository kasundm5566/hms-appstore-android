package hms.appstore.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import hms.appstore.android.util.Log;
import hms.appstore.android.util.PushNotification;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHandler";
    private static DatabaseHandler instance = null;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "appstore";

    private static final String TABLE_PUSH_MSG = "pushmessage";

    private static final String TABLE_PUSH_MSG_ID = "id";
    private static final String TABLE_PUSH_MSG_CONTENT = "message";
    private static final String TABLE_PUSH_MSG_READ = "isread";
    private static final String TABLE_PUSH_MSG_DATE = "date";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static DatabaseHandler getInstance(Context context) {

        if (instance == null) {
            instance = new DatabaseHandler(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_TABLE_MESSAGE = "CREATE TABLE " + TABLE_PUSH_MSG + "("
                + TABLE_PUSH_MSG_ID + " TEXT PRIMARY KEY,"
                + TABLE_PUSH_MSG_CONTENT + " TEXT,"
                + TABLE_PUSH_MSG_READ + " INTEGER DEFAULT 0,"
                + TABLE_PUSH_MSG_DATE + " INTEGER" + ")";

        db.execSQL(CREATE_TABLE_MESSAGE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PUSH_MSG);
        onCreate(db);
    }

    public void addMessage(PushNotification pushNotification) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TABLE_PUSH_MSG_ID, pushNotification.getId());
        values.put(TABLE_PUSH_MSG_CONTENT, pushNotification.getMessage());
        values.put(TABLE_PUSH_MSG_READ, (pushNotification.isRead() ? 1 : 0));
        values.put(TABLE_PUSH_MSG_DATE, pushNotification.getDate());
        //Avoid Key  duplications
        db.insertWithOnConflict(TABLE_PUSH_MSG, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public PushNotification getMessage(String id) {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_PUSH_MSG, new String[]{TABLE_PUSH_MSG_ID,
                        TABLE_PUSH_MSG_CONTENT, TABLE_PUSH_MSG_READ, TABLE_PUSH_MSG_DATE}, TABLE_PUSH_MSG_ID + "=?",
                new String[]{id}, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            PushNotification pushNotification = new PushNotification(cursor.getString(0), cursor.getString(1), cursor.getInt(2) == 1, cursor.getLong(3));
            return pushNotification;
        }
        return null;
    }

    public ArrayList<PushNotification> getAllMessages() {

        ArrayList<PushNotification> messageList = new ArrayList<PushNotification>();

        String selectQuery = "SELECT  * FROM " + TABLE_PUSH_MSG + " ORDER BY " + TABLE_PUSH_MSG_DATE + " DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                PushNotification pushNotification = new PushNotification(cursor.getString(0), cursor.getString(1), cursor.getInt(2) == 1, cursor.getLong(3));
                messageList.add(pushNotification);
            } while (cursor.moveToNext());
        }

        return messageList;
    }

    public boolean updateMessage(PushNotification pushNotification) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TABLE_PUSH_MSG_ID, pushNotification.getId());
        contentValues.put(TABLE_PUSH_MSG_CONTENT, pushNotification.getMessage());
        contentValues.put(TABLE_PUSH_MSG_READ, pushNotification.isRead());
        contentValues.put(TABLE_PUSH_MSG_DATE, pushNotification.getDate());

        db.update(TABLE_PUSH_MSG, contentValues, "id = ?", new String[]{pushNotification.getId()});
        Log.d(TAG, "Message reading state updated");
        return true;
    }

}