package hms.appstore.android.activity.app;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

import hms.appstore.android.R;
import hms.appstore.android.activity.CustomToolBar;
import hms.appstore.android.activity.MainActivity;
import hms.appstore.android.util.AppTags;
import hms.appstore.android.util.Property;
import hms.appstore.android.util.Tags;
import hms.appstore.android.util.widget.LocalizedTextView;

public class AppInstallationActivity extends CustomToolBar {

    private static final String TAG = "AppInstallActivity";
    private String appId, appName, downloadReqUri;
    private long downloadReqId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_install);
        initCustomToolBar();

        appId = getIntent().getStringExtra(Tags.TAG_APP_ID.name());
        appName = getIntent().getStringExtra(Tags.TAG_NAME.name());
        downloadReqId = getIntent().getLongExtra(Property.DOWNLOAD_REQ_ID_EXTRA, 0);
        downloadReqUri = getIntent().getStringExtra(Property.DOWNLOAD_REQ_URI_EXTRA);

        LocalizedTextView appNameText = (LocalizedTextView) findViewById(R.id.installation_app_name);
        appNameText.setText(appName);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        SubMenu sub1 = menu.addSubMenu("").setIcon(R.drawable.abs__ic_menu_moreoverflow_normal_holo_dark);
        sub1.add(0, 1, 1, getString(R.string.menu_item_home_caption))
                .setIcon(R.drawable.android_home)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        sub1.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home || item.getItemId() == 0) {
            return false;
        }

        int selection = item.getItemId();

        if (selection == 1) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
        }
        return true;
    }

    public void onInstallAppClick(View view) {
        Intent installationIntent = new Intent(Intent.ACTION_VIEW);
        installationIntent.setDataAndType(Uri.parse(downloadReqUri), AppTags.APK_MIME_TYPE);
        startActivity(installationIntent);
        super.finish();
    }

    public void onCancelButtonClick(View view) {
        super.finish();
    }

}
