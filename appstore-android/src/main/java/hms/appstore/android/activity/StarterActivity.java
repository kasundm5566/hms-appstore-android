/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.gson.Gson;

import java.util.Locale;

import hms.appstore.android.AppstoreApp;
import hms.appstore.android.R;
import hms.appstore.android.activity.app.SettingsActivity;
import hms.appstore.android.activity.user.SignInActivity;
import hms.appstore.android.rest.ConnectionRefusedException;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.rest.RestException;
import hms.appstore.android.rest.response.AuthProviderResponse;
import hms.appstore.android.rest.response.AutoLoginResponse;
import hms.appstore.android.util.AppstoreLocale;
import hms.appstore.android.util.ErrorCodeMapping;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.LoginKeys;
import hms.appstore.android.util.ProfileProperty;
import hms.appstore.android.util.Property;
import hms.appstore.android.util.SessionKeys;
import hms.appstore.android.util.manager.user.LoggedUser;

public class StarterActivity extends AppCompatActivity {
    private Locale locale = null;
    public static boolean isAcceptTermsAndConditions = false;
    public static boolean isAutoInstallAppsChecked = false;

    SharedPreferences sharedPreferences;
    Intent intentMainActivity;
    String autoLoginUrl = Property.AUTO_LOGIN_URL;
    String authProviderUrl = Property.AUTH_PROVIDER_URL;
    GetAuthProvider getAuthProviderAsyncTask = new GetAuthProvider();
    GetAutoLogin getAutoLoginAsyncTask = new GetAutoLogin();

    protected boolean _active = true;

    static final String LOG_TAG = StarterActivity.class.getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        //Get a Tracker (should auto-report)
        ((AppstoreApp) getApplication()).getTracker(AppstoreApp.TrackerName.APP_TRACKER);

        sharedPreferences = getSharedPreferences(Property.PREFS_NAME, 0);
        SettingsActivity.isShowAndroidAppsOnly = sharedPreferences.getBoolean("IsShowAndroidAppsOnly", false);
        isAcceptTermsAndConditions = sharedPreferences.getBoolean("isAcceptTermsAndConditions", false);
        isAutoInstallAppsChecked = sharedPreferences.getBoolean(Property.PREFS_AUTO_INSTALL_DOWNLOADABLES, true);

        checkUserSessionAvailability();

        if (getResources().getBoolean(R.bool.multi_language_support)) {
            AppstoreLocale.appLocale = sharedPreferences.getString(Property.PREFS_APP_LOCALE, ProfileProperty.DEFAULT_LOCALE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(Property.PREFS_APP_LOCALE, AppstoreLocale.appLocale);
            editor.commit();
            changeLanguage();
        }

        if (getResources().getBoolean(R.bool.auto_login_enabled)) {
            getAuthProviderAsyncTask.execute();
        }

        // thread for displaying the SplashScreen
        Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    while (_active && (waited < Property.SPLASH_DURATION)) {
                        sleep(100);
                        if (_active) {
                            waited += 100;
                        }
                    }
                } catch (InterruptedException e) {
                    // do nothing
                } finally {
                    if (!isAcceptTermsAndConditions) {
                        Intent i = new Intent(getApplicationContext(), TermsAndConditionActivity.class);
                        startActivity(i);
                    } else {
                        intentMainActivity = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intentMainActivity);
                        finish();
                    }
                }
            }
        };
        splashTread.start();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
            setContentView(R.layout.splash_screen_land);
        else
            setContentView(R.layout.splash_screen);
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts & uncaught exceptions etc.
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("IsShowAndroidAppsOnly", SettingsActivity.isShowAndroidAppsOnly);
        editor.putBoolean("isAcceptTermsAndConditions", isAcceptTermsAndConditions);
        editor.putString(Property.PREFS_APP_LOCALE, AppstoreLocale.appLocale);
        editor.putBoolean(Property.PREFS_AUTO_INSTALL_DOWNLOADABLES, isAutoInstallAppsChecked);
        editor.commit();
        //Stop the analytics tracking
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
    }

    private void checkUserSessionAvailability() {
        String loggedUserJson = sharedPreferences.getString(Property.PREFS_LOGGED_USER, "");
        if (!loggedUserJson.equals("")) {
            LoggedUser loggedUser = new Gson().fromJson(loggedUserJson, LoggedUser.class);
            long userSessionExpiryTime =
                    Long.valueOf(getResources().getInteger(R.integer.login_session_expiry_time_seconds)) * 1000;
            if ((loggedUser.getLoggedInTime() + userSessionExpiryTime) > System.currentTimeMillis()) {
                SignInActivity.session.put(SessionKeys.SESSION_ID.name(), loggedUser.getSessionId());
                SignInActivity.session.put(SessionKeys.LOGIN_METHOD.name(), loggedUser.getLoginMethod().name());
            }
        }
    }

    public void changeLanguage() {
        Configuration config = getBaseContext().getResources().getConfiguration();
        if (!"".equals(AppstoreLocale.appLocale) && !config.locale.getLanguage().equals(AppstoreLocale.appLocale)) {
            locale = new Locale(AppstoreLocale.appLocale);
            Locale.setDefault(locale);
            Configuration conf = new Configuration(config);
            conf.locale = locale;
            getBaseContext().getResources().updateConfiguration(conf, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    public class GetAutoLogin extends AsyncTask<Void, Context, AutoLoginResponse> {

        @Override
        protected AutoLoginResponse doInBackground(Void... params) {

            AutoLoginResponse autoLoginResponse = null;
            String json;
            try {
                json = new RestClient().getHttp(autoLoginUrl);
                autoLoginResponse = new Gson().fromJson(json, AutoLoginResponse.class);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (ConnectionRefusedException e) {
                Intent i = new Intent(getApplicationContext(), NoConnectionActivity.class);
                startActivity(i);
                finish();
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }
            String sessionId;
            if (autoLoginResponse != null && ErrorCodeMapping.isSuccess(autoLoginResponse.getStatusCode())) {
                sessionId = autoLoginResponse.getSessionId();
                SignInActivity.session.put(SessionKeys.SESSION_ID.name(), sessionId);
                SignInActivity.session.put(SessionKeys.LOGIN_METHOD.name(), LoginKeys.AUTO_LOGIN.name());
                SignInActivity.session.put(SessionKeys.LOGIN_STATUS_CODE.name(), autoLoginResponse.getStatusCode());
            }
            return autoLoginResponse;
        }

        @Override
        protected void onPostExecute(AutoLoginResponse autoLoginResponse) {


        }
    }

    public class GetAuthProvider extends AsyncTask<Void, Context, AuthProviderResponse> {

        @Override
        protected AuthProviderResponse doInBackground(Void... params) {

            AuthProviderResponse authProviderResponse = null;
            String json;
            try {
                json = new RestClient().getHttp(authProviderUrl);
                authProviderResponse = new Gson().fromJson(json, AuthProviderResponse.class);
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }

            return authProviderResponse;
        }

        @Override
        protected void onPostExecute(AuthProviderResponse authProviderResponse) {

            if (authProviderResponse != null) {
                if (authProviderResponse.getPreferredProvider().equals(getString(R.string.auth_by_msisdn))) {
                    getAutoLoginAsyncTask.execute();
                }
            }

        }
    }


}
