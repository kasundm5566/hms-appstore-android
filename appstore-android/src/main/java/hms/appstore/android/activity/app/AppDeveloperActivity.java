package hms.appstore.android.activity.app;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;

import hms.appstore.android.R;
import hms.appstore.android.activity.CustomToolBar;
import hms.appstore.android.fragment.PopularAppsFragment;
import hms.appstore.android.rest.ConnectionRefusedException;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.rest.RestException;
import hms.appstore.android.rest.request.ProfileDetailsRequest;
import hms.appstore.android.rest.response.Application;
import hms.appstore.android.rest.response.ApplicationListResponse;
import hms.appstore.android.rest.response.CategoriesBanner;
import hms.appstore.android.rest.response.LocationBanner;
import hms.appstore.android.rest.response.ProfileDetailsResponse;
import hms.appstore.android.util.ApplicationLayoutHelper;
import hms.appstore.android.util.ErrorCodeMapping;
import hms.appstore.android.util.GridRecyclerAdapter;
import hms.appstore.android.util.HeaderGridView;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.PaginGridAdapter;
import hms.appstore.android.util.ProfileProperty;
import hms.appstore.android.util.Property;
import hms.appstore.android.util.Tags;

import static hms.appstore.android.activity.app.AppDescriptionActivity.LOG_TAG;

public class AppDeveloperActivity extends CustomToolBar implements BaseSliderView.OnSliderClickListener {

    private String devAppsUrl = Property.DEV_APPS_URL;
    private String developerName;
    private ImageLoader imageLoader;
    private ImageLoaderConfiguration config;
    private DisplayImageOptions options;
    private Context context;
    private TextView tvTitle, tvDesc;
    private ImageView imgProfile;
    private SliderLayout slider;
    private ProfileDetailsResponse profileDetailsResponse = null;
    private ArrayList<Application> appList = new ArrayList<>();
    private ImageView imgBanner;
    private PaginGridAdapter adapter = null;
    private HeaderGridView gridView;
    private int offset = 0;
    boolean isLoading = false;
    private boolean loadNextPage = false;
    private LinearLayout retryLayout, llSlider, llAppList, llFooterLoading, loadingLayout;
    private ArrayList<Application> devAppList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_all_fragment);
        context = this;
        setUI();

        adapter = new PaginGridAdapter(this, appList);
        gridView.setAdapter(adapter);

        DevBannersAsyncTask devBannersAsyncTask = new DevBannersAsyncTask();
        devBannersAsyncTask.execute();
        DevProfileDetailsAsyncTask devProfileDetailsAsyncTask = new DevProfileDetailsAsyncTask();
        devProfileDetailsAsyncTask.execute(developerName);
        GetDeveloperApps getDeveloperApps = new GetDeveloperApps();
        getDeveloperApps.execute();
    }

    private void setUI() {

        developerName = getIntent().getExtras().getString("DEVELOPER_NAME");
        initCustomToolBar();
        getSupportActionBar().setTitle(developerName);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        retryLayout = (LinearLayout) findViewById(R.id.retry_layout);
        llAppList = (LinearLayout) findViewById(R.id.llAppList);
        llFooterLoading = (LinearLayout) findViewById(R.id.llFooterLoading);
        retryLayout.setVisibility(View.GONE);
        gridView = (HeaderGridView) findViewById(R.id.rvApps);
        LinearLayout headerLoading = (LinearLayout) findViewById(R.id.loading_layout);
        headerLoading.setVisibility(View.GONE);

        View headerView = getLayoutInflater().inflate(R.layout.app_developer, null);
        gridView.addHeaderView(headerView);

        tvTitle = (TextView) headerView.findViewById(R.id.tvTitle);
        tvDesc = (TextView) headerView.findViewById(R.id.tvDesc);
        imgProfile = (ImageView) headerView.findViewById(R.id.imgProfile);
        slider = (SliderLayout) headerView.findViewById(R.id.sliderDev);
        imgBanner = (ImageView) headerView.findViewById(R.id.imgBanner);
        llSlider = (LinearLayout) headerView.findViewById(R.id.llHomeBanner);
        loadingLayout = (LinearLayout) headerView.findViewById(R.id.loading_layout);

        // Get singletone instance of ImageLoader
        imageLoader = ImageLoader.getInstance();
        // Initialize ImageLoader with configuration. Do it once.
        config = new ImageLoaderConfiguration.Builder(this)
                .threadPoolSize(5)
                .memoryCache(new UsingFreqLimitedMemoryCache(2 * 1024 * 1024))
                .build();
        imageLoader.init(config);

        options = new DisplayImageOptions.Builder()
                .cacheInMemory()
                .build();

        tvTitle.setText(developerName);

        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (loadNextPage && !isLoading) {
                    if (firstVisibleItem + visibleItemCount >= totalItemCount) {
                        // End has been reached
                        System.out.println(LOG_TAG + " Pagination bottom");
                        llFooterLoading.setVisibility(View.VISIBLE);
                        offset += Property.LIST_ITEM_LIMIT;
                        GetDeveloperApps getDeveloperApps = new GetDeveloperApps();
                        getDeveloperApps.execute();
                    }
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public class GetDeveloperApps extends AsyncTask<String, Context, ApplicationListResponse> {

        boolean showRetry = false;

        @Override
        protected void onPreExecute() {
            isLoading = true;
        }

        @Override
        protected ApplicationListResponse doInBackground(String... params) {
            ApplicationListResponse applicationListResponse = null;
            String fullUrl;
            fullUrl = devAppsUrl + developerName + "/start/" + offset + "/limit/" + Property.LIST_ITEM_LIMIT;

            try {
                String json = new RestClient().get(fullUrl);
                applicationListResponse = new Gson().fromJson(json, ApplicationListResponse.class);
                showRetry = false;
            } catch (ConnectionRefusedException e) {
                Log.d(LOG_TAG, "ConnectionRefusedException ", e);
                showRetry = true;
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
                showRetry = true;
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                showRetry = true;
            }

            if (applicationListResponse != null && ErrorCodeMapping.isSuccess(applicationListResponse.getStatusCode()) && applicationListResponse.getResults().length >= 0)
                return applicationListResponse;
            return null;
        }

        protected void onPostExecute(ApplicationListResponse applicationListResponse) {

            isLoading = false;
            loadNextPage = true;
            llFooterLoading.setVisibility(View.GONE);
            if (showRetry) {
                retryLayout.setVisibility(View.VISIBLE);
            } else {
                retryLayout.setVisibility(View.GONE);
                if (applicationListResponse != null && ErrorCodeMapping.isSuccess(applicationListResponse.getStatusCode())) {
                    devAppList = new ArrayList<Application>(Arrays.asList(applicationListResponse.getResults()));
                }
                if (devAppList.size() != 0) {
                    appList.addAll(devAppList);

                    if (appList.size() == 0)
                        gridView.setVisibility(View.GONE);
                    else {
                        gridView.setVisibility(View.VISIBLE);
                        adapter.setAppList(appList);
                        adapter.notifyDataSetChanged();
                    }

                } else {
                    loadNextPage = false;
                    System.out.println(LOG_TAG + " next page false");
                }
            }
            loadingLayout.setVisibility(View.GONE);
        }
    }

    public class DevBannersAsyncTask extends AsyncTask<String, Context, ArrayList<HashMap<String, LocationBanner>>> {

        ArrayList<HashMap<String, LocationBanner>> categoryBannerList = new ArrayList<HashMap<String, LocationBanner>>();

        @Override
        protected ArrayList<HashMap<String, LocationBanner>> doInBackground(String... params) {

            CategoriesBanner categoriesBanner = null;
            try {
                String json = new RestClient().get(Property.APP_DEV_BANNERS);
                Log.d("JSON", json);
                categoriesBanner = new Gson().fromJson(json, CategoriesBanner.class);
            } catch (ConnectionRefusedException e) {
                Log.d(LOG_TAG, "ConnectionRefusedException ", e);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }

            if (categoriesBanner != null && ErrorCodeMapping.isSuccess(categoriesBanner.getStatusCode())) {
                for (int i = 0; i < categoriesBanner.getResults().length; i++) {
                    HashMap<String, LocationBanner> banner = new HashMap<String, LocationBanner>();
                    banner.put(categoriesBanner.getResults()[i].getDescription(), categoriesBanner.getResults()[i]);
                    categoryBannerList.add(banner);
                }
            }
            return categoryBannerList;
        }

        @Override
        protected void onPostExecute(ArrayList<HashMap<String, LocationBanner>> hashMaps) {
            super.onPostExecute(hashMaps);

            if (categoryBannerList != null && categoryBannerList.size() == 0) {
                imgBanner.setVisibility(View.GONE);
                slider.setVisibility(View.GONE);
                llSlider.setVisibility(View.GONE);
            } else {
                llSlider.setVisibility(View.VISIBLE);
                if (categoryBannerList.size() < 2) {
                    slider.setVisibility(View.GONE);
                    imgBanner.setVisibility(View.VISIBLE);
                    final LocationBanner banner = categoryBannerList.get(0).entrySet().iterator().next().getValue();
                    loadSingleBanner(banner.getImage_url(), imgBanner);
                    imgBanner.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (banner != null && banner.getLink() != null && !banner.getLink().isEmpty()) {
                                try {
                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(banner.getLink()));
                                    startActivity(browserIntent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                } else {
                    imgBanner.setVisibility(View.GONE);
                    slider.setVisibility(View.VISIBLE);
                    setImageSlider(categoryBannerList);
                }
            }
        }
    }

    private void loadSingleBanner(String appImgUrl, ImageView imgView) {
        String imageUrl = ProfileProperty.APPSTORE_IMAGE_URL + appImgUrl;
        Glide.with(this)
                .load(imageUrl)
                .asBitmap()
                .into(imgView);
    }

    private void setImageSlider(ArrayList<HashMap<String, LocationBanner>> categoryBannerList) {

        for (HashMap<String, LocationBanner> bannerMap : categoryBannerList) {

            DefaultSliderView textSliderView = new DefaultSliderView(this);
            textSliderView
//                    .description(name)
                    .image(ProfileProperty.APPSTORE_IMAGE_URL.concat(bannerMap.entrySet().iterator().next().getValue().getImage_url()))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra", bannerMap.entrySet().iterator().next().getValue().getLink());
            slider.addSlider(textSliderView);
        }
        slider.setPresetTransformer(SliderLayout.Transformer.Default);
        slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new DescriptionAnimation());
        slider.setDuration(5000);
        slider.addOnPageChangeListener(null);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

        if (slider.getBundle().get("extra") != null && !slider.getBundle().get("extra").toString().equals("")) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(slider.getBundle().get("extra").toString()));
            try {
                startActivity(browserIntent);
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                e.printStackTrace();
            }
        }
    }

    public class DevProfileDetailsAsyncTask extends AsyncTask<String, Context, ProfileDetailsResponse> {

        @Override
        protected ProfileDetailsResponse doInBackground(String... params) {
            String profileDetailsJson = "";
            String username = params[0];
            ProfileDetailsRequest profileDetailsRequest = new ProfileDetailsRequest(username);
            try {
                profileDetailsJson = new RestClient().post(Property.DEV_ACCOUNT_PROFILE_DETAIL_URL, profileDetailsRequest);
                profileDetailsResponse = new Gson().fromJson(profileDetailsJson, ProfileDetailsResponse.class);
            } catch (RestException e) {
                Log.d(SettingsActivity.LOG_TAG, "RestException ", e);
            } catch (ConnectionRefusedException e) {
                Log.d(SettingsActivity.LOG_TAG, "ConnectionRefusedException ", e);
            } catch (Exception e) {
                Log.d(SettingsActivity.LOG_TAG, "Exception ", e);
            }
            Log.d("JSON", "JSON object recieved in Async Task\n" + profileDetailsJson);

            return profileDetailsResponse;
        }

        @Override
        protected void onPostExecute(ProfileDetailsResponse result) {
            super.onPostExecute(result);

            if (result != null) {
                setImage(ProfileProperty.APPSTORE_IMAGE_URL + result.getImage());
                tvDesc.setText(result.getEmail());
            }
        }
    }

    private void setImage(String image) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        // Initialize ImageLoader with configuration. Do it once.
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .threadPoolSize(5)
                .memoryCache(new UsingFreqLimitedMemoryCache(2 * 1024 * 1024))
                .build();
        imageLoader.init(config);

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory()
                .displayer(new RoundedBitmapDisplayer(1000))
                .build();

        // Load and display image asynchronously
        imageLoader.displayImage(image, imgProfile, options);
    }

    private void loadAppImage(String appImgUrl, ImageView imgView) {
        String imageUrl = ProfileProperty.APPSTORE_IMAGE_URL + appImgUrl;
        // Load and display image asynchronously
        imageLoader.displayImage(imageUrl, imgView, options);
    }

    private View.OnClickListener setAppOnClickAction(final Application app) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = ((TextView) v.findViewById(R.id.single_app_name)).getText().toString();
                String app_id = ((TextView) v.findViewById(R.id.single_app_id)).getText().toString();

                // Starting new intent
                Intent in = new Intent(context, AppDescriptionActivity.class);
                in.putExtra(Tags.TAG_NAME.name(), name);
                in.putExtra(Tags.TAG_APP_ID.name(), app_id);
                in.putExtra("app", app);
                context.startActivity(in);
            }
        };
    }
}
