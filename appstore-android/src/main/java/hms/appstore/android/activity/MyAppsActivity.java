/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import java.util.HashMap;

import hms.appstore.android.R;
import hms.appstore.android.activity.app.SearchActivity;
import hms.appstore.android.activity.app.SettingsActivity;
import hms.appstore.android.activity.user.SignInActivity;
import hms.appstore.android.fragment.MyAppsUIFragmentAdapter;
import hms.appstore.android.util.ConnectionDetector;
import hms.appstore.android.util.LoginKeys;
import hms.appstore.android.util.Property;
import hms.appstore.android.util.RedirectKeys;
import hms.appstore.android.util.SessionKeys;

public class MyAppsActivity extends CustomToolBar {
    ConnectionDetector cd = null;
    private TabLayout tabLayout;
    int tabSelectedColor, tabDefaultColor;
    private int tabPosition = 0;
    private  MyAppsUIFragmentAdapter mAdapter;
    private ViewPager mPager;
    private Bundle extras = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_app_page_with_tabs);
        initCustomToolBar();
        getSupportActionBar().setTitle(R.string.title_my_apps_store);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        extras = getIntent().getExtras();


        tabSelectedColor = ContextCompat.getColor(this, R.color.colorPrimary);
        tabDefaultColor = ContextCompat.getColor(this, R.color.feature_icon_default_color);
        mAdapter = new MyAppsUIFragmentAdapter(getSupportFragmentManager(), this);

        cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            mPager = (ViewPager) findViewById(R.id.pager);
            mPager.setAdapter(mAdapter);
            mPager.setOffscreenPageLimit(2); //determines the number of fragments to be pre-loaded
            mPager.setCurrentItem(0);
            tabLayout = (TabLayout) findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(mPager);
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
            createTabIcons(this);

            if (getIntent().hasExtra(Property.REDIRECT_KEY)) {
                int tabItem = Property.MYAPPS_FRAG_MAP.get(extras.getString(Property.REDIRECT_KEY));
                mPager.setCurrentItem(tabItem);
            }
        } else {
            Intent i = new Intent(getApplicationContext(), NoConnectionActivity.class);
            startActivity(i);
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            if(i == tabPosition)
                tabLayout.getTabAt(i).getIcon().setColorFilter(tabSelectedColor, PorterDuff.Mode.SRC_IN);
            else
                tabLayout.getTabAt(i).getIcon().setColorFilter(tabDefaultColor, PorterDuff.Mode.SRC_IN);
        }
        invalidateOptionsMenu();
    }

    private void createTabIcons(final Context context) {

        tabLayout.getTabAt(0).setIcon(R.drawable.nav_subscribe);
        tabLayout.getTabAt(0).getIcon().setColorFilter(tabSelectedColor, PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(1).setIcon(R.drawable.nav_downloadable);
        tabLayout.getTabAt(1).getIcon().setColorFilter(tabDefaultColor, PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(2).setIcon(R.drawable.update);
        tabLayout.getTabAt(2).getIcon().setColorFilter(tabDefaultColor, PorterDuff.Mode.SRC_IN);

        tabLayout.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(mPager) {

                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        super.onTabSelected(tab);
                        tabPosition = tab.getPosition();
                        tab.getIcon().setColorFilter(tabSelectedColor, PorterDuff.Mode.SRC_IN);
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                        tab.getIcon().setColorFilter(tabDefaultColor, PorterDuff.Mode.SRC_IN);
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }
                }
        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}