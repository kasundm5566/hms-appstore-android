package hms.appstore.android.activity.user;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.gson.Gson;

import hms.appstore.android.R;
import hms.appstore.android.activity.CustomToolBar;
import hms.appstore.android.rest.ConnectionRefusedException;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.rest.RestException;
import hms.appstore.android.rest.request.ChangePasswordRequest;
import hms.appstore.android.rest.response.Response;
import hms.appstore.android.util.Commons;
import hms.appstore.android.util.ErrorCodeMapping;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.Property;
import hms.appstore.android.util.RedirectKeys;
import hms.appstore.android.util.widget.LocalizedTextView;

public class ChangePasswordActivity extends CustomToolBar {

    LinearLayout notificationArea;
    EditText currentPasswordTxt, newPasswordTxt, newPasswordVerifyTxt;
    String username;
    static final String LOG_TAG = ChangePasswordActivity.class.getCanonicalName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password_v2);
        initCustomToolBar();

        username = getIntent().getStringExtra(Property.USERNAME_EXTRA);

        notificationArea = (LinearLayout) findViewById(R.id.notificationArea);
        currentPasswordTxt = (EditText) findViewById(R.id.currentPasswordText);
        newPasswordTxt = (EditText) findViewById(R.id.newPasswordText);
        newPasswordVerifyTxt = (EditText) findViewById(R.id.retypeNewPasswordText);
    }

    public void onNotificationDismiss(View view) {
        if(notificationArea.getVisibility() == View.VISIBLE) {
            Animation out = AnimationUtils.loadAnimation(this, R.anim.slide_out_up);
            out.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {}

                @Override
                public void onAnimationEnd(Animation animation) {
                    notificationArea.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {}
            });
            notificationArea.startAnimation(out);
        }
    }

    public void onPasswordInfoBtnClick(View view) {
        final Dialog passwordInfoDialog = new Dialog(this, R.style.AppDefaultDialogThemeWithNoTitle);
        passwordInfoDialog.setContentView(R.layout.alert_box_with_button);

        LocalizedTextView dialogTitle = (LocalizedTextView) passwordInfoDialog.findViewById(R.id.dialogTitle);
        dialogTitle.setText(getString(R.string.change_pw_password_criteria_dialog_title));
        LocalizedTextView dialogMessage = (LocalizedTextView) passwordInfoDialog.findViewById(R.id.dialogMessage);
        dialogMessage.setText(String.format(
                getString(R.string.change_pw_password_criteria_dialog_content),
                getResources().getInteger(R.integer.user_password_min_length)));
        Button dismissButton = (Button) passwordInfoDialog.findViewById(R.id.btnPositive);
        dismissButton.setText(getString(R.string.change_pw_password_criteria_dialog_btn_text));
        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { passwordInfoDialog.dismiss(); }
        });
        passwordInfoDialog.show();
    }

    public void onChangePasswordBtnClick(View view) {
        if(isValidParams()) {
            String[] params = new String[3];
            params[0] = username;
            params[1] = currentPasswordTxt.getText().toString().trim();
            params[2] = newPasswordTxt.getText().toString().trim();
            ChangePasswordAsyncTask changePasswordAsyncTask = new ChangePasswordAsyncTask();
            changePasswordAsyncTask.execute(params);
        }
    }

    private boolean isValidParams() {
        boolean isValid = true;
        int passwordMinLength = getResources().getInteger(R.integer.user_password_min_length);

        if (Commons.isEditTextEmpty(currentPasswordTxt,
                getString(R.string.change_pw_field_empty_error))) isValid = false;

        if (Commons.isEditTextEmpty(newPasswordTxt,
                getString(R.string.change_pw_field_empty_error))) isValid = false;
        else if (newPasswordTxt.getText().toString().trim().length() < passwordMinLength) {
            newPasswordTxt.setError(String.format(getString(R.string.change_pw_password_length_error), passwordMinLength));
            isValid = false;
        }

        if (Commons.isEditTextEmpty(newPasswordVerifyTxt,
                getString(R.string.register_re_password_error_empty))) isValid = false;
        else if (!newPasswordVerifyTxt.getText().toString().trim().equals(newPasswordTxt.getText().toString().trim())) {
            newPasswordVerifyTxt.setError(getString(R.string.change_pw_re_password_error_mismatch));
            isValid = false;
        }

        return isValid;
    }

    public class ChangePasswordAsyncTask extends AsyncTask<String, Context, Response> {

        ProgressDialog dialog = new ProgressDialog(ChangePasswordActivity.this);

        @Override
        protected void onPreExecute() {
            dialog.setMessage(getString(R.string.change_pw_request_processing));
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.show();
        }

        @Override
        protected Response doInBackground(String... params) {
            String username = params[0];
            String oldPassword = params[1];
            String newPassword = params[2];

            ChangePasswordRequest changePasswordRequest =
                    new ChangePasswordRequest(username, oldPassword, newPassword);
            Response changePasswordResponse = null;
            String changePasswordRespJson = "";

            try {
                changePasswordRespJson = new RestClient().post(Property.CHANGE_PASSWORD_URL, changePasswordRequest);
                changePasswordResponse = new Gson().fromJson(changePasswordRespJson, Response.class);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (ConnectionRefusedException e) {
                Log.d(LOG_TAG, "ConnectionRefusedException ", e);
            } catch (Exception e) {
                Log.d(LOG_TAG, "Exception ", e);
            }
            Log.d("JSON", "JASON object recieved in Async Task\n" + changePasswordRespJson);

            return changePasswordResponse;
        }

        @Override
        protected void onPostExecute(Response result) {
            super.onPostExecute(result);
            dialog.dismiss();

            if (result != null) {
                if (ErrorCodeMapping.isSuccess(result.getStatusCode())) {
                    Intent intent = new Intent(getApplicationContext(), SignInActivity.class);
                    intent.putExtra(Property.NOTIFICATION_EXTRA, getString(R.string.change_pw_request_successfully_completed));
                    intent.putExtra(Property.REDIRECT_KEY, RedirectKeys.HOME.name());
                    startActivity(intent);
                } else {
                    Commons.showToastMessage(result.getStatusDescription(), ChangePasswordActivity.this);
                }
            } else {
                Commons.showToastMessage(getString(R.string.E5000), ChangePasswordActivity.this);
            }
        }
    }
}
