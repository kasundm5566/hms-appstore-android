/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.activity.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import hms.appstore.android.R;
import hms.appstore.android.activity.CustomToolBar;
import hms.appstore.android.activity.MainActivity;
import hms.appstore.android.util.AppstoreLocale;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.Property;
import hms.appstore.android.util.widget.LocalizedCheckbox;
import hms.appstore.android.util.widget.LocalizedRadioButton;
import hms.appstore.android.util.widget.LocalizedTextView;

public class SettingsActivity extends CustomToolBar implements CompoundButton.OnCheckedChangeListener {

    public static boolean isShowAndroidAppsOnly = false;

    Map<String, Integer> localeMap;
    private boolean isOptionsChanged = false;
    private Locale locale = null;
    private Configuration config = null;
    SharedPreferences sharedPerference;

    LocalizedCheckbox androidAppsOnlyCheckBox;
    LocalizedCheckbox autoUpdateCheckBox;
    LocalizedCheckbox autoInstallAppsCheckbox;
    LinearLayout langSupportArea;
    LinearLayout androidAppOnlySettingArea;
    LinearLayout autoInstallAppsSettingArea;
    RadioGroup languageRadioGroup;
    LocalizedRadioButton englishLangSupportRadioBtn;
    LocalizedRadioButton bengaliLangSupportRadioBtn;
    LocalizedRadioButton russianLangSupportRadioBtn;
    LocalizedTextView version;

    static final String LOG_TAG = SettingsActivity.class.getCanonicalName();
    private TextView tvUpdate, tvAutoInstall, tvAppsOnly;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        initCustomToolBar();
        getSupportActionBar().setTitle(R.string.main_screen_title_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedPerference = getSharedPreferences(Property.PREFS_NAME, 0);
        config = new Configuration();
        AppstoreLocale.appLocale = sharedPerference.getString(Property.PREFS_APP_LOCALE, AppstoreLocale.ENGLISH_LOCALE);

        initializeUIElements();
        activateAndroidAppsOnlySetting();
        activateLanguageSupportSettings();
        activateAutoAppInstallationSetting();

        try {
            version.setText(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (isOptionsChanged) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }
    }

    private void changeLanguage(Integer buttonId) {
        for (Map.Entry<String, Integer> localeMapEntry : localeMap.entrySet()) {
            if (buttonId.equals(localeMapEntry.getValue())) {
                AppstoreLocale.appLocale = localeMapEntry.getKey();
            }
        }

        SharedPreferences.Editor editor = sharedPerference.edit();
        editor.putString(Property.PREFS_APP_LOCALE, AppstoreLocale.appLocale);
        editor.commit();

        locale = new Locale(AppstoreLocale.appLocale);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, null);
        finish();
        startActivity(getIntent());
    }

    private void initializeUIElements() {
        version = (LocalizedTextView) findViewById(R.id.settings_version_value);
        autoUpdateCheckBox = (LocalizedCheckbox) findViewById(R.id.auto_update_checkbox);
        androidAppsOnlyCheckBox = (LocalizedCheckbox) findViewById(R.id.android_apps_only_checkbox);
        autoInstallAppsCheckbox = (LocalizedCheckbox) findViewById(R.id.app_auto_install_checkbox);
        langSupportArea = (LinearLayout) findViewById(R.id.language_support_area);
        androidAppOnlySettingArea = (LinearLayout) findViewById(R.id.android_apps_only_setting_area);
        autoInstallAppsSettingArea = (LinearLayout) findViewById(R.id.app_auto_install_setting_area);
        languageRadioGroup = (RadioGroup) findViewById(R.id.language_radio_group);
        englishLangSupportRadioBtn = (LocalizedRadioButton) findViewById(R.id.lang_english_radio_btn);
        bengaliLangSupportRadioBtn = (LocalizedRadioButton) findViewById(R.id.lang_bengali_radio_btn);
        russianLangSupportRadioBtn = (LocalizedRadioButton) findViewById(R.id.lang_russian_radio_btn);
        tvUpdate = (TextView) findViewById(R.id.tvUpdate);
        tvAutoInstall = (TextView) findViewById(R.id.tvAutoInstall);
        tvAppsOnly = (TextView) findViewById(R.id.tvAppsOnly);

        autoUpdateCheckBox.setOnCheckedChangeListener(this);
        androidAppsOnlyCheckBox.setOnCheckedChangeListener(this);
        autoInstallAppsCheckbox.setOnCheckedChangeListener(this);
    }

    private void activateAutoAppInstallationSetting() {
        if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) && getResources().getBoolean(R.bool.auto_install_downloadable_apps)) {
            autoInstallAppsSettingArea.setVisibility(View.VISIBLE);
            autoInstallAppsCheckbox.setChecked(sharedPerference.getBoolean(Property.PREFS_AUTO_INSTALL_DOWNLOADABLES, true));

            if(sharedPerference.getBoolean(Property.PREFS_AUTO_INSTALL_DOWNLOADABLES, true))
                tvAutoInstall.setText(R.string.setting_enable);
            else
                tvAutoInstall.setText(R.string.setting_disable);

            autoInstallAppsCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    SharedPreferences.Editor editor = sharedPerference.edit();
                    editor.putBoolean(Property.PREFS_AUTO_INSTALL_DOWNLOADABLES, isChecked);
                    editor.commit();
                    if (isChecked)
                        tvAutoInstall.setText(R.string.setting_enable);
                    else
                        tvAutoInstall.setText(R.string.setting_disable);
                }
            });
        } else {
            autoInstallAppsSettingArea.setVisibility(View.GONE);
        }
    }


    private void activateAndroidAppsOnlySetting() {
        if (getResources().getBoolean(R.bool.android_apps_only_setting_enable)) {
            androidAppsOnlyCheckBox.setChecked(isShowAndroidAppsOnly);
            androidAppsOnlyCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    isShowAndroidAppsOnly = androidAppsOnlyCheckBox.isChecked();
                    isOptionsChanged = true;
                }
            });
        } else {
            androidAppOnlySettingArea.setVisibility(View.GONE);
        }
    }

    private void activateLanguageSupportSettings() {
        if (getResources().getBoolean(R.bool.multi_language_support)) {
            localeMap = new HashMap<>();
            localeMap.put(AppstoreLocale.ENGLISH_LOCALE, englishLangSupportRadioBtn.getId());

            if (getResources().getBoolean(R.bool.bengali_language_support)) {
                localeMap.put(AppstoreLocale.BENGALI_LOCALE, bengaliLangSupportRadioBtn.getId());
            } else {
                bengaliLangSupportRadioBtn.setVisibility(View.GONE);
            }

            if (getResources().getBoolean(R.bool.russian_language_support)) {
                localeMap.put(AppstoreLocale.RUSSIAN_LOCALE, russianLangSupportRadioBtn.getId());
            } else {
                russianLangSupportRadioBtn.setVisibility(View.GONE);
            }

            languageRadioGroup.check(localeMap.get(AppstoreLocale.appLocale));
            languageRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    changeLanguage(checkedId);
                }
            });

        } else {
            langSupportArea.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        switch (buttonView.getId()) {

            case R.id.auto_update_checkbox:
                if (isChecked)
                    tvUpdate.setText(R.string.setting_enable);
                else
                    tvUpdate.setText(R.string.setting_disable);
                break;

            case R.id.android_apps_only_checkbox:
                if (isChecked)
                    tvAppsOnly.setText(R.string.setting_enable);
                else
                    tvAppsOnly.setText(R.string.setting_disable);
                break;
        }
    }
}
