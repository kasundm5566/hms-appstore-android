/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.activity.app;

import android.support.v4.view.ViewPager;

import hms.appstore.android.activity.CustomToolBar;
import hms.appstore.android.fragment.category.CategoryUIFragmentAdapter;

public class AppsForCategoryParentActivity extends CustomToolBar {
    CategoryUIFragmentAdapter mAdapter;
    ViewPager mPager;
}
