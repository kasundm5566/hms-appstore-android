package hms.appstore.android.activity.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.signature.StringSignature;
import com.google.gson.Gson;

import hms.appstore.android.R;
import hms.appstore.android.activity.CustomToolBar;
import hms.appstore.android.activity.user.SignInActivity;
import hms.appstore.android.rest.ConnectionRefusedException;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.rest.RestException;
import hms.appstore.android.rest.request.ProfileDetailsRequest;
import hms.appstore.android.rest.response.ProfileDetailsResponse;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.ProfileProperty;
import hms.appstore.android.util.Property;
import hms.appstore.android.util.SessionKeys;

import static hms.appstore.android.activity.app.SettingsActivity.LOG_TAG;

public class MyAccountActivity extends CustomToolBar {

    TextView tvProfileName, tvProfileEmail, tvMobile, tvDOB;
    ImageView imgProfileEdit, imgProfile;
    ProfileDetailsResponse profileDetailsResponse = null;
    SharedPreferences sharedPerference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_account);
        setUI();
        setListeners();
        sharedPerference = getSharedPreferences(Property.PREFS_NAME, 0);
    }

    private void setListeners() {
        imgProfileEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), EditMyAccountActivity.class);
                if (profileDetailsResponse != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString("PROFILE_FULL_NAME", profileDetailsResponse.getFirstName() + " " + profileDetailsResponse.getLastName());
                    bundle.putString("PROFILE_BD", profileDetailsResponse.getBirthDate());
                    bundle.putString("PROFILE_MOBILE_NO", profileDetailsResponse.getMsisdn().split(getString(R.string.country_mobile_code))[1]);
                    bundle.putString("PROFILE_EMAIL", profileDetailsResponse.getEmail());
                    bundle.putString("PROFILE_IMAGE", profileDetailsResponse.getImage());
                    bundle.putString("PROFILE_USER_TYPE", profileDetailsResponse.getUserType());
                    i.putExtras(bundle);
                }
                startActivity(i);
            }
        });
    }

    private void setUI() {
        initCustomToolBar();
        getSupportActionBar().setTitle(R.string.my_account_header);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        imgProfileEdit = (ImageView) findViewById(R.id.imgProfileEdit);
        tvProfileName = (TextView) findViewById(R.id.tvProfileName);
        tvProfileEmail = (TextView) findViewById(R.id.tvProfileEmail);
        tvMobile = (TextView) findViewById(R.id.tvMobile);
        tvDOB = (TextView) findViewById(R.id.tvDOB);
    }

    public class MyAccountProfileDetailsAsyncTask extends AsyncTask<String, Context, ProfileDetailsResponse> {

        ProgressDialog dialog = new ProgressDialog(MyAccountActivity.this);

        @Override
        protected void onPreExecute() {
            dialog.setMessage(getString(R.string.my_account_profile_loading));
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.show();
        }

        @Override
        protected ProfileDetailsResponse doInBackground(String... params) {
            String profileDetailsJson = "";
            String  url = Property.MY_ACCOUNT_PROFILE_DETAILS;
            if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
                url += "/session-id/" + SignInActivity.session.get(SessionKeys.SESSION_ID.name());
            }
            try {
                profileDetailsJson = new RestClient().get(url);
                profileDetailsResponse = new Gson().fromJson(profileDetailsJson, ProfileDetailsResponse.class);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (ConnectionRefusedException e) {
                Log.d(LOG_TAG, "ConnectionRefusedException ", e);
            } catch (Exception e) {
                Log.d(LOG_TAG, "Exception ", e);
            }
            Log.d("JSON", "JSON object recieved in Async Task\n" + profileDetailsJson);

            return profileDetailsResponse;
        }

        @Override
        protected void onPostExecute(ProfileDetailsResponse result) {
            super.onPostExecute(result);
            imgProfileEdit.setVisibility(View.VISIBLE);

            if (result != null) {
                tvProfileName.setText(result.getFirstName() + " " + result.getLastName());
                tvProfileEmail.setText(result.getEmail());
                tvMobile.setText(result.getMsisdn());
                tvDOB.setText(result.getBirthDate());
                setImage(ProfileProperty.APPSTORE_IMAGE_URL + result.getImage());
            }
            dialog.dismiss();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setImage(String image) {
        System.out.println("MyAccount SetImage " + image);
        Glide.with(getApplicationContext()).load(image).asBitmap().signature(new StringSignature(String.valueOf(System.currentTimeMillis()))).placeholder(R.drawable.icon_user).error(R.drawable.icon_user).centerCrop().into(new BitmapImageViewTarget(imgProfile) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(view.getContext().getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                imgProfile.setImageDrawable(circularBitmapDrawable);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("MyAccount resume");
        MyAccountProfileDetailsAsyncTask myAccountProfileDetailsAsyncTask = new MyAccountProfileDetailsAsyncTask();
        myAccountProfileDetailsAsyncTask.execute(sharedPerference.getString(Property.PREFS_USERNAME, ""));
    }
}
