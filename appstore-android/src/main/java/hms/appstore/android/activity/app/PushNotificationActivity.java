package hms.appstore.android.activity.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;

import hms.appstore.android.R;
import hms.appstore.android.activity.CustomToolBar;
import hms.appstore.android.activity.user.SignInActivity;
import hms.appstore.android.db.DatabaseHandler;
import hms.appstore.android.rest.ConnectionRefusedException;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.rest.RestException;
import hms.appstore.android.rest.response.PushNotificationResponse;
import hms.appstore.android.util.ErrorCodeMapping;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.ProfileProperty;
import hms.appstore.android.util.Property;
import hms.appstore.android.util.PushNotification;
import hms.appstore.android.util.PushNotificationListAdapter;
import hms.appstore.android.util.RedirectKeys;
import hms.appstore.android.util.SessionKeys;
import hms.appstore.android.util.widget.PushMessage;

public class PushNotificationActivity extends CustomToolBar {

    private ListView lvNotifications;
    private TextView tvNoMessages;
    private LinearLayout loadingLayout;
    private LinearLayout retryLayout;
    private ArrayList<PushMessage> messageList = null;
    private PushNotificationListAdapter adapter;
    private Context context;
    private GetPushMessageAsync getPushMessageAsync;
    private BroadcastReceiver broadcastReceiver;
    private static final String LOG_TAG = PushNotification.class.getCanonicalName();
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.push_notifications);
        sharedPreferences = getSharedPreferences(Property.PREFS_NAME, 0);
        context = this;
        setUI();
        checkSignInStatus();

        adapter = new PushNotificationListAdapter(this, R.layout.list_item_notifications, messageList);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("PUSH_NOTIFICATION_RECEIVED")) {
                    adapter.setNotificationArrayList(null);
                    adapter.notifyDataSetChanged();

                    getPushMessageAsync = new GetPushMessageAsync();
                    getPushMessageAsync.execute();
                }
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("PUSH_NOTIFICATION_RECEIVED"));
    }

    private void checkSignInStatus() {
        if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
            System.out.println("Notification Message SESSION Available");
        } else {
            if (SignInActivity.session.containsKey(SessionKeys.LOGIN_STATUS_CODE.name())) {
                if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name()) && SignInActivity.session.get(SessionKeys.LOGIN_STATUS_CODE.name()).equalsIgnoreCase(Property.NO_USER_FOR_MSISDN_STATUS)) {
                    new AlertDialog.Builder(this)
                            .setTitle(R.string.register_dialog_title)
                            .setMessage(R.string.register_dialog_message)
                            .setPositiveButton(R.string.register_dialog_title, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent(Intent.ACTION_VIEW);
                                    Uri u = Uri.parse(ProfileProperty.REGISTRATION_LINK);
                                    intent.setData(u);
                                    startActivity(intent);
                                }

                            })
                            .setNegativeButton(R.string.register_dialog_positive_negative_text, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .show();
                }

            } else {
                handleSignInAction();
            }
        }
    }

    private void handleSignInAction() {
        if (!SignInActivity.session.isEmpty()) {
            SignInActivity.session.clear();
        }
        Toast.makeText(this, R.string.login_message, Toast.LENGTH_LONG).show();
        Intent i = new Intent(getApplicationContext(), SignInActivity.class);
        i.putExtra(Property.REDIRECT_KEY, RedirectKeys.PUSH_NOTIFICATIONS.name());
        startActivity(i);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (broadcastReceiver != null)
            unregisterReceiver(broadcastReceiver);
    }

    private void setUI() {
        initCustomToolBar();
        getSupportActionBar().setTitle(R.string.main_screen_title_push_notifications);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        lvNotifications = (ListView) findViewById(R.id.lvNotifications);
        tvNoMessages = (TextView) findViewById(R.id.tvNoMessages);
        loadingLayout = (LinearLayout) findViewById(R.id.categories_loading_layout);
        retryLayout = (LinearLayout) findViewById(R.id.retry_layout);

        Button retryButton = (Button) retryLayout.findViewById(R.id.btn_no_connection);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryLayout.setVisibility(View.GONE);
                loadingLayout.setVisibility(View.VISIBLE);
                getPushMessageAsync = new GetPushMessageAsync();
                getPushMessageAsync.execute();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public class GetPushMessageAsync extends AsyncTask<String, Context, ArrayList<PushMessage>> {

        boolean showRetry = false;
        ArrayList<PushMessage> messageList = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            loadingLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<PushMessage> doInBackground(String... params) {
            String url = Property.APP_PUSH_NOTIFICATION_URL;
            String sessionID = "";
            if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
                sessionID =  SignInActivity.session.get(SessionKeys.SESSION_ID.name());
            }

            System.out.println("Notification Message SESSION ID" + SignInActivity.session.get(SessionKeys.SESSION_ID.name()));
            url += "/session-id/" + sessionID + "/firebase-token-id/" + sharedPreferences.getString(Property.PREFS_FCM_TOKEN, "") + "/start/" + 0 + "/limit/" + Property.PUSH_MESSAGE_LIMIT;

            PushNotificationResponse pushNotificationResponse = null;
            try {
                String json = new RestClient().get(url);
                Log.d("JSON", json);
                pushNotificationResponse = new Gson().fromJson(json, PushNotificationResponse.class);
                showRetry = false;
            } catch (ConnectionRefusedException e) {
                Log.d(LOG_TAG, "ConnectionRefusedException ", e);
                showRetry = true;
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
                showRetry = true;
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                showRetry = true;
            }

            if (pushNotificationResponse != null && ErrorCodeMapping.isSuccess(pushNotificationResponse.getStatusCode())) {
                for (int i = 0; i < pushNotificationResponse.getMessages().length; i++) {

                    PushMessage pushMessage = pushNotificationResponse.getMessages()[i];
                    if (DatabaseHandler.getInstance(context).getMessage(pushMessage.getRequestId()) != null) {
                        if (DatabaseHandler.getInstance(context).getMessage(pushMessage.getRequestId()).isRead()) {
                            pushMessage.setRead(true);
                        } else {
                            pushMessage.setRead(false);
                        }
                    } else {
                        DatabaseHandler.getInstance(context).addMessage(new PushNotification(pushMessage.getRequestId(), false));
                        pushMessage.setRead(false);
                    }
                    messageList.add(pushMessage);
                }
            }
            return messageList;
        }

        @Override
        protected void onPostExecute(ArrayList<PushMessage> result) {
            super.onPostExecute(result);

            if (result != null && result.size() != 0) {
                tvNoMessages.setVisibility(View.GONE);
                lvNotifications.setVisibility(View.VISIBLE);
            } else
                tvNoMessages.setVisibility(View.VISIBLE);

            if (showRetry) {
                retryLayout.setVisibility(View.VISIBLE);
                tvNoMessages.setVisibility(View.GONE);
            } else {
                retryLayout.setVisibility(View.GONE);
                adapter.setNotificationArrayList(result);
                lvNotifications.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
            loadingLayout.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.setNotificationArrayList(null);
        adapter.notifyDataSetChanged();
        loadingLayout.setVisibility(View.VISIBLE);
        getPushMessageAsync = new GetPushMessageAsync();
        getPushMessageAsync.execute();
    }
}
