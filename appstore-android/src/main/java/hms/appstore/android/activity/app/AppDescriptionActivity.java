/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.activity.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import hms.appstore.android.R;
import hms.appstore.android.activity.CustomToolBar;
import hms.appstore.android.activity.NoConnectionActivity;
import hms.appstore.android.activity.user.SignInActivity;
import hms.appstore.android.db.DatabaseHandler;
import hms.appstore.android.rest.ConnectionRefusedException;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.rest.RestException;
import hms.appstore.android.rest.request.ProfileDetailsRequest;
import hms.appstore.android.rest.request.RatingRequest;
import hms.appstore.android.rest.request.UserCommentRequest;
import hms.appstore.android.rest.response.AppDownloadResponse;
import hms.appstore.android.rest.response.AppInstruction;
import hms.appstore.android.rest.response.Application;
import hms.appstore.android.rest.response.ApplicationDetailResponse;
import hms.appstore.android.rest.response.ApplicationListResponse;
import hms.appstore.android.rest.response.AuthProviderResponse;
import hms.appstore.android.rest.response.AutoLoginResponse;
import hms.appstore.android.rest.response.ChargeByPIResponse;
import hms.appstore.android.rest.response.DownloadableBinary;
import hms.appstore.android.rest.response.DownloadableBinaryDetails;
import hms.appstore.android.rest.response.ProfileDetailsResponse;
import hms.appstore.android.rest.response.Response;
import hms.appstore.android.util.AppTags;
import hms.appstore.android.util.ErrorCodeMapping;
import hms.appstore.android.util.Formatter;
import hms.appstore.android.util.GridRecyclerAdapter;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.LoginKeys;
import hms.appstore.android.util.ProfileProperty;
import hms.appstore.android.util.Property;
import hms.appstore.android.util.PushNotification;
import hms.appstore.android.util.RedirectKeys;
import hms.appstore.android.util.SessionKeys;
import hms.appstore.android.util.Tags;
import hms.appstore.android.util.ToastExpander;
import hms.appstore.android.util.UserReview;
import hms.appstore.android.util.manager.analytics.AnalyticEventDispatcher;
import hms.appstore.android.util.manager.downloadable.DownloadableApplication;
import hms.appstore.android.util.manager.downloadable.DownloadableApplications;
import hms.appstore.android.util.widget.LocalizedButton;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class AppDescriptionActivity extends CustomToolBar {

    static final String LOG_TAG = AppDescriptionActivity.class.getCanonicalName();
    private static final int PERMISSION_REQUEST_CODE = 1;
    public static String session = "";
    private static String appId;
    final String piDownloadUrl = Property.CHARGE_DOWNLOAD_URL;
    String[] params;
    Toolbar toolbar;
    int bitmap_count = 0;
    ArrayList<HashMap<String, String>> singleApp = null;
    HashMap<String, String> chargingDetailsWithOperators = null;
    GridRecyclerAdapter adapter = null;
    AppDescriptionActivity sfa;
    LinearLayout app_screenshot_layout = null;
    boolean isSubscriptionApp = false;
    boolean isDownloadRequiresSubscription = false;
    boolean isDownloadableApp = false;
    boolean isUpdateAvailable = false;
    boolean isSubscribed = false;
    boolean isRegPending = false;
    boolean isDownloadAvailable = false;
    boolean isLaunchAppActionHandler = false;
    boolean isAddComment = false;
    Bundle extras = null;
    List<AppInstruction> appInstructionList = new ArrayList<AppInstruction>();
    List<DownloadableBinaryDetails> downloadableBinaryList = new ArrayList<DownloadableBinaryDetails>();
    List<UserReview> userReviewList = new ArrayList<UserReview>();
    ArrayList<UserReview> userReviewListAll = new ArrayList<UserReview>();
    String commentAllUrl = Property.ALL_USER_COMMENT_URL;
    // Auto Login
    String autoLoginUrl = Property.AUTO_LOGIN_URL;
    String authProviderUrl = Property.AUTH_PROVIDER_URL;
    GetAuthProvider getAuthProviderAsyncTask = new GetAuthProvider();

    // Validate user by session ID
    GetAutoLogin getAutoLoginAsyncTask = new GetAutoLogin();
    String validateUserBySessionIdUrl = Property.VALIDATE_USER_BY_SESSION_ID_URL;
    String validateUserSuffix = Property.VALIDATE_USER_BY_SESSION_ID_URL_SUFFIX;
    EditText search;
    GetSingleApp getSingleAppAsyncTask;
    DoDownload doDownloadAsyncTask;
    DoUnsubscribe doUnsubscribeAsyncTask;
    DoSubscribe doSubscribeAsyncTask;
    GetRelatedApps getRelatedAppsAsyncTask;
    BitmapDownloaderTask bitmapDownloaderTask;
    AddCommentAsyncTask addCommentAsyncTask;
    ReportAbuseAsyncTask reportAbuseAsyncTask;
    GetAllComments getAllCommentsAsyncTask;
    TextView txtAppName, txtAppCategory, txtAppChargingLabel, txtAppCreatedBy, txtAppDescription,
            txtCharging, txtSupportedVersions, txtSupportedVersionsCaption, viewAllLink, descriptionLabel, costLabel, instructionsLabel;
    Button button;
    ImageView appIcon;
    HorizontalScrollView screenShotScroller;
    Button addReviewButton;
    // PI Dialog
    EditText edtTxtPin;
    Button btnInAppConfirm;
    String piDonwloadFullUrl;
    AppDownloadResponse appDownloadResponseResult;
    int isPISelected = -1;  // 0 for Mobile Account , 1 for M-PAiSA and so on

    // Version Selection Dialog
    Dialog confirmDialog;
    int curOsVersion;

    // Subscribe and Unsubscribe Dialogs
    Dialog subscribeDialog;
    AlertDialog unsubscribeDialog;

//    String sessionIdTemp = "";

    TextView readMore, tvDownloads, tvRatings, tvUsers;
    String detailedDesc;
    String detailedCharging;
    String detailedChargingLbl;
    String detailedAppName;
    String detailedAppCreatedBy;
    RecyclerView recyclerView;
    LocalizedButton btnDeveloperApps;
    Context context;
    private String devAppsUrl = Property.DEV_APPS_URL;
    LinearLayout similarAppsLayout;
    LinearLayout devAppsLinearLayout;
    ImageLoader imageLoader;
    ImageLoaderConfiguration config;
    DisplayImageOptions options;
    ImageView imgProfile;
    SharedPreferences sharedPerference;
    LocalizedButton viewAllSimilarApps;
    LinearLayout llSimilarApps;
    ArrayList<Application> similarApps = null;
    RatingBar rateThis;
    LinearLayout llRateThis;
    LinearLayout relatedApps;
    float rateValue;
    TextView tvAppRating;
    LinearLayout llDownloadsLable;
    TextView lblDownloads;
    ImageView imgSubscription;
    ListView relatedAppList;
    private RelativeLayout rlDownload;
    int LIST_ITEM_WIDTH = Property.LIST_ITEM_WIDTH;

    Typeface font = null;

    RadioGroup paymentListRadioGroup;

    //AutoInstalation Manager
    DownloadManager downloadManager;
    long downloadQueueId;
    SharedPreferences sharedPrefs;

    boolean isNoSessionWithAutoLogin = false;

    private Application application = null;
    private String url = Property.APP_DETAILS_URL;
    private TextWatcher filterTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

    };
    private long downloadsCount = 0;
    private List<String> imageUrlList = null;

    public static String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.app_description);
        initCustomToolBar();

        if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
            session = SignInActivity.session.get(SessionKeys.SESSION_ID.name());
        }

        sharedPrefs = getSharedPreferences(Property.PREFS_NAME, 0);
        context = this;

        txtAppName = (TextView) findViewById(R.id.txt_app_des_name);
        txtAppCategory = (TextView) findViewById(R.id.txt_app_des_category);
        txtAppCreatedBy = (TextView) findViewById(R.id.txt_app_des_created_by);
        txtAppChargingLabel = (TextView) findViewById(R.id.txt_app_des_charging_label);
        txtAppDescription = (TextView) findViewById(R.id.app_des_app_description);
        txtCharging = (TextView) findViewById(R.id.app_des_subscriber_subscription);
        txtSupportedVersions = (TextView) findViewById(R.id.app_des_supported_versions);
        txtSupportedVersionsCaption = (TextView) findViewById(R.id.supported_versions_caption);
        button = (Button) findViewById(R.id.btn_download);
        tvAppRating = (TextView) findViewById(R.id.single_app_rating);
        appIcon = (ImageView) findViewById(R.id.app_des_app_icon);
        screenShotScroller = (HorizontalScrollView) findViewById(R.id.screenshot_layout_scroll);
        addReviewButton = (Button) findViewById(R.id.add_comment_button);
        viewAllLink = (TextView) findViewById(R.id.lnkViewAllComments);
        descriptionLabel = (TextView) findViewById(R.id.description_label);
        costLabel = (TextView) findViewById(R.id.cost_label);
        instructionsLabel = (TextView) findViewById(R.id.instructions_label);
        readMore = (TextView) findViewById(R.id.readMore);
        readMore.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showReadMoreDialog(detailedDesc);
            }
        });
        tvDownloads = (TextView) findViewById(R.id.tvDownloads);
        tvRatings = (TextView) findViewById(R.id.tvRatings);
        tvUsers = (TextView) findViewById(R.id.tvUsers);
        recyclerView = (RecyclerView) findViewById(R.id.rvSimilarApps);
        btnDeveloperApps = (LocalizedButton) findViewById(R.id.btnDeveloperApps);
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        viewAllSimilarApps = (LocalizedButton) findViewById(R.id.viewAllSimilarApps);
        llSimilarApps = (LinearLayout) findViewById(R.id.llSimilarApps);
        rateThis = (RatingBar) findViewById(R.id.rateThis);
        llRateThis = (LinearLayout) findViewById(R.id.llRateThis);
        llDownloadsLable = (LinearLayout) findViewById(R.id.llDownloadsLable);
        lblDownloads = (TextView) findViewById(R.id.lblDownloads);
        imgSubscription = (ImageView) findViewById(R.id.imgSubscription);
        imgSubscription.setColorFilter(ContextCompat.getColor(context, R.color.feature_icon_default_color), android.graphics.PorterDuff.Mode.SRC_IN);
        rlDownload = (RelativeLayout) findViewById(R.id.rlDownload);
        // Get singletone instance of ImageLoader
        imageLoader = ImageLoader.getInstance();
        // Initialize ImageLoader with configuration. Do it once.
        config = new ImageLoaderConfiguration.Builder(this)
                .threadPoolSize(5)
                .memoryCache(new UsingFreqLimitedMemoryCache(2 * 1024 * 1024))
                .build();
        imageLoader.init(config);

        options = new DisplayImageOptions.Builder()
                .cacheInMemory()
                .build();

        txtAppCreatedBy.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startAppDeveloperActivity();
            }
        });

        btnDeveloperApps.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startAppDeveloperActivity();
            }
        });

        similarAppsLayout = (LinearLayout) findViewById(R.id.similarAppsLinearLayout);
        devAppsLinearLayout = (LinearLayout) findViewById(R.id.devAppsLinearLayout);

        relatedApps = (LinearLayout) findViewById(R.id.related_app_layout);

        relatedAppList = (ListView) findViewById(R.id.related_app_list);

        sfa = AppDescriptionActivity.this;

        getSingleAppAsyncTask = new GetSingleApp();

        extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(Tags.TAG_APP_ID.name())) {
                setAppId(extras.getCharSequence(Tags.TAG_APP_ID.name()).toString());
                getSingleAppAsyncTask.execute(url + getAppId());
            }

            if(extras.containsKey(Tags.TAG_PUSH_NOTIFICATION_ID.name())){
                DatabaseHandler.getInstance(context).addMessage(new PushNotification(extras.getString(Tags.TAG_PUSH_NOTIFICATION_ID.name()), true));
            }
        }

        if (getResources().getBoolean(R.bool.multi_deployment_mode)) {
            txtCharging.setVisibility(View.GONE);
            costLabel.setVisibility(View.GONE);

            instructionsLabel.setText("Costs & Instructions");
        }

        setInitialData();
        if (getSupportActionBar() != null && application != null) {
            getSupportActionBar().setTitle(application.getName());
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (application != null) {
            //Sending the Analytic Event
            AnalyticEventDispatcher analyticEventDispatcher =
                    new AnalyticEventDispatcher("Application", "View", String.format("%s[%s]", application.getName(), application.getId()));
            analyticEventDispatcher.dispatch(this);
        }

        singleApp = getSingleAppAsyncTask.appDetails;
        app_screenshot_layout = (LinearLayout) findViewById(R.id.screenshot_layout);

        sharedPerference = getSharedPreferences(Property.PREFS_NAME, 0);
        MyAccountProfileDetailsAsyncTask myAccountProfileDetailsAsyncTask = new MyAccountProfileDetailsAsyncTask();
        myAccountProfileDetailsAsyncTask.execute(sharedPerference.getString(Property.PREFS_USERNAME, ""));

        GetDeveloperApps getDeveloperApps = new GetDeveloperApps(0, 3);
        getDeveloperApps.execute();

        addReviewButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SignInActivity.session.containsKey(SessionKeys.LOGIN_STATUS_CODE.name())) {
                    if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name()) && SignInActivity.session.get(SessionKeys.LOGIN_STATUS_CODE.name()).equalsIgnoreCase(Property.SUCCESS_STATUS)) {
                        showAddCommentDialog();
                    } else if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name()) && SignInActivity.session.get(SessionKeys.LOGIN_STATUS_CODE.name()).equalsIgnoreCase(Property.NO_USER_FOR_MSISDN_STATUS)) {
                        isAddComment = true;
                        new AlertDialog.Builder(sfa)
                                .setTitle(R.string.register_dialog_title)
                                .setMessage(R.string.register_dialog_message)
                                .setPositiveButton(R.string.register_dialog_title, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        Uri u = Uri.parse(ProfileProperty.REGISTRATION_LINK);
                                        intent.setData(u);
                                        startActivity(intent);
                                    }

                                })
                                .setNegativeButton(R.string.register_dialog_positive_negative_text, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();
                    }

                } else {
                    isAddComment = true;
                    handleSignInAction();
                }

            }
        });

        viewAllLink.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getAllCommentsAsyncTask = new GetAllComments();
                getAllCommentsAsyncTask.execute(commentAllUrl);


            }
        });

        viewAllSimilarApps.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ViewAllActivity.class);
                i.putExtra("APP_CATEGORY", getString(R.string.app_description_similar_apps));
                if (similarApps != null)
                    i.putExtra("URL", Property.RELATED_APPS_URL + getAppId());
                startActivity(i);
            }
        });

        llRateThis.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
                    LayoutInflater inflater = LayoutInflater.from(AppDescriptionActivity.this);
                    final View addView = inflater.inflate(R.layout.dialog_rate_this, null);
                    RatingBar ratingBar = (RatingBar) addView.findViewById(R.id.rating);
                    ratingBar.setRating(sharedPerference.getFloat(sharedPrefs.getString(Property.PREFS_USERNAME, "") + "_" + application.getName(), 0));
                    rateValue = sharedPerference.getFloat(sharedPrefs.getString(Property.PREFS_USERNAME, "") + "_" + application.getName(), 0);

                    ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                        public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                            rateValue = rating;
                            if (fromUser) {
                                ratingBar.setRating((float) Math.ceil(rating));
                            }
                        }
                    });

                    final Dialog rateDialog = new Dialog(context);
                    rateDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    rateDialog.setContentView(addView);
                    rateDialog.show();

                    Button okBtn = (Button) addView.findViewById(R.id.ok_btn);
                    okBtn.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SendRatingsAsyncTask sendRatingsAsyncTask = new SendRatingsAsyncTask();
                            sendRatingsAsyncTask.execute();
                            rateThis.setRating(rateValue);
                            SharedPreferences.Editor editor = sharedPerference.edit();
                            editor.putFloat(sharedPrefs.getString(Property.PREFS_USERNAME, "") + "_" + application.getName(), rateValue);
                            editor.commit();
                            rateDialog.dismiss();

                            finish();
                            startActivity(getIntent());
                        }
                    });
                } else {
                    if (SignInActivity.session.containsKey(SessionKeys.LOGIN_STATUS_CODE.name())) {
                       if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name()) && SignInActivity.session.get(SessionKeys.LOGIN_STATUS_CODE.name()).equalsIgnoreCase(Property.NO_USER_FOR_MSISDN_STATUS)) {
                            new AlertDialog.Builder(sfa)
                                    .setTitle(R.string.register_dialog_title)
                                    .setMessage(R.string.register_dialog_message)
                                    .setPositiveButton(R.string.register_dialog_title, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            Intent intent = new Intent(Intent.ACTION_VIEW);
                                            Uri u = Uri.parse(ProfileProperty.REGISTRATION_LINK);
                                            intent.setData(u);
                                            startActivity(intent);
                                        }

                                    })
                                    .setNegativeButton(R.string.register_dialog_positive_negative_text, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    })
                                    .show();
                        }

                    } else {
                        handleSignInAction();
                    }

                }
            }
        });

        app_screenshot_layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageUrlList != null) {
                    LayoutInflater inflater = LayoutInflater.from(AppDescriptionActivity.this);
                    final View addView = inflater.inflate(R.layout.screenshots_view, null);
                    SliderLayout slider = (SliderLayout) addView.findViewById(R.id.sliderSS);

                    for (String url : imageUrlList) {

                        DefaultSliderView textSliderView = new DefaultSliderView(AppDescriptionActivity.this);
                        textSliderView
//                    .description(name)
                                .image(url)
                                .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                                .setOnSliderClickListener(null);

                        //add extra information
                        textSliderView.bundle(new Bundle());
                        slider.addSlider(textSliderView);
                    }
                    slider.setPresetTransformer(SliderLayout.Transformer.Default);
                    slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                    slider.setCustomAnimation(new DescriptionAnimation());
                    slider.setDuration(5000);
                    slider.addOnPageChangeListener(null);

                    final Dialog allCommentsDialog = new Dialog(context);
                    allCommentsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    allCommentsDialog.setContentView(addView);
                    allCommentsDialog.show();

                    Button dialogCloseBtn = (Button) addView.findViewById(R.id.close_btn);
                    dialogCloseBtn.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            allCommentsDialog.dismiss();
                        }
                    });

                }
            }
        });
        if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
            try {
                rateThis.setRating(sharedPerference.getFloat(sharedPrefs.getString(Property.PREFS_USERNAME, "") + "_" + application.getName(), 0));
            } catch (Exception e) {
                e.printStackTrace();
                rateThis.setRating(0);
            }
        }
        else
            rateThis.setRating(0);
    }

    public void startAppDeveloperActivity() {
        Intent i = new Intent(getApplicationContext(), AppDeveloperActivity.class);
        i.putExtra("DEVELOPER_NAME", detailedAppCreatedBy);
        startActivity(i);
    }

    private void setPIDialogListners(final View viewDialog, final AlertDialog alertDialog) {

        final GetChargeByPI getChargeByPI = new GetChargeByPI();

        btnInAppConfirm.setOnClickListener(new OnClickListener() {
            @TargetApi(Build.VERSION_CODES.GINGERBREAD)
            @Override
            public void onClick(View v) {

                if (isPISelected == 0) {
                    getChargeByPI.execute(piDonwloadFullUrl);
                    alertDialog.dismiss();
                    viewDialog.setVisibility(View.GONE);
                } else if (isPISelected == 1 && !edtTxtPin.getText().toString().isEmpty()) {
                    String mPin = edtTxtPin.getText().toString();
                    piDonwloadFullUrl = piDonwloadFullUrl + "/pin/" + mPin;
                    getChargeByPI.execute(piDonwloadFullUrl);
                    viewDialog.setVisibility(View.GONE);
                    alertDialog.dismiss();
                } else if (isPISelected == -1) {
                    showMessage(getResources().getString(R.string.pi_dialog_message), getApplicationContext());
                    edtTxtPin.setEnabled(false);
                } else if (isPISelected == 1 && edtTxtPin.getText().toString().isEmpty()) {
                    showMessage(getResources().getString(R.string.pi_dialog_pin_message), getApplicationContext());
                } else {
                    showMessage(getString(R.string.default_error), getApplicationContext());
                    viewDialog.setVisibility(View.GONE);
                    alertDialog.dismiss();
                }
            }
        });
    }

    private void setInitialData() {
        if (extras != null && extras.containsKey("app")) {
            application = (Application) extras.getSerializable("app");
            txtAppName.setText(application.getName());
            txtAppCategory.setText(application.getCategory());
            txtAppChargingLabel.setText(application.getChargingLabel());
            txtAppCreatedBy.setText(application.getDeveloper());
            txtAppDescription.setText(application.getDescription());
            txtCharging.setText(application.getChargingDetails());
            tvAppRating.setText(String.valueOf(application.getRating()));
            tvRatings.setText(String.valueOf(application.getRating()));
            tvUsers.setText(String.valueOf(application.getRateCount()));
            detailedDesc = application.getDescription();
            detailedCharging = application.getChargingDetails();
            detailedChargingLbl = application.getChargingLabel();
            detailedAppName = application.getName();
            detailedAppCreatedBy = application.getDeveloper();
        }
    }

    private void showAddCommentDialog() {
        LayoutInflater inflater = LayoutInflater.from(AppDescriptionActivity.this);
        final View addView = inflater.inflate(R.layout.write_review_form, null);

        final Dialog writeReviewDialog = new Dialog(context);
        writeReviewDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        writeReviewDialog.setContentView(addView);
        writeReviewDialog.show();

        Button submitReviewBtn = (Button) addView.findViewById(R.id.submit_review_btn);
        ImageView dialogCloseButton = (ImageView) addView.findViewById(R.id.dialog_close_button);
        final RadioButton userCommentRadioBtn = (RadioButton) addView.findViewById(R.id.userCommentRadioBtn);
        final RadioButton reportAbuseRadioBtn = (RadioButton) addView.findViewById(R.id.reportAbuseRadioBtn);

        if (!getResources().getBoolean(R.bool.report_abuse_enable)) {
            reportAbuseRadioBtn.setVisibility(View.GONE);
        }

        dialogCloseButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                writeReviewDialog.dismiss();
            }
        });
        submitReviewBtn.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                TextView comment = (TextView) addView.findViewById(R.id.user_review_input);
                if (comment.getText() != null && !comment.getText().equals("") && !comment.getText().toString().equals("")
                        && !comment.getText().toString().trim().equals("")) {
                    if (userCommentRadioBtn.isChecked()) {
                        addCommentAsyncTask = new AddCommentAsyncTask();
                        addCommentAsyncTask.execute(comment.getText().toString());
                    } else if (reportAbuseRadioBtn.isChecked()) {
                        reportAbuseAsyncTask = new ReportAbuseAsyncTask();
                        reportAbuseAsyncTask.execute(comment.getText().toString());
                    }
                    writeReviewDialog.dismiss();
                    finish();
                    startActivity(getIntent());
                } else if (comment.getText() == null || comment.getText().equals("") ||
                        comment.getText().toString().equals("")
                        || comment.getText().toString().trim().equals("")) {
                    Toast.makeText(AppDescriptionActivity.this, getString(R.string.user_review_empty_msg), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (getSingleAppAsyncTask != null && !getSingleAppAsyncTask.isCancelled()) {
            getSingleAppAsyncTask.cancel(true);
        }
        if (getRelatedAppsAsyncTask != null && !getRelatedAppsAsyncTask.isCancelled()) {
            getRelatedAppsAsyncTask.cancel(true);
        }
        if (doDownloadAsyncTask != null && !doDownloadAsyncTask.isCancelled()) {
            doDownloadAsyncTask.cancel(true);
        }
        if (doSubscribeAsyncTask != null && !doSubscribeAsyncTask.isCancelled()) {
            doSubscribeAsyncTask.cancel(true);
        }
        if (doUnsubscribeAsyncTask != null && !doUnsubscribeAsyncTask.isCancelled()) {
            doUnsubscribeAsyncTask.cancel(true);
        }
        if (bitmapDownloaderTask != null && !bitmapDownloaderTask.isCancelled()) {
            bitmapDownloaderTask.cancel(true);
        }
        if (addCommentAsyncTask != null && !addCommentAsyncTask.isCancelled()) {
            addCommentAsyncTask.cancel(true);
        }
        if (reportAbuseAsyncTask != null && !reportAbuseAsyncTask.isCancelled()) {
            reportAbuseAsyncTask.cancel(true);
        }
        if (getAllCommentsAsyncTask != null && !getAllCommentsAsyncTask.isCancelled()) {
            getAllCommentsAsyncTask.cancel(true);
        }
        if (getAuthProviderAsyncTask != null && !getAuthProviderAsyncTask.isCancelled()) {
            getAuthProviderAsyncTask.cancel(true);
        }
        if (getAutoLoginAsyncTask != null && !getAutoLoginAsyncTask.isCancelled()) {
            getAuthProviderAsyncTask.cancel(true);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        int beforeCount = newConfig.screenHeightDp / LIST_ITEM_WIDTH;
        int afterCount = newConfig.screenWidthDp / LIST_ITEM_WIDTH;

        int currentPosition = relatedAppList.getFirstVisiblePosition() * beforeCount;
        if (adapter != null) {
            //adapter.enableAutoMeasure(LIST_ITEM_WIDTH);
            adapter.notifyDataSetChanged();
            relatedAppList.setSelectionFromTop(currentPosition / afterCount, 0);
        }
    }

    @Override
    public void recreate() {
        Intent i = new Intent(sfa, AppDescriptionActivity.class);
        i.putExtra(Tags.TAG_APP_ID.name(), appId);
        startActivity(i);

        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

//        SubMenu sub1 = menu.addSubMenu("").setIcon(R.drawable.abs__ic_menu_moreoverflow_normal_holo_dark);
//        if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
//            sub1.add(0, 1, 1, R.string.menu_item_home_caption)
//                    .setIcon(R.drawable.android_home)
//                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//            sub1.add(0, 2, 2, R.string.menu_item_my_app_store_caption)
//                    .setIcon(R.drawable.appsstore)
//                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//            if (SignInActivity.session.containsKey(SessionKeys.LOGIN_METHOD.name())) {
//                if (SignInActivity.session.get(SessionKeys.LOGIN_METHOD.name()).equalsIgnoreCase(LoginKeys.SIGN_IN.name())) {
//                    sub1.add(0, 3, 3, R.string.menu_item_sign_out_caption)
//                            .setIcon(R.drawable.android_sign_out)
//                            .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//                }
//            }
//            sub1.add(0, 4, 4, R.string.menu_item_settings_caption)
//                    .setIcon(R.drawable.action_settings)
//                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//            sub1.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
//        } else {
//            sub1.add(0, 1, 1, R.string.menu_item_home_caption)
//                    .setIcon(R.drawable.android_home)
//                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//            sub1.add(0, 2, 2, R.string.menu_item_my_app_store_caption)
//                    .setIcon(R.drawable.appsstore)
//                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//            sub1.add(0, 3, 3, R.string.menu_item_sign_in_caption)
//                    .setIcon(R.drawable.device_access_accounts)
//                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//            sub1.add(0, 4, 4, R.string.menu_item_settings_caption)
//                    .setIcon(R.drawable.action_settings)
//                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//            sub1.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
//        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void appActionHandler() {

        final String sessionId = session;
        boolean versionIncluded = false;
        if (isDownloadableApp && !isSubscriptionApp) {
            String[] supportedOsVersionsArray = new String[downloadableBinaryList.size()];
            int i = 0;
            for (DownloadableBinaryDetails downloadableBinaryDetails : downloadableBinaryList) {
                supportedOsVersionsArray[i] = downloadableBinaryDetails.getOsVersion();
                if(Build.VERSION.RELEASE.equals(downloadableBinaryDetails.getOsVersion())){
                    versionIncluded = true;
                    String contentId = downloadableBinaryDetails.getDownloadableBinary().get(0).getContentId();
                    params = new String[2];
                    params[0] = sessionId;
                    params[1] = contentId;

                    if (checkPermission(AppDescriptionActivity.this, WRITE_EXTERNAL_STORAGE)) {
                        doDownloadAsyncTask = new DoDownload();
                        doDownloadAsyncTask.execute(params);
                    } else {
                        requestPermission(AppDescriptionActivity.this, WRITE_EXTERNAL_STORAGE, PERMISSION_REQUEST_CODE);
                    }
                }
                i++;
            }
            try {
                curOsVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            if(!versionIncluded){
                setVersionDialog(supportedOsVersionsArray, sessionId);
            }

//            DownloadableBinaryDetails downloadableBinary = ApiLevelMapper.getDownloadableBinaries(Build.VERSION.SDK_INT, downloadableBinaryList);
//            if (downloadableBinary != null) {
//                String contentId = downloadableBinary.getDownloadableBinary().get(0).getContentId();
//                String[] params = new String[2];
//                params[0] = sessionId;
//                params[1] = contentId;
//                doDownloadAsyncTask = new DoDownload();
//                doDownloadAsyncTask.execute(params);
//            } else {
//                // todo: put the else scenario for 'compatible apk for device not found'
//                setVersionDialog(supportedOsVersionsArray, sessionId);
//            }

            //setVersionDialog(supportedOsVersionsArray, sessionId);
        } else if (isSubscriptionApp && !isDownloadableApp) {
            if (isSubscribed) {
                View doUnsubscribeView = getLayoutInflater().inflate(R.layout.do_unsubscribe_dialog, null);
                Button btnDoUnSubYes = (Button) doUnsubscribeView.findViewById(R.id.btn_do_unsub_yes);
                Button btnDoUnSubNo = (Button) doUnsubscribeView.findViewById(R.id.btn_do_unsub_no);
                final Dialog subscribeDialog = new Dialog(context);
                subscribeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                subscribeDialog.setContentView(doUnsubscribeView);

                btnDoUnSubYes.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        doUnsubscribeAsyncTask = new DoUnsubscribe();
                        doUnsubscribeAsyncTask.execute(sessionId);
                        if(subscribeDialog != null)
                            subscribeDialog.dismiss();
                    }
                });
                btnDoUnSubNo.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(subscribeDialog != null)
                            subscribeDialog.dismiss();
                    }
                });
                subscribeDialog.show();
            } else {
                if (getResources().getBoolean(R.bool.pre_subscribe_enabled)) {
                    handleSubscriptionDialog(sessionId);
                } else {
                    doSubscribeAsyncTask = new DoSubscribe();
                    doSubscribeAsyncTask.execute(sessionId);
                }
            }

            isLaunchAppActionHandler = false;

        } else if (isSubscriptionApp && isDownloadableApp) {
            if (isDownloadRequiresSubscription) {
                if (isSubscribed) {
                    View doOptionsView = getLayoutInflater().inflate(R.layout.unsubscribe_or_download_dialog, null);
                    Button btnDoUnSubYes = (Button) doOptionsView.findViewById(R.id.btn_do_unsub_yes);
                    Button btnDoDownloadYes = (Button) doOptionsView.findViewById(R.id.btn_do_download_yes);
                    Button btnClose = (Button) doOptionsView.findViewById(R.id.btn_close);
                    final Dialog subscribeDialog = new Dialog(context);
                    subscribeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    subscribeDialog.setContentView(doOptionsView);

                    btnDoUnSubYes.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            doUnsubscribeAsyncTask = new DoUnsubscribe();
                            doUnsubscribeAsyncTask.execute(sessionId);
                            if(subscribeDialog != null)
                                subscribeDialog.dismiss();
                        }
                    });
                    btnDoDownloadYes.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            CharSequence[] supportedOsVersionsArray = new CharSequence[downloadableBinaryList.size()];
                            setVersionDialog(supportedOsVersionsArray, sessionId);
                            if(subscribeDialog != null)
                                subscribeDialog.dismiss();
                        }
                    });
                    btnClose.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(subscribeDialog != null)
                                subscribeDialog.dismiss();
                        }
                    });
                    subscribeDialog.show();
                } else {
                    if (getResources().getBoolean(R.bool.pre_subscribe_enabled)) {
                        handleSubscriptionDialog(sessionId);
                    } else {
                        doSubscribeAsyncTask = new DoSubscribe();
                        doSubscribeAsyncTask.execute(sessionId);
                    }
                }
            } else {
                CharSequence[] supportedOsVersionsArray = new CharSequence[downloadableBinaryList.size()];
                setVersionDialog(supportedOsVersionsArray, sessionId);
            }

            isLaunchAppActionHandler = false;

        } else {
            isLaunchAppActionHandler = true;
            handleSignInAction();
        }

    }

    private void handleSubscriptionDialog(final String sessionId) {
        View doSubscribeView = getLayoutInflater().inflate(R.layout.do_subscribe_dialog, null);
        Button btnDoSubYes = (Button) doSubscribeView.findViewById(R.id.btn_do_sub_yes);
        Button btnDoSubNo = (Button) doSubscribeView.findViewById(R.id.btn_do_sub_no);
        Button btnDoSubOk = (Button) doSubscribeView.findViewById(R.id.btn_do_sub_ok);
        TextView confirmationTxt = (TextView) doSubscribeView.findViewById(R.id.txt_do_subscribe_desc);
        final Dialog subscribeDialog = new Dialog(context);
        subscribeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        subscribeDialog.setContentView(doSubscribeView);

        btnDoSubYes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                doSubscribeAsyncTask = new DoSubscribe();
                doSubscribeAsyncTask.execute(sessionId);
                if(subscribeDialog != null)
                    subscribeDialog.dismiss();
            }
        });
        btnDoSubNo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(subscribeDialog != null)
                    subscribeDialog.dismiss();
            }
        });
        btnDoSubOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(subscribeDialog != null)
                    subscribeDialog.dismiss();
            }
        });

        if (application != null) {
            if (application.getChargingDetails().toLowerCase().contains("free")) {
                confirmationTxt.setText(getString(R.string.do_subscribe_dialog_description_free));
            } else {
                confirmationTxt.setText(String.format(
                        getString(R.string.do_subscribe_dialog_description_non_free),
                        application.getChargingDetails()));
            }
        } else {
            confirmationTxt.setText(getString(R.string.do_subscribe_dialog_description_error));
            btnDoSubYes.setVisibility(View.GONE);
            btnDoSubNo.setVisibility(View.GONE);
            btnDoSubOk.setVisibility(View.VISIBLE);
        }
        subscribeDialog.show();
    }

    private void handleSignInAction() {
        if (!SignInActivity.session.isEmpty()) {
            SignInActivity.session.clear();
        }
        Toast.makeText(this, R.string.login_message, Toast.LENGTH_LONG).show();
        Intent i = new Intent(getApplicationContext(), SignInActivity.class);
        i.putExtra(Property.REDIRECT_KEY, RedirectKeys.DESCRIPTION.name());
        i.putExtra(Tags.TAG_APP_ID.name(), appId);
        if (isLaunchAppActionHandler) {
            i.putExtra("isLaunchAppActionHandler", true);
        } else if (isAddComment) {
            i.putExtra("isAddComment", true);
        }
        startActivity(i);
        finish();
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void setAppDetails(HashMap<String, String> appDetailMap) {
        txtAppName.setText(appDetailMap.get(AppTags.TAG_NAME));
        txtAppCategory.setText(appDetailMap.get(AppTags.TAG_CATEGORY));
        txtAppChargingLabel.setText(appDetailMap.get(AppTags.TAG_CHARGING_LABEL));
        txtAppCreatedBy.setText(appDetailMap.get(AppTags.TAG_CREATED_BY));

        txtAppDescription.setText(appDetailMap.get(AppTags.TAG_DESCRIPTION));

        detailedDesc = appDetailMap.get(AppTags.TAG_DESCRIPTION);
        detailedChargingLbl = appDetailMap.get(AppTags.TAG_CHARGING_LABEL);
        detailedAppName = appDetailMap.get(AppTags.TAG_NAME);
        detailedAppCreatedBy = appDetailMap.get(AppTags.TAG_CREATED_BY);

        if (appDetailMap.containsKey(AppTags.TAG_CHARGING_DETAIL)) {
            txtCharging.setText(appDetailMap.get(AppTags.TAG_CHARGING_DETAIL));
            detailedCharging = appDetailMap.get(AppTags.TAG_CHARGING_DETAIL);
        }


        if (isDownloadableApp && appDetailMap.containsKey(AppTags.TAG_SUPPORTED_OS_VERSIONS)) {
            txtSupportedVersions.setText(appDetailMap.get(AppTags.TAG_SUPPORTED_OS_VERSIONS));
            txtSupportedVersions.setVisibility(View.VISIBLE);
            txtSupportedVersionsCaption.setVisibility(View.VISIBLE);
        } else {
            txtSupportedVersionsCaption.setVisibility(View.GONE);
            txtSupportedVersions.setVisibility(View.GONE);
        }


        if (isDownloadableApp && !isSubscriptionApp) {
            rlDownload.setVisibility(View.VISIBLE);
            if (isUpdateAvailable) {
                button.setText(R.string.update_text);
                button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_download, 0, 0, 0);
                button.setEnabled(true);
                button.setVisibility(View.VISIBLE);
                txtCharging.setText(R.string.update_available_text);
                detailedCharging = this.getResources().getString(R.string.update_available_text);
                txtAppChargingLabel.setText(R.string.app_charge_free_text);
                detailedChargingLbl = this.getResources().getString(R.string.app_charge_free_text);
            } else {
                button.setText(R.string.download_text);
                button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_download, 0, 0, 0);
                button.setEnabled(true);
                button.setVisibility(View.VISIBLE);
            }
        } else if (isSubscriptionApp && !isDownloadableApp) {
            rlDownload.setVisibility(View.VISIBLE);
            if (isSubscribed) {
                button.setText(R.string.unsubscribe_text);
                button.setEnabled(true);
                button.setVisibility(View.VISIBLE);
            } else {
                button.setText(R.string.subscribe_text);
                button.setEnabled(true);
                button.setVisibility(View.VISIBLE);
            }
            if (isRegPending) {
                button.setText(R.string.pending_charge_text);
                button.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                button.setEnabled(false);
                button.setVisibility(View.VISIBLE);
            }
            tvDownloads.setText(appDetailMap.get(AppTags.TAG_SUBSCRIPTION_COUNT));
            lblDownloads.setText(R.string.app_description_subscription_label);
            imgSubscription.setImageResource(R.drawable.nav_subscribe);
            imgSubscription.setColorFilter(ContextCompat.getColor(context, R.color.feature_icon_default_color), android.graphics.PorterDuff.Mode.SRC_IN);

        } else if (isSubscriptionApp && isDownloadableApp) {
            rlDownload.setVisibility(View.VISIBLE);
            if (!isDownloadRequiresSubscription) {
                button.setText(R.string.download_text);
                button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_download, 0, 0, 0);
                button.setEnabled(true);
                button.setVisibility(View.VISIBLE);
            } else {
                if (isSubscribed) {
                    if (isUpdateAvailable) {
                        button.setText(R.string.update_text);
                        button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_download, 0, 0, 0);
                        button.setEnabled(true);
                        button.setVisibility(View.VISIBLE);
                    } else {
                        button.setText(R.string.options_text);
                        button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_download, 0, 0, 0);
                        button.setEnabled(true);
                        button.setVisibility(View.VISIBLE);
                    }
                } else {
                    /*button.setText(R.string.subscribe_text);
                    button.setEnabled(true);
                    button.setVisibility(View.VISIBLE);*/
                }
                if (isRegPending) {
                    button.setText(R.string.pending_charge_text);
                    button.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    button.setEnabled(false);
                    button.setVisibility(View.VISIBLE);
                }
            }

        } else {
            button.setVisibility(View.INVISIBLE);
            rlDownload.setVisibility(View.GONE);
        }
        button.requestFocus();
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getResources().getBoolean(R.bool.auto_login_enabled)) {
                    new GetAuthProvider().execute();
                } else {
                    if (!SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
                        isLaunchAppActionHandler = true;
                        handleSignInAction();
                    } else {
                        session = SignInActivity.session.get(SessionKeys.SESSION_ID.name());
                        appActionHandler();
                    }
                }
            }
        });

        String appRating = "0";

        try {
            appRating = appDetailMap.get(AppTags.TAG_RATING);
        } catch (Exception ex) {
            Log.d(LOG_TAG, ex.getMessage(), ex.fillInStackTrace());
        }


        tvAppRating.setText(String.valueOf(appRating));

        String fullImagePath = ProfileProperty.APPSTORE_IMAGE_URL + appDetailMap.get(AppTags.TAG_ICON_PATH);

        Log.d("Image Url ", ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + fullImagePath);

        // Load and display image asynchronously
        imageLoader.displayImage(fullImagePath, appIcon, options);


        if (bitmap_count > 0) {
            screenShotScroller.setVisibility(View.VISIBLE);
        } else {
            screenShotScroller.setVisibility(View.GONE);
        }
    }

    private void showMessage(String results, Context context) {
        if (results != null) {
            Toast aToast = Toast.makeText(context, results, Toast.LENGTH_SHORT);
            ToastExpander.showFor(aToast, 3000);
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.E5000), Toast.LENGTH_SHORT).show();
        }

    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void setVersionDialog(final CharSequence[] supportedOsVersionsArray, final String sessionId) {

        ViewGroup viewGroup = (ViewGroup) getLayoutInflater().inflate(R.layout.dialog_android_version_selection, null);
        LinearLayout linearLayout = (LinearLayout) viewGroup.getChildAt(1);
        final Dialog confirmDialog = new Dialog(context);
        confirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmDialog.setContentView(viewGroup);
        int i = 0;
        for (DownloadableBinaryDetails downloadableBinaryDetails : downloadableBinaryList) {
            final int j = i;
            supportedOsVersionsArray[i] = downloadableBinaryDetails.getOsVersion();

            Button btnOsVersion = new Button(AppDescriptionActivity.this);
            btnOsVersion.setText(supportedOsVersionsArray[i]);
            btnOsVersion.setBackgroundColor(getResources().getColor(R.color.background_color_for_apps_list));
            btnOsVersion.setTextColor(getResources().getColor(R.color.text_color));
            btnOsVersion.setGravity(Gravity.LEFT);
            btnOsVersion.setId(100 + i);
            btnOsVersion.setLayoutParams(new ViewGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            btnOsVersion.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    isPISelected = -1;
                    String contentId = downloadableBinaryList.get(j).getDownloadableBinary().get(0).getContentId();
                    params = new String[2];
                    params[0] = sessionId;
                    params[1] = contentId;

                    //permission handling

                    if (checkPermission(AppDescriptionActivity.this, WRITE_EXTERNAL_STORAGE)) {
                        //Toast.makeText(getApplicationContext(), "All Permissions Granted Successfully", Toast.LENGTH_LONG).show();
                        doDownloadAsyncTask = new DoDownload();
                        doDownloadAsyncTask.execute(params);
                        if(confirmDialog != null)
                            confirmDialog.dismiss();
                    } else {
                        requestPermission(AppDescriptionActivity.this, WRITE_EXTERNAL_STORAGE, PERMISSION_REQUEST_CODE);
                    }
                    if(confirmDialog != null)
                        confirmDialog.dismiss();
                }
            });


            View line = new View(this);
            line.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 2));
            line.setBackgroundColor(getResources().getColor(R.color.app_description_header_seperator));
            linearLayout.addView(btnOsVersion);
            linearLayout.addView(line);
//            viewGroup.addView(btnOsVersion);
            i++;
        }
        confirmDialog.show();
    }

    /**
     * check whether the permission has already been granted by the user for downloading the app
     *
     * @param context
     * @param Permission
     * @return permission
     */

    public boolean checkPermission(Context context, String Permission) {
        boolean permission = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Permission) == PackageManager.PERMISSION_GRANTED) {
                permission = true;
            } else {
                permission = false;
            }
        }
        return permission;
    }

    /**
     * if the requested permission has not been already granted by the user, then application request
     * for permission
     *
     * @param thisActivity
     * @param Permission   external_storage access permission
     * @param Code         permsission code
     */
    public void requestPermission(Activity thisActivity, String Permission, int Code) {
        if (ContextCompat.checkSelfPermission(thisActivity, Permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(thisActivity, Permission)) {
                // Toast.makeText(this, "Write External Storage permission allows us to do store application. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(thisActivity, new String[]{Permission}, Code);

            } else {
                ActivityCompat.requestPermissions(thisActivity, new String[]{Permission}, Code);
            }
        }
    }

    /**
     * when requesting the permission(s), if the user grants the permission for accessing the
     * external storage, Downloading app is initiated
     *
     * @param requestCode  permission_code
     * @param permission   permission for accessing external_storage
     * @param grantResults status of granting permission
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permission, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permission, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("Permission", " Granted");
                    Toast.makeText(this, "Permission Granted", Toast.LENGTH_LONG).show();
                    doDownloadAsyncTask = new DoDownload();
                    doDownloadAsyncTask.execute(params);
                    if(confirmDialog != null)
                        confirmDialog.dismiss();

                } else {
                    Log.i("Permission", " Denied");
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void doDirectDownload(AppDownloadResponse result) {
        boolean isAutoInstallRequired = sharedPrefs.getBoolean(Property.PREFS_AUTO_INSTALL_DOWNLOADABLES, true);
        if (getResources().getBoolean(R.bool.auto_install_downloadable_apps) && isAutoInstallRequired) {
            doDirectDownloadViaDownloadManager(result.getWapUrl());
        } else {
            doDirectDownloadViaBrowser(result.getWapUrl());
        }
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    private void doDirectDownloadViaDownloadManager(String wapUrl) {
        try {
            String appName = txtAppName.getText().toString().trim();
            downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(wapUrl));
            request.setMimeType(AppTags.APK_MIME_TYPE);
            request.setTitle(appName);
            request.setDestinationInExternalPublicDir(ProfileProperty.APK_FOLDER_PATH,
                    (appName.toLowerCase().replace(" ", "") + ".apk"));
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
                request.setShowRunningNotification(true);
            } else {
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            }

            downloadQueueId = downloadManager.enqueue(request);
            DownloadableApplication downloadableApp =
                    new DownloadableApplication(getAppId(), appName, downloadQueueId);
            Map<String, DownloadableApplication> downloadableAppsMap = getAppQueue(downloadQueueId);
            downloadableAppsMap.put(String.valueOf(downloadQueueId), downloadableApp);

            DownloadableApplications downloadableApplications = new DownloadableApplications();
            downloadableApplications.setDownloadableAppsMap(downloadableAppsMap);

            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putString(Property.PREFS_DOWNLOADABLE_APP_QUEUE, downloadableApplications.toJsonString());
            editor.commit();
        } catch (Exception e) {
            Toast.makeText(AppDescriptionActivity.this, getString(R.string.app_install_download_failed_msg), Toast.LENGTH_SHORT);
        }
    }

    private Map<String, DownloadableApplication> getAppQueue(long currentReqId) {
        String queueJson = sharedPrefs.getString(Property.PREFS_DOWNLOADABLE_APP_QUEUE, "");
        Map<String, DownloadableApplication> downloadableQueue = new HashMap<>();
        DownloadableApplications downloadableApplications;

        if (!queueJson.equals("")) {
            downloadableApplications = new Gson().fromJson(queueJson, DownloadableApplications.class);
            downloadableQueue = downloadableApplications.getDownloadableAppsMap();

            Iterator<Map.Entry<String, DownloadableApplication>> iter = downloadableQueue.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<String, DownloadableApplication> entry = iter.next();
                if (entry.getValue().isDownloadComplete() || (entry.getValue().getDownloadQueueId() < currentReqId - 15)) {
                    iter.remove();
                }
            }
        }

        return downloadableQueue;
    }

    /**
     * Application Direct Download Method for Devices running below Android API Level 9
     * Opens up the WAP Url in the browser
     */
    private void doDirectDownloadViaBrowser(String wapUrl) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        Uri u = Uri.parse(wapUrl);
        i.setData(u);
        startActivity(i);
    }


    /**
     * Async Task to retrieve Application Details
     */
    public class GetSingleApp extends AsyncTask<String, Context, HashMap<String, String>> {

        ArrayList<HashMap<String, String>> appDetails = new ArrayList<HashMap<String, String>>();
        ProgressDialog dialog = new ProgressDialog(sfa);

        public GetSingleApp() {

        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage(getResources().getString(R.string.loading_apps_text));
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.show();
        }

        protected void showDialog() {

        }

        @Override
        protected HashMap<String, String> doInBackground(String... params) {
            String url = params[0];
            // getting JSON string from URL
            if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
                url += "/session-id/" + SignInActivity.session.get(SessionKeys.SESSION_ID.name());
            }
            ApplicationDetailResponse applicationDetailResponse = null;
            try {
                String json = new RestClient().get(url);
                applicationDetailResponse = new Gson().fromJson(json, ApplicationDetailResponse.class);
            } catch (ConnectionRefusedException e) {
                Intent i = new Intent(getApplicationContext(), NoConnectionActivity.class);
                startActivity(i);
                finish();
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
                return null;
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                return null;
            }
            if (applicationDetailResponse != null && ErrorCodeMapping.isSuccess(applicationDetailResponse.getStatusCode())) {
                HashMap<String, String> appDetailMap = new HashMap<String, String>();

                application = applicationDetailResponse.getResult();
                downloadsCount = application.getDownloadsCount();

                String downloadedContentId = "0";
                if (application.getDownloadStatus() != null) {
                    downloadedContentId = application.getDownloadStatus().getContentId();
                }

                for (Map.Entry<String, ArrayList<HashMap<String, ArrayList<DownloadableBinary>>>>
                        downloadedBinary : application.getDownloadableBinaries().entrySet()) {
                    String binaryOs = downloadedBinary.getKey();
                    if (binaryOs.equalsIgnoreCase("android")) {
                        for (HashMap<String, ArrayList<DownloadableBinary>> list : downloadedBinary.getValue()) {
                            for (Map.Entry<String, ArrayList<DownloadableBinary>> dataWithVersion : list.entrySet()) {
                                for (DownloadableBinary binaryData : dataWithVersion.getValue()) {
                                    if (Long.parseLong(downloadedContentId) < Long.parseLong(binaryData.getContentId())
                                            && Long.parseLong(downloadedContentId) != 0) {
                                        isUpdateAvailable = true;
                                    }
                                }
                            }
                        }
                    }
                }

                HashMap<String, String> instruction = application.getInstructions();

                if (!getResources().getBoolean(R.bool.multi_deployment_mode)) {
                    if (instruction.keySet().iterator().hasNext()) {
                        String provider = instruction.keySet().iterator().next();
                        String details = instruction.get(provider);

                        AppInstruction appInstruction = new AppInstruction();
                        appInstruction.setServiceProvider(R.string.sp_prefix + provider.toUpperCase() + R.string.sp_postfix);
                        appInstruction.setInstruction(details);

                        appInstructionList.add(appInstruction);
                    }
                } else {
                    for (Map.Entry<String, String> entry : instruction.entrySet()) {
                        AppInstruction appInstruction = new AppInstruction();
                        appInstruction.setServiceProvider(R.string.sp_prefix + entry.getKey().toUpperCase() + R.string.sp_postfix);
                        appInstruction.setInstruction(entry.getValue());

                        appInstructionList.add(appInstruction);
                    }
                }

                isDownloadableApp = application.isDownloadable();
                isSubscriptionApp = application.isSubscription();
                isDownloadRequiresSubscription = application.isDownloadRequiresSubscription();

                if (application.getSubscriptionStatus() != null && !application.getSubscriptionStatus().equals("")) {
                    String subscriptionStatus = application.getSubscriptionStatus();
                    if (subscriptionStatus == null || subscriptionStatus.equals("UNREGISTERED")) {
                        isSubscribed = false;
                    } else if (subscriptionStatus.equals("REG_PENDING") || subscriptionStatus.equals("PENDING CHARGE")) {
                        isRegPending = true;
                    } else if (subscriptionStatus.equals("REGISTERED")) {
                        isSubscribed = true;
                    }
                }

                String charging = application.getChargingDetails();

                if (application.getAppScreenShots() != null && application.getAppScreenShots().length > 0) {
                    imageUrlList = new ArrayList<String>();
                    for (int i = 0; i < application.getAppScreenShots().length; i++) {
                        if (application.getAppScreenShots()[i].getUrl().contains("mobile")) {
                            imageUrlList.add(ProfileProperty.APPSTORE_IMAGE_URL + application.getAppScreenShots()[i].getUrl());
                        }
                    }
                    bitmap_count = imageUrlList.size();
                    loadAppIcons(imageUrlList);
                }

                if (isDownloadableApp && application.getDownloadableBinaries() != null) {
                    isDownloadAvailable = true;

                    if (application.getDownloadableBinaries().containsKey("Android")) {
                        ArrayList<HashMap<String, ArrayList<DownloadableBinary>>> platforms = application.getDownloadableBinaries().get("Android");

                        String supportedVersions = "Android - ";
                        try {
                            curOsVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                        for (HashMap<String, ArrayList<DownloadableBinary>> platform : platforms) {
                            Iterator osVersionIterator = platform.keySet().iterator();

                            while (osVersionIterator.hasNext()) {
                                String osVersion = (String) osVersionIterator.next();


                                supportedVersions += osVersion + ", ";
                                List<DownloadableBinary> dBinaryList = platform.get(osVersion);
                                downloadableBinaryList.add(new DownloadableBinaryDetails(osVersion, dBinaryList));
                            }
                            appDetailMap.put(AppTags.TAG_SUPPORTED_OS_VERSIONS, supportedVersions.substring(0, supportedVersions.lastIndexOf(",")));
                        }

                    } else {
                        appDetailMap.put(AppTags.TAG_SUPPORTED_OS_VERSIONS, getResources().getString(R.string.no_supported_versions_text));
                        appDetailMap.put(AppTags.TAG_NO_APP, getResources().getString(R.string.no_app_available_text));
                    }
                } else {
                    isDownloadAvailable = false;
//
                }

                if (application.getUserComments() != null) {
                    userReviewList = Arrays.asList(application.getUserComments());
                }

                appDetailMap.put(AppTags.TAG_ID, application.getId());
                appDetailMap.put(AppTags.TAG_NAME, application.getName());
                appDetailMap.put(AppTags.TAG_CATEGORY, application.getCategory());
                appDetailMap.put(AppTags.TAG_DESCRIPTION, application.getDescription());
                appDetailMap.put(AppTags.TAG_CREATED_BY, application.getDeveloper());
                appDetailMap.put(AppTags.TAG_RATING, String.valueOf(application.getRating()));
                appDetailMap.put(AppTags.TAG_ICON_PATH, application.getAppIcon());
                appDetailMap.put(AppTags.TAG_CHARGING_LABEL, application.getChargingLabel());
                appDetailMap.put(AppTags.TAG_CHARGING_DETAIL, charging);
                appDetailMap.put(AppTags.TAG_SUBSCRIPTION_COUNT, String.valueOf(application.getSubscriptionsCount()));


                return appDetailMap;
            }
            return null;
        }

        @Override
        protected void onPostExecute(HashMap<String, String> result) {
            super.onPostExecute(result);

            if (getSupportActionBar() != null && application != null) {
                getSupportActionBar().setTitle(application.getName());
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }

            if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
                try {
                    rateThis.setRating(sharedPerference.getFloat(sharedPrefs.getString(Property.PREFS_USERNAME, "") + "_" + application.getName(), 0));
                } catch (Exception e) {
                    e.printStackTrace();
                    rateThis.setRating(0);
                }
            }
            else
                rateThis.setRating(0);

            if(application!= null ) {
                if (application.getDownloadsCount() == -1)
                    tvDownloads.setText("0");
                else
                    tvDownloads.setText(String.valueOf(Formatter.format(application.getDownloadsCount())));

            }
            if (application!= null) {
                tvRatings.setText(String.valueOf(application.getRating()));
                tvUsers.setText(String.valueOf(application.getRateCount()));
            }

            if (result != null && !result.containsKey(AppTags.TAG_NO_APP)) {
                setAppDetails(result);
                readMore.setVisibility(View.VISIBLE);

//                setAppInstructions();

                if (userReviewList.size() > 0) {
                    TextView userReviewsEmptyMsg = (TextView) findViewById(R.id.reviews_empty_msg);
                    userReviewsEmptyMsg.setVisibility(View.GONE);
                    setUserComments();
                } else {
                    LinearLayout userReviewLayout = (LinearLayout) findViewById(R.id.user_reviews_layout);
                    userReviewLayout.setVisibility(View.GONE);
                }

                if (extras != null && extras.containsKey("isLaunchAppActionHandler") && extras.getBoolean("isLaunchAppActionHandler")) {
                    appActionHandler();
                } else if (extras != null && extras.containsKey("isAddComment") && extras.getBoolean("isAddComment")) {
                    showAddCommentDialog();
                }

            } else {
                Toast.makeText(getApplicationContext(), R.string.error_while_loading_app_text, Toast.LENGTH_LONG).show();
                //todo: add handling method for app detail loading failed scenario


            }
            dialog.dismiss();
            getRelatedAppsAsyncTask = new GetRelatedApps();

            relatedApps.setVisibility(View.VISIBLE);

            if (!(getAppId() == null || getAppId().equals(""))) {
                getRelatedAppsAsyncTask.execute(Property.RELATED_APPS_URL + getAppId() + "/start/0/limit/" + Property.RELATED_APPS_LIMIT);
            }
        }

        private void setUserComments() {
            LinearLayout userComments = (LinearLayout) findViewById(R.id.user_reviews_layout);
            int count = 0;
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .cacheInMemory()
                    .displayer(new RoundedBitmapDisplayer(1000))
                    .build();

            for (UserReview userReview : userReviewList) {
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                LinearLayout single_app_instruction = (LinearLayout) inflater.inflate(R.layout.single_user_comment_in_app_desc, null);

                TextView user = (TextView) single_app_instruction.findViewById(R.id.userName);
                TextView commentDate = (TextView) single_app_instruction.findViewById(R.id.comment_date);
                ImageView imgUser = (ImageView) single_app_instruction.findViewById(R.id.imgUser);

                user.setText(userReview.getUsername());
                commentDate.setText(userReview.getDisplayDate());
                if (userReview.getImage() != null && !userReview.getImage().isEmpty())
                    imageLoader.displayImage(ProfileProperty.APPSTORE_IMAGE_URL + userReview.getImage(), imgUser, options);

                TextView comment = (TextView) single_app_instruction.findViewById(R.id.comment);
                comment.setText(userReview.getComment());

                userComments.addView(single_app_instruction);
                if (++count >= 1) {
                    viewAllLink.setVisibility(View.VISIBLE);
                    return;
                }
            }
        }

        private void loadAppIcons(List<String> urls) {
            for (String image_src : urls) {
                bitmapDownloaderTask = new BitmapDownloaderTask();
                bitmapDownloaderTask.execute(image_src);
            }

        }


    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Application screenshots downloading and displaying
     */
    class BitmapDownloaderTask extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        // Actual download method, run in the task thread
        protected Bitmap doInBackground(String... params) {
            // params comes from the execute() call: params[0] is the url.
            return downloadBitmap(params[0]);
        }

        private Bitmap downloadBitmap(String src) {
            try {
                // Create a trust manager that does not validate certificate chains
                TrustManager[] trustAllCerts = new TrustManager[]{
                        new X509TrustManager() {
                            public X509Certificate[] getAcceptedIssuers() {
                                return null;
                            }

                            public void checkClientTrusted(
                                    X509Certificate[] certs, String authType) {
                            }

                            public void checkServerTrusted(
                                    X509Certificate[] certs, String authType) {
                            }
                        }
                };

// Install the all-trusting trust manager
                try {
                    SSLContext sc = SSLContext.getInstance("TLS");
                    sc.init(null, trustAllCerts, new SecureRandom());
                    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                } catch (GeneralSecurityException e) {
                }
// Now you can access an https URL without having the certificate in the truststore

                URL url = new URL(src);

                HttpURLConnection connection = null;

                if (url.getProtocol().equalsIgnoreCase("https")) {
                    Log.d("Image Downloader", "######################## https");
                    Log.d("Image Downloader - Image URL :", src);
                    HttpsURLConnection.setDefaultHostnameVerifier(new NullHostNameVerifier());
                    connection = (HttpsURLConnection) url.openConnection();
                } else if (url.getProtocol().equalsIgnoreCase("http")) {
                    Log.d("Image Downloader", "######################## http");
                    connection = (HttpURLConnection) url.openConnection();
                }

                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                return BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            ImageView imgPhoto = new ImageView(AppDescriptionActivity.this, null, R.style.AppScreenshot);
            imgPhoto.setMinimumHeight(100);
            imgPhoto.setMaxHeight(R.dimen.app_screen_shot_max_height);
            imgPhoto.setPadding(20, 5, 20, 5);

            if (bitmap != null) {
                imgPhoto.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 200, 300, true));
            }

            app_screenshot_layout.addView(imgPhoto);
        }
    }

    public class NullHostNameVerifier implements HostnameVerifier {

        public boolean verify(String hostname, SSLSession session) {
            Log.i("RestUtilImpl", "Approving certificate for " + hostname);
            return true;
        }
    }

    /**
     * Get Related applications
     */
    class GetRelatedApps extends AsyncTask<String, Context, ArrayList<Application>> {

        GetRelatedApps() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<Application> doInBackground(String... strings) {
            String url = strings[0];
            // getting JSON string from URL
            ApplicationListResponse applicationListResponse = null;
            try {
                String json = new RestClient().get(url);
                applicationListResponse = new Gson().fromJson(json, ApplicationListResponse.class);

            } catch (ConnectionRefusedException e) {
                Intent i = new Intent(getApplicationContext(), NoConnectionActivity.class);
                startActivity(i);
                finish();
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }
            Log.d("GetRelatedApps", "JASON object received in Async Task");
            ArrayList<Application> relatedAppList = new ArrayList<Application>();

            if (applicationListResponse != null && ErrorCodeMapping.isSuccess(applicationListResponse.getStatusCode())) {
                try {
                    relatedAppList = new ArrayList<Application>(Arrays.asList(applicationListResponse.getResults()));
                } catch (Exception e) {
                    Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                }
            }

            return relatedAppList;
        }

        @Override
        protected void onPostExecute(ArrayList<Application> result) {
            super.onPostExecute(result);

//            adapter = new DefaultBucketListAdapter(sfa, result, font);
//            //adapter.enableAutoMeasure(LIST_ITEM_WIDTH);
//
//            relatedAppList.setAdapter(adapter);
//            setListViewHeightBasedOnChildren(relatedAppList);

//            int numberOfColumns = 2;
//            recyclerView.setLayoutManager(new GridLayoutManager(context, numberOfColumns));
//            adapter = new GridRecyclerAdapter(context, result);
//            recyclerView.setAdapter(adapter);
            similarApps = result;
            if(similarApps.size() != 0) {
                llSimilarApps.setVisibility(View.VISIBLE);
                populateApps(similarAppsLayout, result, false);
            } else{
                llSimilarApps.setVisibility(View.GONE);
            }

            ProgressBar progressBar = (ProgressBar) relatedApps.findViewById(R.id.related_apps_loading_progress_bar);

            progressBar.setVisibility(View.GONE);
        }
    }

    /**
     * Subscribe for an application
     */
    class DoSubscribe extends AsyncTask<String, Context, Response> {

        ProgressDialog dialog = new ProgressDialog(sfa);

        DoSubscribe() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog.setMessage(getResources().getString(R.string.please_wait_message));
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.show();
        }

        @Override
        protected Response doInBackground(String... strings) {
            String sessionId = strings[0];
            String url = Property.SUBSCRIBE_URL + sessionId + "/app-id/" + getAppId();
            //HttpResponse response = null;
            Response response = null;
            try {
                String json = new RestClient().get(url);
                response = new Gson().fromJson(json, Response.class);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (ConnectionRefusedException e) {
                Intent i = new Intent(getApplicationContext(), NoConnectionActivity.class);
                startActivity(i);
                finish();
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }
            return response;
        }

        @Override
        protected void onPostExecute(final Response result) {
            super.onPostExecute(result);
            dialog.hide();
            if (getResources().getBoolean(R.bool.post_subscribe_enabled)) {

                View doSubscribeView = getLayoutInflater().inflate(R.layout.post_subscribe_dialog, null);
                Button btnDoSubYes = (Button) doSubscribeView.findViewById(R.id.btn_do_sub_yes);
                TextView confirmationTxt = (TextView) doSubscribeView.findViewById(R.id.txt_do_subscribe_desc);
                final Dialog subscribeDialog = new Dialog(context);
                subscribeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                subscribeDialog.setContentView(doSubscribeView);

                btnDoSubYes.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sfa.recreate();
                        if(subscribeDialog != null)
                            subscribeDialog.dismiss();
                    }
                });

                if (result != null) {
                    if (ErrorCodeMapping.isSuccess(result.getStatusCode())) {
                        confirmationTxt.setText(getString(R.string.do_subscribe_dialog_post_description));
                    } else {
                        confirmationTxt.setText(result.getStatusDescription());
                    }
                } else {
                    confirmationTxt.setText(getString(R.string.do_subscribe_dialog_description_error));
                    btnDoSubYes.setVisibility(View.GONE);
                }

                subscribeDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        sfa.recreate();
                    }
                });
                subscribeDialog.show();
            } else {
                if (result != null) {
                    if (ErrorCodeMapping.isSuccess(result.getStatusCode())) {
                        showMessage(result.getStatusDescription(), AppDescriptionActivity.this);
                        sfa.recreate();
                    } else {
                        showMessage(result.getStatusDescription(), AppDescriptionActivity.this);
                    }
                } else {
                    showMessage(getString(R.string.default_error), AppDescriptionActivity.this);
                }
            }
        }


    }

    /**
     * Unsubscribe for an application
     */
    class DoUnsubscribe extends AsyncTask<String, Context, Response> {

        ProgressDialog dialog = new ProgressDialog(sfa);

        DoUnsubscribe() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog.setMessage(getResources().getString(R.string.please_wait_message));
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.show();
        }

        protected Response doInBackground(String... strings) {
            String sessionId = strings[0];
            String url = Property.UNSUBSCRIBE_URL + sessionId + "/app-id/" + appId;
            Response response = null;
            try {
                String json = new RestClient().get(url);
                response = new Gson().fromJson(json, Response.class);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (ConnectionRefusedException e) {
                Intent i = new Intent(getApplicationContext(), NoConnectionActivity.class);
                startActivity(i);
                finish();
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }
            return response;
        }

        protected void onPostExecute(Response result) {
            super.onPostExecute(result);
            dialog.hide();
            if (result != null) {
                if (ErrorCodeMapping.isSuccess(result.getStatusCode())) {
                    showMessage(result.getStatusDescription(), AppDescriptionActivity.this);
                    Log.d("Status", ": " + result.getStatusDescription());
                    sfa.recreate();
                } else {
                    showMessage(result.getStatusDescription(), AppDescriptionActivity.this);
                }
            } else {
                showMessage(getString(R.string.default_error), AppDescriptionActivity.this);
            }
        }


    }

    /**
     * Downloading an application
     */
    class DoDownload extends AsyncTask<String, Context, AppDownloadResponse> {
        ProgressDialog dialog = new ProgressDialog(sfa);

        DoDownload() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog.setMessage(getResources().getString(R.string.please_wait_message));
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.show();
        }

        @Override
        protected AppDownloadResponse doInBackground(String... strings) {


            String sessionId = strings[0];
            String contentId = strings[1];
            String url = Property.DOWNLOAD_URL + sessionId + "/app-id/" + appId + "/content-id/" + contentId + "/channel/android";
            AppDownloadResponse response = null;
            try {
                String json = new RestClient().get(url);
                Log.d("L", "AppDownloadResponse " + json);
                response = new Gson().fromJson(json, AppDownloadResponse.class);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException", e);
            } catch (ConnectionRefusedException e) {
                Intent i = new Intent(getApplicationContext(), NoConnectionActivity.class);
                startActivity(i);
                finish();
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }
            return response;
        }

        protected void onPostExecute(AppDownloadResponse result) {

            appDownloadResponseResult = result;
            super.onPostExecute(result);
            dialog.hide();
            if (result != null) {
                String statusCode = result.getStatusCode();
                if (statusCode.equalsIgnoreCase(Property.SUCCESS_STATUS)) {
                    if (result.getWapUrl() != null) {
                        doDirectDownload(result);

                    } else {

                        new AlertDialog.Builder(sfa)
                                .setTitle(R.string.success_dialog_title)
                                .setMessage(result.getStatusDescription())
                                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();
                    }
                } else if (statusCode.equalsIgnoreCase(Property.MULTIPLE_PI_AVAILABLE_STATUS)) {
                    View layout = AppDescriptionActivity.this.getLayoutInflater().inflate(R.layout.payment_list, null);
                    paymentListRadioGroup = (RadioGroup) layout.findViewById(R.id.rd_grp_inapp_payment_list);
                    int i = 0;
                    for (AppDownloadResponse.AvailablePI piList : result.getPaymentInstrumentList()) {

                        final AppDownloadResponse.AvailablePI pi = piList;
                        RadioButton radioButton = new RadioButton(getApplicationContext());
                        radioButton.setText(pi.getName());
                        radioButton.setTextColor(getResources().getColor(R.color.text_color));
                        radioButton.setGravity(Gravity.CENTER_VERTICAL);

                        radioButton.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                piDonwloadFullUrl = piDownloadUrl + session + "/request-id/"
                                        + appDownloadResponseResult.getDownloadRequestId() + "/pi-name/"
                                        + pi.getName();
                                if (pi.getName().equals("M-PAiSA")) {
                                    isPISelected = 1;
                                    edtTxtPin.setVisibility(View.VISIBLE);
                                    edtTxtPin.setFocusableInTouchMode(true);
                                } else {
                                    isPISelected = 0;
                                    edtTxtPin.setVisibility(View.GONE);
                                }
                            }
                        });
                        paymentListRadioGroup.addView(radioButton);
                        i++;
                    }
                    edtTxtPin = (EditText) layout.findViewById(R.id.edt_txt_inapp_pin);
                    edtTxtPin.setTypeface(Typeface.DEFAULT);
                    btnInAppConfirm = (Button) layout.findViewById(R.id.btn_inapp_confirm);

                    if (result.getPaymentInstrumentList() != null) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(AppDescriptionActivity.this, R.style.AppstoreAlertDialog)).setView(layout);
                        AlertDialog piSelectionDialog = builder.create();
                        piSelectionDialog.show();
                        setPIDialogListners(layout, piSelectionDialog);
                    }
                } else {
                    showMessage(result.getStatusDescription(), AppDescriptionActivity.this);
                }
            } else {
                showMessage(getString(R.string.default_error), AppDescriptionActivity.this);
            }
        }
    }

    /**
     * Add comments(limited to 100) for an application
     */
    public class AddCommentAsyncTask extends AsyncTask<String, Context, Response> {

        ProgressDialog dialog = new ProgressDialog(sfa);

        public AddCommentAsyncTask() {

        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage(getString(R.string.awaiting_response_txt));
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.show();
        }

        protected void showDialog() {

        }

        @Override
        protected Response doInBackground(String... params) {
            String comment = params[0];

            final String fullUrl = Property.USER_COMMENT_URL + SignInActivity.session.get(SessionKeys.SESSION_ID.name());

            RestClient restClient = new RestClient();
            Response response = null;
            try {
                String json = restClient.post(fullUrl, new UserCommentRequest(appId, SignInActivity.session.get(SessionKeys.USERNAME.name()), comment, new Date().getTime()));
                Log.d("JSON", json);
                response = new Gson().fromJson(json, Response.class);
            } catch (RestException e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            } catch (ConnectionRefusedException e) {
                Intent i = new Intent(getApplicationContext(), NoConnectionActivity.class);
                startActivity(i);
                finish();
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }
            return response;
        }

        @Override
        protected void onPostExecute(Response result) {
            super.onPostExecute(result);

            dialog.dismiss();

            if (result != null && ErrorCodeMapping.isSuccess(result.getStatusCode())) {
                showMessage(getString(R.string.user_review_added_successfully), getApplicationContext());
//                sfa.recreate();
            } else {
                showMessage(getString(R.string.user_review_adding_failed_message), getApplicationContext());
            }
        }


    }

    /**
     * Report Abuse of An Application
     */
    public class ReportAbuseAsyncTask extends AsyncTask<String, Context, Response> {

        ProgressDialog dialog = new ProgressDialog(sfa);

        public ReportAbuseAsyncTask() {

        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage(getString(R.string.awaiting_response_txt));
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.show();
        }

        protected void showDialog() {

        }

        @Override
        protected Response doInBackground(String... params) {
            String comment = params[0];

            final String fullUrl = MessageFormat.format(Property.REPORT_ABUSE_URL,
                    SignInActivity.session.get(SessionKeys.SESSION_ID.name()), appId, comment);

            RestClient restClient = new RestClient();
            Response response = null;
            try {
                String json = restClient.get(fullUrl);
                Log.d("JSON", json);
                response = new Gson().fromJson(json, Response.class);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (ConnectionRefusedException e) {
                Intent i = new Intent(getApplicationContext(), NoConnectionActivity.class);
                startActivity(i);
                finish();
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }
            return response;
        }

        @Override
        protected void onPostExecute(Response result) {
            super.onPostExecute(result);

            dialog.dismiss();

            if (result != null && ErrorCodeMapping.isSuccess(result.getStatusCode())) {
                showMessage(getString(R.string.abuse_report_added_successfully), getApplicationContext());
//                sfa.recreate();
            } else {
                showMessage(getString(R.string.abuse_report_adding_failed_message), getApplicationContext());
            }
        }

    }

    /**
     * Get all the comments(limited to 100) from server for an application
     */
    public class GetAllComments extends AsyncTask<String, Context, UserReview[]> {

        ProgressDialog dialog = new ProgressDialog(AppDescriptionActivity.this);

        public GetAllComments() {

        }

        protected void onPreExecute() {
            dialog.setMessage("Please wait...");
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.show();
        }

        protected void showDialog() {

        }


        protected UserReview[] doInBackground(String... params) {
            Log.e("AppID", getAppId());
            commentAllUrl = Property.ALL_USER_COMMENT_URL + getAppId() + "/start/0/limit/100";
            UserReview[] userReviews = null;
            try {
                String json = new RestClient().get(commentAllUrl);
                Log.e("json_Object_result", json);
                userReviews = new Gson().fromJson(json, UserReview[].class);
            } catch (ConnectionRefusedException e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }
            return userReviews;
        }

        protected void onPostExecute(UserReview[] result) {
            super.onPostExecute(result);
            dialog.cancel();
            LayoutInflater inflater = LayoutInflater.from(AppDescriptionActivity.this);
            final View addView = inflater.inflate(R.layout.all_comments, null);
            LinearLayout userComments = (LinearLayout) addView.findViewById(R.id.user_comments_layout);
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .cacheInMemory()
                    .displayer(new RoundedBitmapDisplayer(1000))
                    .build();

            for (UserReview userReview : result) {

                LinearLayout single_app_instruction = (LinearLayout) inflater.inflate(R.layout.single_user_comment, null);

                TextView user = (TextView) single_app_instruction.findViewById(R.id.userName);
                user.setText(userReview.getUsername());
                TextView commentDate = (TextView) single_app_instruction.findViewById(R.id.comment_date);
                commentDate.setText(userReview.getDisplayDate());
                ImageView imgUser = (ImageView) single_app_instruction.findViewById(R.id.imgUser);

                System.out.println("COMMENT " + userReview.getImage());
                if (userReview.getImage() != null && !userReview.getImage().isEmpty())
                    imageLoader.displayImage(ProfileProperty.APPSTORE_IMAGE_URL + userReview.getImage(), imgUser, options);

                TextView comment = (TextView) single_app_instruction.findViewById(R.id.comment);
                comment.setText(userReview.getComment());

                userComments.addView(single_app_instruction);
            }
            final Dialog allCommentsDialog = new Dialog(context);
            allCommentsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            allCommentsDialog.setContentView(addView);
            allCommentsDialog.show();

            Button dialogCloseBtn = (Button) addView.findViewById(R.id.close_btn);
            dialogCloseBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    allCommentsDialog.dismiss();
                }
            });

        }
    }

    /**
     * Execute Auto Login if MSISDN is available in the header
     */
    public class GetAutoLogin extends AsyncTask<Void, Context, AutoLoginResponse> {

        @Override
        protected AutoLoginResponse doInBackground(Void... params) {

            AutoLoginResponse autoLoginResponse = null;
            String json;
            try {
                json = new RestClient().getHttp(autoLoginUrl);
                autoLoginResponse = new Gson().fromJson(json, AutoLoginResponse.class);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (ConnectionRefusedException e) {
                Intent i = new Intent(getApplicationContext(), NoConnectionActivity.class);
                startActivity(i);
                finish();
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }
            String sessionId;
            Log.d(LOG_TAG, "The auto login response " + autoLoginResponse.getStatusDescription());
            if (autoLoginResponse != null && ErrorCodeMapping.isSuccess(autoLoginResponse.getStatusCode())) {
                sessionId = autoLoginResponse.getSessionId();
                SignInActivity.session.put(SessionKeys.SESSION_ID.name(), sessionId);
                SignInActivity.session.put(SessionKeys.LOGIN_METHOD.name(), LoginKeys.AUTO_LOGIN.name());
                session = sessionId;
            }
            return autoLoginResponse;
        }

        @Override
        protected void onPostExecute(AutoLoginResponse autoLoginResponse) {

            if (isNoSessionWithAutoLogin) {
                appActionHandler();
            }

        }
    }

    public class GetAuthProvider extends AsyncTask<Void, Context, AuthProviderResponse> {

        @Override
        protected AuthProviderResponse doInBackground(Void... params) {

            AuthProviderResponse authProviderResponse = null;
            String json;
            try {
                json = new RestClient().getHttp(authProviderUrl);
                Log.d("Status", " : Auto Login Response " + json);
                authProviderResponse = new Gson().fromJson(json, AuthProviderResponse.class);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (ConnectionRefusedException e) {
                Intent i = new Intent(getApplicationContext(), NoConnectionActivity.class);
                startActivity(i);
                finish();
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }

            return authProviderResponse;
        }

        @Override
        protected void onPostExecute(AuthProviderResponse authProviderResponse) {

            if (authProviderResponse != null) {
                // User has already auto logged in and from the second check also the preferred way of login is auto login
                if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name()) && authProviderResponse.getPreferredProvider().equals(getString(R.string.auth_by_msisdn))) {
                    Log.d(LOG_TAG, "Executing auth by msisdn with the session id");
                    session = SignInActivity.session.get(SessionKeys.SESSION_ID.name());
                    appActionHandler();

                } else if (!SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name()) && authProviderResponse.getPreferredProvider().equals(getString(R.string.auth_by_msisdn))) {
                    Log.d(LOG_TAG, "Executing auth by msisdn without the session id");
                    getAutoLoginAsyncTask.execute();
                    isNoSessionWithAutoLogin = true;

                    // In the second check the preferred way of login is signing in
                } else if (!SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name()) && authProviderResponse.getPreferredProvider().equals(getString(R.string.auth_by_password))) {
                    Log.d(LOG_TAG, "Executing auth by password without session id");
                    isLaunchAppActionHandler = true;
                    handleSignInAction();
                } else if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name()) && authProviderResponse.getPreferredProvider().equals(getString(R.string.auth_by_password))) {
                    Log.d(LOG_TAG, "Executing auth by password with the session id");
                    session = SignInActivity.session.get(SessionKeys.SESSION_ID.name());
                    appActionHandler();

                }
            } else {
                Log.d(LOG_TAG, "Error Occured on Executing Auth Provider Request");
                showMessage(getString(R.string.login_first_to_continue_caption), getApplicationContext());
            }
        }
    }

    public void setAppInstructions(View addView) {
        chargingDetailsWithOperators = application.getChargingDetailsWithOperators();
        LinearLayout appInstructions = (LinearLayout) addView.findViewById(R.id.app_instructions_layout);

        String[] operators = application.getOperators();
        int count = 0;

        if (getResources().getBoolean(R.bool.multi_deployment_mode)) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LinearLayout multi_app_instruction = (LinearLayout) inflater.inflate(R.layout.multi_charging_detail, null);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, 0, 20);

            TabHost host = (TabHost) multi_app_instruction.findViewById(R.id.tabHost);
            host.setup();

            for (String operator : operators) {
                TabHost.TabSpec spec = host.newTabSpec(operator);
                HashMap<String, String> instructions = application.getInstructions();

                TextView instructionDetail;
                TextView costDetail;

                if (count < 1) {
                    instructionDetail = (TextView) multi_app_instruction.findViewById(R.id.instruction_detail1);
                    costDetail = (TextView) multi_app_instruction.findViewById(R.id.cost_detail1);
                    spec.setContent(R.id.tab1);

                    if (instructions.containsKey(operator)) {
                        instructionDetail.setText(instructions.get(operator));
                    }
                    costDetail.setText(chargingDetailsWithOperators.get(operator));
                } else {
                    instructionDetail = (TextView) multi_app_instruction.findViewById(R.id.instruction_detail2);
                    costDetail = (TextView) multi_app_instruction.findViewById(R.id.cost_detail2);
                    spec.setContent(R.id.tab2);

                    if (instructions.containsKey(operator)) {
                        instructionDetail.setText(instructions.get(operator));
                    }
                    costDetail.setText(chargingDetailsWithOperators.get(operator));
                }

                String[] operatorsList = getResources().getStringArray(R.array.operators);
                if (operatorsList.length > 1) {
                    Map<String, Integer> icons = new HashMap<>();
                    for (String op : operatorsList) {
                        int operatorImage = getApplicationContext().getResources().getIdentifier("instruction_" + op + "_img", "drawable", getApplicationContext().getPackageName());
                        icons.put(op, operatorImage);
                    }
                    spec.setIndicator("", getResources().getDrawable(icons.get(operator)));
                }

                host.addTab(spec);
                count++;
            }

            appInstructions.addView(multi_app_instruction, layoutParams);

            for (int i = 0; i < host.getTabWidget().getChildCount(); i++) {
                TextView text = (TextView) host.getTabWidget().getChildTabViewAt(i).findViewById(android.R.id.title);
                text.setTextColor(Color.parseColor("#4a4d4e"));
            }
        } else {
            for (AppInstruction appInstruction : appInstructionList) {
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                LinearLayout single_app_instruction = (LinearLayout) inflater.inflate(R.layout.single_charging_detail, null);

                TextView app_instruction = (TextView) single_app_instruction.findViewById(R.id.app_des_subscriber_detail);
                app_instruction.setText(appInstruction.getInstruction());

                appInstructions.addView(single_app_instruction);
            }
        }
    }

    public void showReadMoreDialog(String detailedDesc) {
        LayoutInflater inflater = LayoutInflater.from(AppDescriptionActivity.this);
        final View addView = inflater.inflate(R.layout.app_detailed_description, null);

        TextView tvDesc = (TextView) addView.findViewById(R.id.tvDesc);
        TextView tvCost = (TextView) addView.findViewById(R.id.tvCost);
        TextView tvInstructionLbl = (TextView) addView.findViewById(R.id.instructions_label);
        TextView tvAppName = (TextView) addView.findViewById(R.id.tvAppName);

        tvAppName.setText(detailedAppName);
        tvDesc.setText(detailedDesc);
        tvCost.setText(detailedCharging);
        setAppInstructions(addView);

        final Dialog allCommentsDialog = new Dialog(context);
        allCommentsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        allCommentsDialog.setContentView(addView);
        allCommentsDialog.show();

        Button dialogCloseBtn = (Button) addView.findViewById(R.id.close_btn);
        dialogCloseBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                allCommentsDialog.dismiss();
            }
        });
    }

    public class GetChargeByPI extends AsyncTask<String, Context, ChargeByPIResponse> {

        ProgressDialog dialog = new ProgressDialog(sfa);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage(getResources().getString(R.string.please_wait_message));
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.show();
        }

        @Override
        protected ChargeByPIResponse doInBackground(String... params) {
            String getChargeByPIUrl = params[0];

            ChargeByPIResponse chargeByPIResponse = null;
            String json;
            try {
                json = new RestClient().get(getChargeByPIUrl);
                chargeByPIResponse = new Gson().fromJson(json, ChargeByPIResponse.class);
                Log.d("L", "ChargeByResponse " + json);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (ConnectionRefusedException e) {
                Intent i = new Intent(getApplicationContext(), NoConnectionActivity.class);
                startActivity(i);
                finish();
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }

            return chargeByPIResponse;
        }

        @Override
        protected void onPostExecute(ChargeByPIResponse chargeByPIResponse) {
            dialog.hide();
            if (chargeByPIResponse != null) {
                if (ErrorCodeMapping.isSuccess(chargeByPIResponse.getStatusCode())) {
                    if (chargeByPIResponse.getWapUrl() != null) {
                        doDirectDownload(chargeByPIResponse);
                    }
                } else {
                    showMessage(chargeByPIResponse.getStatusDescription(), getApplicationContext());
                }

            } else {
                showMessage(getString(R.string.default_error), getApplicationContext());
            }
        }
    }

    public class GetDeveloperApps extends AsyncTask<String, Context, ApplicationListResponse> {

        boolean showRetry = false;

        public GetDeveloperApps(int start, int limit) {
            devAppsUrl = MessageFormat.format(Property.DEV_APPS_URL,
                    NumberFormat.getInstance(Locale.ENGLISH).format(start),
                    NumberFormat.getInstance(Locale.ENGLISH).format(limit));
        }

        @Override
        protected ApplicationListResponse doInBackground(String... params) {
            ApplicationListResponse applicationListResponse = null;
            try {
                String json = new RestClient().get(devAppsUrl + detailedAppCreatedBy);
                applicationListResponse = new Gson().fromJson(json, ApplicationListResponse.class);
                showRetry = false;
            } catch (ConnectionRefusedException e) {
                Log.d(LOG_TAG, "ConnectionRefusedException ", e);
                showRetry = true;
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
                showRetry = true;
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                showRetry = true;
            }

            if (applicationListResponse != null && ErrorCodeMapping.isSuccess(applicationListResponse.getStatusCode()) && applicationListResponse.getResults().length >= 0)
                return applicationListResponse;
            return null;
        }

        protected void onPostExecute(ApplicationListResponse applicationListResponse) {
            ArrayList<Application> appList = new ArrayList<Application>(Arrays.asList(applicationListResponse.getResults()));
            populateApps(devAppsLinearLayout, appList, false);
//            if (allApps != null) {
//                populateApps(featuredAppsLayout, new ArrayList<Application>(Arrays.asList(allApps.getFeaturedApps())), true);
//                populateApps(mostlyUsedAppsLayout, new ArrayList<Application>(Arrays.asList(allApps.getMostlyUsedApps())), false);
//                populateApps(newlyAddedAppsLayout, new ArrayList<Application>(Arrays.asList(allApps.getNewlyAddedApps())), false);
//                populateApps(freeAppsLayout, new ArrayList<Application>(Arrays.asList(allApps.getFreeApps())), false);
//            }
//
//            if (showRetry) {
//                retryLayout.setVisibility(View.VISIBLE);
//                appSummaryLayout.setVisibility(View.GONE);
//            } else {
//                retryLayout.setVisibility(View.GONE);
//                appSummaryLayout.setVisibility(View.VISIBLE);
//            }
//            loadingLayout.setVisibility(View.GONE);
        }
    }

    private void populateApps(LinearLayout layout, ArrayList<Application> appList, boolean isFeatured) {
        for (final Application app : appList) {

            int singleAppLayout;
            if (isFeatured) {
                singleAppLayout = R.layout.featured_app_single;
            } else singleAppLayout = R.layout.single_app_small;

            View appView = LayoutInflater.from(context).inflate(singleAppLayout, null);

            TextView txtAppId = (TextView) appView.findViewById(R.id.single_app_id);
            txtAppId.setText(app.getId());

            TextView appName = (TextView) appView.findViewById(R.id.single_app_name);
            appName.setText(app.getName());

            TextView appDescription = (TextView) appView.findViewById(R.id.single_app_description);
            appDescription.setText(app.getDescription());

            TextView appRating = (TextView) appView.findViewById(R.id.single_app_rating);
            appRating.setText(String.valueOf(app.getRating()));

            TextView appAuthor = (TextView) appView.findViewById(R.id.single_app_cost);
            appAuthor.setText(app.getDeveloper());

            ImageView appIcon = (ImageView) appView.findViewById(R.id.icon);
            if (isFeatured) {
                String appBanner;
                if (app.getAppBanners().length > 0) {
                    appBanner = app.getAppBanners()[0].getUrl();
                } else appBanner = "";

                loadAppImage(appBanner, appIcon);
            } else loadAppImage(app.getAppIcon(), appIcon);

            appView.setOnClickListener(setAppOnClickAction(app));

            layout.addView(appView);
        }
    }

    private void loadAppImage(String appImgUrl, ImageView imgView) {
        String imageUrl = ProfileProperty.APPSTORE_IMAGE_URL + appImgUrl;
        // Load and display image asynchronously
        imageLoader.displayImage(imageUrl, imgView, options);
    }

    private View.OnClickListener setAppOnClickAction(final Application app) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = ((TextView) v.findViewById(R.id.single_app_name)).getText().toString();
                String app_id = ((TextView) v.findViewById(R.id.single_app_id)).getText().toString();

                // Starting new intent
                Intent in = new Intent(context, AppDescriptionActivity.class);
                in.putExtra(Tags.TAG_NAME.name(), name);
                in.putExtra(Tags.TAG_APP_ID.name(), app_id);
                in.putExtra("app", app);
                context.startActivity(in);
            }
        };
    }

    public class MyAccountProfileDetailsAsyncTask extends AsyncTask<String, Context, ProfileDetailsResponse> {

        @Override
        protected ProfileDetailsResponse doInBackground(String... params) {
            String profileDetailsJson = "";
            ProfileDetailsResponse profileDetailsResponse = null;
            String  profUrl = Property.MY_ACCOUNT_PROFILE_DETAILS;
            if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
                profUrl += "/session-id/" + SignInActivity.session.get(SessionKeys.SESSION_ID.name());
            }
            try {
                profileDetailsJson = new RestClient().get(profUrl);
                profileDetailsResponse = new Gson().fromJson(profileDetailsJson, ProfileDetailsResponse.class);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (ConnectionRefusedException e) {
                Log.d(LOG_TAG, "ConnectionRefusedException ", e);
            } catch (Exception e) {
                Log.d(LOG_TAG, "Exception ", e);
            }
            Log.d("JSON", "JSON object recieved in Async Task\n" + profileDetailsJson);

            return profileDetailsResponse;
        }

        @Override
        protected void onPostExecute(ProfileDetailsResponse result) {
            super.onPostExecute(result);

            if (result != null) {
                if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
                    setImage(ProfileProperty.APPSTORE_IMAGE_URL + result.getImage());
                } else {
                    imgProfile.setImageDrawable(getResources().getDrawable(R.drawable.icon_user));
                }
            }
        }
    }

    private void setImage(String image) {

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory()
                .displayer(new RoundedBitmapDisplayer(1000))
                .build();

        imageLoader.displayImage(image, imgProfile, options);
    }

    public class SendRatingsAsyncTask extends AsyncTask<String, Context, ApplicationListResponse> {

        @Override
        protected ApplicationListResponse doInBackground(String... params) {
            try {
                RatingRequest ratingRequest = new RatingRequest(appId, rateValue);
                String json = json = new RestClient().post(Property.APP_RATE_THIS_URL, ratingRequest);
                Response response = new Gson().fromJson(json, Response.class);
            } catch (ConnectionRefusedException e) {
                Log.d(LOG_TAG, "ConnectionRefusedException ", e);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }

            return null;
        }
    }
}




