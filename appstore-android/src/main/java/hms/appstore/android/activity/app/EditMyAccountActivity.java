package hms.appstore.android.activity.app;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.signature.StringSignature;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;

import hms.appstore.android.R;
import hms.appstore.android.activity.CustomToolBar;
import hms.appstore.android.activity.user.SignInActivity;
import hms.appstore.android.rest.ConnectionRefusedException;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.rest.RestException;
import hms.appstore.android.rest.request.ProfileUpdateRequest;
import hms.appstore.android.rest.response.ProfileDetailsResponse;
import hms.appstore.android.rest.response.Response;
import hms.appstore.android.util.Commons;
import hms.appstore.android.util.ErrorCodeMapping;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.ProfileProperty;
import hms.appstore.android.util.Property;
import hms.appstore.android.util.SessionKeys;
import hms.appstore.android.util.UserType;
import hms.appstore.android.util.widget.LocalizedButton;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static hms.appstore.android.activity.app.SettingsActivity.LOG_TAG;

public class EditMyAccountActivity extends CustomToolBar {

    private EditText fullNameTxt, mobileNoTxt, emailAddressTxt, etMobilePrefix;
    private ImageView imgProfile;
    private LocalizedButton btnSave, btnCancel;
    private hms.appstore.android.rest.response.Response response;
    static final int DATE_DIALOG_ID = 999;
    private int birthYear, birthMonth, birthDay;
    private String filePath = null;
    SharedPreferences sharedPerference;
    ProgressDialog dialog;
    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_my_account);
        setUI();
        setListeners();
        fillProfileValues(getIntent().getExtras());
    }

    private void fillProfileValues(Bundle extras) {
        if (extras != null) {
            fullNameTxt.setText(extras.getString("PROFILE_FULL_NAME"));
            mobileNoTxt.setText(extras.getString("PROFILE_MOBILE_NO"));

            String userType = extras.getString("PROFILE_USER_TYPE");
            if(userType != null && !userType.isEmpty()){
                if(userType.equals(UserType.INDIVIDUAL.toString()))
                   emailAddressTxt.setEnabled(true);
                else
                    emailAddressTxt.setEnabled(false);
            }
            emailAddressTxt.setText(extras.getString("PROFILE_EMAIL"));
            setImage(ProfileProperty.APPSTORE_IMAGE_URL + extras.get("PROFILE_IMAGE").toString());
        }
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("EditMyAcc","Permission is granted");
                return true;
            } else {

                Log.v("EditMyAcc","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("EditMyAcc","Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){
            Log.v("EditMyAcc","Permission: "+permissions[0]+ "was "+grantResults[0]);
            Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickPhoto, 0);
        }
    }

    private void setListeners() {

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isStoragePermissionGranted()) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto, 0);
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new ProgressDialog(EditMyAccountActivity.this);
                dialog.setMessage(getString(R.string.my_account_profile_update));
                dialog.setIndeterminate(true);
                dialog.setCancelable(true);
                dialog.show();
                btnSave.setEnabled(false);

                if (isPersonalDetailValid()) {
//                    ProfileDetailsUpdateAsyncTask profileDetailsUpdateAsyncTask = new ProfileDetailsUpdateAsyncTask();
//                    profileDetailsUpdateAsyncTask.execute(getUpdateReqParams());
                    updateProfile(getUpdateReqParams());
                } else{
                    dialog.dismiss();
                    btnSave.setEnabled(true);
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateProfile(String[] params) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Service service = new Retrofit.Builder().baseUrl(ProfileProperty.REST_HOST).addConverterFactory(GsonConverterFactory.create()).client(client).build().create(Service.class);

        MultipartBody.Part body = null;
        if (filePath != null) {
            File file = new File(filePath);
            File compressedImageFile = file;
            try {
                compressedImageFile = new Compressor(this)
                        .setMaxWidth(480)
                        .setMaxHeight(480)
                        .setQuality(10)
                        .setCompressFormat(Bitmap.CompressFormat.WEBP)
                        .compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), compressedImageFile);
            body = MultipartBody.Part.createFormData("image", compressedImageFile.getName(), reqFile);
        }
        sharedPerference = getSharedPreferences(Property.PREFS_NAME, 0);
        String url = ProfileProperty.REST_HOST + Property.MY_ACCOUNT_PROFILE_UPDATE;
        if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
            url += "/session-id/" + SignInActivity.session.get(SessionKeys.SESSION_ID.name());
        }
        RequestBody firstName = RequestBody.create(MediaType.parse("text/plain"), params[2].trim());
        RequestBody lastName = RequestBody.create(MediaType.parse("text/plain"), params[3].trim());
        RequestBody contactNo = RequestBody.create(MediaType.parse("text/plain"), params[0]);
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), params[4]);

        retrofit2.Call<Response> req = service.postImage(url, body, firstName, lastName, contactNo, email);
        req.enqueue(new Callback<Response>() {

                        @Override
                        public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                            if (response.body() != null) {
                                System.out.println("Retrofit-" + response.body().getStatusCode());
                                if (!response.body().getStatusCode().equals(Property.SUCCESS_STATUS)) {
                                    if(response.body().getStatusCode().equals(Property.ERROR_EMAIL_ALREADY_USED)){
                                        Toast.makeText(getApplication(), R.string.my_account_profile_update_email_already_used, Toast.LENGTH_LONG).show();
                                    } else{
                                        Toast.makeText(getApplication(), R.string.my_account_profile_update_failed, Toast.LENGTH_LONG).show();
                                    }
                                    dialog.dismiss();
                                    btnSave.setEnabled(true);
                                } else{
                                    finish();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Response> call, Throwable t) {
                            System.out.println("Retrofit- Failure");
                            t.printStackTrace();
                            dialog.dismiss();
                            btnSave.setEnabled(true);
                            Toast.makeText(getApplication(), R.string.my_account_profile_update_failed, Toast.LENGTH_LONG).show();
                        }
                    }
        );

    }

    private void setUI() {
        initCustomToolBar();
        getSupportActionBar().setTitle(R.string.my_account_edit_info_header);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        imgProfile = (ImageView) findViewById(R.id.imgProfile);
        fullNameTxt = (EditText) findViewById(R.id.txtFullName);
        mobileNoTxt = (EditText) findViewById(R.id.txtMobileNo);
        emailAddressTxt = (EditText) findViewById(R.id.txtEmailAddress);
        btnSave = (LocalizedButton) findViewById(R.id.btnSave);
        btnCancel = (LocalizedButton) findViewById(R.id.btnCancel);
        etMobilePrefix = (EditText) findViewById(R.id.etMobilePrefix);
        mobileNoTxt.setEnabled(false);
        etMobilePrefix.setEnabled(false);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    android.database.Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    if (cursor == null)
                        return;

                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    filePath = cursor.getString(columnIndex);
                    cursor.close();

                    if (filePath != null) {
                        Glide.with(this).load(selectedImage).asBitmap().signature(new StringSignature(String.valueOf(System.currentTimeMillis()))).placeholder(R.drawable.icon_user).error(R.drawable.icon_user).centerCrop().into(new BitmapImageViewTarget(imgProfile) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(view.getContext().getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                imgProfile.setImageDrawable(circularBitmapDrawable);
                            }
                        });
                    } else
                        Toast.makeText(this, R.string.profile_img_ipload_error, Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private boolean isPersonalDetailValid() {
        boolean isValid = true;

        if (Commons.isEditTextEmpty(fullNameTxt, getString(R.string.register_fullname_error_empty)))
            isValid = false;
        else if (fullNameTxt.getText().toString().trim().split(" ").length < 2) {
            fullNameTxt.setError(getString(R.string.register_fullname_error_missing_lastname));
            isValid = false;
        }

        if (Commons.isEditTextEmpty(mobileNoTxt, getString(R.string.register_mobileno_error_empty)))
            isValid = false;
        else if (mobileNoTxt.getText().toString().trim().length() <
                getResources().getInteger(R.integer.register_screen_mobile_num_length)) {
            mobileNoTxt.setError(getString(R.string.register_mobileno_error_invalid_format));
            isValid = false;
        }

        if (Commons.isEditTextEmpty(emailAddressTxt, getString(R.string.register_email_error_empty)))
            isValid = false;
        else if (!emailAddressTxt.getText().toString().trim().matches(
                getApplicationContext().getString(R.string.register_screen_email_regex))) {
            emailAddressTxt.setError(getString(R.string.register_email_error_invalid_format));
            isValid = false;
        }

        return isValid;
    }

    private String[] getUpdateReqParams() {

        String[] updateParams = new String[5];
        updateParams[0] = getString(R.string.country_mobile_code) + mobileNoTxt.getText().toString().trim(); //msisdn Param
        updateParams[1] = filePath; //image Param
        String[] nameArray = fullNameTxt.getText().toString().trim().split(" ");
        updateParams[2] = nameArray[0]; //firstName Param
        updateParams[3] = "";
        if (nameArray.length > 2) {
            for (int i = 1; i < nameArray.length; i++) {
                updateParams[3] = updateParams[3] + " " + nameArray[i];
            }
        } else
            updateParams[3] = nameArray[1]; //last Param
        updateParams[4] = emailAddressTxt.getText().toString().trim(); //email Param

        return updateParams;
    }

    public class ProfileDetailsUpdateAsyncTask extends AsyncTask<String, Context, Response> {

        ProgressDialog dialog = new ProgressDialog(EditMyAccountActivity.this);

        @Override
        protected void onPreExecute() {
            dialog.setMessage(getString(R.string.my_account_profile_update));
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.show();
        }

        @Override
        protected Response doInBackground(String... params) {

            String msisdn = params[0];
            String image = params[1];
            String firstName = params[2];
            String lastName = params[3];
            String email = params[4];

            String profileUpdateJson = "";
            ProfileUpdateRequest profileUpdateRequest = new ProfileUpdateRequest(msisdn, image, firstName, lastName, email);
            try {
                profileUpdateJson = new RestClient().post(Property.MY_ACCOUNT_PROFILE_UPDATE, profileUpdateRequest);
                response = new Gson().fromJson(profileUpdateJson, ProfileDetailsResponse.class);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (ConnectionRefusedException e) {
                Log.d(LOG_TAG, "ConnectionRefusedException ", e);
            } catch (Exception e) {
                Log.d(LOG_TAG, "Exception ", e);
            }
            Log.d("JSON", "JSON object recieved in Async Task\n" + profileUpdateJson);

            return response;
        }

        @Override
        protected void onPostExecute(Response result) {
            super.onPostExecute(result);
            dialog.dismiss();
            finish();
            if (result != null) {
                if (ErrorCodeMapping.isSuccess(result.getStatusCode())) {
                    showMessage(getString(R.string.my_account_profile_update_success), EditMyAccountActivity.this);
                } else {
                    showMessage(result.getStatusDescription(), EditMyAccountActivity.this);
                }
            } else {
                showMessage(getString(R.string.E5000), EditMyAccountActivity.this);
            }
        }
    }

    protected void showMessage(String results, Context context) {
        if (results != null) {
            Toast aToast = Toast.makeText(context, results, Toast.LENGTH_SHORT);
            hms.appstore.android.util.ToastExpander.showFor(aToast, 5000);
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.E5000), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setImage(String image) {
        System.out.println("MyAccount SetImage " + image);
        Glide.with(this).load(image).asBitmap().signature(new StringSignature(String.valueOf(System.currentTimeMillis()))).placeholder(R.drawable.icon_user).error(R.drawable.icon_user).centerCrop().into(new BitmapImageViewTarget(imgProfile) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(view.getContext().getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                imgProfile.setImageDrawable(circularBitmapDrawable);
            }
        });
    }

}
