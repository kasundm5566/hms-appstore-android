package hms.appstore.android.activity;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import hms.appstore.android.R;

public class CustomToolBar extends AppCompatActivity {

    public void initCustomToolBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
    }
}
