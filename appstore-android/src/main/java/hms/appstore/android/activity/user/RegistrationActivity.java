package hms.appstore.android.activity.user;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.Calendar;

import hms.appstore.android.R;
import hms.appstore.android.activity.CustomToolBar;
import hms.appstore.android.rest.ConnectionRefusedException;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.rest.RestException;
import hms.appstore.android.rest.request.CreateUserRequest;
import hms.appstore.android.rest.response.Response;
import hms.appstore.android.util.Commons;
import hms.appstore.android.util.ErrorCodeMapping;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.ProfileProperty;
import hms.appstore.android.util.Property;

public class RegistrationActivity extends CustomToolBar {

    private EditText fullNameTxt, birthdayTxt, mobileNoTxt, emailAddressTxt, userNameTxt, passwordTxt, rePasswordTxt;
    private LinearLayout personalDetailsLayout, accountDetailsLayout, passwordArea, retypePasswordArea;
    CreateUserAsyncTask createUserAsyncTask;

    static final int DATE_DIALOG_ID = 999;
    static final String LOG_TAG = RegistrationActivity.class.getCanonicalName();

    private int birthYear, birthMonth, birthDay;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_v2);
        initCustomToolBar();
        getSupportActionBar().setTitle(R.string.menu_item_sign_up_caption);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setCurrentDateOnView();
        initializeViewItems();
    }


    @Override
    protected void onStop() {
        super.onStop();

        if (createUserAsyncTask != null && !createUserAsyncTask.isCancelled()) {
            createUserAsyncTask.cancel(true);
        }
    }

    public void onFinalSubmitButtonClick(View view) {
        if (isAccountDetailsValid()) {
            createUserAsyncTask = new CreateUserAsyncTask();
            createUserAsyncTask.execute(getRegisterReqParams());
        }
    }

    public void onCancelButtonClick(View view) {
        super.finish();
    }

    public void onContinueButtonClick(View view) {
        if (isPersonalDetailValid()) {
            personalDetailsLayout.setVisibility(View.GONE);
            accountDetailsLayout.setVisibility(View.VISIBLE);
        }
    }

    public void onBackButtonClick(View view) {
        accountDetailsLayout.setVisibility(View.GONE);
        personalDetailsLayout.setVisibility(View.VISIBLE);
    }

    public void onBirthdayTextClick(View view) {
        showDialog(DATE_DIALOG_ID);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID: {
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, AlertDialog.THEME_HOLO_LIGHT, datePickerListener, birthYear, birthMonth, birthDay);
                datePickerDialog.setTitle(R.string.register_set_birthday_dialog_text);
                return datePickerDialog;
            }
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            birthYear = selectedYear;
            birthMonth = selectedMonth;
            birthDay = selectedDay;

            birthdayTxt.setText(new StringBuilder().append(birthDay)
                    .append("/").append(birthMonth + 1).append("/").append(birthYear));
        }
    };

    private void setCurrentDateOnView() {
        final Calendar c = Calendar.getInstance();
        birthYear = c.get(Calendar.YEAR);
        birthMonth = c.get(Calendar.MONTH);
        birthDay = c.get(Calendar.DAY_OF_MONTH);
    }

    private void initializeViewItems() {
        personalDetailsLayout = (LinearLayout) findViewById(R.id.personalDetailsLayout);
        accountDetailsLayout = (LinearLayout) findViewById(R.id.accountDetailsLayout);
        passwordArea = (LinearLayout) findViewById(R.id.passwordArea);
        retypePasswordArea = (LinearLayout) findViewById(R.id.retypePasswordArea);
        birthdayTxt = (EditText) findViewById(R.id.txtBirthday);
        fullNameTxt = (EditText) findViewById(R.id.txtFullName);
        mobileNoTxt = (EditText) findViewById(R.id.txtMobileNo);
        emailAddressTxt = (EditText) findViewById(R.id.txtEmailAddress);
        userNameTxt = (EditText) findViewById(R.id.txtRegisterUsername);
        passwordTxt = (EditText) findViewById(R.id.txtRegisterPassword);
        rePasswordTxt = (EditText) findViewById(R.id.txtRegisterRetypePassword);

        if (getResources().getBoolean(R.bool.sys_gen_password_on_user_registration)) {
            passwordArea.setVisibility(View.GONE);
            retypePasswordArea.setVisibility(View.GONE);
        }
    }

    private boolean isPersonalDetailValid() {
        boolean isValid = true;

        if (Commons.isEditTextEmpty(fullNameTxt, getString(R.string.register_fullname_error_empty)))
            isValid = false;
        else if (fullNameTxt.getText().toString().trim().split(" ").length < 2) {
            fullNameTxt.setError(getString(R.string.register_fullname_error_missing_lastname));
            isValid = false;
        }

        if (Commons.isEditTextEmpty(birthdayTxt, getString(R.string.register_birthday_error_empty)))
            isValid = false;

        if (Commons.isEditTextEmpty(mobileNoTxt, getString(R.string.register_mobileno_error_empty)))
            isValid = false;
        else if (mobileNoTxt.getText().toString().trim().length() <
                getResources().getInteger(R.integer.register_screen_mobile_num_length)) {
            mobileNoTxt.setError(getString(R.string.register_mobileno_error_invalid_format));
            isValid = false;
        }

        if (Commons.isEditTextEmpty(emailAddressTxt, getString(R.string.register_email_error_empty)))
            isValid = false;
        else if (!emailAddressTxt.getText().toString().trim().matches(
                getApplicationContext().getString(R.string.register_screen_email_regex))) {
            emailAddressTxt.setError(getString(R.string.register_email_error_invalid_format));
            isValid = false;
        }

        return isValid;
    }

    private boolean isAccountDetailsValid() {
        boolean isValid = true;

        if (Commons.isEditTextEmpty(userNameTxt, getString(R.string.register_username_error_empty)))
            isValid = false;
        else if (userNameTxt.getText().toString().trim().length() <
                getResources().getInteger(R.integer.register_screen_username_min_length)) {
            userNameTxt.setError(getString(R.string.register_username_error_length));
            isValid = false;
        }

        if (!getResources().getBoolean(R.bool.sys_gen_password_on_user_registration)) {
            int passwordMinLength = getResources().getInteger(R.integer.user_password_min_length);

            if (Commons.isEditTextEmpty(passwordTxt, getString(R.string.register_password_error_empty)))
                isValid = false;
            else if (passwordTxt.getText().toString().trim().length() < passwordMinLength) {
                passwordTxt.setError(String.format(getString(R.string.register_password_error_length), passwordMinLength));
                isValid = false;
            }

            if (Commons.isEditTextEmpty(rePasswordTxt, getString(R.string.register_re_password_error_empty)))
                isValid = false;
            else if (!rePasswordTxt.getText().toString().trim().equals(passwordTxt.getText().toString().trim())) {
                rePasswordTxt.setError(getString(R.string.register_re_password_error_mismatch));
                isValid = false;
            }
        }

        return isValid;
    }


    private String[] getRegisterReqParams() {
        String[] registerParams = new String[7];

        String[] nameArray = fullNameTxt.getText().toString().trim().split(" ");

        registerParams[0] = nameArray[0]; //firstName Param
        for (int i = 1; i < nameArray.length; i++) { //lastName Param
            if (registerParams[1] != null)
                registerParams[1] = registerParams[1] + " " + nameArray[i];
            else registerParams[1] = nameArray[i];
        }
        registerParams[2] = getString(R.string.country_mobile_code) +
                mobileNoTxt.getText().toString().trim(); //msisdn Param
        registerParams[3] = userNameTxt.getText().toString().trim(); //username Param
        registerParams[4] = passwordTxt.getText().toString().trim(); //password Param
        registerParams[5] = birthdayTxt.getText().toString().trim(); //birth-date Param
        registerParams[6] = emailAddressTxt.getText().toString().trim(); //email Param

        return registerParams;
    }


    public class CreateUserAsyncTask extends AsyncTask<String, Context, Response> {

        ProgressDialog dialog = new ProgressDialog(RegistrationActivity.this);

        @Override
        protected void onPreExecute() {
            dialog.setMessage(getString(R.string.register_request_processing));
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.show();
        }

        @Override
        protected Response doInBackground(String... params) {
            String firstName = params[0];
            String lastName = params[1];
            String msisdn = params[2];
            String username = params[3];
            String password = params[4];
            String birthDate = params[5];
            String email = params[6];
            String operator = ProfileProperty.OPERATOR_NAME;
            String domain = getString(R.string.register_domain);
            boolean verifyMsisdn = getResources().getBoolean(R.bool.register_verify_user_by_msisdn);

            CreateUserRequest createUserRequest =
                    new CreateUserRequest(firstName, lastName, username, password, msisdn, email, birthDate, domain, operator, verifyMsisdn);
            Response createUserResponse = null;
            String createUserRespJson = "";

            try {
                createUserRespJson = new RestClient().post(Property.CREATE_USER_URL, createUserRequest);
                createUserResponse = new Gson().fromJson(createUserRespJson, Response.class);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (ConnectionRefusedException e) {
                Log.d(LOG_TAG, "ConnectionRefusedException ", e);
            } catch (Exception e) {
                Log.d(LOG_TAG, "Exception ", e);
            }
            Log.d("JSON", "JSON object recieved in Async Task\n" + createUserRespJson);

            return createUserResponse;
        }

        @Override
        protected void onPostExecute(Response result) {
            super.onPostExecute(result);
            dialog.dismiss();

            if (result != null) {
                if (ErrorCodeMapping.isSuccess(result.getStatusCode())) {
                    Intent intent = new Intent(getApplicationContext(), AccountVerificationActivity.class);
                    intent.putExtra(Property.MOBILE_NO_EXTRA, mobileNoTxt.getText().toString().trim());
                    intent.putExtra(Property.USERNAME_EXTRA, userNameTxt.getText().toString().trim());
                    startActivity(intent);
                } else {
                    showMessage(result.getStatusDescription(), RegistrationActivity.this);
                }
            } else {
                showMessage(getString(R.string.E5000), RegistrationActivity.this);
            }
        }

        protected void showMessage(String results, Context context) {
            if (results != null) {
                Toast aToast = Toast.makeText(context, results, Toast.LENGTH_SHORT);
                hms.appstore.android.util.ToastExpander.showFor(aToast, 5000);
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.E5000), Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
