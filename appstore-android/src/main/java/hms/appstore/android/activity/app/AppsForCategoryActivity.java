/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.activity.app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import java.util.HashMap;

import hms.appstore.android.R;
import hms.appstore.android.activity.MainActivity;
import hms.appstore.android.activity.MyAppsActivity;
import hms.appstore.android.activity.NoConnectionActivity;
import hms.appstore.android.activity.user.SignInActivity;
import hms.appstore.android.fragment.category.CategoryUIFragmentAdapter;
import hms.appstore.android.util.ConnectionDetector;
import hms.appstore.android.util.Property;
import hms.appstore.android.util.RedirectKeys;
import hms.appstore.android.util.SessionKeys;
import hms.appstore.android.util.Tags;

public class AppsForCategoryActivity extends AppsForCategoryParentActivity implements OnClickListener {
    ConnectionDetector cd = null;

    public static String categoryName = null;
    private TabLayout tabLayout;
    int tabSelectedColor, tabDefaultColor;
    EditText search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_app_page_with_tabs);
        initCustomToolBar();
        search = (EditText) findViewById(R.id.txt_home_search);
        tabSelectedColor = ContextCompat.getColor(this, R.color.colorPrimary);
        tabDefaultColor = ContextCompat.getColor(this, R.color.feature_icon_default_color);
        mAdapter = new CategoryUIFragmentAdapter(getSupportFragmentManager(), getApplicationContext());

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(Tags.TAG_CATEGORY.name())) {
                categoryName = extras.getCharSequence(Tags.TAG_CATEGORY.name()).toString();
            }
        }

        getSupportActionBar().setTitle(categoryName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            mPager = (ViewPager) findViewById(R.id.pager);
            mPager.setAdapter(mAdapter);
            mPager.setOffscreenPageLimit(3);
            mPager.setCurrentItem(0);
            tabLayout = (TabLayout) findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(mPager);
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
            createTabIcons(this);
        } else {
            Intent i = new Intent(getApplicationContext(), NoConnectionActivity.class);
            startActivity(i);
            finish();
        }
    }

    private void createTabIcons(final Context context) {

        tabLayout.getTabAt(0).setIcon(R.drawable.all);
        tabLayout.getTabAt(0).getIcon().setColorFilter(tabSelectedColor, PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(1).setIcon(R.drawable.most_used);
        tabLayout.getTabAt(1).getIcon().setColorFilter(tabDefaultColor, PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(2).setIcon(R.drawable.newly_added);
        tabLayout.getTabAt(2).getIcon().setColorFilter(tabDefaultColor, PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(3).setIcon(R.drawable.free_apps);
        tabLayout.getTabAt(3).getIcon().setColorFilter(tabDefaultColor, PorterDuff.Mode.SRC_IN);

        tabLayout.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(mPager) {

                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        super.onTabSelected(tab);
                        tab.getIcon().setColorFilter(tabSelectedColor, PorterDuff.Mode.SRC_IN);
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                        tab.getIcon().setColorFilter(tabDefaultColor, PorterDuff.Mode.SRC_IN);
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }
                }
        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(getString(R.string.alert_box_ok_caption), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //MainActivity.this.finish();
            }
        });
        alertDialog.setIcon(R.drawable.no_internet);
        alertDialog.show();
    }

    @Override
    public boolean onSearchRequested() {
        search = (EditText) findViewById(R.id.txt_home_search);
        Toast.makeText(this, "Searching for " + search.getText().toString(), Toast.LENGTH_LONG).show();
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home || item.getItemId() == 0) {
           this.finish();
            return true;
        }

        int selection = item.getItemId();

        if (selection == 1) {
        }
        if (selection == 2) {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
                    //user has logged in - redirect to My AppStore UI
                    Intent i = new Intent(getApplicationContext(), MyAppsActivity.class);
                    startActivity(i);
                } else {
                    //user is not signed in - redirect to Sign In UI
                    Toast.makeText(this, getString(R.string.login_first_to_continue_caption), Toast.LENGTH_LONG).show();
                    Intent i = new Intent(getApplicationContext(), SignInActivity.class);
                    i.putExtra(Property.REDIRECT_KEY, RedirectKeys.MY_APPSSTORE.name());
                    startActivity(i);
                }
            } else {
                //user is not signed in - redirect to Sign In UI
                Toast.makeText(this, getString(R.string.login_first_to_continue_caption), Toast.LENGTH_LONG).show();
                Intent i = new Intent(getApplicationContext(), SignInActivity.class);
                i.putExtra(Property.REDIRECT_KEY, RedirectKeys.MY_APPSSTORE.name());
                startActivity(i);
            }
        } else if (selection == 3) {
            Bundle extras = getIntent().getExtras();

            if (extras != null) {
                if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
                    //user logged in -signing out
                    SignInActivity.session = new HashMap<String, String>();
                    Toast.makeText(this, getString(R.string.signinig_out_caption), Toast.LENGTH_LONG).show();
                    //redirecting to HOME UI
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                } else {
                    //user not logged in - redirect to Sign In UI
                    Intent i = new Intent(getApplicationContext(), SignInActivity.class);
                    i.putExtra(Property.REDIRECT_KEY, RedirectKeys.CATEGORIES_HOME.name());
                    startActivity(i);
//                    this.finish();
                }
            } else {
                //user not logged in - redirect to Sign In UI
                Intent i = new Intent(getApplicationContext(), SignInActivity.class);
                i.putExtra(Property.REDIRECT_KEY, RedirectKeys.CATEGORIES_HOME.name());
                startActivity(i);
//                this.finish();
            }
        } else if (selection == 4) {
            Intent i = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(i);
        } else if (selection == 5) {
            final MenuItem item1 = item;
            search = (EditText) item.getActionView();
            search.addTextChangedListener(filterTextWatcher);
            search.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            search.setOnEditorActionListener(new OnEditorActionListener() {


                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    Intent i = new Intent(getApplicationContext(), SearchActivity.class);
                    i.putExtra("SearchQuery", search.getText().toString());
                    startActivity(i);
                    if (item1.isActionViewExpanded()) {
                        item1.collapseActionView();
                    }
                    return false;
                }
            });

            search.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if (!b) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(search.getWindowToken(), 0);
                    }
                }
            });
        }
        return super.onOptionsItemSelected(item);
    }

    private TextWatcher filterTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //search logic here
            //search = (EditText)findViewById(R.id.txt_home_search);

            //Toast.makeText(MainActivity.this, "Searching for " + search.getText().toString(), Toast.LENGTH_SHORT).show();
        }

    };

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

    }
}