/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.activity.user;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.gsma.android.mobileconnect.authorization.Authorization;
import com.gsma.android.mobileconnect.authorization.AuthorizationListener;
import com.gsma.android.mobileconnect.authorization.AuthorizationOptions;
import com.gsma.android.mobileconnect.token.Token;
import com.gsma.android.mobileconnect.token.TokenData;
import com.gsma.android.mobileconnect.token.TokenListener;
import com.gsma.android.mobileconnect.values.Display;
import com.gsma.android.mobileconnect.values.Prompt;
import com.gsma.android.mobileconnect.values.ResponseType;
import com.gsma.android.oneapi.discovery.Api;
import com.gsma.android.oneapi.discovery.DiscoveryItem;
import com.gsma.android.oneapi.discovery.DiscoveryListener;
import com.gsma.android.oneapi.discovery.DiscoveryProvider;
import com.gsma.android.oneapi.valuesDiscovery.DiscoveryCredentials;

import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;

import hms.appstore.android.R;
import hms.appstore.android.activity.CustomToolBar;
import hms.appstore.android.activity.MainActivity;
import hms.appstore.android.activity.NoConnectionActivity;
import hms.appstore.android.activity.app.SettingsActivity;
import hms.appstore.android.fcm.DeviceTokenAsyncTask;
import hms.appstore.android.iap.proxy.listener.InAppListener;
import hms.appstore.android.iap.proxy.listener.InAppProxyService;
import hms.appstore.android.iap.proxy.util.InAppKeys;
import hms.appstore.android.rest.ConnectionRefusedException;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.rest.RestException;
import hms.appstore.android.rest.request.DeviceTokenRequest;
import hms.appstore.android.rest.request.MobileConnectLoginRequest;
import hms.appstore.android.rest.request.SignInRequest;
import hms.appstore.android.rest.response.AuthProviderResponse;
import hms.appstore.android.rest.response.AutoLoginResponse;
import hms.appstore.android.rest.response.Response;
import hms.appstore.android.rest.response.SignInResponse;
import hms.appstore.android.util.ErrorCodeMapping;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.LoginKeys;
import hms.appstore.android.util.ProfileProperty;
import hms.appstore.android.util.Property;
import hms.appstore.android.util.RedirectKeys;
import hms.appstore.android.util.SessionKeys;
import hms.appstore.android.util.Tags;
import hms.appstore.android.util.manager.user.LoggedUser;
import hms.appstore.android.util.widget.LocalizedCheckbox;

import static hms.appstore.android.util.SessionKeys.SESSION_ID;

public class SignInActivity extends CustomToolBar implements DiscoveryListener, AuthorizationListener {

    public static Map<String, String> session = new HashMap<String, String>();
    EditText txtUserName, txtPassword;
    TextView forgetPasswordLink, registerLink;
    LinearLayout notificationArea;
    LocalizedCheckbox keepMeLoggedInCheckBox;
    Button signInButton;

    SharedPreferences sharedPerference;
    String autoLoginUrl = Property.AUTO_LOGIN_URL;
    String authProviderUrl = Property.AUTH_PROVIDER_URL;
    String mobileConnectLoginUrl = Property.MOBILE_CONNECT_TOKEN_LOGIN_URL;
    String url = Property.SIGN_IN_URL;

    SignIn signInAsyncTask;
    SignInAuthToken signInAuthTokenAsyncTask;
    GetAutoLogin getAutoLoginAsyncTask = new GetAutoLogin();
    GetAuthProvider getAuthProviderAsyncTask = new GetAuthProvider();
    static final String LOG_TAG = SignInActivity.class.getCanonicalName();

    private DiscoveryItem discoveryItem;
    private ProgressDialog mobileConnectDialog;
    private LinearLayout mobileConnectLoadingLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPerference = getSharedPreferences(Property.PREFS_NAME, 0);

        if (getResources().getBoolean(R.bool.mobile_connect_login)) {
            setContentView(R.layout.mobile_connect_login);
            initCustomToolBar();
            getSupportActionBar().setTitle(R.string.menu_item_sign_in_caption);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mobileConnectLoadingLayout = (LinearLayout) findViewById(R.id.mobile_connect_loading);
            mobileConnectDialog = new ProgressDialog(SignInActivity.this);
            mobileConnectDialog.setMessage(getString(R.string.mobile_connect_sighing_in));
            mobileConnectDialog.setIndeterminate(true);
            mobileConnectDialog.setCancelable(true);
            mobileConnectDialog.show();

            mobileOperatorHandler();
            HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
            SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
            socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
            HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);

        } else {
            setContentView(R.layout.sign_in);
            initCustomToolBar();
            getSupportActionBar().setTitle(R.string.menu_item_sign_in_caption);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (getResources().getBoolean(R.bool.auto_login_enabled)) {
                getAuthProviderAsyncTask.execute();

                String sessionsIdByAutoLogin = session.get(SESSION_ID);
                if (sessionsIdByAutoLogin != null && !sessionsIdByAutoLogin.equals("")) {
                    /**For in-app-purchase signIn popup send sessionId*/
                    if (getIntent().hasExtra(InAppKeys.INAPP_SIGNIN)) {
                        if (InAppListener.isServiceAlive()) {
                            InAppProxyService.inAppProcessor.onSessionIdAvailable(sessionsIdByAutoLogin);
                        }
                        finish();
                        return;
                    }
                }
            }

            initViewItems();
            initForgotPasswordLink();
            initRegisterLink();

            if (getIntent().hasExtra(Property.NOTIFICATION_EXTRA)) {
                notificationArea.setVisibility(View.VISIBLE);
                TextView notificationAreaTxt = (TextView) findViewById(R.id.notificationAreaTxt);
                notificationAreaTxt.setText(getIntent().getStringExtra(Property.NOTIFICATION_EXTRA));
            }

            /**For in-app-purchase sign-in popup- no links needed*/
            if (getIntent().hasExtra(InAppKeys.INAPP_SIGNIN)) {
                forgetPasswordLink.setVisibility(View.GONE);
                keepMeLoggedInCheckBox.setVisibility(View.GONE);
                registerLink.setVisibility(View.GONE);
            }

        }
    }

    private void initViewItems() {
        forgetPasswordLink = (TextView) findViewById(R.id.lnkForgetPassword);
        registerLink = (TextView) findViewById(R.id.lnkRegister);
        notificationArea = (LinearLayout) findViewById(R.id.notificationArea);
        keepMeLoggedInCheckBox = (LocalizedCheckbox) findViewById(R.id.keepMeLoggedCheckbox);

        txtUserName = (EditText) findViewById(R.id.txtUserName);
        txtUserName.setText(sharedPerference.getString(Property.PREFS_USERNAME, ""));
        txtUserName.requestLayout();
        txtPassword = (EditText) findViewById(R.id.txtsigninpassword);
        txtPassword.setTypeface(Typeface.DEFAULT);
        if(!txtUserName.getText().toString().trim().equals("")) {
            txtPassword.requestFocus();
        }

        signInButton = (Button) findViewById(R.id.btnSignIn);
        signInButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                validateInputs();
                InputMethodManager inputManager = (InputMethodManager)
                        SignInActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(SignInActivity.this.getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
            }
        });
    }

    private void initForgotPasswordLink(){
        forgetPasswordLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getResources().getBoolean(R.bool.user_account_recovery_enabled)) {
                    Intent intent = new Intent(getApplicationContext(), ForgotPasswordActivity.class);
                    startActivity(intent);
                } else {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    Uri u = Uri.parse(ProfileProperty.FORGOT_PASSWORD_LINK);
                    i.setData(u);
                    startActivity(i);
                }
            }
        });
    }

    private void initRegisterLink(){
        registerLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getResources().getBoolean(R.bool.user_registration_enabled)) {
                    Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
                    startActivity(intent);
                } else {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    Uri u = Uri.parse(ProfileProperty.REGISTRATION_LINK);
                    i.setData(u);
                    startActivity(i);
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (signInAsyncTask != null && !signInAsyncTask.isCancelled()) {
            signInAsyncTask.cancel(true);
        }
    }

    public void onNotificationDismiss(View view) {
        if(notificationArea.getVisibility() == View.VISIBLE) {
            Animation out = AnimationUtils.loadAnimation(this, R.anim.slide_out_up);
            out.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    notificationArea.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            notificationArea.startAnimation(out);
        }
    }

    private void validateInputs() {
        //flag to check whether there are any errors
        boolean noErrors = true;

        if (txtUserName.getText().toString().trim().length() == 0) {
            txtUserName.setError(getString(R.string.user_name_is_required_error_message));
            noErrors = false;
        } else {
            txtUserName.setError(null);
        }

        if (txtPassword.getText().toString().trim().length() == 0) {
            txtPassword.setError(getString(R.string.password_requires_error_message));
            noErrors = false;
        } else {
            txtUserName.setError(null);
        }

        if (noErrors) {
            String[] params = new String[2];
            params[0] = txtUserName.getText().toString().trim();
            params[1] = txtPassword.getText().toString().trim();

            signInAsyncTask = new SignIn();
            signInAsyncTask.execute(params);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(getIntent().hasExtra(InAppKeys.INAPP_SIGNIN)){
            if (InAppListener.isServiceAlive()) {
                InAppProxyService.inAppProcessor.onSessionIdAvailable(null);
            }
        }

        if (getIntent().hasExtra(Property.REDIRECT_KEY)
                && getIntent().getStringExtra(Property.REDIRECT_KEY).equals(RedirectKeys.DESCRIPTION.name())) {
            String redirect = getIntent().getStringExtra(Property.REDIRECT_KEY);
            if (Property.REDIRECT_MAP.containsKey(redirect)) {
                Intent i = new Intent(getApplicationContext(), Property.REDIRECT_MAP.get(redirect));
                if (getIntent().hasExtra(Tags.TAG_APP_ID.name())) {
                    i.putExtra(Tags.TAG_APP_ID.name(), getIntent().getStringExtra(Tags.TAG_APP_ID.name()));
                }
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
            finish();
        } else {
            super.onBackPressed();
        }
    }

    /**
     * For in-app-purchase signIn popup, finish if calling app unbound
     */
    @Override
    public void onResume() {
        super.onResume();
        if (getIntent().hasExtra(InAppKeys.INAPP_SIGNIN)) {
            if (!InAppListener.isServiceAlive()) {
                Log.d("SignInActivity", "Client Activity closed, Stopping InApp Activity");
                finish();
            }
        }
    }

    /**
     *  Select Mobile Operator
     */
    private void mobileOperatorHandler() {
        String mcc = null;
        String mnc = null;
        TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        String simOperator = telephonyManager.getSimOperator();

        if(simOperator.length()>3){
            Pattern pattern = Pattern.compile(Property.MOBILE_CONNECT_OPERATOR_REGEX);
            Matcher matcher = pattern.matcher(simOperator);
            if (matcher.matches()) {
                pattern.matcher(simOperator);
                mcc = matcher.group(1);
                mnc = matcher.group(2);
                if (!mobileOperatorMccMncValidator(mcc, mnc)) {
                    Toast.makeText(getApplicationContext(), "This Operator Not Supported", Toast.LENGTH_LONG).show();
                    mobileConnectDialog.dismiss();
                } else {
                    mcc = ProfileProperty.MOBILE_CONNECT_DEFAULT_MCC;
                    mnc = ProfileProperty.MOBILE_CONNECT_DEFAULT_MNC;
                    mobileConnectDiscoveryLogin(mcc, mnc);
                }
            } else {
                mobileConnectDialog.dismiss();
            }
        }else { // when sim not available
            mcc = ProfileProperty.MOBILE_CONNECT_DEFAULT_MCC;
            mnc = ProfileProperty.MOBILE_CONNECT_DEFAULT_MNC;
            mobileConnectDiscoveryLogin(mcc, mnc);
        }


    }

    /**
     * Select Operator for Mobile-connect login
     *
     * @param mcc
     * @param mnc
     * @return
     */
    private boolean mobileOperatorMccMncValidator(String mcc, String mnc) {
        if (mcc.equals(ProfileProperty.MOBILE_CONNECT_DEFAULT_MCC) && mnc.equals(ProfileProperty.MOBILE_CONNECT_DEFAULT_MNC)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Start Active Discovery
     *
     * @param mcc
     * @param mnc
     */
    private void mobileConnectDiscoveryLogin(String mcc, String mnc) {
        DiscoveryProvider discoveryProvider = new DiscoveryProvider();
        discoveryProvider.clearCacheDiscoveryItem(SignInActivity.this);
        discoveryProvider.getDiscoveryActive(ProfileProperty.MOBILE_CONNECT_DISCOVERY_URL, ProfileProperty.MOBILE_CONNECT_CLIENT_KEY,
                ProfileProperty.MOBILE_CONNECT_CLIENT_SECRET, null, mcc, mnc, null, SignInActivity.this, SignInActivity.this, DiscoveryCredentials.PLAIN,
                ProfileProperty.MOBILE_CONNECT_REDIRECT_URL);
    }

    @Override
    public void discoveryInfo(DiscoveryItem discoveryItem) { // Mobile-connect discovery Complete
        if (discoveryItem.getResponse() != null) {
            this.discoveryItem = discoveryItem;
            if (mobileConnectDialog != null) {
                mobileConnectDialog.dismiss();
                mobileConnectLoadingLayout.setVisibility(View.VISIBLE);
            }
            authorization(discoveryItem);
        }
    }

    @Override
    public void errorDiscoveryInfo(JSONObject jsonObject) { // Mobile-connect discovery Failed
        Log.e("SignInActivity", "Error Discovery Response " + jsonObject);
    }

    @Override
    public void authorizationCodeResponse(String state, String authorizationCode, String error, String clientId, String clientSecret, String scopes, String returnUri) {
        Api api = discoveryItem.getResponse().getApi(Property.MOBILE_CONNECT_OPERATOR_ID_PREFIX);
        String tokenUri = api.getHref(Property.MOBILE_CONNECT_TOKEN_URL_ID);
        generateToken(tokenUri, clientId, clientSecret, returnUri, authorizationCode);
    }

    @Override
    public void authorizationError(String s) { // mobile-connect authorization failed
        Log.e("SignInActivity", "Error authorization Response" + s);
    }

    /**
     *  Generate Mobile-connect Token from code
     * @param tokenUri
     * @param clientId
     * @param clientSecret
     * @param redirectUri
     * @param code
     */
    private void generateToken(final String tokenUri, String clientId, String clientSecret, String redirectUri, String code) {
        Token token = new Token();
        token.tokenFromAuthorizationCode(tokenUri, code, clientId, clientSecret, redirectUri, new TokenListener() {
            @Override
            public void tokenResponse(TokenData tokenData) {

                if (tokenData != null) {
                    String[] token = new String[1];
                    token[0] = tokenData.getAccess_Token();
                    signInAuthTokenAsyncTask = new SignInAuthToken();
                    signInAuthTokenAsyncTask.execute(token);
                    mobileConnectLoadingLayout.setVisibility(View.GONE);
                } else {
                    if (mobileConnectDialog != null) {
                        mobileConnectDialog.dismiss();
                    }
                }
            }

            @Override
            public void errorToken(JSONObject jsonObject) {
                Log.e("SignInActivity", "Error Token Response" + jsonObject);
            }
        });

    }

    /**
     *  Start mobile-connect authorization
     * @param discoveryItem
     */
    private void authorization(DiscoveryItem discoveryItem) {

        Api operatorIdEndpoint = discoveryItem.getResponse() != null ? discoveryItem.getResponse().getApi(Property.MOBILE_CONNECT_OPERATOR_ID_PREFIX) : null;
        String authUri = operatorIdEndpoint.getHref(Property.MOBILE_CONNECT_AUTHORIZATION_PREFIX);
        String clientId = discoveryItem.getResponse().getClient_id();
        String clientSecret = discoveryItem.getResponse().getClient_secret();
        String state = UUID.randomUUID().toString();
        String nonce = UUID.randomUUID().toString();

        AuthorizationOptions authorizationOptions = new AuthorizationOptions();
        authorizationOptions.setClaimsLocales(ProfileProperty.MOBILE_CONNECT_CLAIMS_LOCALES);
        authorizationOptions.setUILocales(ProfileProperty.MOBILE_CONNECT_UI_LOCALES);
        authorizationOptions.setDisplay(Display.PAGE);

        TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        String msisdn = telephonyManager.getLine1Number();
        if (msisdn != null) {
            authorizationOptions.setLoginHint(msisdn);
        }

        Prompt prompt = Prompt.LOGIN;

        try {
            Authorization authorization = new Authorization();
            authorization.authorize(authUri, ResponseType.CODE, clientId, clientSecret, ProfileProperty.MOBILE_CONNECT_OPENID_SCOPES, ProfileProperty.MOBILE_CONNECT_REDIRECT_URL, state, nonce, prompt,
                    Property.MOBILE_CONNECT_AUTHENTICATION_MAX_AGE, Property.MOBILE_CONNECT_AUTHENTICATION_ACR_VALUE, authorizationOptions, SignInActivity.this, SignInActivity.this);
        } catch (NullPointerException ex) {
            Log.e("SignActivity", "Error Occurred while Authorization" + ex);
        }

    }

    public class SignIn extends AsyncTask<String, Context, SignInResponse> {

        ProgressDialog dialog = new ProgressDialog(SignInActivity.this);

        public SignIn() {}

        @Override
        protected void onPreExecute() {
            dialog.setMessage(getString(R.string.please_wait_caption));
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.show();
        }

        protected void showDialog() {}

        @Override
        protected SignInResponse doInBackground(String... params) {
            String userName = params[0];
            String password = params[1];

            SignInRequest signInRequest = new SignInRequest(userName, password);

            SignInResponse signInResponse = null;
            String json = "";
            try {
                json = new RestClient().post(url, signInRequest);
                signInResponse = new Gson().fromJson(json, SignInResponse.class);
            } catch (RestException e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            } catch (ConnectionRefusedException e) {
                Intent i = new Intent(getApplicationContext(), NoConnectionActivity.class);
                startActivity(i);
                finish();
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }
            Log.d("JSON", "JASON object recieved in Async Task\n" + json);

            return signInResponse;
        }

        @Override
        protected void onPostExecute(SignInResponse result) {
            super.onPostExecute(result);

            dialog.dismiss();

            if (result != null) {
                if (ErrorCodeMapping.isSuccess(result.getStatusCode()) && result.isMsisdnVerified()) {

                    /** Setting Session Variables */
                    String sessionId = result.getSessionId();
                    String statusCode = result.getStatusCode();

                    if (result.getMsisdn() != null) {
                        session.put(SessionKeys.MOBILE_NUMBER.name(), result.getMsisdn());
                    }
                    session.put(SessionKeys.USERNAME.name(), txtUserName.getText().toString());
                    session.put(SessionKeys.PASSWORD.name(), txtPassword.getText().toString());
                    session.put(SessionKeys.SESSION_ID.name(), sessionId);
                    session.put(SessionKeys.LOGIN_STATUS_CODE.name(), statusCode);
                    session.put(SessionKeys.LOGIN_METHOD.name(), LoginKeys.SIGN_IN.name());

//                    storeSessionId(sessionId);
                    /** For in-app-purchase signIn popup send sessionId */
                    if(getIntent().hasExtra(InAppKeys.INAPP_SIGNIN)){
                        if (InAppListener.isServiceAlive()) {
                            InAppProxyService.inAppProcessor.onSessionIdAvailable(result.getSessionId());
                        }
                        finish();
                        return;
                    }
                    /** storing the successfully logged user in shared preferences */
                    storeLoggedUser(result.getSessionId(), txtUserName.getText().toString().trim());
                    sendDeviceToken();
                    if (getIntent().hasExtra(Property.REDIRECT_KEY)) {
                        loginUserRedirect();
                        showMessage(result.getStatusDescription(), SignInActivity.this);
                        finish();
                    }

                } else if(Property.INACTIVE_USER_STATUS.equals(result.getStatusCode()) || !result.isMsisdnVerified()) {
                    if(getResources().getBoolean(R.bool.account_activation_via_msisdn_enabled)){
                        Intent intent = new Intent(getApplicationContext(), AccountVerificationActivity.class);
                        intent.putExtra(Property.MOBILE_NO_EXTRA, result.getMsisdn());
                        intent.putExtra(Property.USERNAME_EXTRA, txtUserName.getText().toString().trim());
                        startActivity(intent);
                    }
                } else if(Property.INITIAL_TEMP_USER_LOGIN_STATUS.equals(result.getStatusCode()) ||
                        Property.CHANGED_PASSWORD_TEMP_USER_LOGIN_STATUS.equals(result.getStatusCode())) {
                    Intent intent = new Intent(getApplicationContext(), ChangePasswordActivity.class);
                    intent.putExtra(Property.USERNAME_EXTRA, txtUserName.getText().toString().trim());
                    startActivity(intent);
                } else {
                    showMessage(result.getStatusDescription(), SignInActivity.this);
                    txtPassword.setText(null);
                }
            } else {
                session = new HashMap<String, String>();
                showMessage(getString(R.string.E5000), SignInActivity.this);
                txtPassword.setText(null);
            }
        }


    }

//    private void storeSessionId(String sessionId){
//        SharedPreferences.Editor editor = sharedPerference.edit();
//        editor.putString(Property.PREFS_SESSION_ID, sessionId);
//        editor.commit();
//        System.out.println("SESSION ID stored in PREF");
//    }

    private void sendDeviceToken() {
        DeviceTokenAsyncTask deviceTokenAsyncTask = new DeviceTokenAsyncTask(sharedPerference);
        deviceTokenAsyncTask.execute();
    }

    /**
     * show error state in the toast message
     */
    protected void showMessage(String results, Context context) {
        if (results != null) {
            Toast aToast = Toast.makeText(context, results, Toast.LENGTH_SHORT);
            hms.appstore.android.util.ToastExpander.showFor(aToast, 5000);
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.E5000), Toast.LENGTH_SHORT).show();
        }

    }

    private void storeLoggedUser(String sessionId, String username){
        SharedPreferences.Editor editor = sharedPerference.edit();
        editor.putString(Property.PREFS_USERNAME, username);
        if(keepMeLoggedInCheckBox != null && keepMeLoggedInCheckBox.isChecked()) {
            LoggedUser loggedUser =
                    new LoggedUser(sessionId, LoginKeys.SIGN_IN, System.currentTimeMillis());
            editor.putString(Property.PREFS_LOGGED_USER, loggedUser.toJsonString());
        }
        editor.commit();
    }

    public class SignInAuthToken extends AsyncTask<String, Context, SignInResponse> {

        //ProgressDialog dialog = new ProgressDialog(SignInActivity.this);
        AlertDialog dialog = new AlertDialog.Builder(SignInActivity.this).create();

        @Override
        protected void onPreExecute() {
            dialog.setMessage(getString(R.string.please_wait_caption));
            //dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.show();
        }

        @Override
        protected SignInResponse doInBackground(String... params) {
            String token = params[0];
            MobileConnectLoginRequest mobileConnectLoginRequest = new MobileConnectLoginRequest(token);
            SignInResponse signInResponse = null;
            String json = "";
            try {
                json = new RestClient().post(mobileConnectLoginUrl, mobileConnectLoginRequest);
                signInResponse = new Gson().fromJson(json, SignInResponse.class);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (ConnectionRefusedException e) {
                Intent i = new Intent(getApplicationContext(), NoConnectionActivity.class);
                startActivity(i);
                finish();
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }
            Log.d("JSON","JASON object recieved in Async Task\n" + json);

            if (signInResponse != null && ErrorCodeMapping.isSuccess(signInResponse.getStatusCode())) {
                String sessionId = signInResponse.getSessionId();
                String statusCode = signInResponse.getStatusCode();
                String mobileNumber = signInResponse.getMsisdn();

                if (signInResponse.getMsisdn() != null) {
                    session.put(SessionKeys.MOBILE_NUMBER.name(), signInResponse.getMsisdn());
                }
                session.put(SessionKeys.USERNAME.name(), mobileNumber);
                session.put(SessionKeys.SESSION_ID.name(), sessionId);
                session.put(SessionKeys.LOGIN_STATUS_CODE.name(), statusCode);
                session.put(SessionKeys.LOGIN_METHOD.name(), LoginKeys.SIGN_IN.name());
                session.put(SessionKeys.ACCESS_TOKEN.name(), token);

//                storeSessionId(sessionId);
            } else {
                session = new HashMap<String, String>();
            }

            return signInResponse;
        }

        @Override
        protected void onPostExecute(SignInResponse signInResponse) {

            dialog.dismiss();

            if(signInResponse!=null){
                if (ErrorCodeMapping.isSuccess(signInResponse.getStatusCode())) {
                    /** For in-app-purchase signIn popup send sessionId */
                    if(getIntent().hasExtra(InAppKeys.INAPP_SIGNIN)){
                        if (InAppListener.isServiceAlive()) {
                            InAppProxyService.inAppProcessor.onSessionIdAvailable(signInResponse.getSessionId());
                        }
                        finish();
                        return;
                    }

                    /** storing the successfully logged user in shared preferences */
                    storeLoggedUser(signInResponse.getSessionId(), signInResponse.getMsisdn());
                    if (getIntent().hasExtra(Property.REDIRECT_KEY)) {
                        loginUserRedirect();
                        showMessage(signInResponse.getStatusDescription(), SignInActivity.this);
                        finish();
                    }
                }else {
                    showMessage(signInResponse.getStatusDescription(), SignInActivity.this);
                    Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(intent);
                }
            }

        }
    }

    private void loginUserRedirect(){
        String redirect = getIntent().getStringExtra(Property.REDIRECT_KEY);
        if (Property.REDIRECT_MAP.containsKey(redirect)) {
            Intent i = new Intent(getApplicationContext(), Property.REDIRECT_MAP.get(redirect));
            if (getIntent().hasExtra(Tags.TAG_APP_ID.name())) {
                i.putExtra(Tags.TAG_APP_ID.name(), getIntent().getStringExtra(Tags.TAG_APP_ID.name()));
            }
            if (getIntent().hasExtra("isLaunchAppActionHandler")) {
                i.putExtra("isLaunchAppActionHandler", getIntent().getBooleanExtra("isLaunchAppActionHandler", true));
            } else if (getIntent().hasExtra("isAddComment")) {
                i.putExtra("isAddComment", getIntent().getBooleanExtra("isAddComment", true));
            }

            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
    }


    public class GetAutoLogin extends AsyncTask<Void, Context, AutoLoginResponse> {

        @Override
        protected AutoLoginResponse doInBackground(Void... params) {

            AutoLoginResponse autoLoginResponse = null;
            String json;
            try {
                json = new RestClient().getHttp(autoLoginUrl);
                autoLoginResponse = new Gson().fromJson(json, AutoLoginResponse.class);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (ConnectionRefusedException e) {
                Intent i = new Intent(getApplicationContext(), NoConnectionActivity.class);
                startActivity(i);
                finish();
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }
            String sessionId;
            if (autoLoginResponse != null && ErrorCodeMapping.isSuccess(autoLoginResponse.getStatusCode())) {
                sessionId = autoLoginResponse.getSessionId();
                SignInActivity.session.put(SessionKeys.SESSION_ID.name(), sessionId);
                SignInActivity.session.put(SessionKeys.LOGIN_METHOD.name(), LoginKeys.AUTO_LOGIN.name());
                SignInActivity.session.put(SessionKeys.LOGIN_STATUS_CODE.name(), autoLoginResponse.getStatusCode());
//                storeSessionId(sessionId);
            }
            return autoLoginResponse;
        }


        @Override
        protected void onPostExecute(AutoLoginResponse autoLoginResponse) {

            if (autoLoginResponse != null && ErrorCodeMapping.isSuccess(autoLoginResponse.getStatusCode())) {
                String sessionsIdByAutoLogin = autoLoginResponse.getSessionId();
                /**For in-app-purchase signIn popup send sessionId*/
                sendDeviceToken();
                if (getIntent().hasExtra(InAppKeys.INAPP_SIGNIN)) {
                    if (InAppListener.isServiceAlive()) {
                        InAppProxyService.inAppProcessor.onSessionIdAvailable(sessionsIdByAutoLogin);
                    }
                    finish();
                    return;
                } else {
//                    Intent mainActivity = new Intent(getApplicationContext(), MainActivity.class);
//                    Intent intent = new Intent("finish_activity");
//                    sendBroadcast(intent);
//                    startActivity(mainActivity);
                    SignInActivity.this.finish();
                }
            }
        }
    }

    public class GetAuthProvider extends AsyncTask<Void, Context, AuthProviderResponse> {

        @Override
        protected AuthProviderResponse doInBackground(Void... params) {

            AuthProviderResponse authProviderResponse = null;
            String json;
            try {
                json = new RestClient().getHttp(authProviderUrl);
                authProviderResponse = new Gson().fromJson(json, AuthProviderResponse.class);
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }
            return authProviderResponse;
        }

        @Override
        protected void onPostExecute(AuthProviderResponse authProviderResponse) {

            if (authProviderResponse != null) {
                if (authProviderResponse.getPreferredProvider().equals(getString(R.string.auth_by_msisdn))) {
                    getAutoLoginAsyncTask.execute();
                }

            }

        }
    }

}
