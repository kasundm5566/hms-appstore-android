package hms.appstore.android.activity.user;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import hms.appstore.android.R;
import hms.appstore.android.activity.CustomToolBar;
import hms.appstore.android.rest.ConnectionRefusedException;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.rest.RestException;
import hms.appstore.android.rest.request.AccountVerifyRequest;
import hms.appstore.android.rest.request.GetVerifyCodeRequest;
import hms.appstore.android.rest.response.Response;
import hms.appstore.android.util.ErrorCodeMapping;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.Property;
import hms.appstore.android.util.RedirectKeys;
import hms.appstore.android.util.widget.LocalizedTextView;

public class AccountVerificationActivity extends CustomToolBar {

    LinearLayout notificationAreaLayout;
    LinearLayout mobileNumberLayout;
    LocalizedTextView notificationAreaText;
    LocalizedTextView mobileNumberLocalizedTextView;
    LocalizedTextView verificationCodeEntryText;
    LocalizedTextView editMsisdnText;
    LinearLayout verificationCodeLayout;
    EditText mobileNoText;
    EditText countryCodeText;
    EditText verificationCodeText;
    TextView msisdnSeperatorText;
    String username;
    String vcodeRequestType;
    Button editMsisdnButton;

    Dialog editMsisdnDialog;
    EditText newMobileTxt;

    static final String LOG_TAG = AccountVerificationActivity.class.getCanonicalName();

    //Get Verification Code Request Types
    static final String VCODE_RT_CHANGE_MSISDN = "change-msisdn";
    static final String VCODE_RT_REQUEST_VCODE = "vcode-request";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_verification_v2);
        initCustomToolBar();

        mobileNumberLocalizedTextView = (LocalizedTextView) findViewById(R.id.mobileNumberLocalizedTextView);
        mobileNumberLayout = (LinearLayout) findViewById(R.id.mobileNumberLayout);
        msisdnSeperatorText = (TextView) findViewById(R.id.msisdnSeperatorText);
        countryCodeText = (EditText) findViewById(R.id.countryCodeText);
        verificationCodeEntryText = (LocalizedTextView) findViewById(R.id.verificationCodeEntryText);
        editMsisdnButton = (Button) findViewById(R.id.editMsisdnButton);
        editMsisdnText = (LocalizedTextView) findViewById(R.id.editMsisdnText);
        verificationCodeLayout = (LinearLayout) findViewById(R.id.verificationCodeLayout);
        notificationAreaLayout = (LinearLayout) findViewById(R.id.notificationAreaLayout);
        notificationAreaText = (LocalizedTextView) findViewById(R.id.notificationAreaTxt);
        mobileNoText = (EditText) findViewById(R.id.mobileNoText);
        verificationCodeText = (EditText) findViewById(R.id.verificationCodeTxt);
        username = getIntent().getStringExtra(Property.USERNAME_EXTRA);

        viewHandler(getIntent().getStringExtra(Property.MOBILE_NO_EXTRA));
    }

    private void viewHandler(String mobileNo) {
        if(mobileNo != ""){
            if(mobileNo.startsWith(getResources().getString(R.string.country_mobile_code))){
                notificationAreaText.setText(R.string.account_verify_not_verified_operator_number);
                String trimmedNumber = mobileNo.substring((getResources().getString(R.string.country_mobile_code).length()), mobileNo.length());
                mobileNoText.setText(trimmedNumber);
            } else {
                notificationAreaText.setText(String.format(getString(R.string.account_verify_not_verified_non_operator_number), getString(R.string.operator_name)));
                mobileNoText.setText(mobileNo);
                editMsisdnText.setText(R.string.account_verify_msisdn_edit_text_view_text);
                countryCodeText.setVisibility(View.GONE);
                msisdnSeperatorText.setVisibility(View.GONE);
                verificationCodeText.setVisibility(View.GONE);
                verificationCodeEntryText.setVisibility(View.GONE);
                verificationCodeLayout.setVisibility(View.GONE);
            }

        } else {
            mobileNoText.setText(mobileNo);
            notificationAreaText.setText(String.format(getString(R.string.account_verify_not_verified_no_number), getString(R.string.operator_name)));
            verificationCodeLayout.setVisibility(View.GONE);
            mobileNumberLayout.setVisibility(View.GONE);
            verificationCodeText.setVisibility(View.GONE);
            verificationCodeEntryText.setVisibility(View.GONE);
            mobileNumberLocalizedTextView.setVisibility(View.GONE);
            editMsisdnText.setText(R.string.account_verify_msisdn_add_text_view_text);
            editMsisdnButton.setText(R.string.account_verify_msisdn_add_btn_text);
        }

    }

    public void onActivateAccountBtnClick(View view) {
        if (isValidParams()) {
            String[] requestParams = new String[2];
            requestParams[0] = username;
            requestParams[1] = verificationCodeText.getText().toString().trim();

            AccountVerifyAsyncTask accountVerifyAsyncTask = new AccountVerifyAsyncTask();
            accountVerifyAsyncTask.execute(requestParams);
        }
    }

    public void onVCodeRequestBtnClick(View view) {
        getRequestVCodeConfirmationDialog();
    }

    public void onEditMsisdnBtnClick(View view) {
        getEditMsisdnDialog();
    }

    public void getEditMsisdnDialog() {
        editMsisdnDialog = new Dialog(this, R.style.AppDefaultDialogThemeWithNoTitle);
        editMsisdnDialog.setContentView(R.layout.change_msisdn_dialog);

        newMobileTxt = (EditText) editMsisdnDialog.findViewById(R.id.txtNewMobileNo);

        Button btnNegative = (Button) editMsisdnDialog.findViewById(R.id.btnNegative);
        btnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { editMsisdnDialog.dismiss(); }
        });

        Button btnPositive = (Button) editMsisdnDialog.findViewById(R.id.btnPositive);
        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(newMobileTxt.getText().toString().length() == getResources().getInteger(R.integer.register_screen_mobile_num_length)) {
                    String[] requestParams = new String[3];
                    requestParams[0] = getResources().getString(R.string.country_mobile_code) +
                            newMobileTxt.getText().toString();
                    requestParams[1] = username;
                    requestParams[2] = VCODE_RT_CHANGE_MSISDN;

                    RequestVerifyCodeAsyncTask requestVerifyCodeAsyncTask = new RequestVerifyCodeAsyncTask();
                    requestVerifyCodeAsyncTask.execute(requestParams);
                    verificationCodeLayout.setVisibility(View.VISIBLE);
                    mobileNumberLayout.setVisibility(View.VISIBLE);
                    editMsisdnText.setText(R.string.account_verify_msisdn_edit_text_view_text);
                    editMsisdnButton.setText(R.string.account_verify_msisdn_edit_btn_text);
                    msisdnSeperatorText.setVisibility(View.VISIBLE);
                    countryCodeText.setVisibility(View.VISIBLE);
                    verificationCodeText.setVisibility(View.VISIBLE);
                    mobileNumberLocalizedTextView.setVisibility(View.VISIBLE);
                    verificationCodeEntryText.setVisibility(View.VISIBLE);
                    editMsisdnDialog.dismiss();
                } else {
                    Toast.makeText(getApplicationContext(),
                            getString(R.string.account_verify_change_msisdn_dialog_error_invalid_format),
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        editMsisdnDialog.show();
    }

    public void getRequestVCodeConfirmationDialog() {
        final Dialog dialog = new Dialog(this, R.style.AppDefaultDialogThemeWithNoTitle);
        dialog.setContentView(R.layout.verification_code_request_confirmation);

        Button btnNegative = (Button) dialog.findViewById(R.id.btnNegative);
        btnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { dialog.dismiss(); }
        });

        Button btnPositive = (Button) dialog.findViewById(R.id.btnPositive);
        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] requestParams = new String[3];
                requestParams[0] = getResources().getString(R.string.country_mobile_code) +
                        mobileNoText.getText().toString();
                requestParams[1] = username;
                requestParams[2] = VCODE_RT_REQUEST_VCODE;

                RequestVerifyCodeAsyncTask requestVerifyCodeAsyncTask = new RequestVerifyCodeAsyncTask();
                requestVerifyCodeAsyncTask.execute(requestParams);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void onNotificationDismiss(View view) {
        if (notificationAreaLayout.getVisibility() == View.VISIBLE) {
            Animation out = AnimationUtils.loadAnimation(this, R.anim.slide_out_up);
            out.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    notificationAreaLayout.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            });
            notificationAreaLayout.startAnimation(out);
        }
    }

    private boolean isValidParams() {
        if (verificationCodeText.getText().toString().trim().length() == 0) {
            verificationCodeText.setError(getString(R.string.account_verify_vcode_error_empty));
            return false;
        } else return true;
    }

    public class AccountVerifyAsyncTask extends AsyncTask<String, Context, Response> {

        ProgressDialog dialog = new ProgressDialog(AccountVerificationActivity.this);

        @Override
        protected void onPreExecute() {
            dialog.setMessage(getString(R.string.account_verify_request_processing));
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.show();
        }

        @Override
        protected Response doInBackground(String... params) {
            String username = params[0];
            String verificationCode = params[1];

            AccountVerifyRequest accountVerifyRequest = new AccountVerifyRequest(username, verificationCode);
            Response accountVerifyResponse = null;
            String accountVerifyRespJson = "";

            try {
                accountVerifyRespJson = new RestClient().post(Property.ACCOUNT_VERIFY_URL, accountVerifyRequest);
                accountVerifyResponse = new Gson().fromJson(accountVerifyRespJson, Response.class);
            } catch (RestException e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            } catch (ConnectionRefusedException e) {
                Log.d(LOG_TAG, "ConnectionRefusedException", e);
            } catch (Exception e) {
                Log.d(LOG_TAG, "Exception", e);
            }
            Log.d("JSON", "JASON object recieved in Async Task\n" + accountVerifyRespJson);

            return accountVerifyResponse;
        }

        @Override
        protected void onPostExecute(Response result) {
            super.onPostExecute(result);
            dialog.dismiss();

            if (result != null) {
                if (ErrorCodeMapping.isSuccess(result.getStatusCode())) {
                    Intent intent = new Intent(getApplicationContext(), SignInActivity.class);
                    intent.putExtra(Property.NOTIFICATION_EXTRA, getString(R.string.account_verify_success_notification));
                    intent.putExtra(Property.REDIRECT_KEY, RedirectKeys.HOME.name());
                    startActivity(intent);
                } else {
                    showMessage(result.getStatusDescription(), AccountVerificationActivity.this);
                }
            } else {
                showMessage(getString(R.string.E5000), AccountVerificationActivity.this);
            }
        }

        protected void showMessage(String results, Context context) {
            if (results != null) {
                Toast aToast = Toast.makeText(context, results, Toast.LENGTH_SHORT);
                hms.appstore.android.util.ToastExpander.showFor(aToast, 5000);
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.E5000), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class RequestVerifyCodeAsyncTask extends AsyncTask<String, Context, Response> {

        ProgressDialog dialog = new ProgressDialog(AccountVerificationActivity.this);

        @Override
        protected void onPreExecute() {
            dialog.setMessage(getString(R.string.account_verify_general_request_processing));
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.show();
        }

        @Override
        protected Response doInBackground(String... params) {
            String msisdn = params[0];
            String username = params[1];

            GetVerifyCodeRequest getVerifyCodeRequest = new GetVerifyCodeRequest(msisdn, username);
            Response getVerifyCodeResponse = null;
            String getVerifyCodeRespJson = "";

            try {
                vcodeRequestType = params[2];
                getVerifyCodeRespJson = new RestClient().post(Property.REQUEST_VERIFY_CODE_URL, getVerifyCodeRequest);
                getVerifyCodeResponse = new Gson().fromJson(getVerifyCodeRespJson, Response.class);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (ConnectionRefusedException e) {
                Log.d(LOG_TAG, "ConnectionRefusedException ", e);
            } catch (Exception e) {
                Log.d(LOG_TAG, "Exception ", e);
            }
            Log.d("JSON", "JASON object recieved in Async Task\n" + getVerifyCodeRespJson);

            return getVerifyCodeResponse;
        }

        @Override
        protected void onPostExecute(Response result) {
            super.onPostExecute(result);
            dialog.dismiss();

            if (result != null) {
                if (ErrorCodeMapping.isSuccess(result.getStatusCode())) {
                    notificationAreaLayout.setVisibility(View.VISIBLE);
                    if (vcodeRequestType.equals(VCODE_RT_CHANGE_MSISDN)) {
                        mobileNoText.setText(newMobileTxt.getText().toString().trim());
                        editMsisdnDialog.dismiss();
                        notificationAreaText.setText(getString(R.string.account_verify_change_msisdn_notification));
                    } else {
                        notificationAreaText.setText(getString(R.string.account_verify_request_vcode_notification));
                    }
                } else {
                    showMessage(result.getStatusDescription(), AccountVerificationActivity.this);
                }
            } else {
                showMessage(getString(R.string.E5000), AccountVerificationActivity.this);
            }
        }

        protected void showMessage(String results, Context context) {
            if (results != null) {
                Toast aToast = Toast.makeText(context, results, Toast.LENGTH_SHORT);
                hms.appstore.android.util.ToastExpander.showFor(aToast, 5000);
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.E5000), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
