package hms.appstore.android.activity.user;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;

import hms.appstore.android.R;
import hms.appstore.android.activity.CustomToolBar;
import hms.appstore.android.rest.ConnectionRefusedException;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.rest.RestException;
import hms.appstore.android.rest.request.AccountRecoveryRequest;
import hms.appstore.android.rest.response.Response;
import hms.appstore.android.util.ErrorCodeMapping;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.Property;
import hms.appstore.android.util.RedirectKeys;

public class ForgotPasswordActivity extends CustomToolBar {

    LinearLayout notificationArea;
    EditText recoverText;
    static final String LOG_TAG = ForgotPasswordActivity.class.getCanonicalName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_v2);
        initCustomToolBar();

        notificationArea = (LinearLayout) findViewById(R.id.notificationArea);
        recoverText = (EditText) findViewById(R.id.recoverText);
    }

    public void onCancelButtonClick(View view) {
        super.finish();
    }

    public void onForgotPasswordSubmitBtnClick(View view) {
        if(isValidParams()) {
            AccountRecoveryAsyncTask accountRecoveryAsyncTask = new AccountRecoveryAsyncTask();
            accountRecoveryAsyncTask.execute(recoverText.getText().toString().trim());
        }
    }

    public void onNotificationDismiss(View view) {
        if(notificationArea.getVisibility() == View.VISIBLE) {
            Animation out = AnimationUtils.loadAnimation(this, R.anim.slide_out_up);
            out.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {}

                @Override
                public void onAnimationEnd(Animation animation) {
                    notificationArea.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {}
            });
            notificationArea.startAnimation(out);
        }
    }

    private boolean isValidParams() {
        if (recoverText.getText().toString().trim().length() == 0) {
            recoverText.setError(getString(R.string.forgot_pw_recover_text_error_empty));
            return false;
        } else return true;
    }

    public class AccountRecoveryAsyncTask extends AsyncTask<String, Context, Response> {

        ProgressDialog dialog = new ProgressDialog(ForgotPasswordActivity.this);

        @Override
        protected void onPreExecute() {
            dialog.setMessage(getString(R.string.forgot_pw_request_processing));
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.show();
        }

        @Override
        protected Response doInBackground(String... params) {
            String recoverText = params[0];

            AccountRecoveryRequest accountRecoveryRequest = new AccountRecoveryRequest(recoverText);
            Response accountRecoveryResponse = null;
            String accountRecoveryRespJson = "";

            try {
                accountRecoveryRespJson = new RestClient().post(Property.ACCOUNT_RECOVERY_URL, accountRecoveryRequest);
                accountRecoveryResponse = new Gson().fromJson(accountRecoveryRespJson, Response.class);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e.fillInStackTrace());
            } catch (ConnectionRefusedException e) {
                Log.d(LOG_TAG, "ConnectionRefusedException ", e.fillInStackTrace());
            } catch (Exception e) {
                Log.d(LOG_TAG, "Exception ", e.fillInStackTrace());
            }
            Log.d("JSON", "JASON object recieved in Async Task\n" + accountRecoveryRespJson);

            return accountRecoveryResponse;
        }

        @Override
        protected void onPostExecute(Response result) {
            super.onPostExecute(result);
            dialog.dismiss();

            if (result != null) {
                if (ErrorCodeMapping.isSuccess(result.getStatusCode())) {
                    Intent intent = new Intent(getApplicationContext(), SignInActivity.class);
                    intent.putExtra(Property.NOTIFICATION_EXTRA, getString(R.string.forgot_pw_success_msg));
                    intent.putExtra(Property.REDIRECT_KEY, RedirectKeys.HOME.name());
                    startActivity(intent);
                } else {
                    showMessage(result.getStatusDescription(), ForgotPasswordActivity.this);
                }
            } else {
                showMessage(getString(R.string.E5000), ForgotPasswordActivity.this);
            }
        }

        protected void showMessage(String results, Context context) {
            if (results != null) {
                Toast aToast = Toast.makeText(context, results, Toast.LENGTH_SHORT);
                hms.appstore.android.util.ToastExpander.showFor(aToast, 5000);
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.E5000), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
