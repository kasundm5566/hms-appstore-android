/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.activity.app;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;

import hms.appstore.android.R;
import hms.appstore.android.activity.CustomToolBar;
import hms.appstore.android.activity.NoConnectionActivity;
import hms.appstore.android.rest.ConnectionRefusedException;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.rest.RestException;
import hms.appstore.android.rest.response.Application;
import hms.appstore.android.rest.response.ApplicationListResponse;
import hms.appstore.android.util.ConnectionDetector;
import hms.appstore.android.util.ErrorCodeMapping;
import hms.appstore.android.util.HeaderGridView;
import hms.appstore.android.util.PaginGridAdapter;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.Property;

public class SearchActivity extends CustomToolBar {

    private String url = Property.APP_SEARCH_URL;
    private ArrayList<Application> featuredAppsList = new ArrayList<>();
    private SearchActivity sfa;
    private String searchQuery = "";
    private ConnectionDetector cd = null;
    private TextView emptySearchResultsTxt;
    private int offset = 0;
    private SearchAsyncTask searchAsyncTask;
    private boolean isLoading = false;
    private EditText etSearch;
    private HeaderGridView gridView;
    private PaginGridAdapter adapter = null;
    private TextView tvHomeAppName;
    private LinearLayout retryLayout, llFooterLoading, loadingLayout;
    private boolean loadNextPage = false;
    private static final String SEARCH_QUERY = "SEARCH_QUERY";

    static final String LOG_TAG = SearchActivity.class.getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sfa = SearchActivity.this;
        setContentView(R.layout.search_page);
        initCustomToolBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        emptySearchResultsTxt = (TextView) findViewById(R.id.list_empty);
        loadingLayout = (LinearLayout) findViewById(R.id.loading_layout);
        llFooterLoading = (LinearLayout) findViewById(R.id.llFooterLoading);
        retryLayout = (LinearLayout) findViewById(R.id.retry_layout);
        retryLayout.setVisibility(View.GONE);
        gridView = (HeaderGridView) findViewById(R.id.rvApps);

        adapter = new PaginGridAdapter(this, featuredAppsList);
        gridView.setAdapter(adapter);

        String uri = url;
        Bundle extras = getIntent().getExtras();
        searchAsyncTask = new SearchAsyncTask();

        if (extras != null && extras.containsKey("SearchQuery") && !extras.get("SearchQuery").equals("") && !extras.get("SearchQuery").equals(" ")) {
            searchQuery = extras.getString("SearchQuery");
            if(savedInstanceState != null){
                searchQuery = savedInstanceState.getString(SEARCH_QUERY);
            }
            searchAsyncTask.execute(uri);
        }
        setSearchOptionInToolbar();

        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (loadNextPage && !isLoading) {
                    if (firstVisibleItem + visibleItemCount >= totalItemCount) {
                        // End has been reached
                        System.out.println(LOG_TAG + " Pagination bottom");
                        llFooterLoading.setVisibility(View.VISIBLE);
                        offset += Property.LIST_ITEM_LIMIT;
                        searchAsyncTask = new SearchAsyncTask();
                        searchAsyncTask.execute(url);
                    }
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(SEARCH_QUERY, etSearch.getText().toString());
    }

    private void setSearchOptionInToolbar() {
        etSearch = (EditText) findViewById(R.id.etSearch);
        tvHomeAppName = (TextView) findViewById(R.id.tvHomeAppName);
        final ImageView toolbarBack = (ImageView) findViewById(R.id.toolbar_home_background);

        if (!searchQuery.isEmpty()) {
            enableSearchField();
        }

        toolbarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enableSearchField();
            }
        });

        etSearch.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int keyCode, KeyEvent keyevent) {
                if ((keyevent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    searchQuery = etSearch.getText().toString();
                    if (!searchQuery.isEmpty()) {
                        clearList();
                        searchAsyncTask = new SearchAsyncTask();
                        searchAsyncTask.execute(url);
                    } else {
                        etSearch.setVisibility(View.VISIBLE);
                        tvHomeAppName.setVisibility(View.GONE);
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void clearList() {
        featuredAppsList.clear();
        adapter.setAppList(featuredAppsList);
        adapter.notifyDataSetChanged();
        offset = 0;
        loadingLayout.setVisibility(View.VISIBLE);
    }

    private void enableSearchField() {
        tvHomeAppName.setVisibility(View.GONE);
        etSearch.setVisibility(View.VISIBLE);
        etSearch.requestFocus();
        if (!searchQuery.isEmpty()) {
            etSearch.setText(searchQuery);
            etSearch.setSelection(etSearch.getText().length());
        }
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (searchAsyncTask != null && !searchAsyncTask.isCancelled()) {
            searchAsyncTask.cancel(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }

        int selection = item.getItemId();

        if (selection == 5) {
            searchQuery = etSearch.getText().toString();
            if (!searchQuery.isEmpty()) {
                clearList();
                searchAsyncTask = new SearchAsyncTask();
                searchAsyncTask.execute(url);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public class SearchAsyncTask extends AsyncTask<String, Context, ApplicationListResponse> {

        ArrayList<Application> appList = new ArrayList<Application>();
        boolean showRetry = false;

        @Override
        protected void onPreExecute() {
            isLoading = true;
        }

        protected void showDialog() {

        }

        @Override
        protected ApplicationListResponse doInBackground(String... params) {
            String fullUrl = null;
            try {
                fullUrl = url + URLEncoder.encode(searchQuery, "UTF-8") + "/start/" + offset + "/limit/" + Property.LIST_ITEM_LIMIT;
            } catch (UnsupportedEncodingException e) {
                Log.d("Url encoding error occurred ", e.getMessage());
            }
            ApplicationListResponse applicationListResponse = null;
            try {
                String json = new RestClient().get(fullUrl);
                Log.d("JSON_Content", json);
                applicationListResponse = new Gson().fromJson(json, ApplicationListResponse.class);
                showRetry = false;
            } catch (ConnectionRefusedException e) {
                Intent i = new Intent(sfa, NoConnectionActivity.class);
                startActivity(i);
                sfa.finish();
                showRetry = true;
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
                showRetry = true;
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                showRetry = true;
            }

            if (applicationListResponse != null && ErrorCodeMapping.isSuccess(applicationListResponse.getStatusCode()) && applicationListResponse.getResults().length > 0) {
                appList = new ArrayList<Application>(Arrays.asList(applicationListResponse.getResults()));
            }

            return applicationListResponse;
        }

        @Override
        protected void onPostExecute(ApplicationListResponse result) {
            super.onPostExecute(result);
            etSearch.requestFocus();
            isLoading = false;
            loadNextPage = true;
            llFooterLoading.setVisibility(View.GONE);

            if (showRetry) {
                retryLayout.setVisibility(View.VISIBLE);
            } else {
                retryLayout.setVisibility(View.GONE);
                if (result != null && ErrorCodeMapping.isSuccess(result.getStatusCode())) {
                    appList = new ArrayList<Application>(Arrays.asList(result.getResults()));
                }
                if (appList.size() != 0) {
                    featuredAppsList.addAll(appList);

                    if (featuredAppsList.size() == 0)
                        gridView.setVisibility(View.GONE);
                    else {
                        gridView.setVisibility(View.VISIBLE);
                        adapter.setAppList(featuredAppsList);
                        adapter.notifyDataSetChanged();
                    }

                } else {
                    loadNextPage = false;
                    System.out.println(LOG_TAG + " next page false");
                }
            }
            loadingLayout.setVisibility(View.GONE);
            if (featuredAppsList.size() != 0) {
                emptySearchResultsTxt.setVisibility(View.GONE);
            } else {
                emptySearchResultsTxt.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            menu.add(1, 5, 0, getString(R.string.menu_item_search_caption))
                    .setIcon(R.drawable.action_search)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);

            return true;
        } else {
            return false;
        }

    }
}