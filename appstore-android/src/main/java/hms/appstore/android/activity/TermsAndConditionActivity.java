/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import hms.appstore.android.R;
import hms.appstore.android.util.Property;

public class TermsAndConditionActivity extends CustomToolBar {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms_and_conditions);
        initCustomToolBar();

        final Button acceptButton = (Button) findViewById(R.id.terms_and_conditions_accept_button);
        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StarterActivity.isAcceptTermsAndConditions = true;
                setTermsSelectionAsPrefData(true);
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                finish();
            }
        });

        final Button disagreeButton = (Button) findViewById(R.id.terms_and_conditions_disagree_button);
        disagreeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StarterActivity.isAcceptTermsAndConditions = false;
                setTermsSelectionAsPrefData(false);
                finish();
            }
        });

        TextView termsText = (TextView) findViewById(R.id.chkTermsAndCondition);
        termsText.setText(Html.fromHtml(getString(R.string.terms_and_condition_caption)));
        termsText.setMovementMethod(LinkMovementMethod.getInstance());
    }


    private void setTermsSelectionAsPrefData(boolean isAccepted) {
        SharedPreferences settings = getSharedPreferences(Property.PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("isAcceptTermsAndConditions", isAccepted);
        editor.commit();
    }

}
