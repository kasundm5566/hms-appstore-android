package hms.appstore.android.activity.app;

import hms.appstore.android.rest.response.Response;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

interface Service {
    @Multipart
    @POST
    Call<Response> postImage(@Url String url, @Part MultipartBody.Part image, @Part("firstName") RequestBody firstName, @Part("lastName") RequestBody lastName, @Part("contactNo") RequestBody contactNo, @Part("email") RequestBody email);
}