package hms.appstore.android.activity.app;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import hms.appstore.android.R;
import hms.appstore.android.activity.CustomToolBar;
import hms.appstore.android.rest.ConnectionRefusedException;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.rest.RestException;
import hms.appstore.android.rest.response.Application;
import hms.appstore.android.rest.response.ApplicationListResponse;
import hms.appstore.android.rest.response.CategoryBasedApplications;
import hms.appstore.android.util.ErrorCodeMapping;
import hms.appstore.android.util.HeaderGridView;
import hms.appstore.android.util.PaginGridAdapter;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.Property;

public class ViewAllActivity extends CustomToolBar {

    private String appCategory;
    private ArrayList<Application> viewAllList = new ArrayList<>();
    private ArrayList<Application> appList;
    private LinearLayout loadingLayout;
    private TextView emptySearchResultsTxt;
    private HeaderGridView gridView;
    private PaginGridAdapter adapter = null;
    private String url;
    private ViewAllAsyncTask viewAllAsyncTask;
    private int offset = 0;
    boolean isLoading = false;
    private boolean loadNextPage = false;
    private static String LOG_TAG = ViewAllActivity.class.getCanonicalName();
    private LinearLayout llFooterLoading, retryLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_all_fragment);
        appCategory = getIntent().getStringExtra("APP_CATEGORY");
        url = getIntent().getStringExtra("URL");
        setUI();
        setAppList();

        viewAllAsyncTask = new ViewAllAsyncTask();
        viewAllAsyncTask.execute(url);
    }

    private void setAppList() {
        adapter = new PaginGridAdapter(this, viewAllList);
        gridView.setAdapter(adapter);
    }

    private void setUI() {
        initCustomToolBar();
        getSupportActionBar().setTitle(appCategory);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        gridView = (HeaderGridView) findViewById(R.id.rvApps);
        emptySearchResultsTxt = (TextView) findViewById(R.id.list_empty);
        loadingLayout = (LinearLayout) findViewById(R.id.loading_layout);
        llFooterLoading = (LinearLayout) findViewById(R.id.llFooterLoading);
        retryLayout = (LinearLayout) findViewById(R.id.retry_layout);

        Button retryButton = (Button) retryLayout.findViewById(R.id.btn_no_connection);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryLayout.setVisibility(View.GONE);
                loadingLayout.setVisibility(View.VISIBLE);
                viewAllAsyncTask = new ViewAllAsyncTask();
                viewAllAsyncTask.execute(url);
            }
        });

        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (loadNextPage && !isLoading) {
                    if (firstVisibleItem + visibleItemCount >= totalItemCount) {
                        // End has been reached
                        System.out.println(LOG_TAG + " Pagination bottom");
                        llFooterLoading.setVisibility(View.VISIBLE);
                        offset += Property.LIST_ITEM_LIMIT;
                        viewAllAsyncTask = new ViewAllAsyncTask();
                        viewAllAsyncTask.execute(url);
                    }
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public class ViewAllAsyncTask extends AsyncTask<String, Context, ApplicationListResponse> {
        CategoryBasedApplications allApplications = null;
        boolean showRetry = false;
        String fullUrl;

        public ViewAllAsyncTask() {
            fullUrl = url + "/start/" + offset + "/limit/" + Property.LIST_ITEM_LIMIT;
        }

        @Override
        protected void onPreExecute() {
            isLoading = true;
        }

        @Override
        protected ApplicationListResponse doInBackground(String... params) {
            ApplicationListResponse applicationListResponse = null;
            try {
                String json = new RestClient().get(fullUrl);
                applicationListResponse = new Gson().fromJson(json, ApplicationListResponse.class);
                showRetry = false;
            } catch (ConnectionRefusedException e) {
                Log.d(LOG_TAG, "ConnectionRefusedException ", e);
                showRetry = true;
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
                showRetry = true;
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                showRetry = true;
            }

            return applicationListResponse;
        }

        protected void onPostExecute(ApplicationListResponse result) {

            isLoading = false;
            loadNextPage = true;
            llFooterLoading.setVisibility(View.GONE);
            if (showRetry) {
                retryLayout.setVisibility(View.VISIBLE);
            } else {
                retryLayout.setVisibility(View.GONE);
                if (result != null && ErrorCodeMapping.isSuccess(result.getStatusCode())) {
                    appList = new ArrayList<Application>(Arrays.asList(result.getResults()));
                }
                if (appList.size() != 0) {
                    viewAllList.addAll(appList);

                    if (viewAllList.size() == 0)
                        gridView.setVisibility(View.GONE);
                    else {
                        gridView.setVisibility(View.VISIBLE);
                        adapter.setAppList(viewAllList);
                        adapter.notifyDataSetChanged();
                    }

                } else {
                    loadNextPage = false;
                    System.out.println(LOG_TAG + " next page false");
                }
            }
            loadingLayout.setVisibility(View.GONE);
        }
    }
}
