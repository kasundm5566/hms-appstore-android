/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.signature.StringSignature;
import com.google.gson.Gson;
import com.gsma.android.mobileconnect.token.Token;
import com.gsma.android.mobileconnect.token.TokenData;
import com.gsma.android.mobileconnect.token.TokenListener;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;

import hms.appstore.android.R;
import hms.appstore.android.activity.app.MyAccountActivity;
import hms.appstore.android.activity.app.PushNotificationActivity;
import hms.appstore.android.activity.app.SearchActivity;
import hms.appstore.android.activity.app.SettingsActivity;
import hms.appstore.android.activity.user.SignInActivity;
import hms.appstore.android.fcm.DeviceTokenAsyncTask;
import hms.appstore.android.fragment.MainUIFragmentAdapter;
import hms.appstore.android.rest.ConnectionRefusedException;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.rest.RestException;
import hms.appstore.android.rest.request.ProfileDetailsRequest;
import hms.appstore.android.rest.response.ProfileDetailsResponse;
import hms.appstore.android.util.AppstoreLocale;
import hms.appstore.android.util.ConnectionDetector;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.ProfileProperty;
import hms.appstore.android.util.Property;
import hms.appstore.android.util.RedirectKeys;
import hms.appstore.android.util.SessionKeys;
import hms.appstore.android.util.Tags;

public class MainActivity extends FragmentParentActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Locale locale = null;
    ConnectionDetector cd = null;
    EditText search;
    SharedPreferences sharedPreferences;
    Bundle extras = null;
    public static final int DISPLAY_SHOW_TITLE = 0x8;
    private TabLayout tabLayout;
    private TextView tvProfileName;
    private TextView tvProfileEmail;
    private ImageView imgProfile;
    static final String LOG_TAG = MainActivity.class.getCanonicalName();
    private NavigationView navigationView;
    boolean doubleBackToExitPressedOnce = false;
    int tabSelectedColor, tabDefaultColor;
    private DrawerLayout drawer = null;
    private int tabPosition = 0;
    private boolean signedIn = false;
    private EditText etSearch;
    private TextView tvHomeAppName;
    private BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getResources().getBoolean(R.bool.multi_language_support)) {
            changeLanguage();
        }
        setContentView(R.layout.activity_main);
        initCustomToolBar();
        getSupportActionBar().setTitle(R.string.home_page_app_name);
        sharedPreferences = getSharedPreferences(Property.PREFS_NAME, 0);
        extras = getIntent().getExtras();
        tabSelectedColor = ContextCompat.getColor(this, R.color.colorPrimary);
        tabDefaultColor = ContextCompat.getColor(this, R.color.feature_icon_default_color);

        search = (EditText) findViewById(R.id.txt_home_search);
        mAdapter = new MainUIFragmentAdapter(getSupportFragmentManager(), getApplicationContext());
        setSearchOptionInToolbar();

        broadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent intent) {
                String action = intent.getAction();
                if (action.equals("finish_activity")) {
                    finish();
                }
            }
        };

        cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            mPager = (ViewPager) findViewById(R.id.pager);
            mPager.setAdapter(mAdapter);
            mPager.setOffscreenPageLimit(5); //determines the number of fragments to be pre-loaded
            mPager.setCurrentItem(0);
            tabLayout = (TabLayout) findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(mPager);
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
            createTabIcons(this);
            tabLayout.getTabAt(0).setIcon(R.drawable.nav_home_active);
        } else {
            Intent i = new Intent(getApplicationContext(), NoConnectionActivity.class);
            startActivity(i);
            finish();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        final View headerView = navigationView.inflateHeaderView(R.layout.nav_header_main);
        tvProfileName = (TextView) headerView.findViewById(R.id.tvProfileName);
        tvProfileEmail = (TextView) headerView.findViewById(R.id.tvProfileEmail);
        imgProfile = (ImageView) headerView.findViewById(R.id.imgProfile);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                navigationView.getMenu().getItem(0).getSubMenu().getItem(tabPosition).setChecked(false);
                super.onDrawerClosed(view);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {

                int size = navigationView.getMenu().getItem(0).getSubMenu().size();
                for (int i = 0; i < size; i++) {
                    if(i == tabPosition){
                        navigationView.getMenu().getItem(0).getSubMenu().getItem(tabPosition).setChecked(true);
                        navigationView.getMenu().getItem(0).getSubMenu().getItem(tabPosition).getIcon().setColorFilter(tabSelectedColor, PorterDuff.Mode.SRC_IN);
                    }else {
                        navigationView.getMenu().getItem(0).getSubMenu().getItem(i).setChecked(false);
                        navigationView.getMenu().getItem(0).getSubMenu().getItem(i).getIcon().setColorFilter(tabDefaultColor, PorterDuff.Mode.SRC_IN);
                    }
                }

                if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
                    System.out.println("User Logged");
                    headerView.setVisibility(View.VISIBLE);
                    customizeMenuItems();
                    if(!signedIn) {
                        MyAccountProfileDetailsAsyncTask myAccountProfileDetailsAsyncTask = new MyAccountProfileDetailsAsyncTask();
                        myAccountProfileDetailsAsyncTask.execute(sharedPreferences.getString(Property.PREFS_USERNAME, ""));
                        signedIn = true;
                    }
                } else {
                    System.out.println("User not logged");
                    headerView.setVisibility(View.GONE);
                }

                super.onDrawerOpened(drawerView);
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        customizeMenuItems();

        if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
            System.out.println("User Logged");
            sendDeviceToken();
            MyAccountProfileDetailsAsyncTask myAccountProfileDetailsAsyncTask = new MyAccountProfileDetailsAsyncTask();
            myAccountProfileDetailsAsyncTask.execute(sharedPreferences.getString(Property.PREFS_USERNAME, ""));
        } else {
            System.out.println("User not logged");
            headerView.setVisibility(View.GONE);
        }
    }

    private void sendDeviceToken() {
        DeviceTokenAsyncTask deviceTokenAsyncTask = new DeviceTokenAsyncTask(sharedPreferences);
        deviceTokenAsyncTask.execute();
    }

    private void setSearchOptionInToolbar() {
        etSearch = (EditText) findViewById(R.id.etSearch);
        tvHomeAppName = (TextView) findViewById(R.id.tvHomeAppName);
        final ImageView toolbarBack = (ImageView) findViewById(R.id.toolbar_home_background);

        toolbarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enableSearchField();
            }
        });

        etSearch.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int keyCode, KeyEvent keyevent) {
                if ((keyevent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {

                    if(!etSearch.getText().toString().isEmpty()) {
                        Intent i = new Intent(getApplicationContext(), SearchActivity.class);
                        i.putExtra("SearchQuery", etSearch.getText().toString());
                        startActivity(i);
                        etSearch.setVisibility(View.GONE);
                        tvHomeAppName.setVisibility(View.VISIBLE);
                        etSearch.setText("");
                    } else{
                        etSearch.setVisibility(View.VISIBLE);
                        tvHomeAppName.setVisibility(View.GONE);
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void enableSearchField() {
        tvHomeAppName.setVisibility(View.GONE);
        etSearch.setVisibility(View.VISIBLE);
        etSearch.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    private void customizeMenuItems() {
        Menu nav_Menu = navigationView.getMenu();
        if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
            nav_Menu.findItem(R.id.nav_account).setVisible(true);
            nav_Menu.findItem(R.id.nav_signout).setTitle(R.string.menu_item_sign_out_caption);
        } else{
            nav_Menu.findItem(R.id.nav_account).setVisible(false);
            nav_Menu.findItem(R.id.nav_signout).setTitle(R.string.menu_item_sign_in_caption);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("Main", "onResume");
        registerReceiver(broadcastReceiver, new IntentFilter("finish_activity"));

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            if(i == tabPosition)
                tabLayout.getTabAt(i).getIcon().setColorFilter(tabSelectedColor, PorterDuff.Mode.SRC_IN);
            else
                tabLayout.getTabAt(i).getIcon().setColorFilter(tabDefaultColor, PorterDuff.Mode.SRC_IN);
        }
        MyAccountProfileDetailsAsyncTask myAccountProfileDetailsAsyncTask = new MyAccountProfileDetailsAsyncTask();
        myAccountProfileDetailsAsyncTask.execute(sharedPreferences.getString(Property.PREFS_USERNAME, ""));
        if (search != null) {
            search.setText("");
            search.clearFocus();
            search.invalidate();
        }
        invalidateOptionsMenu();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            menu.add(1, 5, 0, getString(R.string.menu_item_search_caption))
                    .setIcon(R.drawable.action_search)
//                    .setActionView(R.layout.collapsible_edittext)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
//            SubMenu sub1 = menu.addSubMenu("").setIcon(R.drawable.abs__ic_menu_moreoverflow_normal_holo_dark);

//            if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
//                sub1.add(0, 2, 2, getString(R.string.menu_item_my_app_store_caption))
//                        .setIcon(R.drawable.appsstore)
//                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//                sub1.add(0, 4, 4, getString(R.string.menu_item_settings_caption))
//                        .setIcon(R.drawable.action_settings)
//                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//                sub1.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
//                if (SignInActivity.session.containsKey(SessionKeys.LOGIN_METHOD.name())) {
//                    if (SignInActivity.session.get(SessionKeys.LOGIN_METHOD.name()).equals(LoginKeys.SIGN_IN.name())) {
//                        sub1.add(0, 3, 3, R.string.menu_item_sign_out_caption)
//                                .setIcon(R.drawable.android_sign_out)
//                                .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//                    }
//                }
//
//            } else {
//
//                sub1.add(0, 3, 3, getString(R.string.menu_item_sign_in_caption))
//                        .setIcon(R.drawable.device_access_accounts)
//                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//                sub1.add(0, 4, 4, getString(R.string.menu_item_settings_caption))
//                        .setIcon(R.drawable.action_settings)
//                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//                sub1.getItem().setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
//            }


            return true;
        } else {
            return false;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home || item.getItemId() == 0) {
            return false;
        }

        int selection = item.getItemId();

        if (selection == 2) {
            if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
                //user has logged in - redirect to My AppStore UI
                Intent i = new Intent(getApplicationContext(), MyAppsActivity.class);
                startActivity(i);
            } else if (!SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
                //user is not signed in - redirect to Sign In UI
                Toast.makeText(this, getString(R.string.login_first_to_continue_caption), Toast.LENGTH_LONG).show();
                Intent i = new Intent(getApplicationContext(), SignInActivity.class);
                i.putExtra(Property.REDIRECT_KEY, RedirectKeys.MY_APPSSTORE.name());
                startActivity(i);
            }
        } else if (selection == 3) {
            if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
                //user logged in -signing out
                SignInActivity.session = new HashMap<String, String>();

                if (getResources().getBoolean(R.bool.mobile_connect_login)) {
                    String accessToken = SignInActivity.session.get(SessionKeys.ACCESS_TOKEN.name());
                    revokeAccessToken(accessToken);
                }

                Toast.makeText(this, getString(R.string.signinig_out_caption), Toast.LENGTH_LONG).show();
                //removing shared preference for logged user
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(Property.PREFS_LOGGED_USER, "");
                editor.commit();
                //redirecting to HOME UI
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            } else {
                //user not logged in - redirect to Sign In UI
                Intent i = new Intent(getApplicationContext(), SignInActivity.class);
                i.putExtra(Property.REDIRECT_KEY, RedirectKeys.HOME.name());
                startActivity(i);
            }
        } else if (selection == 4) {
            if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {

                Intent i = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(i);
            } else {
                Intent i = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(i);
            }
        } else if (selection == 5) {
            if(!etSearch.getText().toString().isEmpty()) {
                Intent i = new Intent(getApplicationContext(), SearchActivity.class);
                i.putExtra("SearchQuery", etSearch.getText().toString());
                startActivity(i);
                etSearch.setVisibility(View.GONE);
                tvHomeAppName.setVisibility(View.VISIBLE);
                etSearch.setText("");
            } else{
                etSearch.setVisibility(View.VISIBLE);
                tvHomeAppName.setVisibility(View.GONE);
                etSearch.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        }
        return true;
    }

    public void moveToPage(int tabNumber) {
        mPager.setCurrentItem(tabNumber);
    }

    public void changeLanguage() {
        Configuration config = getBaseContext().getResources().getConfiguration();
        if (!"".equals(AppstoreLocale.appLocale) && !config.locale.getLanguage().equals(AppstoreLocale.appLocale)) {
            locale = new Locale(AppstoreLocale.appLocale);
            Locale.setDefault(locale);
            Configuration conf = new Configuration(config);
            conf.locale = locale;
            getBaseContext().getResources().updateConfiguration(conf, getBaseContext().getResources().getDisplayMetrics());
        }
    }

    public void onAppUpdateNotificationClick(View view) {
        Intent i = new Intent(getApplicationContext(), MyAppsActivity.class);
        i.putExtra(Property.REDIRECT_KEY, RedirectKeys.MY_UPDATES.name());
        startActivity(i);
    }

    private void revokeAccessToken(String accessToken) {
        Token token = new Token();
        if (accessToken != null) {
            token.revokingAccessToken(ProfileProperty.MOBILE_CONNECT_DIALOG_TOKEN_URL, accessToken, "x-aBSN3DAo2Sqb5I2ZlwCCyoHm3DLVOn1i",
                    "x-r8flCtDEbaC0zpKr9zt3STdTGGUpDm6g", new TokenListener() {
                        @Override
                        public void tokenResponse(TokenData arg0) {
                            TokenData td = arg0;
                            if (td != null) {
                                Toast.makeText(getApplicationContext(), "Accees Token Revoke Success", Toast.LENGTH_LONG).show();
                                System.out.println("revoke Access Token Success");
                            } else {
                                System.out.println("revoke Access token Success without Response");
                            }

                        }

                        @Override
                        public void errorToken(JSONObject arg1) {
                            System.out.println("revoke Access token Failed");
                        }
                    });
        }

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_home) {
            mPager.setCurrentItem(0);
            tabLayout.getTabAt(0).setIcon(R.drawable.nav_home_active);
        } else if (id == R.id.nav_popular) {
            mPager.setCurrentItem(1);
            tabLayout.getTabAt(1).setIcon(R.drawable.nav_popular_active);
        } else if (id == R.id.nav_featured) {
            mPager.setCurrentItem(2);
            tabLayout.getTabAt(2).setIcon(R.drawable.nav_featured_active);
        } else if (id == R.id.nav_subscription) {
            mPager.setCurrentItem(3);
            tabLayout.getTabAt(3).setIcon(R.drawable.nav_subscribe_active);
        } else if (id == R.id.nav_downloadable) {
            mPager.setCurrentItem(4);
            tabLayout.getTabAt(4).setIcon(R.drawable.nav_downloadable_active);
        } else if (id == R.id.nav_categories) {
            mPager.setCurrentItem(5);
            tabLayout.getTabAt(5).setIcon(R.drawable.nav_categories_active);
        } else if (id == R.id.nav_notification) {
            showPushNotifications();
        } else if (id == R.id.nav_myapps) {
            showMyApps();
        } else if (id == R.id.nav_account) {
            showMyAccount();
        } else if (id == R.id.nav_settings) {
            showSettingActivity();
        } else if (id == R.id.nav_signout) {
            signedIn = false;
            showSignInActivity();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showPushNotifications() {
        if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
            Intent i = new Intent(getApplicationContext(), PushNotificationActivity.class);
            startActivity(i);
        } else{
            if (SignInActivity.session.containsKey(SessionKeys.LOGIN_STATUS_CODE.name())) {
                if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name()) && SignInActivity.session.get(SessionKeys.LOGIN_STATUS_CODE.name()).equalsIgnoreCase(Property.NO_USER_FOR_MSISDN_STATUS)) {
                    new AlertDialog.Builder(this)
                            .setTitle(R.string.register_dialog_title)
                            .setMessage(R.string.register_dialog_message)
                            .setPositiveButton(R.string.register_dialog_title, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent(Intent.ACTION_VIEW);
                                    Uri u = Uri.parse(ProfileProperty.REGISTRATION_LINK);
                                    intent.setData(u);
                                    startActivity(intent);
                                }

                            })
                            .setNegativeButton(R.string.register_dialog_positive_negative_text, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .show();
                }

            } else {
                handleSignInAction();
            }
        }
    }

    private void handleSignInAction() {
        if (!SignInActivity.session.isEmpty()) {
            SignInActivity.session.clear();
        }
        Toast.makeText(this, R.string.login_message, Toast.LENGTH_LONG).show();
        Intent i = new Intent(getApplicationContext(), SignInActivity.class);
        i.putExtra(Property.REDIRECT_KEY, RedirectKeys.PUSH_NOTIFICATIONS.name());
        startActivity(i);
    }

    private void showMyApps() {
        if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
            //user has logged in - redirect to My AppStore UI
            Intent i = new Intent(getApplicationContext(), MyAppsActivity.class);
            startActivity(i);
        } else {
            //user is not signed in - redirect to Sign In UI
            Toast.makeText(this, getString(R.string.login_first_to_continue_caption), Toast.LENGTH_LONG).show();
            Intent i = new Intent(getApplicationContext(), SignInActivity.class);
            i.putExtra(Property.REDIRECT_KEY, RedirectKeys.MY_APPSSTORE.name());
            startActivity(i);
        }
    }

    private void showMyAccount() {
        Intent i = new Intent(getApplicationContext(), MyAccountActivity.class);
        startActivity(i);
    }

    private void showSettingActivity() {
        if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {

            Intent i = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(i);
        } else {
            Intent i = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(i);
        }
    }

    private void showSignInActivity() {
        if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
            //user logged in -signing out
            SignInActivity.session = new HashMap<String, String>();

            if (getResources().getBoolean(R.bool.mobile_connect_login)) {
                String accessToken = SignInActivity.session.get(SessionKeys.ACCESS_TOKEN.name());
                revokeAccessToken(accessToken);
            }

            Toast.makeText(this, getString(R.string.signinig_out_caption), Toast.LENGTH_LONG).show();
            //removing shared preference for logged user
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(Property.PREFS_LOGGED_USER, "");
            editor.commit();
            //redirecting to HOME UI
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        } else {
            //user not logged in - redirect to Sign In UI
            Intent i = new Intent(getApplicationContext(), SignInActivity.class);
            i.putExtra(Property.REDIRECT_KEY, RedirectKeys.HOME.name());
            startActivity(i);
        }
    }

    private void createTabIcons(final Context context) {

        tabLayout.getTabAt(0).setIcon(R.drawable.nav_home);
        tabLayout.getTabAt(0).getIcon().setColorFilter(tabSelectedColor, PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(1).setIcon(R.drawable.nav_popular);
        tabLayout.getTabAt(1).getIcon().setColorFilter(tabDefaultColor, PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(2).setIcon(R.drawable.nav_featured);
        tabLayout.getTabAt(2).getIcon().setColorFilter(tabDefaultColor, PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(3).setIcon(R.drawable.nav_subscribe);
        tabLayout.getTabAt(3).getIcon().setColorFilter(tabDefaultColor, PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(4).setIcon(R.drawable.nav_downloadable);
        tabLayout.getTabAt(4).getIcon().setColorFilter(tabDefaultColor, PorterDuff.Mode.SRC_IN);
        tabLayout.getTabAt(5).setIcon(R.drawable.nav_categories);
        tabLayout.getTabAt(5).getIcon().setColorFilter(tabDefaultColor, PorterDuff.Mode.SRC_IN);

        tabLayout.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(mPager) {

                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        super.onTabSelected(tab);
                        tabPosition = tab.getPosition();
                        tab.getIcon().setColorFilter(tabSelectedColor, PorterDuff.Mode.SRC_IN);
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                        tab.getIcon().setColorFilter(tabDefaultColor, PorterDuff.Mode.SRC_IN);
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }
                }
        );
    }

    public class MyAccountProfileDetailsAsyncTask extends AsyncTask<String, Context, ProfileDetailsResponse> {

        @Override
        protected ProfileDetailsResponse doInBackground(String... params) {
            String profileDetailsJson = "";
            String  url = Property.MY_ACCOUNT_PROFILE_DETAILS;
            if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
                url += "/session-id/" + SignInActivity.session.get(SessionKeys.SESSION_ID.name());
            }            ProfileDetailsResponse profileDetailsResponse = null;
            try {
                profileDetailsJson = new RestClient().get(url);
                profileDetailsResponse = new Gson().fromJson(profileDetailsJson, ProfileDetailsResponse.class);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (ConnectionRefusedException e) {
                Log.d(LOG_TAG, "ConnectionRefusedException ", e);
            } catch (Exception e) {
                Log.d(LOG_TAG, "Exception ", e);
            }
            Log.d("JSON", "JSON object recieved in Async Task\n" + profileDetailsJson);

            return profileDetailsResponse;
        }

        @Override
        protected void onPostExecute(ProfileDetailsResponse result) {
            super.onPostExecute(result);

            if (result != null) {
                tvProfileName.setText(result.getFirstName() + " " + result.getLastName());
                tvProfileEmail.setText(result.getEmail());
                setImage(ProfileProperty.APPSTORE_IMAGE_URL + result.getImage());
            }
        }
    }

    private void setImage(String image) {
        Glide.with(getApplicationContext()).load(image).asBitmap().signature(new StringSignature(String.valueOf(System.currentTimeMillis()))).placeholder(R.drawable.icon_user).error(R.drawable.icon_user).centerCrop().into(new BitmapImageViewTarget(imgProfile) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(view.getContext().getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                imgProfile.setImageDrawable(circularBitmapDrawable);
            }
        });
    }

    @Override
    public void onBackPressed() {

        etSearch.setVisibility(View.GONE);
        tvHomeAppName.setVisibility(View.VISIBLE);
        etSearch.setText("");

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.app_back_click_twice_to_exit_message, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}