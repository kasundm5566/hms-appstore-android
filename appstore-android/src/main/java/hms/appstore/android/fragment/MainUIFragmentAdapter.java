/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.fragment;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import hms.appstore.android.util.Log;

import hms.appstore.android.R;

public class MainUIFragmentAdapter extends FragmentPagerAdapter {

    Context context;
    private int mCount = 6;

    public MainUIFragmentAdapter(FragmentManager fm, Context nContext) {
        super(fm);
        context = nContext;
    }

    @Override
    public Fragment getItem(int position) {
        android.support.v4.app.Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new HomeSummaryFragment();
                break;
            case 1:
                fragment = new PopularAppsFragment();
                break;
            case 2:
                fragment = new FeaturedAppsFragment();
                break;
            case 3:
                fragment = new SubscriptionAppsFragment();
                break;
            case 4:
                fragment = new DownloadableAppsFragment();
                break;
            case 5:
                fragment = new CategoriesFragment();
                break;
        }
        Log.d("Selected Fragment", Integer.toString(position));
        return fragment;
    }

    @Override
    public int getCount() {
        return mCount;
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }

    @Override
    public String getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getString(R.string.view_pager_main_tabs_home);
            case 1:
                return context.getString(R.string.view_pager_main_tabs_popular);
            case 2:
                return context.getString(R.string.view_pager_main_tabs_featured);
            case 3:
                return context.getString(R.string.view_pager_main_tabs_subscription);
            case 4:
                return context.getString(R.string.view_pager_main_tabs_downloadable);
            case 5:
                return context.getString(R.string.view_pager_main_tabs_categories);
        }
        return null;
    }
}
