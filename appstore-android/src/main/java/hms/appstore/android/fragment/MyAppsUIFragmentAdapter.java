/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.fragment;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import hms.appstore.android.R;
import hms.appstore.android.util.Log;

public class MyAppsUIFragmentAdapter extends FragmentPagerAdapter {

    private Context context;

    public MyAppsUIFragmentAdapter(FragmentManager fm,Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public android.support.v4.app.Fragment getItem(int position) {
        android.support.v4.app.Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new MySubscriptionsFragment();
                break;
            case 1:
                fragment = new MyDownloadsFragment();
                break;
            case 2:
                fragment = new MyUpdatesFragment();
                break;

        }
        Log.d("Selected Fragment", Integer.toString(position));
        return fragment;
    }

    @Override
    public int getCount() {
        Resources res = context.getResources();
        return res.getStringArray(R.array.page_titles).length;
    }

    /*public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }*/

    @Override
    public String getPageTitle(int position) {
        Resources res = context.getResources();
        String[] title = res.getStringArray(R.array.page_titles);
        return title[position].toUpperCase();
    }
}
