package hms.appstore.android.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import hms.appstore.android.R;
import hms.appstore.android.activity.NoConnectionActivity;
import hms.appstore.android.activity.app.AppDescriptionActivity;
import hms.appstore.android.activity.user.SignInActivity;
import hms.appstore.android.rest.ConnectionRefusedException;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.rest.RestException;
import hms.appstore.android.rest.response.Application;
import hms.appstore.android.rest.response.ApplicationListResponse;
import hms.appstore.android.rest.response.CategoriesBanner;
import hms.appstore.android.rest.response.DownloadableBinary;
import hms.appstore.android.rest.response.LocationBanner;
import hms.appstore.android.util.ApplicationLayoutHelper;
import hms.appstore.android.util.ErrorCodeMapping;
import hms.appstore.android.util.GridRecyclerAdapter;
import hms.appstore.android.util.ProfileProperty;
import hms.appstore.android.util.Property;
import hms.appstore.android.util.SessionKeys;
import hms.appstore.android.util.Tags;

public class MyUpdatesFragment extends ListFragment implements BaseSliderView.OnSliderClickListener{
    // url to make request
    private String url = Property.MY_UPDATES_URL;

    int LIST_ITEM_WIDTH = Property.LIST_ITEM_WIDTH;

    ArrayList<Application> featuredAppsList = null;

    //    MyDownloadsBucketListAdapter adapter = null;
    GridRecyclerAdapter adapter = null;
    LinearLayout loadingLayout, llSlider, llAppList;
    private SliderLayout slider;
    RecyclerView recyclerView;

    ArrayList<Application> appList = new ArrayList<Application>();

    int offset = 0;

    int lastPosition = 0;

    boolean isLoading = false;
    boolean isLoaded = false;
    boolean isFirstTime = false;

    int totalAppCount = 0;

    // Put mu downloads here
    GetDownloadedApps getDownloadedAppsAsyncTask;

    View footer;

    LinearLayout retryLayout;
    int numberOfColumns = 3;
    private ImageView imgBanner;
    Typeface font = null;
    static final String LOG_TAG = MyDownloadsFragment.class.getCanonicalName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.featured_apps_fragment, container, false);

        font = Typeface.createFromAsset(getActivity().getAssets(), "SamsungSinhala.ttf");

        LIST_ITEM_WIDTH = 200;
        Log.d("Status", "Download Fragment Called");
        loadingLayout = (LinearLayout) v.findViewById(R.id.loading_layout);
        llSlider = (LinearLayout) v.findViewById(R.id.llSlider);
        slider = (SliderLayout) v.findViewById(R.id.sliderFeatured);
        recyclerView = (RecyclerView) v.findViewById(R.id.rvApps);
        retryLayout = (LinearLayout) v.findViewById(R.id.retry_layout);
        retryLayout.setVisibility(View.GONE);
        llAppList = (LinearLayout) v.findViewById(R.id.llAppList);
        imgBanner = (ImageView) v.findViewById(R.id.imgBanner);

        footer = View.inflate(getContext(), R.layout.loading_apps, null);

        numberOfColumns = ApplicationLayoutHelper.getNumberOfColumns(getContext());
        isFirstTime = true;
        DevBannersAsyncTask devBannersAsyncTask = new DevBannersAsyncTask();
        devBannersAsyncTask.execute();

        if (getResources().getBoolean(R.bool.my_app_updates_enabled)) {
            getDownloadedAppsAsyncTask = new GetDownloadedApps();
            getDownloadedAppsAsyncTask.execute(url);
        }
        Button retryButton = (Button) retryLayout.findViewById(R.id.btn_no_connection);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryLayout.setVisibility(View.GONE);
                loadingLayout.setVisibility(View.VISIBLE);
                getDownloadedAppsAsyncTask = new GetDownloadedApps();
                getDownloadedAppsAsyncTask.execute(url);
            }
        });

        featuredAppsList = appList;
        Log.d("JSON", "JASON object received");

        adapter = new GridRecyclerAdapter(getContext(), featuredAppsList);
//        adapter = new MyDownloadsBucketListAdapter((Activity) getContext(), featuredAppsList, font);
//        setListAdapter(adapter);
        if (container == null) {
            return null;
        }

        return v;
    }

    public void onResume() {
        super.onResume();


    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void onStart() {
        super.onStart();
    }


    public void onPause() {
        super.onPause();
    }

    public void onStop() {
        super.onStop();

        if (getDownloadedAppsAsyncTask != null && !getDownloadedAppsAsyncTask.isCancelled()) {
            getDownloadedAppsAsyncTask.cancel(true);
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onDetach() {
        super.onDetach();
    }

    public class GetDownloadedApps extends AsyncTask<String, Context, ApplicationListResponse> {
        // Hashmap for ListView
        ArrayList<Application> appList = new ArrayList<Application>();

        ProgressDialog dialog = new ProgressDialog(getContext());

        public GetDownloadedApps() {

        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage(getString(R.string.loading_apps));
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.show();
        }

        protected void showDialog() {

        }

        @Override
        protected ApplicationListResponse doInBackground(String... params) {
            String fullUrl = "";
            if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
                fullUrl = String.format(url, SignInActivity.session.get(SessionKeys.SESSION_ID.name()));
            }
            ApplicationListResponse applicationListResponse = null;
            try {
                String json = new RestClient().get(fullUrl);
                Log.d("JSON MUF==>", json);
                applicationListResponse = new Gson().fromJson(json, ApplicationListResponse.class);
            } catch (ConnectionRefusedException e) {
                Intent i = new Intent(getActivity().getApplicationContext(), NoConnectionActivity.class);
                getActivity().finish();  // !@# todo: Change the order and try
                startActivity(i);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }
            Log.d("JSON", "JASON object received in Async Task");

            if (applicationListResponse != null && ErrorCodeMapping.isSuccess(applicationListResponse.getStatusCode())) {

                Application[] appResult = applicationListResponse.getResults();

                for (Application app : appResult) {
                    String downloadedContentId = app.getDownloadStatus().getContentId();

                    for (Map.Entry<String, ArrayList<HashMap<String, ArrayList<DownloadableBinary>>>>
                            downloadedBinary : app.getDownloadableBinaries().entrySet()) {
                        String binaryOs = downloadedBinary.getKey();
                        if (binaryOs.equalsIgnoreCase("android")) {
                            for (HashMap<String, ArrayList<DownloadableBinary>> list : downloadedBinary.getValue()) {
                                for (Map.Entry<String, ArrayList<DownloadableBinary>> dataWithVersion : list.entrySet()) {
                                    for (DownloadableBinary binaryData : dataWithVersion.getValue()) {
                                        if (Long.parseLong(downloadedContentId) < Long.parseLong(binaryData.getContentId())
                                                && appList.indexOf(app) == -1) {
                                            appList.add(app);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                featuredAppsList = appList;
            }

            return applicationListResponse;
        }

        @Override
        protected void onPostExecute(ApplicationListResponse result) {
            super.onPostExecute(result);
//            adapter = new MyDownloadsBucketListAdapter(getActivity(), featuredAppsList, font);
//            setListAdapter(adapter);

            if(featuredAppsList.size() == 0)
                llAppList.setVisibility(View.GONE);
            else
                llAppList.setVisibility(View.VISIBLE);

            recyclerView.setLayoutManager(new GridLayoutManager(getContext(), numberOfColumns));
            adapter = new GridRecyclerAdapter(getContext(), featuredAppsList);
            recyclerView.setAdapter(adapter);
            if (result != null) {
                ListView lv = getListView();

                // Launching new screen on Selecting Single ListItem
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        // getting values from selected ListItem
                        String name = ((TextView) view.findViewById(R.id.single_app_name)).getText().toString();
                        String appId = ((TextView) view.findViewById(R.id.single_app_id)).getText().toString();

                        // Starting new intent
                        Intent in = new Intent(getContext(), AppDescriptionActivity.class);
                        in.putExtra(Tags.TAG_NAME.name(), name);
                        in.putExtra(Tags.TAG_APP_ID.name(), appId);
                        startActivity(in);
                    }
                });
                isLoaded = true;
                dialog.dismiss();
            } else {
                dialog.hide();
                Toast.makeText(getActivity().getApplicationContext(), getString(R.string.E5000), Toast.LENGTH_SHORT).show();
            }
            loadingLayout.setVisibility(View.GONE);
        }
    }

    public class DevBannersAsyncTask extends AsyncTask<String, Context, ArrayList<HashMap<String, LocationBanner>>> {

        ArrayList<HashMap<String, LocationBanner>> categoryBannerList = new ArrayList<HashMap<String, LocationBanner>>();

        @Override
        protected ArrayList<HashMap<String, LocationBanner>> doInBackground(String... params) {

            CategoriesBanner categoriesBanner = null;
            try {
                String json = new RestClient().get(Property.APP_MY_APPS_BANNERS);
                hms.appstore.android.util.Log.d("JSON", json);
                categoriesBanner = new Gson().fromJson(json, CategoriesBanner.class);
            } catch (ConnectionRefusedException e) {
                hms.appstore.android.util.Log.d(LOG_TAG, "ConnectionRefusedException ", e);
            } catch (RestException e) {
                hms.appstore.android.util.Log.d(LOG_TAG, "RestException ", e);
            } catch (Exception e) {
                hms.appstore.android.util.Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }

            if (categoriesBanner != null && ErrorCodeMapping.isSuccess(categoriesBanner.getStatusCode())) {
                for (int i = 0; i < categoriesBanner.getResults().length; i++) {
                    HashMap<String, LocationBanner> banner = new HashMap<String, LocationBanner>();
                    banner.put(categoriesBanner.getResults()[i].getDescription(), categoriesBanner.getResults()[i]);
                    categoryBannerList.add(banner);
                }
            }
            return categoryBannerList;
        }

        @Override
        protected void onPostExecute(ArrayList<HashMap<String, LocationBanner>> hashMaps) {
            super.onPostExecute(hashMaps);

            if (categoryBannerList != null && categoryBannerList.size() == 0) {
                imgBanner.setVisibility(View.GONE);
                slider.setVisibility(View.GONE);
                llSlider.setVisibility(View.GONE);
            } else {
                llSlider.setVisibility(View.VISIBLE);
                if (categoryBannerList.size() < 2) {
                    slider.setVisibility(View.GONE);
                    imgBanner.setVisibility(View.VISIBLE);
                    final LocationBanner banner = categoryBannerList.get(0).entrySet().iterator().next().getValue();
                    loadSingleBanner(banner.getImage_url(), imgBanner);
                    imgBanner.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (banner != null && banner.getLink() != null && !banner.getLink().isEmpty()) {
                                try {
                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(banner.getLink()));
                                    startActivity(browserIntent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                } else {
                    imgBanner.setVisibility(View.GONE);
                    slider.setVisibility(View.VISIBLE);
                    setImageSlider(categoryBannerList);
                }
            }
        }
    }

    private void loadSingleBanner(String appImgUrl, ImageView imgView) {
        String imageUrl = ProfileProperty.APPSTORE_IMAGE_URL + appImgUrl;
        Glide.with(getContext())
                .load(imageUrl)
                .asBitmap()
                .into(imgView);
    }

    private void setImageSlider(ArrayList<HashMap<String, LocationBanner>> categoryBannerList) {

        for (HashMap<String, LocationBanner> bannerMap : categoryBannerList) {

            DefaultSliderView textSliderView = new DefaultSliderView(this.getContext());
            textSliderView
//                    .description(name)
                    .image(ProfileProperty.APPSTORE_IMAGE_URL.concat(bannerMap.entrySet().iterator().next().getValue().getImage_url()))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra", bannerMap.entrySet().iterator().next().getValue().getLink());
            slider.addSlider(textSliderView);
        }
        slider.setPresetTransformer(SliderLayout.Transformer.Default);
        slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new DescriptionAnimation());
        slider.setDuration(5000);
        slider.addOnPageChangeListener(null);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

        if(slider.getBundle().get("extra") != null && !slider.getBundle().get("extra").toString().equals("")) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(slider.getBundle().get("extra").toString()));
            try {
                startActivity(browserIntent);
            } catch (Exception e) {
                hms.appstore.android.util.Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                e.printStackTrace();
            }
        }
    }
}



