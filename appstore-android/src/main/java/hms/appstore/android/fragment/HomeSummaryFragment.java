/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import hms.appstore.android.R;
import hms.appstore.android.activity.MainActivity;
import hms.appstore.android.activity.NoConnectionActivity;
import hms.appstore.android.activity.app.AppDescriptionActivity;
import hms.appstore.android.activity.app.HomeFragmentViewAllActivity;
import hms.appstore.android.activity.user.SignInActivity;
import hms.appstore.android.rest.ConnectionRefusedException;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.rest.RestException;
import hms.appstore.android.rest.response.AllAppsResponse;
import hms.appstore.android.rest.response.AppUpdateCountResponse;
import hms.appstore.android.rest.response.Application;
import hms.appstore.android.rest.response.CategoriesBanner;
import hms.appstore.android.rest.response.CategoryBasedApplications;
import hms.appstore.android.rest.response.LocationBanner;
import hms.appstore.android.rest.response.VersionUpdateResponse;
import hms.appstore.android.util.AppIconUtil;
import hms.appstore.android.util.ErrorCodeMapping;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.ProfileProperty;
import hms.appstore.android.util.Property;
import hms.appstore.android.util.SessionKeys;
import hms.appstore.android.util.Tags;
import hms.appstore.android.util.widget.LocalizedButton;

public class HomeSummaryFragment extends Fragment implements View.OnClickListener, BaseSliderView.OnSliderClickListener {

    static final String LOG_TAG = HomeSummaryFragment.class.getCanonicalName();
    FragmentActivity fragmentActivity;
    LayoutInflater homeInflatter;
    LinearLayout loadingLayout;
    LinearLayout retryLayout;
    RelativeLayout appSummaryLayout;
    GetVersionUpdate getVersionUpdateAsyncTask;
    UpdateCountAsyncTask updateCountAsyncTask;
    Button versionUpdateButton;
    Button appUpdateButton;
    int currentVersion;
    GetAllApps getAllAppsAsyncTask;
    CategoryBasedApplications allApps = null;
    LinearLayout featuredAppsLayout;
    LinearLayout mostlyUsedAppsLayout;
    LinearLayout newlyAddedAppsLayout;
    LinearLayout freeAppsLayout;
    private String allAppsUrl = Property.ALL_APPS_URL;
    private String versionUpdateUrl = Property.VERSION_UPDATE_URL;
    private String appUpdateCountUrl = Property.APP_UPDATE_COUNT_URL;
    SliderLayout slider;
    LocalizedButton viewAllMostUsed, viewAllNewlyAdded, viewAllFree;
    private ImageView imgBanner;
    private LinearLayout llHomeBanner;

    private static ImageLoaderConfiguration imageConfig;
    private static DisplayImageOptions displayImgOptions;
    private static ImageLoader imageLoader = ImageLoader.getInstance();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.home_fragment, container, false);

        homeInflatter = inflater;
        fragmentActivity = getActivity();

        imageConfig = new ImageLoaderConfiguration.Builder(fragmentActivity)
                .threadPoolSize(5)
                .memoryCache(new UsingFreqLimitedMemoryCache(5 * 1024 * 1024))
                .imageDownloader(new BaseImageDownloader(getContext()))
                .build();
        displayImgOptions = new DisplayImageOptions.Builder()
                .cacheInMemory()
                .cacheOnDisc()
                .delayBeforeLoading(500)
                .build();

        loadingLayout = (LinearLayout) rootView.findViewById(R.id.loading_layout);
        retryLayout = (LinearLayout) rootView.findViewById(R.id.retry_layout);
        appSummaryLayout = (RelativeLayout) rootView.findViewById(R.id.home_app_summary_layout);
        slider = (SliderLayout) rootView.findViewById(R.id.sliderHome);
        imgBanner = (ImageView) rootView.findViewById(R.id.imgBanner);
        llHomeBanner = (LinearLayout) rootView.findViewById(R.id.llHomeBanner);
        appSummaryLayout.setVisibility(View.GONE);
        retryLayout.setVisibility(View.GONE);
//
//        setViewAllButton(rootView, R.id.viewAllMostlyUsed, 3);
//        setViewAllButton(rootView, R.id.viewAllNewlyAdded, 4);
//        setViewAllButton(rootView, R.id.viewAllFreeApps, 5);

        viewAllMostUsed = (LocalizedButton) rootView.findViewById(R.id.viewAllMostlyUsed);
        viewAllNewlyAdded = (LocalizedButton) rootView.findViewById(R.id.viewAllNewlyAdded);
        viewAllFree = (LocalizedButton) rootView.findViewById(R.id.viewAllFreeApps);
        viewAllMostUsed.setOnClickListener(this);
        viewAllNewlyAdded.setOnClickListener(this);
        viewAllFree.setOnClickListener(this);

        //Version Update Notification
        versionUpdateButton = (Button) rootView.findViewById(R.id.versionUpdateBtn);
        versionUpdateButton.setVisibility(View.GONE);
        appUpdateButton = (Button) rootView.findViewById(R.id.appUpdateNotifyButton);
        appUpdateButton.setVisibility(View.GONE);
        getVersionUpdateAsyncTask = new GetVersionUpdate();
        updateCountAsyncTask = new UpdateCountAsyncTask();

        currentVersion = Build.VERSION.SDK_INT;
        getVersionUpdateAsyncTask.execute(currentVersion);
        if (getResources().getBoolean(R.bool.my_app_updates_enabled)) {
            updateCountAsyncTask.execute();
        }

        //All Apps Data Loading
        executeGetAllAppsAsyncTask();
        allApps = getAllAppsAsyncTask.allApplications;

        HomeBannersAsyncTask homeBannersAsyncTask = new HomeBannersAsyncTask();
        homeBannersAsyncTask.execute();

        Button retryButton = (Button) retryLayout.findViewById(R.id.btn_no_connection);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryLayout.setVisibility(View.GONE);
                loadingLayout.setVisibility(View.VISIBLE);
                executeGetAllAppsAsyncTask();
            }
        });

        featuredAppsLayout = (LinearLayout) rootView.findViewById(R.id.featuredAppsLinearLayout);
        mostlyUsedAppsLayout = (LinearLayout) rootView.findViewById(R.id.mostlyUsedLinearLayout);
        newlyAddedAppsLayout = (LinearLayout) rootView.findViewById(R.id.newlyAddedLinearLayout);
        freeAppsLayout = (LinearLayout) rootView.findViewById(R.id.freeAppsLinearLayout);

        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();

        if (getAllAppsAsyncTask != null && !getAllAppsAsyncTask.isCancelled()) {
            getAllAppsAsyncTask.cancel(true);
        }

        if (getVersionUpdateAsyncTask != null && !getVersionUpdateAsyncTask.isCancelled()) {
            getVersionUpdateAsyncTask.cancel(true);
        }
    }

    private void executeGetAllAppsAsyncTask() {
        getAllAppsAsyncTask = new GetAllApps(
                getResources().getInteger(R.integer.home_summary_get_all_apps_start),
                getResources().getInteger(R.integer.home_summary_get_all_apps_limit));
        getAllAppsAsyncTask.execute(allAppsUrl);
    }

    private void setViewAllButton(View view, int buttonId, final int tabNumber) {
        LocalizedButton viewAllButton = (LocalizedButton) view.findViewById(buttonId);
        viewAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.moveToPage(tabNumber);
            }
        });
    }

    private void populateApps(LinearLayout layout, ArrayList<Application> appList, boolean isFeatured) {
        for (final Application app : appList) {

            int singleAppLayout;
            if (isFeatured) {
                singleAppLayout = R.layout.featured_app_single;
            } else singleAppLayout = R.layout.single_app_small;

            View appView = homeInflatter.inflate(singleAppLayout, null);

            TextView txtAppId = (TextView) appView.findViewById(R.id.single_app_id);
            txtAppId.setText(app.getId());

            TextView appName = (TextView) appView.findViewById(R.id.single_app_name);
            appName.setText(app.getName());

            TextView appDescription = (TextView) appView.findViewById(R.id.single_app_description);
            appDescription.setText(app.getDescription());

            TextView appRating = (TextView) appView.findViewById(R.id.single_app_rating);
            appRating.setText(String.valueOf(app.getRating()));

            TextView appAuthor = (TextView) appView.findViewById(R.id.single_app_cost);
            appAuthor.setText(app.getDeveloper());

            ImageView appIcon = (ImageView) appView.findViewById(R.id.icon);
            if (isFeatured) {
                String appBanner;
                if (app.getAppBanners().length > 0) {
                    appBanner = app.getAppBanners()[0].getUrl();
                } else appBanner = "";

                loadAppImage(appBanner, appIcon);
            } else loadAppImage(app.getAppIcon(), appIcon);

            appView.setOnClickListener(setAppOnClickAction(app));

            layout.addView(appView);
        }
    }

    private void loadAppImage(String appImgUrl, ImageView imgView) {
        String imageUrl = ProfileProperty.APPSTORE_IMAGE_URL + appImgUrl;
        // Initialize ImageLoader with configuration. Do it once.
//        imageLoader.init(imageConfig);
//        // Load and display image asynchronously
//        imageLoader.displayImage(imageUrl, imgView, displayImgOptions);
        Glide.with(getContext())
                .load(imageUrl)
                .asBitmap()
                .into(AppIconUtil.getRoundedImageTarget(getContext(), imgView, 20));
    }

    private void loadSingleBanner(String appImgUrl, ImageView imgView) {
        String imageUrl = ProfileProperty.APPSTORE_IMAGE_URL + appImgUrl;
        if(getContext() != null && !this.getActivity().isFinishing()) {
            Glide.with(getContext().getApplicationContext())
                    .load(imageUrl)
                    .asBitmap()
                    .into(imgView);
        }
    }

    private View.OnClickListener setAppOnClickAction(final Application app) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = ((TextView) v.findViewById(R.id.single_app_name)).getText().toString();
                String app_id = ((TextView) v.findViewById(R.id.single_app_id)).getText().toString();

                // Starting new intent
                Intent in = new Intent(fragmentActivity, AppDescriptionActivity.class);
                in.putExtra(Tags.TAG_NAME.name(), name);
                in.putExtra(Tags.TAG_APP_ID.name(), app_id);
                in.putExtra("app", app);

                fragmentActivity.startActivity(in);
            }
        };
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(getContext(), HomeFragmentViewAllActivity.class);
        switch (v.getId()) {
            case R.id.viewAllMostlyUsed:
                i.putExtra("APP_CATEGORY", HomeFragmentViewAllActivity.mostUsed);
                break;
            case R.id.viewAllNewlyAdded:
                i.putExtra("APP_CATEGORY", HomeFragmentViewAllActivity.newlyAdded);
                break;
            case R.id.viewAllFreeApps:
                i.putExtra("APP_CATEGORY", HomeFragmentViewAllActivity.free);
                break;
        }
        i.putExtra("URL", Property.ALL_APPS_URL);
        i.putExtra("FROM", HomeSummaryFragment.class.getCanonicalName());
        startActivity(i);
    }

    public class UpdateCountAsyncTask extends AsyncTask<Void, Context, AppUpdateCountResponse> {
        @Override
        protected AppUpdateCountResponse doInBackground(Void... params) {
            AppUpdateCountResponse appUpdateResp = new AppUpdateCountResponse();
            String json = "";
            try {
                if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
                    json = new RestClient().get(appUpdateCountUrl + SignInActivity.session.get(SessionKeys.SESSION_ID.name()));
//                    appUpdateResp = new Gson().fromJson(json, AppUpdateCountResponse.class);
//                    TODO De-serialization is not happening properly, fix it and uncomment above and remove below 02 lines
                    String jsonObject = new JSONObject(json).getJSONObject("result").getString("count");
                    appUpdateResp.setCount(Integer.parseInt(jsonObject));
                }
            } catch (RestException e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            } catch (ConnectionRefusedException e) {
                Intent i = new Intent(getActivity().getApplicationContext(), NoConnectionActivity.class);
                startActivity(i);
                getActivity().finish();
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }
            Log.d("JSON", "JSON object received in Async task \n" + json);

            return appUpdateResp;
        }

        @Override
        protected void onPostExecute(AppUpdateCountResponse appUpdateCountResponse) {

            if (appUpdateCountResponse != null) {
                int updateCount = appUpdateCountResponse.getCount();

                if (updateCount > 0) {
                    appUpdateButton.setVisibility(View.VISIBLE);
                    String s;
                    if (updateCount > 1) {
                        s = "s";
                    } else {
                        s = "";
                    }
                    appUpdateButton.setText(String.format(getString(R.string.app_update_notification), updateCount, s));
                } else {
                    appUpdateButton.setVisibility(View.GONE);
                }
            }
        }
    }

    public class GetVersionUpdate extends AsyncTask<Integer, Context, VersionUpdateResponse> {

        @Override
        protected VersionUpdateResponse doInBackground(Integer... params) {
            Integer osVersion = params[0];
            VersionUpdateResponse versionUpdateResponse = null;
            String json = "";
            try {
                json = new RestClient().get(versionUpdateUrl + Property.ANDROID_VERSION_PREFIX + osVersion);
                versionUpdateResponse = new Gson().fromJson(json, VersionUpdateResponse.class);
            } catch (RestException e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            } catch (ConnectionRefusedException e) {
                Intent i = new Intent(getActivity().getApplicationContext(), NoConnectionActivity.class);
                startActivity(i);
                getActivity().finish();
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }
            Log.d("JSON", "JASON object received in Async Task \n" + json);

            return versionUpdateResponse;
        }

        @Override
        protected void onPostExecute(VersionUpdateResponse versionUpdateResponse) {

            int currentClientVersionCode = -1;
            int newClientVersionCode;

            if (versionUpdateResponse != null) {
                try {
                    currentClientVersionCode = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionCode;
                    Log.d(LOG_TAG, "Current package name : " + getActivity().getPackageName());
                } catch (PackageManager.NameNotFoundException e) {
                    Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                } catch (NullPointerException e) {
                    Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                }

                if (ErrorCodeMapping.isSuccess(versionUpdateResponse.getStatusCode())) {
                    final String clientLocationUrl = versionUpdateResponse.getClientLocation();
                    newClientVersionCode = Integer.parseInt(versionUpdateResponse.getNewClientVersion());

                    if (newClientVersionCode > currentClientVersionCode) {
                        versionUpdateButton.setVisibility(View.VISIBLE);
                    } else {
                        versionUpdateButton.setVisibility(View.GONE);
                    }

                    versionUpdateButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(clientLocationUrl));
                            startActivity(intent);
                        }
                    });
                } else {
                    versionUpdateButton.setVisibility(View.GONE);
                    Log.d(LOG_TAG, versionUpdateResponse.getStatusDescription());
                }
            } else {
                versionUpdateButton.setVisibility(View.GONE);
                Log.d(LOG_TAG, "Version Update response is null");
            }
        }
    }

    public class GetAllApps extends AsyncTask<String, Context, AllAppsResponse> {
        CategoryBasedApplications allApplications = null;
        boolean showRetry = false;
        String fullUrl;

        public GetAllApps(int start, int limit) {
            fullUrl = allAppsUrl + "/start/" + start + "/limit/" + limit;
        }

        @Override
        protected void onPreExecute() {
            loadingLayout.setVisibility(View.VISIBLE);
        }

        @Override
        protected AllAppsResponse doInBackground(String... params) {
            AllAppsResponse allAppsResponse = null;
            try {
                String json = new RestClient().get(fullUrl);
                allAppsResponse = new Gson().fromJson(json, AllAppsResponse.class);
                showRetry = false;
            } catch (ConnectionRefusedException e) {
                Log.d(LOG_TAG, "ConnectionRefusedException ", e);
                showRetry = true;
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
                showRetry = true;
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                showRetry = true;
            }

            if (allAppsResponse != null
                    && ErrorCodeMapping.isSuccess(allAppsResponse.getStatusCode())
                    && allAppsResponse.getResult() != null) {
                allApplications = allAppsResponse.getResult();
                allApps = allApplications;
            }

            return allAppsResponse;
        }

        protected void onPostExecute(AllAppsResponse allAppsResponse) {
            if (allApps != null) {
                populateApps(featuredAppsLayout, new ArrayList<Application>(Arrays.asList(allApps.getFeaturedApps())), true);
                populateApps(mostlyUsedAppsLayout, new ArrayList<Application>(Arrays.asList(allApps.getMostlyUsedApps())), false);
                populateApps(newlyAddedAppsLayout, new ArrayList<Application>(Arrays.asList(allApps.getNewlyAddedApps())), false);
                populateApps(freeAppsLayout, new ArrayList<Application>(Arrays.asList(allApps.getFreeApps())), false);
            }

            if (showRetry) {
                retryLayout.setVisibility(View.VISIBLE);
                appSummaryLayout.setVisibility(View.GONE);
            } else {
                retryLayout.setVisibility(View.GONE);
                appSummaryLayout.setVisibility(View.VISIBLE);
            }
            loadingLayout.setVisibility(View.GONE);
        }
    }

    public class HomeBannersAsyncTask extends AsyncTask<String, Context, ArrayList<HashMap<String, LocationBanner>>> {

        // Hashmap for banners
        ArrayList<HashMap<String, LocationBanner>> homeBannerList = new ArrayList<HashMap<String, LocationBanner>>();

        @Override
        protected ArrayList<HashMap<String, LocationBanner>> doInBackground(String... params) {

            CategoriesBanner categoriesBanner = null;
            try {
                String json = new RestClient().get(Property.APP_HOME_BANNERS);
                Log.d("JSON", json);
                categoriesBanner = new Gson().fromJson(json, CategoriesBanner.class);
            } catch (ConnectionRefusedException e) {
                Log.d(LOG_TAG, "ConnectionRefusedException ", e);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }

            if (categoriesBanner != null && ErrorCodeMapping.isSuccess(categoriesBanner.getStatusCode())) {
                if (categoriesBanner.getResults() != null) {
                    for (int i = 0; i < categoriesBanner.getResults().length; i++) {
                        HashMap<String, LocationBanner> banner = new HashMap<String, LocationBanner>();
                        banner.put(categoriesBanner.getResults()[i].getDescription(), categoriesBanner.getResults()[i]);
                        homeBannerList.add(banner);
                    }
                }
            }
            return homeBannerList;
        }

        @Override
        protected void onPostExecute(ArrayList<HashMap<String, LocationBanner>> hashMaps) {
            super.onPostExecute(hashMaps);

            if (homeBannerList != null && homeBannerList.size() == 0) {
                imgBanner.setVisibility(View.GONE);
                slider.setVisibility(View.GONE);
                llHomeBanner.setVisibility(View.GONE);
            } else {
                llHomeBanner.setVisibility(View.VISIBLE);
                if (homeBannerList.size() < 2) {
                    slider.setVisibility(View.GONE);
                    imgBanner.setVisibility(View.VISIBLE);
                    final LocationBanner banner = homeBannerList.get(0).entrySet().iterator().next().getValue();
                    loadSingleBanner(banner.getImage_url(), imgBanner);
                    imgBanner.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (banner != null && banner.getLink() != null && !banner.getLink().isEmpty()) {
                                try {
                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(banner.getLink()));
                                    startActivity(browserIntent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                } else {
                    imgBanner.setVisibility(View.GONE);
                    slider.setVisibility(View.VISIBLE);
                    setImageSlider(homeBannerList);
                }
            }
        }
    }

    private void setImageSlider(ArrayList<HashMap<String, LocationBanner>> homeBannerList) {

        for (HashMap<String, LocationBanner> bannerMap : homeBannerList) {

            DefaultSliderView textSliderView = new DefaultSliderView(getContext());
            textSliderView
//                    .description(name)
                    .image(ProfileProperty.APPSTORE_IMAGE_URL.concat(bannerMap.entrySet().iterator().next().getValue().getImage_url()))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra", bannerMap.entrySet().iterator().next().getValue().getLink());
            slider.addSlider(textSliderView);
        }
        slider.setPresetTransformer(SliderLayout.Transformer.Default);
        slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new DescriptionAnimation());
        slider.setDuration(5000);
        slider.addOnPageChangeListener(null);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

        if(slider.getBundle().get("extra") != null && !slider.getBundle().get("extra").toString().equals("")) {
            try {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(slider.getBundle().get("extra").toString()));
                startActivity(browserIntent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
