/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.fragment.category;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.ListFragment;

import hms.appstore.android.util.Log;

import hms.appstore.android.R;

public class CategoryUIFragmentAdapter extends FragmentPagerAdapter {
    Context context;
    private int mCount = 4;

    public CategoryUIFragmentAdapter(FragmentManager fm, Context nContext) {
        super(fm);
        context = nContext;
    }

    @Override
    public Fragment getItem(int position) {
        android.support.v4.app.Fragment slf = null;
        switch (position) {
            case 0:
                slf = new AllAppsFragment();
                break;
            case 1:
                slf = new MostUsedAppsFragment();
                break;
            case 2:
                slf = new NewlyAddedAppFragment();
                break;
            case 3:
                slf = new FreeAppsFragment();
                break;
        }
        Log.d("Selected Fragment", Integer.toString(position));
        return slf;
    }

    @Override
    public int getCount() {
        return mCount;
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }

    @Override
    public String getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getString(R.string.main_screen_title_all_apps).toUpperCase();
            case 1:
                return context.getString(R.string.main_screen_title_most_used_apps).toUpperCase();
            case 2:
                return context.getString(R.string.main_screen_title_newly_added_apps).toUpperCase();
            case 3:
                return context.getString(R.string.main_screen_title_free_apps).toUpperCase();
        }
        return null;
    }
}
