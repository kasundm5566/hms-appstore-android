/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.fragment.category;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import hms.appstore.android.R;
import hms.appstore.android.activity.NoConnectionActivity;
import hms.appstore.android.activity.app.AppsForCategoryActivity;
import hms.appstore.android.activity.app.SettingsActivity;
import hms.appstore.android.rest.ConnectionRefusedException;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.rest.RestException;
import hms.appstore.android.rest.response.AppCountResponse;
import hms.appstore.android.rest.response.Application;
import hms.appstore.android.rest.response.ApplicationListResponse;
import hms.appstore.android.util.ErrorCodeMapping;
import hms.appstore.android.util.HeaderGridView;
import hms.appstore.android.util.PaginGridAdapter;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.Property;

public class NewlyAddedAppFragment extends ListFragment {

    private ArrayList<Application> featuredAppsList = new ArrayList<>();
    private TextView emptySearchResultsTxt;
    private LinearLayout loadingLayout, retryLayout, llFooterLoading;
    private ArrayList<Application> appList = new ArrayList<Application>();
    private int offset = 0;
    private boolean isLoading = false;
    private String url = Property.NEWLY_ADDED_APPS_URL;
    private GetNewlyAddedApps getNewlyAddedAppsAsyncTask;
    private HeaderGridView gridView;
    private PaginGridAdapter adapter = null;
    private boolean loadNextPage = false;
    private static final String LOG_TAG = NewlyAddedAppFragment.class.getCanonicalName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.view_all_fragment, container, false);
        emptySearchResultsTxt = (TextView) v.findViewById(R.id.list_empty);
        loadingLayout = (LinearLayout) v.findViewById(R.id.loading_layout);
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);
        retryLayout = (LinearLayout) v.findViewById(R.id.retry_layout);
        gridView = (HeaderGridView) v.findViewById(R.id.rvApps);
        llFooterLoading = (LinearLayout) v.findViewById(R.id.llFooterLoading);

        getNewlyAddedAppsAsyncTask = new GetNewlyAddedApps();
        getNewlyAddedAppsAsyncTask.execute(url);

        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (loadNextPage && !isLoading) {
                    if (firstVisibleItem + visibleItemCount >= totalItemCount) {
                        // End has been reached
                        System.out.println(LOG_TAG + " Pagination bottom");
                        llFooterLoading.setVisibility(View.VISIBLE);
                        offset += Property.LIST_ITEM_LIMIT;
                        getNewlyAddedAppsAsyncTask = new GetNewlyAddedApps();
                        getNewlyAddedAppsAsyncTask.execute(url);
                    }
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }
        });

        Button retryButton = (Button) retryLayout.findViewById(R.id.btn_no_connection);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryLayout.setVisibility(View.GONE);
                loadingLayout.setVisibility(View.VISIBLE);
                getNewlyAddedAppsAsyncTask = new GetNewlyAddedApps();
                getNewlyAddedAppsAsyncTask.execute(url);

            }
        });

        adapter = new PaginGridAdapter(getContext(), featuredAppsList);
        gridView.setAdapter(adapter);

        if (container == null) {
            return null;
        }
        return v;
    }

    public void onResume() {
        super.onResume();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void onStart() {
        super.onStart();
    }

    public void onPause() {
        super.onPause();

    }

    public void onStop() {
        super.onStop();

        if (getNewlyAddedAppsAsyncTask != null && !getNewlyAddedAppsAsyncTask.isCancelled()) {
            getNewlyAddedAppsAsyncTask.cancel(true);
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onDetach() {
        super.onDetach();
    }

    public class GetNewlyAddedApps extends AsyncTask<String, Context, ApplicationListResponse> {

        boolean showRetry = false;

        @Override
        protected void onPreExecute() {
            isLoading = true;
        }

        @Override
        protected ApplicationListResponse doInBackground(String... params) {
            String fullUrl;
            if (SettingsActivity.isShowAndroidAppsOnly) {
                fullUrl = url + "/category/" + AppsForCategoryActivity.categoryName + "/platform/" + Property.PLATFORM + "/start/" + offset + "/limit/" + Property.LIST_ITEM_LIMIT;
            } else {
                fullUrl = url + "/category/" + AppsForCategoryActivity.categoryName + "/start/" + offset + "/limit/" + Property.LIST_ITEM_LIMIT;
            }
            if (SettingsActivity.isShowAndroidAppsOnly && offset == 0) {
                try {
                    String json = new RestClient().get(Property.DOWNLOADABLE_APP_COUNT_WITH_CATEGORY_URL + AppsForCategoryActivity.categoryName);
                    Log.d("App Count JSON", json);
                    AppCountResponse count = new Gson().fromJson(json, AppCountResponse.class);
                } catch (Exception e) {
                    Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                }
            }
            ApplicationListResponse applicationListResponse = null;
            try {
                String json = new RestClient().get(fullUrl);
                Log.d("JSON", json);
                applicationListResponse = new Gson().fromJson(json, ApplicationListResponse.class);
                showRetry = false;
            } catch (ConnectionRefusedException e) {
                Intent i = new Intent(getContext(), NoConnectionActivity.class);
                startActivity(i);
                showRetry = true;
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e.fillInStackTrace());
                showRetry = true;
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                showRetry = true;
            }
            Log.d("JSON", "JASON object recieved in Async Task");

            return applicationListResponse;
        }

        @Override
        protected void onPostExecute(ApplicationListResponse result) {
            super.onPostExecute(result);


            isLoading = false;
            loadNextPage = true;
            llFooterLoading.setVisibility(View.GONE);
            if (showRetry) {
                retryLayout.setVisibility(View.VISIBLE);
            } else {
                retryLayout.setVisibility(View.GONE);
                if (result != null && ErrorCodeMapping.isSuccess(result.getStatusCode())) {
                    appList = new ArrayList<Application>(Arrays.asList(result.getResults()));
                }
                if (appList.size() != 0) {
                    featuredAppsList.addAll(appList);

                    if (featuredAppsList.size() == 0)
                        gridView.setVisibility(View.GONE);
                    else {
                        gridView.setVisibility(View.VISIBLE);
                        adapter.setAppList(featuredAppsList);
                        adapter.notifyDataSetChanged();
                    }

                } else {
                    loadNextPage = false;
                    System.out.println(LOG_TAG + " next page false");
                }
                if (featuredAppsList.size() == 0) {
                    emptySearchResultsTxt.setVisibility(View.VISIBLE);
                } else {
                    emptySearchResultsTxt.setVisibility(View.GONE);
                }
            }
            loadingLayout.setVisibility(View.GONE);
        }
    }
}


