package hms.appstore.android.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import hms.appstore.android.R;
import hms.appstore.android.activity.app.SettingsActivity;
import hms.appstore.android.rest.ConnectionRefusedException;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.rest.RestException;
import hms.appstore.android.rest.response.AppCountResponse;
import hms.appstore.android.rest.response.Application;
import hms.appstore.android.rest.response.ApplicationListResponse;
import hms.appstore.android.rest.response.CategoriesBanner;
import hms.appstore.android.rest.response.LocationBanner;
import hms.appstore.android.util.ErrorCodeMapping;
import hms.appstore.android.util.HeaderGridView;
import hms.appstore.android.util.PaginGridAdapter;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.ProfileProperty;
import hms.appstore.android.util.Property;

public class SubscriptionAppsFragment extends ListFragment implements BaseSliderView.OnSliderClickListener{

    private String url = Property.SUBSCRIPTION_APPS_URL;
    private ArrayList<Application> popularAppsList = new ArrayList<>();
    private PaginGridAdapter adapter = null;
    private ArrayList<Application> appList;
    private int offset = 0;
    boolean isLoading = false;
    private LinearLayout retryLayout, llSlider, llAppList, llFooterLoading, loadingLayout;
    private SliderLayout slider;
    private HeaderGridView gridView;
    private ImageView imgBanner;
    private boolean loadNextPage = false;
    static final String LOG_TAG = PopularAppsFragment.class.getCanonicalName();
    private SubscriptionAppsAsyncTask subscriptionAppsAsyncTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.pagin_app_fragment, container, false);
        loadingLayout = (LinearLayout) v.findViewById(R.id.loading_layout);
        retryLayout = (LinearLayout) v.findViewById(R.id.retry_layout);
        llAppList = (LinearLayout) v.findViewById(R.id.llAppList);
        llFooterLoading = (LinearLayout) v.findViewById(R.id.llFooterLoading);
        retryLayout.setVisibility(View.GONE);
        gridView = (HeaderGridView) v.findViewById(R.id.rvApps);

        View headerView = inflater.inflate(R.layout.category_list_header, null);
        gridView.addHeaderView(headerView);
        slider = (SliderLayout) headerView.findViewById(R.id.sliderCategory);
        imgBanner = (ImageView) headerView.findViewById(R.id.imgBanner);
        llSlider = (LinearLayout) headerView.findViewById(R.id.llSliderCategory);

        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (loadNextPage && !isLoading) {
                    if (firstVisibleItem + visibleItemCount >= totalItemCount) {
                        // End has been reached
                        System.out.println(LOG_TAG + " Pagination bottom");
                        llFooterLoading.setVisibility(View.VISIBLE);
                        offset += Property.LIST_ITEM_LIMIT;
                        subscriptionAppsAsyncTask = new SubscriptionAppsAsyncTask();
                        subscriptionAppsAsyncTask.execute(url);
                    }
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }
        });

        subscriptionAppsAsyncTask = new SubscriptionAppsAsyncTask();
        subscriptionAppsAsyncTask.execute(url);

        SubscriptionBannersAsyncTask subscriptionBannersAsyncTask = new SubscriptionBannersAsyncTask();
        subscriptionBannersAsyncTask.execute();

        Button retryButton = (Button) retryLayout.findViewById(R.id.btn_no_connection);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryLayout.setVisibility(View.GONE);
                loadingLayout.setVisibility(View.VISIBLE);
                subscriptionAppsAsyncTask = new SubscriptionAppsAsyncTask();
                subscriptionAppsAsyncTask.execute(url);
            }
        });

        adapter = new PaginGridAdapter(getContext(), popularAppsList);
        gridView.setAdapter(adapter);

        if (container == null) {
            return null;
        }
        return v;
    }

    public void onResume() {
        super.onResume();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void onStart() {
        super.onStart();
    }


    public void onPause() {
        super.onPause();
    }

    public void onStop() {
        super.onStop();

        if (subscriptionAppsAsyncTask != null && !subscriptionAppsAsyncTask.isCancelled()) {
            subscriptionAppsAsyncTask.cancel(true);
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onDetach() {
        super.onDetach();
    }

    public class SubscriptionBannersAsyncTask extends AsyncTask<String, Context, ArrayList<HashMap<String, LocationBanner>>> {

        ArrayList<HashMap<String, LocationBanner>> popularBannerList = new ArrayList<HashMap<String, LocationBanner>>();

        @Override
        protected ArrayList<HashMap<String, LocationBanner>> doInBackground(String... params) {

            CategoriesBanner categoriesBanner = null;
            try {
                String json = new RestClient().get(Property.APP_SUBSCRIPTION_BANNERS);
                Log.d("JSON", "PopularAppsFragment-" + json);
                categoriesBanner = new Gson().fromJson(json, CategoriesBanner.class);
            } catch (ConnectionRefusedException e) {
                Log.d(LOG_TAG, "ConnectionRefusedException ", e);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }

            if (categoriesBanner != null && ErrorCodeMapping.isSuccess(categoriesBanner.getStatusCode())) {
                for (int i = 0; i < categoriesBanner.getResults().length; i++) {
                    HashMap<String, LocationBanner> banner = new HashMap<String, LocationBanner>();
                    banner.put(categoriesBanner.getResults()[i].getDescription(), categoriesBanner.getResults()[i]);
                    popularBannerList.add(banner);
                }
            }
            return popularBannerList;
        }

        @Override
        protected void onPostExecute(ArrayList<HashMap<String, LocationBanner>> hashMaps) {
            super.onPostExecute(hashMaps);

            if (popularBannerList != null && popularBannerList.size() == 0) {
                imgBanner.setVisibility(View.GONE);
                slider.setVisibility(View.GONE);
                llSlider.setVisibility(View.GONE);
            } else {
                llSlider.setVisibility(View.VISIBLE);
                if (popularBannerList.size() < 2) {
                    slider.setVisibility(View.GONE);
                    imgBanner.setVisibility(View.VISIBLE);
                    final LocationBanner banner = popularBannerList.get(0).entrySet().iterator().next().getValue();
                    loadSingleBanner(banner.getImage_url(), imgBanner);
                    imgBanner.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (banner != null && banner.getLink() != null && !banner.getLink().isEmpty()) {
                                try {
                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(banner.getLink()));
                                    startActivity(browserIntent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                } else {
                    imgBanner.setVisibility(View.GONE);
                    slider.setVisibility(View.VISIBLE);
                    setImageSlider(popularBannerList);
                }
            }
        }
    }

    private void loadSingleBanner(String appImgUrl, ImageView imgView) {
        String imageUrl = ProfileProperty.APPSTORE_IMAGE_URL + appImgUrl;
        Glide.with(getContext())
                .load(imageUrl)
                .asBitmap()
                .into(imgView);
    }

    public class SubscriptionAppsAsyncTask extends AsyncTask<String, Context, ApplicationListResponse> {

        boolean showRetry = false;

        @Override
        protected void onPreExecute() {
            isLoading = true;
        }


        @Override
        protected ApplicationListResponse doInBackground(String... params) {
            String fullUrl;
            if (SettingsActivity.isShowAndroidAppsOnly) {
                fullUrl = url + "/platform/" + Property.PLATFORM + "/start/" + offset + "/limit/" + Property.LIST_ITEM_LIMIT;
            } else {
                fullUrl = url + "/start/" + offset + "/limit/" + Property.LIST_ITEM_LIMIT;
            }
            if (SettingsActivity.isShowAndroidAppsOnly && offset == 0) {
                try {
                    String json = new RestClient().get(Property.DOWNLOADABLE_APP_COUNT_URL);
                    Log.d("App Count JSON", "PopularAppsFragment-" + json);
                    AppCountResponse count = new Gson().fromJson(json, AppCountResponse.class);
                } catch (Exception e) {
                    Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                }
            }

            ApplicationListResponse applicationListResponse = null;
            try {
                String json = new RestClient().get(fullUrl);
                Log.d("JSON", "PopularAppsFragment-" + json);
                applicationListResponse = new Gson().fromJson(json, ApplicationListResponse.class);
                showRetry = false;
            } catch (ConnectionRefusedException e) {
                showRetry = true;
            } catch (RestException e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                showRetry = true;
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                showRetry = true;
            }
            return applicationListResponse;
        }

        @Override
        protected void onPostExecute(ApplicationListResponse result) {
            super.onPostExecute(result);

            isLoading = false;
            loadNextPage = true;
            llFooterLoading.setVisibility(View.GONE);
            if (showRetry) {
                retryLayout.setVisibility(View.VISIBLE);
            } else {
                retryLayout.setVisibility(View.GONE);
                if (result != null && ErrorCodeMapping.isSuccess(result.getStatusCode())) {
                    appList = new ArrayList<Application>(Arrays.asList(result.getResults()));
                }
                if (appList.size() != 0) {
                    popularAppsList.addAll(appList);

                    if (popularAppsList.size() == 0)
                        llAppList.setVisibility(View.GONE);
                    else {
                        llAppList.setVisibility(View.VISIBLE);
                        adapter.setAppList(popularAppsList);
                        adapter.notifyDataSetChanged();
                    }

                } else {
                    loadNextPage = false;
                    System.out.println(LOG_TAG + " next page false");
                }
            }
            loadingLayout.setVisibility(View.GONE);
        }
    }

    private void setImageSlider(ArrayList<HashMap<String, LocationBanner>> categoryBannerList) {

        for (HashMap<String, LocationBanner> bannerMap : categoryBannerList) {

            DefaultSliderView textSliderView = new DefaultSliderView(getContext());
            textSliderView
//                    .description(name)
                    .image(ProfileProperty.APPSTORE_IMAGE_URL.concat(bannerMap.entrySet().iterator().next().getValue().getImage_url()))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra", bannerMap.entrySet().iterator().next().getValue().getLink());
            slider.addSlider(textSliderView);
        }
        slider.setPresetTransformer(SliderLayout.Transformer.Default);
        slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new DescriptionAnimation());
        slider.setDuration(5000);
        slider.addOnPageChangeListener(null);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

        if(slider.getBundle().get("extra") != null && !slider.getBundle().get("extra").toString().equals("")) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(slider.getBundle().get("extra").toString()));
            try {
                startActivity(browserIntent);
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                e.printStackTrace();
            }
        }
    }
}

