/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

import hms.appstore.android.R;
import hms.appstore.android.activity.app.AppDescriptionActivity;
import hms.appstore.android.activity.app.SettingsActivity;
import hms.appstore.android.rest.ConnectionRefusedException;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.rest.RestException;
import hms.appstore.android.rest.response.AppCountResponse;
import hms.appstore.android.rest.response.Application;
import hms.appstore.android.rest.response.ApplicationListResponse;
import hms.appstore.android.util.DefaultBucketListAdapter;
import hms.appstore.android.util.ErrorCodeMapping;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.Property;
import hms.appstore.android.util.Tags;

public class FreeAppsFragment extends ListFragment {
    // url to make request
    private static String url = Property.FREE_APPS_URL;

    int LIST_ITEM_WIDTH = Property.LIST_ITEM_WIDTH;

    ArrayList<Application> featuredAppsList = null;

    DefaultBucketListAdapter adapter = null;

    LinearLayout loadingLayout;

    ArrayList<Application> appList = new ArrayList<Application>();

    int offset = 0;

    int lastPosition = 0;

    boolean isLoading = false;
    boolean isLoaded = false;
    boolean isFirstTime = false;

    int totalAppCount = 0;

    GetFreeApps getFreeAppsAsyncTask;

    LinearLayout retryLayout;

    View footer;

    Typeface font = null;

    static final String LOG_TAG = FreeAppsFragment.class.getCanonicalName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.featured_apps_fragment, container, false);

        loadingLayout = (LinearLayout) v.findViewById(R.id.loading_layout);

        retryLayout = (LinearLayout) v.findViewById(R.id.retry_layout);
        retryLayout.setVisibility(View.GONE);

        LIST_ITEM_WIDTH = 400;

        footer = View.inflate(getContext(), R.layout.loading_apps, null);

        isFirstTime = true;

        getFreeAppsAsyncTask = new GetFreeApps();
        getFreeAppsAsyncTask.execute(url);

        Button retryButton = (Button) retryLayout.findViewById(R.id.btn_no_connection);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryLayout.setVisibility(View.GONE);
                loadingLayout.setVisibility(View.VISIBLE);
                getFreeAppsAsyncTask = new GetFreeApps();
                getFreeAppsAsyncTask.execute(url);
            }
        });

        featuredAppsList = appList;

        adapter = new DefaultBucketListAdapter((Activity) getContext(), featuredAppsList, font);

        if (container == null) {
            return null;
        }

        return v;
    }

    @Override
    public void onConfigurationChanged(android.content.res.Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        int beforeCount = newConfig.screenHeightDp / LIST_ITEM_WIDTH;
        int afterCount = newConfig.screenWidthDp / LIST_ITEM_WIDTH;

        int currentPosition = getListView().getFirstVisiblePosition() * beforeCount;
        adapter.notifyDataSetChanged();
        getListView().setSelectionFromTop(currentPosition / afterCount, 0);
    }

    public void onResume() {
        super.onResume();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void onStart() {
        super.onStart();
    }


    public void onPause() {
        super.onPause();

    }

    public void onStop() {
        super.onStop();

        if (getFreeAppsAsyncTask != null && !getFreeAppsAsyncTask.isCancelled()) {
            getFreeAppsAsyncTask.cancel(true);
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onDetach() {
        super.onDetach();
    }

    public class GetFreeApps extends AsyncTask<String, Context, ApplicationListResponse> {
        boolean showRetry = false;


        public GetFreeApps() {

        }

        @Override
        protected void onPreExecute() {
            loadingLayout.setVisibility(View.VISIBLE);
            isLoading = true;
        }

        protected void showDialog() {

        }

        @Override
        protected ApplicationListResponse doInBackground(String... params) {
            String fullUrl;
            if (SettingsActivity.isShowAndroidAppsOnly) {
                fullUrl = url + "/platform/" + Property.PLATFORM + "/start/" + offset + "/limit/" + Property.LIST_ITEM_LIMIT;
            } else {
                fullUrl = url + "/start/" + offset + "/limit/" + Property.LIST_ITEM_LIMIT;
            }
            if (SettingsActivity.isShowAndroidAppsOnly && offset == 0) {
                try {
                    String json = new RestClient().get(Property.DOWNLOADABLE_FREE_APP_COUNT_URL);
                    Log.d("App Count JSON", json);
                    AppCountResponse count = new Gson().fromJson(json, AppCountResponse.class);
                    if (ErrorCodeMapping.isSuccess(count.getStatusCode())) {
                        totalAppCount = count.getResult().getCount();
                    }
                } catch (Exception e) {
                    Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                }
            }
            ApplicationListResponse applicationListResponse = null;
            try {
                String json = new RestClient().get(fullUrl);
                Log.d("JSON", json);
                applicationListResponse = new Gson().fromJson(json, ApplicationListResponse.class);
                showRetry = false;
            } catch (ConnectionRefusedException e) {
                showRetry = true;
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException", e);
                showRetry = true;
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                showRetry = true;
            }

            if (applicationListResponse != null && ErrorCodeMapping.isSuccess(applicationListResponse.getStatusCode()) && applicationListResponse.getResults().length >= 0) {
                int appCount = applicationListResponse.getResults().length;

                if (SettingsActivity.isShowAndroidAppsOnly) {
                    if (totalAppCount > offset) {
                        offset += Property.LIST_ITEM_LIMIT;
                    } else {
                        isLoaded = true;
                    }
                } else {
                    if (appCount > 0) {
                        offset += appCount;
                    }

                    if (appCount < Property.LIST_ITEM_LIMIT) {
                        isLoaded = true;
                    }
                }
            }
            if (applicationListResponse != null && applicationListResponse.getStatusCode().equalsIgnoreCase(Property.NO_APPS_FOR_THE_CRITERIA_STATUS)) {
                if (SettingsActivity.isShowAndroidAppsOnly) {
                    if (totalAppCount > offset) {
                        offset += Property.LIST_ITEM_LIMIT;
                    } else {
                        isLoaded = true;
                    }
                }
            }
            return applicationListResponse;
        }

        @Override
        protected void onPostExecute(final ApplicationListResponse result) {
            super.onPostExecute(result);

            if (showRetry) {
                retryLayout.setVisibility(View.VISIBLE);
            } else {
                retryLayout.setVisibility(View.GONE);

                if (result != null && ErrorCodeMapping.isSuccess(result.getStatusCode())) {
                    appList = new ArrayList<Application>(Arrays.asList(result.getResults()));
                }

                if (isFirstTime) {
                    getListView().addFooterView(footer);
                    isFirstTime = false;
                }
                int currentPosition = getListView().getFirstVisiblePosition();
                for (Application app : appList) {
                    featuredAppsList.add(app);
                }
                adapter = new DefaultBucketListAdapter((Activity) getContext(), featuredAppsList, font);
                setListAdapter(adapter);

                final ListView lv = getListView();
                lv.setSelectionFromTop(currentPosition, 0);
                // Launching new screen on Selecting Single ListItem
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        String name = ((TextView) view.findViewById(R.id.single_app_name)).getText().toString();
                        String app_id = ((TextView) view.findViewById(R.id.single_app_id)).getText().toString();

                        // Starting new intent
                        Intent in = new Intent(getContext(), AppDescriptionActivity.class);
                        in.putExtra(Tags.TAG_NAME.name(), name);
                        in.putExtra(Tags.TAG_APP_ID.name(), app_id);
                        startActivity(in);
                    }
                });
                lv.setOnScrollListener(new AbsListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsListView absListView, int i) {
                        // Do Nothing
                    }

                    @Override
                    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
                        if (!isLoaded && !isLoading && i + i2 >= i3) {
                            lv.addFooterView(footer);
                            adapter.notifyDataSetChanged();

                            getFreeAppsAsyncTask = new GetFreeApps();
                            getFreeAppsAsyncTask.execute(url);
                            lastPosition = i;
                        }
                    }
                });

                isLoading = false;
                try {
                    lv.removeFooterView(footer);
                } catch (Exception e) {
                    Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                }
            }
            loadingLayout.setVisibility(View.GONE);
        }
    }
}




