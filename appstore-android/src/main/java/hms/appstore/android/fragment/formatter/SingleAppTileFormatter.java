/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.fragment.formatter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import hms.appstore.android.R;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.Tags;

public class SingleAppTileFormatter {
    public static ArrayList<HashMap<String, String>> generateDetailMapFromJsonString(String jsonString) throws JSONException {
        ArrayList<HashMap<String, String>> appList = new ArrayList<HashMap<String, String>>();

        JSONArray apps = new JSONArray(jsonString);


        for (int x = 0; x < apps.length(); x++) {
            JSONObject singleFeaturedApp = apps.getJSONObject(x);

            String name = singleFeaturedApp.getString("name");
            String description = singleFeaturedApp.getString("description");
            String rating = singleFeaturedApp.getString("rating");
            String category = singleFeaturedApp.getString("category");
            String created_by = "By: " + singleFeaturedApp.getString("developer");
            String app_id = singleFeaturedApp.getString("id");
            String app_icon_path = singleFeaturedApp.getString("app-icon");

            Log.d("JSON", singleFeaturedApp.toString());

            // creating new HashMap
            HashMap<String, String> map = new HashMap<String, String>();

            // adding each child node to HashMap key => value
            map.put(Tags.TAG_NAME.name(), name);
            map.put(Tags.TAG_DESCRIPTION.name(), description);
            map.put(Tags.TAG_RATING.name(), rating);
            map.put(Tags.TAG_CATEGORY.name(), category);
            map.put(Tags.TAG_CREATED_BY.name(), created_by);
            map.put(Tags.TAG_APP_ID.name(), app_id);
            map.put(Tags.TAG_APP_ICON.name(), app_icon_path);

            // adding HashList to ArrayList
            appList.add(map);
        }
//
        return appList;
    }

    public static String[] getTagArray() {
        return new String[]{Tags.TAG_APP_ID.name(), Tags.TAG_NAME.name(), Tags.TAG_CREATED_BY.name(), Tags.TAG_DESCRIPTION.name(), Tags.TAG_RATING.name()};
    }

    public static int[] getPlaceholderArray() {
        return new int[]{R.id.single_app_id, R.id.single_app_name, R.id.single_app_cost, R.id.single_app_description, R.id.ratingBar1};

    }
}
