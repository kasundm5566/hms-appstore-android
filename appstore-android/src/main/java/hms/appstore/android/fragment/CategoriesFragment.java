/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

import hms.appstore.android.R;
import hms.appstore.android.fragment.category.CategoryListAdapter;
import hms.appstore.android.rest.ConnectionRefusedException;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.rest.RestException;
import hms.appstore.android.rest.response.CategoriesBanner;
import hms.appstore.android.rest.response.CategoriesResponse;
import hms.appstore.android.rest.response.Category;
import hms.appstore.android.rest.response.LocationBanner;
import hms.appstore.android.util.ErrorCodeMapping;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.ProfileProperty;
import hms.appstore.android.util.Property;

public class CategoriesFragment extends Fragment implements BaseSliderView.OnSliderClickListener {

    CategoryListAdapter adapter = null;
    ArrayList<Category> categoriesList1 = null;

    private static final String TAG_CATEGORY = "CategoryName";

    LinearLayout loadingLayout;

    GetCategories getCategoriesAsyncTask;

    CategoryBannersAsyncTask categoryBannersAsyncTask;

    LinearLayout retryLayout;
    TextView lblAllCategories;
    SliderLayout slider;
    private ImageView imgBanner;
    private String categoryBanners = Property.APP_CATEGORIES_BANNERS;
    private LinearLayout llSliderCategory;
    static final String LOG_TAG = CategoriesFragment.class.getCanonicalName();
    private ListView lvCategory;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.categories_fragment, container, false);
        loadingLayout = (LinearLayout) v.findViewById(R.id.categories_loading_layout);
        retryLayout = (LinearLayout) v.findViewById(R.id.retry_layout);
        retryLayout.setVisibility(View.GONE);
        lvCategory = (ListView) v.findViewById(R.id.lvCategory);

        View headerView = inflater.inflate(R.layout.category_list_header, null);
        lvCategory.addHeaderView(headerView);
        slider = (SliderLayout) headerView.findViewById(R.id.sliderCategory);
        imgBanner = (ImageView) headerView.findViewById(R.id.imgBanner);
        llSliderCategory = (LinearLayout) headerView.findViewById(R.id.llSliderCategory);
        lblAllCategories = (TextView) headerView.findViewById(R.id.lblAllCategories);

        getCategoriesAsyncTask = new GetCategories();
        getCategoriesAsyncTask.execute();

        categoryBannersAsyncTask = new CategoryBannersAsyncTask();
        categoryBannersAsyncTask.execute();

        Button retryButton = (Button) retryLayout.findViewById(R.id.btn_no_connection);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                retryLayout.setVisibility(View.GONE);
                loadingLayout.setVisibility(View.VISIBLE);
                getCategoriesAsyncTask = new GetCategories();
                getCategoriesAsyncTask.execute();
            }
        });

        categoriesList1 = getCategoriesAsyncTask.categoriesList;

        Log.d("Category List size >>>>>>>>>>>>>>>>>", " " + categoriesList1.size());
        adapter = new CategoryListAdapter(getContext(), R.layout.list_item_category, categoriesList1);
        lvCategory.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        return v;
    }

    private void setImageSlider(ArrayList<HashMap<String, LocationBanner>> categoryBannerList) {

        for (HashMap<String, LocationBanner> bannerMap : categoryBannerList) {

            DefaultSliderView textSliderView = new DefaultSliderView(getContext());
            textSliderView
//                    .description(name)
                    .image(ProfileProperty.APPSTORE_IMAGE_URL.concat(bannerMap.entrySet().iterator().next().getValue().getImage_url()))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra", bannerMap.entrySet().iterator().next().getValue().getLink());
            slider.addSlider(textSliderView);
        }
        slider.setPresetTransformer(SliderLayout.Transformer.Default);
        slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        slider.setCustomAnimation(new DescriptionAnimation());
        slider.setDuration(5000);
        slider.addOnPageChangeListener(null);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

        if(slider.getBundle().get("extra") != null && !slider.getBundle().get("extra").toString().equals("")) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(slider.getBundle().get("extra").toString()));
            try {
                startActivity(browserIntent);
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        setUserVisibleHint(true);
    }


    public void onResume() {
        super.onResume();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void onStart() {
        super.onStart();
    }


    public void onPause() {
        super.onPause();
    }

    public void onStop() {
        super.onStop();

        if (getCategoriesAsyncTask != null && !getCategoriesAsyncTask.isCancelled()) {
            getCategoriesAsyncTask.cancel(true);
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onDetach() {
        super.onDetach();
    }

    public class GetCategories extends AsyncTask<String, Context, ArrayList<Category>> {

        ArrayList<Category> categoriesList = new ArrayList<Category>();

        boolean showRetry = false;

        public GetCategories() {

        }

        @Override
        protected void onPreExecute() {
            loadingLayout.setVisibility(View.VISIBLE);
        }

        protected void showDialog() {
        }

        @Override
        protected ArrayList<Category> doInBackground(String... params) {
            String url = Property.APP_CATEGORIES_URL;
            CategoriesResponse categoriesResponse = null;
            try {
                String json = new RestClient().get(url);
                Log.d("JSON", json);
                categoriesResponse = new Gson().fromJson(json, CategoriesResponse.class);
                showRetry = false;
            } catch (ConnectionRefusedException e) {
                Log.d(LOG_TAG, "ConnectionRefusedException ", e);
                showRetry = true;
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
                showRetry = true;
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
                showRetry = true;
            }

            if (categoriesResponse != null && ErrorCodeMapping.isSuccess(categoriesResponse.getStatusCode())) {
                for (int i = 0; i < categoriesResponse.getResults().length; i++) {
                    categoriesList.add(categoriesResponse.getResults()[i]);
                }
            }
            return categoriesList;
        }

        @Override
        protected void onPostExecute(ArrayList<Category> result) {
            super.onPostExecute(result);

            if (showRetry) {
                retryLayout.setVisibility(View.VISIBLE);
            } else {
                retryLayout.setVisibility(View.GONE);
                Log.d("Category List size >>>>>>>>>>>>>>>>>", " " + result.size());
                adapter.setCategoriesList(result);
                adapter.notifyDataSetChanged();
            }
            loadingLayout.setVisibility(View.GONE);
        }
    }

    public class CategoryBannersAsyncTask extends AsyncTask<String, Context, ArrayList<HashMap<String, LocationBanner>>> {

        ArrayList<HashMap<String, LocationBanner>> categoryBannerList = new ArrayList<HashMap<String, LocationBanner>>();

        @Override
        protected ArrayList<HashMap<String, LocationBanner>> doInBackground(String... params) {

            CategoriesBanner categoriesBanner = null;
            try {
                String json = new RestClient().get(categoryBanners);
                Log.d("JSON", json);
                categoriesBanner = new Gson().fromJson(json, CategoriesBanner.class);
            } catch (ConnectionRefusedException e) {
                Log.d(LOG_TAG, "ConnectionRefusedException ", e);
            } catch (RestException e) {
                Log.d(LOG_TAG, "RestException ", e);
            } catch (Exception e) {
                Log.d(LOG_TAG, e.getMessage(), e.fillInStackTrace());
            }

            if (categoriesBanner != null && ErrorCodeMapping.isSuccess(categoriesBanner.getStatusCode())) {
                for (int i = 0; i < categoriesBanner.getResults().length; i++) {
                    HashMap<String, LocationBanner> banner = new HashMap<String, LocationBanner>();
                    banner.put(categoriesBanner.getResults()[i].getDescription(), categoriesBanner.getResults()[i]);
                    categoryBannerList.add(banner);
                }
            }
            return categoryBannerList;
        }

        @Override
        protected void onPostExecute(ArrayList<HashMap<String, LocationBanner>> hashMaps) {
            super.onPostExecute(hashMaps);

            if (categoryBannerList != null && categoryBannerList.size() == 0) {
                imgBanner.setVisibility(View.GONE);
                slider.setVisibility(View.GONE);
                llSliderCategory.setVisibility(View.GONE);
                lblAllCategories.setVisibility(View.GONE);
            } else {
                llSliderCategory.setVisibility(View.VISIBLE);
                lblAllCategories.setVisibility(View.VISIBLE);
                if (categoryBannerList.size() < 2) {
                    slider.setVisibility(View.GONE);
                    imgBanner.setVisibility(View.VISIBLE);
                    final LocationBanner banner = categoryBannerList.get(0).entrySet().iterator().next().getValue();
                    loadSingleBanner(banner.getImage_url(), imgBanner);
                    imgBanner.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (banner != null && banner.getLink() != null && !banner.getLink().isEmpty()) {
                                try {
                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(banner.getLink()));
                                    startActivity(browserIntent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });
                } else {
                    imgBanner.setVisibility(View.GONE);
                    slider.setVisibility(View.VISIBLE);
                    setImageSlider(categoryBannerList);
                }
            }
        }
    }

    private void loadSingleBanner(String appImgUrl, ImageView imgView) {
        String imageUrl = ProfileProperty.APPSTORE_IMAGE_URL + appImgUrl;
        Glide.with(getContext())
                .load(imageUrl)
                .asBitmap()
                .into(imgView);
    }
}