package hms.appstore.android.fragment.category;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

import hms.appstore.android.R;
import hms.appstore.android.activity.app.AppsForCategoryActivity;
import hms.appstore.android.rest.response.Category;
import hms.appstore.android.util.ProfileProperty;
import hms.appstore.android.util.Tags;

public class CategoryListAdapter extends ArrayAdapter<Category> {

    private Context context;
    private ArrayList<Category> categoriesList;
    private ImageLoader imageLoader;
    private ImageLoaderConfiguration config;
    private DisplayImageOptions options;

    public CategoryListAdapter(Context context, int resource, ArrayList<Category> categoriesList) {
        super(context, resource, categoriesList);
        this.context = context;
        this.categoriesList = categoriesList;

        imageLoader = ImageLoader.getInstance();
        config = new ImageLoaderConfiguration.Builder(context)
                .threadPoolSize(5)
                .memoryCache(new UsingFreqLimitedMemoryCache(2 * 1024 * 1024))
                .build();
        imageLoader.init(config);
        options = new DisplayImageOptions.Builder()
                .cacheInMemory()
                .build();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.list_item_category, parent, false);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.tvCategory = (TextView) rowView.findViewById(R.id.tvCategory);
            viewHolder.imgIcon = (ImageView) rowView.findViewById(R.id.imgIcon);
            rowView.setTag(viewHolder);
        }
        ViewHolder holder = (ViewHolder) rowView.getTag();
        final Category category = categoriesList.get(position);
        holder.tvCategory.setText(category.getName());
        imageLoader.displayImage(ProfileProperty.APPSTORE_IMAGE_URL + category.getImage_url(), holder.imgIcon, options);

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(getContext(), AppsForCategoryActivity.class);
                in.putExtra(Tags.TAG_CATEGORY.name(), category.getName());
                context.startActivity(in);
            }
        });

        return rowView;
    }

    private static class ViewHolder {
        TextView tvCategory;
        ImageView imgIcon;
    }

    @Override
    public int getCount() {
        return categoriesList.size();
    }

    @Nullable
    @Override
    public Category getItem(int position) {
        return categoriesList.get(position);
    }

    public ArrayList<Category> getCategoriesList() {
        return categoriesList;
    }

    public void setCategoriesList(ArrayList<Category> categoriesList) {
        this.categoriesList = categoriesList;
    }
}
