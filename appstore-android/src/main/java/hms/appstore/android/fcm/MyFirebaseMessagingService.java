package hms.appstore.android.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import hms.appstore.android.R;
import hms.appstore.android.util.Tags;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FCM Service";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.
        String messageBody, messageTitle, clickAction, requestId;

        messageBody = remoteMessage.getData().get("body");
        messageTitle = remoteMessage.getData().get("title");
        clickAction = remoteMessage.getData().get("click_action");
        requestId = remoteMessage.getData().get("requestId");

        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Notification Message Body : " + messageBody);
        Log.d(TAG, "Notification Message Title : " + messageTitle);
        Log.d(TAG, "Notification Message click_action : " + clickAction);
        Log.d(TAG, "Notification Message requestId : " + requestId);

        Intent intent = new Intent(clickAction);

        if(remoteMessage.getData().get("appId") != null){
            Log.d(TAG, "Notification Message appID : " + remoteMessage.getData().get("appId").trim());
            intent.putExtra(Tags.TAG_APP_ID.name(), remoteMessage.getData().get("appId").trim());
            intent.putExtra(Tags.TAG_PUSH_NOTIFICATION_ID.name(),  remoteMessage.getData().get("requestId").trim());
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.apk_icon)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());

        sendBroadcast(new Intent("PUSH_NOTIFICATION_RECEIVED"));
    }
}