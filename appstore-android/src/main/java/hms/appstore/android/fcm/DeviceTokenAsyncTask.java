package hms.appstore.android.fcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;

import com.google.gson.Gson;

import hms.appstore.android.activity.user.SignInActivity;
import hms.appstore.android.rest.ConnectionRefusedException;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.rest.RestException;
import hms.appstore.android.rest.request.DeviceTokenRequest;
import hms.appstore.android.rest.response.Response;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.Property;
import hms.appstore.android.util.SessionKeys;

public class DeviceTokenAsyncTask extends AsyncTask<String, Context, Response> {

    private SharedPreferences sharedPerference;
    private static final String LOG_TAG = "FCM";

    public DeviceTokenAsyncTask(SharedPreferences sharedPerference) {
        this.sharedPerference = sharedPerference;
    }

    @Override
    protected Response doInBackground(String... params) {

        String  url = Property.APP_DEVICE_REGISTER_URL;
        if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
            url += "/" + SignInActivity.session.get(SessionKeys.SESSION_ID.name());
        }
        Response response = null;
        String deviceTokenJson = "";
        String token = sharedPerference.getString(Property.PREFS_FCM_TOKEN, "");
        DeviceTokenRequest deviceTokenRequest = new DeviceTokenRequest(Build.DEVICE, "android", Build.VERSION.RELEASE, token);
        Log.d(LOG_TAG, "sending deviceToken request");
        Log.d(LOG_TAG, "" + Build.DEVICE + " /" + Build.VERSION.RELEASE + " / " +  token);
        try {
            deviceTokenJson = new RestClient().post(url, deviceTokenRequest);
            response = new Gson().fromJson(deviceTokenJson, Response.class);
        } catch (RestException e) {
            Log.d(LOG_TAG, "RestException ", e);
        } catch (ConnectionRefusedException e) {
            Log.d(LOG_TAG, "ConnectionRefusedException ", e);
        } catch (Exception e) {
            Log.d(LOG_TAG, "Exception ", e);
        }
        Log.d(LOG_TAG, "JSON object recieved in Async Task\n" + deviceTokenJson);

        return response;
    }

    @Override
    protected void onPostExecute(Response result) {
        super.onPostExecute(result);
    }
}