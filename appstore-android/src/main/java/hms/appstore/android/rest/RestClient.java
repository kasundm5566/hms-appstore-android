/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.rest;

import android.content.Context;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.Arrays;

import javax.net.ssl.HttpsURLConnection;

import hms.appstore.android.R;
import hms.appstore.android.iap.proxy.connector.comm.ConnectionResult;
import hms.appstore.android.iap.proxy.connector.comm.NewSSLSocketFactory;
import hms.appstore.android.rest.request.Request;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.ProfileProperty;

public class RestClient {

    public RestClient() {
    }

    private static final int TCP_CONNECT_TIMEOUT = 25000;
    private static final int TCP_SOCKET_TIMEOUT = 120000;
    private static final String LOG_TAG = RestClient.class.getCanonicalName();

    public String post(String url, Request req) throws RestException, ConnectionRefusedException, IOException {
/*
* refactoring HttpClient to HttpURLConnection
**/
        URL requestUrl = new URL(ProfileProperty.REST_HOST + url);
        HttpURLConnection urlConnection = (HttpURLConnection) requestUrl.openConnection();
        DataOutputStream postOutStream;
        StringBuilder postResultString = null;

            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("accept", "application/json");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setDoOutput(true);
            urlConnection.connect();

            String reqSt = encode(req);
            Log.i("RestClient", "Sending rest request[" + reqSt + "]");

            postOutStream = new DataOutputStream(urlConnection.getOutputStream());
            postOutStream.writeBytes(reqSt);
            postOutStream.flush();
            postOutStream.close();
            BufferedReader postResultReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            postResultString = new StringBuilder();

            String line;
            while ((line = postResultReader.readLine()) != null) {
                postResultString.append(line);
            }
            if (urlConnection != null) {
                urlConnection.disconnect();
        }
        Log.i("Rest Client :result", postResultString.toString());
        return postResultString.toString();
    }

    public String get(String url) throws RestException, ConnectionRefusedException, MalformedURLException, IOException {
        Log.d("RestClient: GET", "^^^^^^^^^^^^^^^^^^^^^^" + ProfileProperty.REST_HOST + url);
        String fullUrl = (ProfileProperty.REST_HOST + url).replace(" ", "%20");
        URL requestUrl = new URL(fullUrl);

        HttpURLConnection urlConnection = (HttpURLConnection) requestUrl.openConnection();
        urlConnection.setRequestMethod("GET");
        urlConnection.connect();

        InputStream getRequestStream = urlConnection.getInputStream();
        BufferedReader getResultReader = new BufferedReader((new InputStreamReader(getRequestStream)));

        StringBuilder getResultString = new StringBuilder();
        String line;
        while ((line = getResultReader.readLine()) != null) {
            getResultString.append(line);
        }
        return getResultString.toString();
    }


    public String getHttp(String url) throws RestException, ConnectionRefusedException, IOException {
        Log.d("RestClient: GET", "^^^^^^^^^^^^^^^^^^^^^^" + ProfileProperty.REST_HOST + url);
        String fullUrl = (ProfileProperty.REST_HOST_HTTP + url).replace(" ", "%20");

        URL requestUrl = new URL(fullUrl);
        HttpURLConnection urlConnection = (HttpURLConnection) requestUrl.openConnection();
        urlConnection.setRequestMethod("GET");

        boolean isTestModeEnable = Boolean.parseBoolean((ProfileProperty.TEST_MODE_ENABLE));
        if (isTestModeEnable){
            String[] headers = (ProfileProperty.TEST_MODE_REST_CLIENT_HEADERS).split(",");
            for (String s: headers) {
                String[] temp = s.split(":");
                urlConnection.setRequestProperty(temp[0],temp[1]);
            }
            Log.d("AutoLogin ", "Auto login params injected");
        }
        urlConnection.connect();

        InputStream httpsRequestStream = urlConnection.getInputStream();
        BufferedReader httpsResultReader = new BufferedReader((new InputStreamReader(httpsRequestStream)));
        StringBuilder httpsResultString = new StringBuilder();

        String line;
        while ((line = httpsResultReader.readLine()) != null) {
            httpsResultString.append(line);
        }
        return httpsResultString.toString();
    }

    /*
    * In-app purchase requests
    **/
    public static ConnectionResult purchaseRequestPost(String url, String req, Context context) throws KeyStoreException, IOException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyManagementException {
        Log.d(LOG_TAG, "Executing Post : " + url);

        URL requestUrl = null;
        HttpsURLConnection urlConnection = null;
        DataOutputStream postOutStream;
        StringBuilder postResultString = null;

            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            requestUrl = new URL(url);
            urlConnection = (HttpsURLConnection) requestUrl.openConnection();

            boolean isConnectionBypass = context.getResources().getBoolean(R.bool.inapp_request_secure_bypass);
            if (isConnectionBypass){
                urlConnection.setSSLSocketFactory(new NewSSLSocketFactory(trustStore));
            }
            urlConnection.setRequestMethod("POST");
            urlConnection.setConnectTimeout(TCP_CONNECT_TIMEOUT);
            urlConnection.setReadTimeout(TCP_SOCKET_TIMEOUT);
            urlConnection.setRequestProperty("Content-Type", ProfileProperty.IAP_MESSAGE_CONTENT_TYPE);
            urlConnection.setRequestProperty("Accept", ProfileProperty.IAP_MESSAGE_ACCEPT_TYPE);
            urlConnection.connect();

            postOutStream = new DataOutputStream(urlConnection.getOutputStream());
            postOutStream.writeBytes(req);
            postOutStream.flush();
            postOutStream.close();
            BufferedReader postResultReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            postResultString = new StringBuilder();

            String line;
            while ((line = postResultReader.readLine()) != null) {
                postResultString.append(line);
            }

        ConnectionResult res = ConnectionResult.buildResult(postResultString.toString());
        return res;
    }

    public static ConnectionResult purchaseRequestGet(String url) throws IOException {
        Log.d(LOG_TAG, "Executing Get : " + url);

        URL requestUrl = null;
        StringBuilder getResultString = null;

            requestUrl = new URL(url);
            HttpURLConnection urlConnection = (HttpURLConnection) requestUrl.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            InputStream getRequestStream = urlConnection.getInputStream();
            BufferedReader getResultReader = new BufferedReader((new InputStreamReader(getRequestStream)));

            getResultString = new StringBuilder();
            String line;
            while ((line = getResultReader.readLine()) != null) {
                getResultString.append(line);
            }

        return ConnectionResult.buildResult(getResultString.toString());
    }

    private String encode(Request req) {
        Gson gson = new Gson();
        return gson.toJson(req);
    }

}