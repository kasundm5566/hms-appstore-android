/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.rest.response;

import java.util.List;

public class DownloadableBinaryDetails {
    private String osVersion;
    private int apiLevel= -1;
    private List<DownloadableBinary> downloadableBinary;

    public DownloadableBinaryDetails(String osVersion, List<DownloadableBinary> downloadableBinary) {
        this.osVersion = osVersion;
        this.downloadableBinary = downloadableBinary;
    }

    public int getApiLevel() {
        return apiLevel;
    }

    public void setApiLevel(int apiLevel) {
        this.apiLevel = apiLevel;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public List<DownloadableBinary> getDownloadableBinary() {
        return downloadableBinary;
    }

    public void setDownloadableBinary(List<DownloadableBinary> downloadableBinary) {
        this.downloadableBinary = downloadableBinary;
    }
}
