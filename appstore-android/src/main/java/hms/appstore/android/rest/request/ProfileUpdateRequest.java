package hms.appstore.android.rest.request;

public class ProfileUpdateRequest extends Request {

    private String msisdn;
    private String image;
    private String lastName;
    private String firstName;
    private String email;

    public ProfileUpdateRequest(String msisdn, String image, String firstName, String lastName, String email) {
        this.msisdn = msisdn;
        this.image = image;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
