/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.rest.response;

import android.content.Context;
import android.content.res.Resources;
import hms.appstore.android.R;

public class ErrorCode {
    public static String getErrorMessage(String errorCode, Context context) {
        Resources res = context.getResources();
        try {
            int resId = res.getIdentifier(errorCode, "string", context.getPackageName());
            return res.getString(resId);
        } catch (Exception ex) {
            return res.getString(R.string.default_error);
        }
    }
}