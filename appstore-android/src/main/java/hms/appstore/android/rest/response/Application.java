/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.rest.response;

import hms.appstore.android.util.UserReview;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class Application implements Serializable {

    private String id;
    private String name;
    private String category;
    @SerializedName("app-icon")
    private String appIcon;
    private long usage;
    private String currency;
    private String developer;
    @SerializedName("rating")
    private long rating;
    private long subscriptionsCount;
    @SerializedName("rate_count")
    private long rateCount;
    @SerializedName("downloadsCount")
    private long downloadsCount;
    private String[] labels;
    private String description;
    @SerializedName("short-description")
    private String shortDescription;
    @SerializedName("app-types")
    private String[] appTypes;
    @SerializedName("app-screenshots")
    private Screenshot[] appScreenShots;
    @SerializedName("app-banners")
    private Banner[] appBanners;
    private HashMap<String, String> instructions;
    private String[] ncs;
    private boolean subscription;
    private boolean downloadable;
    @SerializedName("download-requires-subscription")
    private boolean downloadRequiresSubscription;
    @SerializedName("requested-date")
    private String requestedDate;
    @SerializedName("subscription-status")
    private String subscriptionStatus;
    @SerializedName("charging-label")
    private String chargingLabel;
    @SerializedName("charging-details")
    private String chargingDetails;
    @SerializedName("user-comments")
    private UserReview[] userComments;
    @SerializedName("download-status")
    private DownloadStatus downloadStatus;
    @SerializedName("downloadable-binaries")
    private HashMap<String, ArrayList<HashMap<String, ArrayList<DownloadableBinary>>>> downloadableBinaries;
    @SerializedName("charging-details-with-operators")
    private HashMap<String, String> chargingDetailsWithOperators;
    @SerializedName("operators")
    private String[] operators;

    public String[] getOperators() {
        return operators;
    }

    public void setOperators(String[] operators) {
        this.operators = operators;
    }

    public HashMap<String, String> getChargingDetailsWithOperators() {
        return chargingDetailsWithOperators;
    }

    public void setChargingDetailsWithOperators(HashMap<String, String> chargingDetailsWithOperators) {
        this.chargingDetailsWithOperators = chargingDetailsWithOperators;
    }

    public DownloadStatus getDownloadStatus() {
        return downloadStatus;
    }

    public void setDownloadStatus(DownloadStatus downloadStatus) {
        this.downloadStatus = downloadStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAppIcon() {
        return appIcon;
    }

    public void setAppIcon(String appIcon) {
        this.appIcon = appIcon;
    }

    public long getUsage() {
        return usage;
    }

    public void setUsage(long usage) {
        this.usage = usage;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public long getRating() {
        return (int)rating;
    }

    public void setRating(long rating) {
        this.rating = rating;
    }

    public String[] getLabels() {
        return labels;
    }

    public void setLabels(String[] labels) {
        this.labels = labels;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String[] getAppTypes() {
        return appTypes;
    }

    public void setAppTypes(String[] appTypes) {
        this.appTypes = appTypes;
    }

    public Screenshot[] getAppScreenShots() {
        return appScreenShots;
    }

    public void setAppScreenShots(Screenshot[] appScreenShots) {
        this.appScreenShots = appScreenShots;
    }

    public HashMap<String, String> getInstructions() {
        return instructions;
    }

    public void setInstructions(HashMap<String, String> instructions) {
        this.instructions = instructions;
    }

    public String[] getNcs() {
        return ncs;
    }

    public void setNcs(String[] ncs) {
        this.ncs = ncs;
    }

    public boolean isSubscription() {
        return subscription;
    }

    public void setSubscription(boolean subscription) {
        this.subscription = subscription;
    }

    public boolean isDownloadable() {
        return downloadable;
    }

    public void setDownloadable(boolean downloadable) {
        this.downloadable = downloadable;
    }

    public boolean isDownloadRequiresSubscription() {
        return downloadRequiresSubscription;
    }

    public void setDownloadRequiresSubscription(boolean downloadRequiresSubscription) {
        this.downloadRequiresSubscription = downloadRequiresSubscription;
    }

    public String getSubscriptionStatus() {
        return subscriptionStatus;
    }

    public void setSubscriptionStatus(String subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }

    public String getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(String requestedDate) {
        this.requestedDate = requestedDate;
    }

    public String getChargingLabel() {
        return chargingLabel;
    }

    public void setChargingLabel(String chargingLabel) {
        this.chargingLabel = chargingLabel;
    }

    public String getChargingDetails() {
        return chargingDetails;
    }

    public void setChargingDetails(String chargingDetails) {
        this.chargingDetails = chargingDetails;
    }

    public HashMap<String, ArrayList<HashMap<String, ArrayList<DownloadableBinary>>>> getDownloadableBinaries() {
        return downloadableBinaries;
    }

    public void setDownloadableBinaries(HashMap<String, ArrayList<HashMap<String, ArrayList<DownloadableBinary>>>> downloadableBinaries) {
        this.downloadableBinaries = downloadableBinaries;
    }

    public UserReview[] getUserComments() {
        return userComments;
    }

    public void setUserComments(UserReview[] userComments) {
        this.userComments = userComments;
    }

    public Banner[] getAppBanners() { return appBanners; }

    public void setAppBanners(Banner[] appBanners) { this.appBanners = appBanners; }

    public class DownloadStatus implements Serializable{

        @SerializedName("download-status")
        private String status;
        @SerializedName("requested-date")
        private String requestedDate;
        @SerializedName("content-id")
        private String contentId;

        public void setContentId(String contentId) {
            this.contentId = contentId;
        }

        public String getContentId() {
            return contentId;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public void setRequestedDate(String requestedDate) {
            this.requestedDate = requestedDate;
        }

        public String getStatus() {
            return status;
        }

        public String getRequestedDate() {
            return requestedDate;
        }
    }

    public long getDownloadsCount() {
        return downloadsCount;
    }

    public void setDownloadsCount(long downloadsCount) {
        this.downloadsCount = downloadsCount;
    }

    public long getRateCount() {
        return rateCount;
    }

    public void setRateCount(long rateCount) {
        this.rateCount = rateCount;
    }

    public long getSubscriptionsCount() {
        return subscriptionsCount;
    }

    public void setSubscriptionsCount(long subscriptionsCount) {
        this.subscriptionsCount = subscriptionsCount;
    }
}
