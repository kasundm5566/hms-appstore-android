package hms.appstore.android.rest.request;

public class RatingRequest extends Request {

    private String appId;
    private float rating;

    public RatingRequest(String appId, float rating) {
        this.appId = appId;
        this.rating = rating;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}
