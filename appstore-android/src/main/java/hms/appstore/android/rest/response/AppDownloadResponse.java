/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.rest.response;

public class AppDownloadResponse extends Response {

    private String wapUrl;
    private AvailablePI[] paymentInstrumentList;
    private String downloadRequestId;

    public AvailablePI[] getPaymentInstrumentList() {
        return paymentInstrumentList;
    }

    public void setPaymentInstrumentList(AvailablePI[] paymentInstrumentList) {
        this.paymentInstrumentList = paymentInstrumentList;
    }

    public String getDownloadRequestId() {
        return downloadRequestId;
    }

    public void setDownloadRequestId(String downloadRequestId) {
        this.downloadRequestId = downloadRequestId;
    }

    public String getWapUrl() {
        return wapUrl;
    }

    public void setWapUrl(String wapUrl) {
        this.wapUrl = wapUrl;
    }

    public class AvailablePI{
        String name;
        String type;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getName() {

            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
