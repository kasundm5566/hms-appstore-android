package hms.appstore.android.rest.request;

import com.google.gson.annotations.SerializedName;

public class AccountVerifyRequest extends Request {

    private String username;
    @SerializedName("msisdn-verification-code")
    private String verificationCode;

    public AccountVerifyRequest(String username, String verificationCode) {
        this.username = username;
        this.verificationCode = verificationCode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }
}
