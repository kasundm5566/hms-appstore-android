package hms.appstore.android.rest.request;

public class DeviceTokenRequest extends Request {

    private String sessionId;
    private String deviceId;
    private String platform;
    private String platformVersion;
    private String notificationToken;

    public DeviceTokenRequest(String deviceId, String platform, String platformVersion, String notificationToken) {
        this.deviceId = deviceId;
        this.platform = platform;
        this.platformVersion = platformVersion;
        this.notificationToken = notificationToken;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getPlatformVersion() {
        return platformVersion;
    }

    public void setPlatformVersion(String platformVersion) {
        this.platformVersion = platformVersion;
    }

    public String getNotificationToken() {
        return notificationToken;
    }

    public void setNotificationToken(String notificationToken) {
        this.notificationToken = notificationToken;
    }
}
