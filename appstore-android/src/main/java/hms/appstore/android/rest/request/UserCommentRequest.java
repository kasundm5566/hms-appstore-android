/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.rest.request;

import com.google.gson.annotations.SerializedName;

public class UserCommentRequest extends Request {

    @SerializedName("app-id")
    private String appId;
    private String comments;
    @SerializedName("date-time")
    private long dateTime;
    private String username;

    public UserCommentRequest() {
    }

    public UserCommentRequest(String appId, String username, String comments, long dateTime) {
        this.appId = appId;
        this.username = username;
        this.comments = comments;
        this.dateTime = dateTime;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
