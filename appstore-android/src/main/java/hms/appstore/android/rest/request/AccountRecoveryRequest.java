package hms.appstore.android.rest.request;

import com.google.gson.annotations.SerializedName;

public class AccountRecoveryRequest extends Request  {

    @SerializedName("recover-text")
    private String recoverText;

    public AccountRecoveryRequest(String recoverText) {
        this.recoverText = recoverText;
    }

    public String getRecoverText() {
        return recoverText;
    }

    public void setRecoverText(String recoverText) {
        this.recoverText = recoverText;
    }
}
