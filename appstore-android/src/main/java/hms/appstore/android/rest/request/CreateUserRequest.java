package hms.appstore.android.rest.request;

import com.google.gson.annotations.SerializedName;

public class CreateUserRequest extends Request {

    @SerializedName("first-name")
    private String firstName;
    @SerializedName("last-name")
    private String lastName;
    private String username;
    private String password;
    private String msisdn;
    private String email;
    @SerializedName("birth-date")
    private String birthDate;
    private String domain;
    private String operator;
    @SerializedName("verify-msisdn")
    private boolean verifyMsisdn;

    public CreateUserRequest() {}

    public CreateUserRequest(String firstName,
                             String lastName,
                             String username,
                             String password,
                             String msisdn,
                             String email,
                             String birthDate,
                             String domain,
                             String operator,
                             boolean verifyMsisdn) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.msisdn = msisdn;
        this.email = email;
        this.birthDate = birthDate;
        this.domain = domain;
        this.operator = operator;
        this.verifyMsisdn = verifyMsisdn;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public boolean isVerifyMsisdn() {
        return verifyMsisdn;
    }

    public void setVerifyMsisdn(boolean verifyMsisdn) {
        this.verifyMsisdn = verifyMsisdn;
    }
}
