/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.rest.response;

public class AuthProviderResponse {

    private String[] availableProviders;
    private String preferredProvider;

    public String getPreferredProvider() {
        return preferredProvider;
    }

    public String[] getAvailableProviders() {

        return availableProviders;
    }

    public void setAvailableProviders(String[] availableProviders) {
        this.availableProviders = availableProviders;
    }
}
