package hms.appstore.android.rest.response;

import hms.appstore.android.util.widget.PushMessage;

public class PushNotificationResponse extends Response {

   private PushMessage [] messages;

    public PushMessage[] getMessages() {
        return messages;
    }

    public void setMessages(PushMessage[] messages) {
        this.messages = messages;
    }
}

