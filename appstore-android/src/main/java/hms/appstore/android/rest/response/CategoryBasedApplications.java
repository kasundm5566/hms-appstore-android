/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.rest.response;

import com.google.gson.annotations.SerializedName;

public class CategoryBasedApplications {

    @SerializedName("featured")
    private Application[] featuredApps;

    @SerializedName("newly-added")
    private Application[] newlyAddedApps;

    @SerializedName("top-rated")
    private Application[] topRatedApps;

    @SerializedName("mostly-used")
    private Application[] mostlyUsedApps;

    @SerializedName("free")
    private Application[] freeApps;

    public Application[] getFeaturedApps() {
        return featuredApps;
    }

    public Application[] getNewlyAddedApps() {
        return newlyAddedApps;
    }

    public Application[] getTopRatedApps() {
        return topRatedApps;
    }

    public Application[] getMostlyUsedApps() {
        return mostlyUsedApps;
    }

    public Application[] getFreeApps() {
        return freeApps;
    }

    public void setFeaturedApps(Application[] featuredApps) {
        this.featuredApps = featuredApps;
    }

    public void setNewlyAddedApps(Application[] newlyAddedApps) {
        this.newlyAddedApps = newlyAddedApps;
    }

    public void setTopRatedApps(Application[] topRatedApps) {
        this.topRatedApps = topRatedApps;
    }

    public void setMostlyUsedApps(Application[] mostlyUsedApps) {
        this.mostlyUsedApps = mostlyUsedApps;
    }

    public void setFreeApps(Application[] freeApps) {
        this.freeApps = freeApps;
    }
}
