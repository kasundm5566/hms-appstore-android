package hms.appstore.android.rest.response;

public class CategoriesBanner extends Response {

    private LocationBanner[] results;

    public LocationBanner[] getResults() {
        return results;
    }

    public void setResults(LocationBanner[] results) {
        this.results = results;
    }
}
