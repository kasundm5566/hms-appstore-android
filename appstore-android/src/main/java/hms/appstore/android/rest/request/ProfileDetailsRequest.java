package hms.appstore.android.rest.request;

public class ProfileDetailsRequest extends Request {

    private String username;

    public ProfileDetailsRequest(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
