package hms.appstore.android.rest.request;

public class GetVerifyCodeRequest extends Request {

    private String msisdn;
    private String username;

    public GetVerifyCodeRequest(String msisdn, String username) {
        this.msisdn = msisdn;
        this.username = username;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
