package hms.appstore.android.rest.response;

import com.google.gson.annotations.SerializedName;

public class AppUpdateCountResponse extends Response {

    @SerializedName("count")
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}