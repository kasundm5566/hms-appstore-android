/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.rest.response;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class SignInResponse extends Response {
    @SerializedName("sessionId")
    private String sessionId;
    @SerializedName("mobileNo")
    private String msisdn;
    @SerializedName("additionalParameters")
    private Map additionalParameters;

    public boolean isMsisdnVerified() {
        if (additionalParameters !=null && additionalParameters.get("msisdn-verified") != null){
            return additionalParameters.get("msisdn-verified").equals("true");
        }
        else {
            return true;
        }
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
}
