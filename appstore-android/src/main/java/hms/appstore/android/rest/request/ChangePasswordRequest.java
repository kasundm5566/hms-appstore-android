package hms.appstore.android.rest.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by pasindu on 11/19/14.
 */
public class ChangePasswordRequest  extends Request  {

    private String username;
    @SerializedName("old-password")
    private String oldPassword;
    @SerializedName("new-password")
    private String newPassword;

    public ChangePasswordRequest(String username, String oldPassword, String newPassword) {
        this.username = username;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
