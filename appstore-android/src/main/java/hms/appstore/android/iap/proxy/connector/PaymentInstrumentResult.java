/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.iap.proxy.connector;

import java.util.ArrayList;

public class PaymentInstrumentResult {

    private ArrayList<String> paymentInstrumentList;
    private String error;

    public PaymentInstrumentResult(ArrayList<String> result, String error) {
        this.error = error;
        this.paymentInstrumentList = result;
    }

    public static PaymentInstrumentResult buildResult(ArrayList<String> result) {
        return new PaymentInstrumentResult(result, null);

    }

    public static PaymentInstrumentResult buildError(String error) {
        return new PaymentInstrumentResult(new ArrayList<String>(), error);
    }

    public ArrayList<String> getResult() {
        return paymentInstrumentList;
    }

    public String getError() {
        return error;
    }
}
