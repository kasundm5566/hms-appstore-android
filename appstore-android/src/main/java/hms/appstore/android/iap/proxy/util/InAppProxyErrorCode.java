/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.iap.proxy.util;

import static hms.appstore.android.util.Property.*;

public enum InAppProxyErrorCode {

    INTERNAL_ERROR
            (IAP_INTERNAL_ERROR_STATUS, IAP_INTERNAL_ERROR_DISPLAY_TXT, IAP_INTERNAL_ERROR_DETAIL),
    USER_CANCELED
            (IAP_USER_CANCELLED_STATUS, IAP_USER_CANCELLED_DISPLAY_TXT, IAP_USER_CANCELLED_DETAIL),
    INVALID_KEY
            (IAP_INVALID_KEY_STATUS, IAP_INVALID_KEY_DISPLAY_TXT, IAP_INVALID_KEY_DETAIL),
    CONNECTION_ERROR
            (IAP_CONNECTION_ERROR_STATUS, IAP_CONNECTION_ERROR_DISPLAY_TXT, IAP_CONNECTION_ERROR_DETAIL),
    NO_PAYMENT_INSTRUMENTS
            (IAP_NO_PI_STATUS, IAP_NO_PI_DISPLAY_TXT, IAP_NO_PI_DETAIL),
    PAYMENT_INSTRUMENTS_INACTIVE_APP
            (IAP_PI_INACTIVE_STATUS, IAP_PI_INACTIVE_DISPLAY_TXT, IAP_PI_INACTIVE_DETAIL),
    PAYMENT_INSTRUMENT_ERROR
            (IAP_PI_ERROR_STATUS, IAP_PI_ERROR_DISPLAY_TXT, IAP_PI_ERROR_DETAIL),
    APP_STORE_BUSY
            (IAP_APPSTORE_BUSY_STATUS, IAP_APPSTORE_BUSY_DISPLAY_TXT, "");

    private final String errorCode;
    private String errorDisplayText;
    private String errorDetail;

    private InAppProxyErrorCode(String errorCode, String errorDetail, String errorDisplayText) {
        this.errorCode = errorCode;
        this.errorDetail = errorDetail;
        this.errorDisplayText = errorDisplayText;
    }

    public String code() {
        return errorCode;
    }

    public String detail() {
        return errorDetail;
    }

    public String displayText() {
        return errorDisplayText;
    }
}
