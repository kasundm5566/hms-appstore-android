/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.iap.proxy.listener;

public enum InAppProcessorState {
    INITIAL,
    WAITING_FOR_USER_CONFIRMATION,
    USER_OKAY,
    USER_CANCELED,
    WAITING_FOR_USER_AUTHENTICATION,
    USER_AUTHENTICATED,
    WAITING_FOR_USER_PI,
    USER_READY,
    PURCHASE_COMPLETED,
    ERROR_OCCURRED
}

