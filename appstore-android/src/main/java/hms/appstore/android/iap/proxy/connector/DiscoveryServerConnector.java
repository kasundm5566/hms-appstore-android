/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.iap.proxy.connector;

import android.content.Context;
import android.content.Intent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import hms.appstore.android.activity.user.SignInActivity;
import hms.appstore.android.iap.proxy.connector.comm.ConnectionResult;
import hms.appstore.android.iap.proxy.util.InAppKeys;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.ProfileProperty;
import hms.appstore.android.util.Property;
import hms.appstore.android.util.SessionKeys;

public class DiscoveryServerConnector {

    private static final String LOG_TAG = DiscoveryServerConnector.class.getCanonicalName();

    public static void querySessionId(Context baseContext, DiscoverySessionListener listener) {
        Log.d(LOG_TAG, "Querying sessionID within AppStore...");
        if (SignInActivity.session.containsKey(SessionKeys.SESSION_ID.name())) {
            Log.d(LOG_TAG, "User Already logged in, Sending sessionId");
            listener.onSessionIdAvailable(SignInActivity.session.get(SessionKeys.SESSION_ID.name()));
        } else {
            Log.d(LOG_TAG, "User not logged in, Starting SignInActivity");
            Intent i = new Intent(baseContext, SignInActivity.class);
            i.putExtra(InAppKeys.INAPP_SIGNIN, InAppKeys.INAPP_SIGNIN);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            baseContext.startActivity(i);
        }
    }

    public static PaymentInstrumentResult queryPaymentInstruments(String sessionId, String appId) throws Exception {

        ArrayList<String> paymentInstrumentList = new ArrayList<>();
        String error = null;

        Log.d(LOG_TAG, "Querying Payment List from  Discovery API...");
        String url = String.format((ProfileProperty.REST_HOST + Property.PI_LIST_URL), sessionId, appId);
        RestClient clientConnection;
        ConnectionResult connectionResult;

        clientConnection = new RestClient();
        connectionResult = RestClient.purchaseRequestGet(url);

        if (connectionResult.getResult() != null) {
            String response = connectionResult.getResult();
            Log.d(LOG_TAG, "Received Discovery API connection results");
            try {
                JSONObject discoveryResults = new JSONObject(response);
                String statusCode = discoveryResults.getString("statusCode");
                //Do not chane it to switch-case: does not work with java1.6 inside android dalvik.
                if ("S1000".equals(statusCode)) {
                    JSONArray paymentJSONArray = discoveryResults.getJSONArray("paymentInstrumentList");
                    for (int i = 0; i < paymentJSONArray.length(); i++) {
                        JSONObject paymentInstrument = new JSONObject(paymentJSONArray.get(i).toString());
                        paymentInstrumentList.add(paymentInstrument.getString("name"));
                    }
                    Log.d(LOG_TAG, "The payment list retrieved");
                } else if ("E5500".equals(statusCode)) {
                    //application has no payment instruments, may be malicious app.
                    error = "E5500";
                } else {
                    Log.e(LOG_TAG, "Discovery API connection failure " + statusCode);
                    error = "Discovery API connection failure " + statusCode;
                }
            } catch (JSONException e) {
                Log.e(LOG_TAG, "Malformed DiscoveryAPI results ", e);
                error = "Malformed Discovery API results";
            }
        } else {
            Log.d(LOG_TAG, "Unable to Connect DiscoveryAPI to query payment instruments" + connectionResult.getError());
            error = "Unable to Connect DiscoveryAPI to query payment instruments" + connectionResult.getError();
        }

        if (error == null) {
            return PaymentInstrumentResult.buildResult(paymentInstrumentList);
        } else {
            return PaymentInstrumentResult.buildError(error);
        }
    }
}
