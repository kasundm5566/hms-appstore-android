/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.iap.proxy.connector;

public class InAppContext {
    private final String sessionId;
    private String paymentInstrumentName;
    private String pin;

    public InAppContext(String sessionId) {
        this.sessionId = sessionId;

    }

    public String getSessionId() {
        return sessionId;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getPaymentInstrumentName() {
        return paymentInstrumentName;
    }

    public void setPaymentInstrumentName(String paymentInstrumentName) {
        this.paymentInstrumentName = paymentInstrumentName;
    }


}
