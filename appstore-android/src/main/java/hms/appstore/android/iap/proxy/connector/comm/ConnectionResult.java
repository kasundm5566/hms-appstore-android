/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.iap.proxy.connector.comm;

public class ConnectionResult {
    private String result;
    private String internalError;

    public ConnectionResult(String result, String internalError) {
        this.internalError = internalError;
        this.result = result;
    }

    public static ConnectionResult buildResult(String result) {
        return new ConnectionResult(result, null);

    }

    public static ConnectionResult buildError(String error) {
        return new ConnectionResult(null, error);
    }

    public String getResult() {
        return result;
    }

    public String getError() {
        return internalError;
    }

}
