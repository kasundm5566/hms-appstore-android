/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.iap.proxy.connector;

import android.content.Context;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.spec.InvalidKeySpecException;

import hms.appstore.android.iap.proxy.connector.comm.ConnectionResult;
import hms.appstore.android.iap.proxy.protocol.InAppChargingRequest;
import hms.appstore.android.iap.proxy.protocol.InAppRequest;
import hms.appstore.android.iap.proxy.protocol.InAppResponse;
import hms.appstore.android.iap.proxy.protocol.ServiceType;
import hms.appstore.android.iap.proxy.util.InAppProxyErrorCode;
import hms.appstore.android.iap.proxy.util.ProxyResponseBuilder;
import hms.appstore.android.rest.RestClient;
import hms.appstore.android.util.Log;
import hms.appstore.android.util.ProfileProperty;


public class IapServerConnector {
    private static final String LOG_TAG = IapServerConnector.class.getCanonicalName();

    /**
     * @param context InAppContext
     * @param request InAppRequest
     * @param inAppAndroidContext
     * @return InAppResponse
     */
    public InAppResponse processRequest(InAppContext context, InAppRequest request, Context inAppAndroidContext) {

        switch (request.getServiceType()) {
            case DIRECT_DEBIT:
                Log.d(LOG_TAG, "Started processing Charging Request from Client");
                return processChargingRequest(context, (InAppChargingRequest) request, inAppAndroidContext);
            case SUBSCRIPTION:
                Log.d(LOG_TAG, "Started processing Subscription Request from Client");
                return ProxyResponseBuilder.buildErrorResponse(request.getExternalTrxId(),
                        ServiceType.DIRECT_DEBIT, InAppProxyErrorCode.INTERNAL_ERROR);
        }
        return ProxyResponseBuilder.buildErrorResponse(request.getExternalTrxId(),
                ServiceType.DIRECT_DEBIT, InAppProxyErrorCode.INTERNAL_ERROR);
    }

    /**
     * @param context              InAppContext
     * @param inAppChargingRequest InAppChargingRequest
     * @param inAppAndroidContext
     * @return InAppResponse
     */
    private InAppResponse processChargingRequest(InAppContext context, InAppChargingRequest inAppChargingRequest, Context inAppAndroidContext) {

        String signedEncodedIapRequest;
        String URL = ProfileProperty.IAP_SERVER_URL;
        ConnectionResult connectionResult = null;

        try {
            signedEncodedIapRequest = ExternalMessageMapper.convertChargingRequest(context, inAppChargingRequest);
            connectionResult = RestClient.purchaseRequestPost(URL, signedEncodedIapRequest,inAppAndroidContext);
        } catch (IOException e) {
            Log.e(LOG_TAG, "IOException", e);
            return ProxyResponseBuilder.buildErrorResponse(inAppChargingRequest.getExternalTrxId(),
                    ServiceType.DIRECT_DEBIT, InAppProxyErrorCode.INVALID_KEY);
        } catch (InvalidKeySpecException e) {
            Log.e(LOG_TAG, "InvalidKeySpecException", e);
            return ProxyResponseBuilder.buildErrorResponse(inAppChargingRequest.getExternalTrxId(),
                    ServiceType.DIRECT_DEBIT, InAppProxyErrorCode.INVALID_KEY);
        } catch (NoSuchAlgorithmException e) {
            Log.e(LOG_TAG, "NoSuchAlgorithmException", e);
            return ProxyResponseBuilder.buildErrorResponse(inAppChargingRequest.getExternalTrxId(),
                    ServiceType.DIRECT_DEBIT, InAppProxyErrorCode.INVALID_KEY);
        } catch (InvalidKeyException e) {
            Log.e(LOG_TAG, "InvalidKeyException", e);
            return ProxyResponseBuilder.buildErrorResponse(inAppChargingRequest.getExternalTrxId(),
                    ServiceType.DIRECT_DEBIT, InAppProxyErrorCode.INVALID_KEY);
        } catch (SignatureException e) {
            Log.e(LOG_TAG, "SignatureException", e);
            return ProxyResponseBuilder.buildErrorResponse(inAppChargingRequest.getExternalTrxId(),
                    ServiceType.DIRECT_DEBIT, InAppProxyErrorCode.INVALID_KEY);
        } catch (UnrecoverableKeyException e) {
            Log.e(LOG_TAG, "UnrecoverableKeyException", e);
        } catch (KeyStoreException e) {
            Log.e(LOG_TAG, "KeyStoreException", e);
        } catch (KeyManagementException e) {
            Log.e(LOG_TAG, "KeyManagementException", e);
        }
        Log.d(LOG_TAG, "Creating connection to iap-server");

        //connectionResult = RestClient.purchaseRequestPost(URL, signedEncodedIapRequest,inAppAndroidContext);


        if (connectionResult.getResult() != null) {
            String response = connectionResult.getResult();
            Log.d(LOG_TAG, "Received connection results");

            try {
                return ExternalMessageMapper.convertChargingResponse(response);
            } catch (IOException e) {
                //Errors are due to server key problem => internalError
                Log.e(LOG_TAG, "IOException", e);
                return ProxyResponseBuilder.buildErrorResponse(inAppChargingRequest.getExternalTrxId(),
                        ServiceType.DIRECT_DEBIT, InAppProxyErrorCode.INTERNAL_ERROR);
            } catch (InvalidKeySpecException e) {
                Log.e(LOG_TAG, "InvalidKeySpecException", e);
                return ProxyResponseBuilder.buildErrorResponse(inAppChargingRequest.getExternalTrxId(),
                        ServiceType.DIRECT_DEBIT,
                        InAppProxyErrorCode.INTERNAL_ERROR);
            } catch (NoSuchAlgorithmException e) {
                Log.e(LOG_TAG, "NoSuchAlgorithmException", e);
                return ProxyResponseBuilder.buildErrorResponse(inAppChargingRequest.getExternalTrxId(),
                        ServiceType.DIRECT_DEBIT,
                        InAppProxyErrorCode.INTERNAL_ERROR);
            } catch (InvalidKeyException e) {
                Log.e(LOG_TAG, "InvalidKeyException", e);
                return ProxyResponseBuilder.buildErrorResponse(inAppChargingRequest.getExternalTrxId(),
                        ServiceType.DIRECT_DEBIT,
                        InAppProxyErrorCode.INTERNAL_ERROR);
            } catch (SignatureException e) {
                Log.e(LOG_TAG, "SignatureException", e);
                return ProxyResponseBuilder.buildErrorResponse(inAppChargingRequest.getExternalTrxId(),
                        ServiceType.DIRECT_DEBIT,
                        InAppProxyErrorCode.INTERNAL_ERROR);
            }
        } else {
            Log.d(LOG_TAG, "Received connection error " + connectionResult.getError());
            return ProxyResponseBuilder.buildErrorResponseWithAppend(inAppChargingRequest.getExternalTrxId(),
                    ServiceType.DIRECT_DEBIT, InAppProxyErrorCode.CONNECTION_ERROR, connectionResult.getError(), null);
        }
    }

}
