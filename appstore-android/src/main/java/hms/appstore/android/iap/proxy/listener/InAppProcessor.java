/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.iap.proxy.listener;

import android.content.Context;
import hms.appstore.android.util.Log;
import hms.appstore.android.iap.proxy.connector.*;
import hms.appstore.android.iap.proxy.protocol.InAppChargingRequest;
import hms.appstore.android.iap.proxy.protocol.InAppChargingResponse;
import hms.appstore.android.iap.proxy.protocol.InAppResponse;
import hms.appstore.android.iap.proxy.protocol.ServiceType;
import hms.appstore.android.iap.proxy.util.InAppProxyErrorCode;
import hms.appstore.android.iap.proxy.util.ProxyResponseBuilder;

public class InAppProcessor implements DiscoverySessionListener {
    private static final String LOG_TAG = InAppProcessor.class.getCanonicalName();
    private InAppProcessorState inAppProcessorState;
    private InAppContext inAppContext;
    private Context inAppAndroidContext;


    public InAppProcessor(Context inAppListener) {
        this.inAppAndroidContext = inAppListener;
        setCurrentState(InAppProcessorState.INITIAL);
    }

    /**
     * start main business call of iap proxy to do a purchase transaction.
     *
     * @param inAppChargingRequest The Direct Debit request to be processed
     * @return InAppChargingResponse
     */
    public synchronized InAppChargingResponse sendRequest(InAppChargingRequest inAppChargingRequest) {
        //make sure no null response returns.
        InAppResponse inAppResponse = ProxyResponseBuilder.buildErrorResponse(inAppChargingRequest.getExternalTrxId(),
                ServiceType.DIRECT_DEBIT, InAppProxyErrorCode.INTERNAL_ERROR);

        try {
            Log.d(LOG_TAG, "Started purchase");
            //Change state before start activity, Start Activity async call and wait.
            setCurrentState(InAppProcessorState.WAITING_FOR_USER_CONFIRMATION);
            ProxyActivityManager.startConfirmActivity(inAppAndroidContext, inAppChargingRequest);
            Log.d(LOG_TAG, "Started Proxy Confirmation Activity");
            while (isCurrentState(InAppProcessorState.WAITING_FOR_USER_CONFIRMATION)) {
                wait();
            }
            //Execution continues after notified by onUserConfirmation() method.
            if (isCurrentState(InAppProcessorState.USER_OKAY)) {
                setCurrentState(InAppProcessorState.WAITING_FOR_USER_AUTHENTICATION);
                //calls appStore to get sessionID, if not logged in have to popup appStore sigIn in activity
                // and return back here, Async call and wait.
                DiscoveryServerConnector.querySessionId(inAppAndroidContext, this);
                while (isCurrentState(InAppProcessorState.WAITING_FOR_USER_AUTHENTICATION)) {
                    wait();
                }
                //Execution continues after notified by onSessionIdAvailable() method.
                if (isCurrentState(InAppProcessorState.USER_AUTHENTICATED)) {
                    PaymentInstrumentResult paymentInstrumentResult = DiscoveryServerConnector.queryPaymentInstruments
                            (inAppContext.getSessionId(), inAppChargingRequest.getApplicationId());
                    Log.d(LOG_TAG, "Retrieved session id from sign in Activity");
                    if (paymentInstrumentResult.getResult() != null && !paymentInstrumentResult.getResult().isEmpty()) {
                        inAppProcessorState = InAppProcessorState.WAITING_FOR_USER_PI;
                        ProxyActivityManager.startPaymentListActivity(inAppAndroidContext, inAppChargingRequest,
                                paymentInstrumentResult.getResult());
                        Log.d(LOG_TAG, "Started Proxy PaymentList Activity");
                        while (isCurrentState(InAppProcessorState.WAITING_FOR_USER_PI)) {
                            wait();
                        }
                        //Execution continues after notified by onPaymentInstrumentSelected() method.
                        if (isCurrentState(InAppProcessorState.USER_READY)) {
                            IapServerConnector iapServerConnector = new IapServerConnector();
                            inAppResponse = iapServerConnector.processRequest(this.inAppContext, inAppChargingRequest,inAppAndroidContext);
                            setCurrentState(InAppProcessorState.PURCHASE_COMPLETED);
                        }//user not selected payment instrument => USER_CANCELED/ERROR_OCCURRED
                    } else {
                        //Payment instrument result is empty or error in discovery connection
                        if (paymentInstrumentResult.getError() != null) {
                            if ("5500".equals(paymentInstrumentResult.getError())) {
                                inAppResponse = ProxyResponseBuilder.buildErrorResponseWithAppend(inAppChargingRequest
                                        .getExternalTrxId(),
                                        ServiceType.DIRECT_DEBIT, InAppProxyErrorCode.PAYMENT_INSTRUMENTS_INACTIVE_APP,
                                        paymentInstrumentResult.getError(), null);
                            } else {
                                inAppResponse = ProxyResponseBuilder.buildErrorResponseWithAppend(inAppChargingRequest
                                        .getExternalTrxId(),
                                        ServiceType.DIRECT_DEBIT, InAppProxyErrorCode.PAYMENT_INSTRUMENT_ERROR,
                                        paymentInstrumentResult.getError(), null);
                            }

                        } else {
                            inAppResponse = ProxyResponseBuilder.buildErrorResponse(inAppChargingRequest
                                    .getExternalTrxId(),
                                    ServiceType.DIRECT_DEBIT, InAppProxyErrorCode.NO_PAYMENT_INSTRUMENTS);
                        }
                        setCurrentState(InAppProcessorState.PURCHASE_COMPLETED);
                    }
                    if (isCurrentState(InAppProcessorState.PURCHASE_COMPLETED)) {
                        ProxyActivityManager.startDoneActivity
                                (inAppAndroidContext, inAppChargingRequest, inAppResponse);
                        Log.d(LOG_TAG, "Started Proxy Payment Done Activity");
                        //asynchronous return to client app ,
                        // regardless of users response from inApp Done activity okay button.
                    }
                }//if user not authenticated => USER_CANCELED/ERROR_OCCURRED
            }//if user not okay in confirmation => USER_CANCELED/ERROR_OCCURRED

        } catch (InterruptedException e) {
            Log.e(LOG_TAG, "InterruptedException", e);
            setCurrentState(InAppProcessorState.ERROR_OCCURRED);
        } catch (Exception e) {
            Log.e(LOG_TAG, "UnKnown Exception", e);
        }
        //User cancelled by whichever state come here and send cancelled response.
        if (isCurrentState(InAppProcessorState.USER_CANCELED)) {
            inAppResponse = ProxyResponseBuilder.buildErrorResponse(inAppChargingRequest.getExternalTrxId(),
                    ServiceType.DIRECT_DEBIT, InAppProxyErrorCode.USER_CANCELED);
        } else if (isCurrentState(InAppProcessorState.ERROR_OCCURRED)) {
            inAppResponse = ProxyResponseBuilder.buildErrorResponse(inAppChargingRequest.getExternalTrxId(),
                    ServiceType.DIRECT_DEBIT, InAppProxyErrorCode.INTERNAL_ERROR);
        }
        Log.d(LOG_TAG, "Purchase operation completed");
        return (InAppChargingResponse) inAppResponse;
    }

    public synchronized void onUserConfirmation(boolean response) {
        Log.d(LOG_TAG, "Received Confirmation Activity Response" + response);
        if (response) {
            inAppProcessorState = InAppProcessorState.USER_OKAY;
        } else {
            inAppProcessorState = InAppProcessorState.USER_CANCELED;
        }
        notifyAll();
    }

    public synchronized void onPaymentInstrumentSelected(String paymentInstrument, String pin) {
        Log.d(LOG_TAG, "Received PaymentList Activity Response");
        if (paymentInstrument != null) {
            this.inAppContext.setPaymentInstrumentName(paymentInstrument);
            this.inAppContext.setPin(pin);
            inAppProcessorState = InAppProcessorState.USER_READY;
        } else {
            inAppProcessorState = InAppProcessorState.USER_CANCELED;
        }
        notifyAll();
    }

    @Override
    public synchronized void onSessionIdAvailable(String sessionId) {
        Log.d(LOG_TAG, "Received Authentication Activity Response");
        if (sessionId != null) {
            this.inAppContext = new InAppContext(sessionId);
            inAppProcessorState = InAppProcessorState.USER_AUTHENTICATED;
        } else {
            inAppProcessorState = InAppProcessorState.USER_CANCELED;
        }
        notifyAll();
    }

    private void setCurrentState(InAppProcessorState inAppProcessorState) {
        this.inAppProcessorState = inAppProcessorState;
    }

    private boolean isCurrentState(InAppProcessorState inAppProcessorState) {
        return this.inAppProcessorState == inAppProcessorState;
    }

}
