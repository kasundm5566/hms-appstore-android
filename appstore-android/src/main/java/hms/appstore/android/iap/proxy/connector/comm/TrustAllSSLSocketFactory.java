///*
// * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
// * All Rights Reserved.
// *
// * These materials are unpublished, proprietary, confidential source code of
// * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
// * of hSenid Mobile Solutions (Pvt) Limited.
// *
// * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
// * property rights in these materials.
// */
//
//package hms.appstore.android.iap.proxy.connector.comm;
//
//import org.apache.http.conn.ssl.SSLSocketFactory;
//
//import java.io.IOException;
//import java.net.Socket;
//import java.security.KeyManagementException;
//import java.security.KeyStore;
//import java.security.KeyStoreException;
//import java.security.NoSuchAlgorithmException;
//import java.security.UnrecoverableKeyException;
//import java.security.cert.CertificateException;
//import java.security.cert.X509Certificate;
//
//import javax.net.ssl.SSLContext;
//import javax.net.ssl.TrustManager;
//import javax.net.ssl.X509TrustManager;
//
//public class TrustAllSSLSocketFactory extends SSLSocketFactory {
//
//    private static X509TrustManager TRUST_ALL_X509_MGR = new TrustAllX509TrustManager();
//
//    static class TrustAllX509TrustManager implements X509TrustManager {
//        @Override
//        public void checkClientTrusted(X509Certificate[] chain,
//                                       String authType) throws CertificateException {
//        }
//
//        @Override
//        public void checkServerTrusted(X509Certificate[] chain,
//                                       String authType) throws CertificateException {
//        }
//
//        @Override
//        public X509Certificate[] getAcceptedIssuers() {
//            return new X509Certificate[0];
//        }
//    }
//
//    SSLContext sslContext = SSLContext.getInstance("TLS");
//
//    public TrustAllSSLSocketFactory(KeyStore truststore)
//            throws NoSuchAlgorithmException, KeyManagementException,
//            KeyStoreException, UnrecoverableKeyException {
//        super(truststore);
//
//
//        sslContext.init(null, new TrustManager[]{TRUST_ALL_X509_MGR}, null);
//    }
//
//    @Override
//    public Socket createSocket(Socket socket, String host, int port,
//                               boolean autoClose) throws IOException {
//        return sslContext.getSocketFactory().createSocket(socket, host, port,
//                autoClose);
//    }
//
//    @Override
//    public Socket createSocket() throws IOException {
//        return sslContext.getSocketFactory().createSocket();
//    }
//
//}
