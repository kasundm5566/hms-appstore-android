/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.iap.proxy.ui;

import android.app.Activity;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import hms.appstore.android.R;
import hms.appstore.android.iap.proxy.util.InAppKeys;
import hms.appstore.android.iap.proxy.util.MessageTemplates;

public class InAppDoneActivity extends Activity implements View.OnClickListener {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_LEFT_ICON);
        setContentView(R.layout.inapp_done);
        getWindow().setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.header_icon);

        String status = getIntent().getExtras().getString(InAppKeys.STATUS_CODE);
        String displayText = getIntent().getExtras().getString(InAppKeys.DISPLAY_TEXT);
        String appCrashedMessage = getIntent().getExtras().getString(InAppKeys.APP_CRASHED_MESSAGE);
        Boolean clientAppAlive = getIntent().getExtras().getBoolean(InAppKeys.IS_CLIENT_APP_ALIVE);

        TextView doneText = (TextView) findViewById(R.id.tv_inapp_done_text);

        if (MessageTemplates.SUCCESS_STATUS_CODE.equals(status)) {

            if (!clientAppAlive) {
                //If client activity killed
                doneText.setText(displayText + "\n" + appCrashedMessage);
                doneText.setTextColor(getResources().getColor(R.color.error_text_color));
            } else {
                doneText.setText(displayText);
            }
        } else {
            doneText.setText(displayText);
            doneText.setTextColor(getResources().getColor(R.color.error_text_color));
        }
        Button okButton = (Button) findViewById(R.id.bt_inapp_done_ok);
        okButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        finish();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Rect dialogBounds = new Rect();
        getWindow().getDecorView().getHitRect(dialogBounds);

        if (!dialogBounds.contains((int) ev.getX(), (int) ev.getY())) {
            // Tapped outside so we do not do anything.
            return false;
        }
        return super.dispatchTouchEvent(ev);
    }
}