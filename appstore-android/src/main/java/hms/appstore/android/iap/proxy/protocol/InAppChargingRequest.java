/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

/**
 * InAppRequest Version 1.0
 */

package hms.appstore.android.iap.proxy.protocol;

import android.os.Parcel;

public class InAppChargingRequest extends InAppRequest{

    /**
     * Factory for creating instances of the Parcelable class.
     */
    public static final Creator<InAppChargingRequest> CREATOR = new Creator<InAppChargingRequest>() {

        /**
         * This method will be called to instantiate a InAppRequest
         * when a Parcel is received.
         * All data fields which where written during the writeToParcel
         * method should be read in the correct sequence during this method.
         */
        @Override
        public InAppChargingRequest createFromParcel(Parcel in) {
            return new InAppChargingRequest(in);
        }

        /**
         * Creates an array of our Parcelable object.
         */
        @Override
        public InAppChargingRequest[] newArray(int size) {
            return new InAppChargingRequest[size];
        }
    };
    /**
     * Name of the item purchased or consumed by the consumer
     */
    private String itemName;
    /**
     * Description of the item purchased by the consumer
     */
    private String itemDescription;
    /**
     * Amount to be charging for the item purchased
     */
    private String amount;
    /**
     * The currency of the amount
     */
    private String currency;


    public InAppChargingRequest(String applicationId,
                                String applicationDisplayName,
                                String applicationKey,
                                String externalTrxId,
                                String itemName,
                                String itemDescription,
                                String amount,
                                String currency) {
        super(ServiceType.DIRECT_DEBIT,applicationId, applicationDisplayName, applicationKey, externalTrxId);
        this.itemName = itemName;
        this.itemDescription = itemDescription;
        this.amount = amount;
        this.currency = currency;
    }

    protected InAppChargingRequest(Parcel in) {
        super(in);
        this.itemName = in.readString();
        this.itemDescription = in.readString();
        this.amount = in.readString();
        this.currency = in.readString();
    }

    @Override
    /**
     * Method which will give additional hints how to process
     * the parcel. For example there could be multiple
     * implementations of an Interface which extends the Parcelable
     * Interface. When such a parcel is received you can use
     * this to determine which object you need to instantiate.
     */
    public int describeContents() {
        return 0;            // nothing special about our content
    }

    @Override
    /**
     * Method which will be called when this object should be
     * marshaled to a Parcelable object.
     * Add all required data fields to the parcel in this
     * method.
     */
    public void writeToParcel(Parcel outParcel, int flags) {
        super.writeToParcel(outParcel, flags);
        outParcel.writeString(itemName);
        outParcel.writeString(itemDescription);
        outParcel.writeString(amount);
        outParcel.writeString(currency);
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
