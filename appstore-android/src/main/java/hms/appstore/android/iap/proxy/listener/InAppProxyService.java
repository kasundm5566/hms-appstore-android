/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.iap.proxy.listener;

import android.content.Context;
import android.os.RemoteException;
import hms.appstore.android.util.Log;

import java.util.concurrent.SynchronousQueue;

import hms.appstore.android.iap.proxy.protocol.IInAppProxyService;
import hms.appstore.android.iap.proxy.protocol.InAppChargingRequest;
import hms.appstore.android.iap.proxy.protocol.InAppChargingResponse;
import hms.appstore.android.iap.proxy.protocol.ServiceType;
import hms.appstore.android.iap.proxy.util.InAppProxyErrorCode;
import hms.appstore.android.iap.proxy.util.ProxyResponseBuilder;

public class InAppProxyService extends IInAppProxyService.Stub {
    private static final String LOG_TAG = InAppProxyService.class.getCanonicalName();
    /**
     * Static InAppProcessor which can be referenced through InApp Activities.
     */
    public volatile static InAppProcessor inAppProcessor;
    private static volatile ServiceState serviceState;
    /**
     * To keep a reference to the parent service.
     */
    private Context androidContext;


    /**
     * Construct with reference to service
     *
     * @param androidContext androidContext to start proxy popup activities.
     */
    public InAppProxyService(Context androidContext) {
        Log.d(LOG_TAG, "Creating new InAppProxyService");
        this.androidContext = androidContext;
        serviceState = ServiceState.IDLE;
    }

    @Override
    public InAppChargingResponse sendRequest(final InAppChargingRequest inAppChargingRequest) throws RemoteException {
        Log.d(LOG_TAG, "Received service call to sendRequest with InAppChargingRequest from client");
        try {
            synchronized (this) {
                if (serviceState == ServiceState.IDLE) {
                    Log.d(LOG_TAG, "Changing service state from IDLE to SINGLE_SERVING");
                    serviceState = ServiceState.SINGLE_SERVING;
                } else {
                    Log.d(LOG_TAG, "Service is busy with another request right now");
                    return (InAppChargingResponse) ProxyResponseBuilder.buildErrorResponse
                            (inAppChargingRequest.getExternalTrxId(),
                                    ServiceType.DIRECT_DEBIT, InAppProxyErrorCode.APP_STORE_BUSY);
                }
            }

            inAppProcessor = new InAppProcessor(androidContext);

            final SynchronousQueue<InAppChargingResponse> queue = new SynchronousQueue<>();

            new Thread() {
                public void run() {
                    queue.add(inAppProcessor.sendRequest(inAppChargingRequest));
                }
            }.start();

            return queue.take();
        } catch (InterruptedException e) {
            Log.e(LOG_TAG, "InterruptedException while waiting for inAppProcessor.sendRequest new thread ", e);
            return (InAppChargingResponse) ProxyResponseBuilder.buildErrorResponse
                    (inAppChargingRequest.getExternalTrxId(),
                            ServiceType.DIRECT_DEBIT, InAppProxyErrorCode.INTERNAL_ERROR);
        } finally {
            synchronized (this) {
                serviceState = ServiceState.IDLE;
                inAppProcessor = null;
            }
        }
    }

    private enum ServiceState {
        //service is idle, no client request are processed
        IDLE,
        //service is working in single service mode, only one client accepted
        SINGLE_SERVING
    }
}
