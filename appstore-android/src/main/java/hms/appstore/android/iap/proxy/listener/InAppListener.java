/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.iap.proxy.listener;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import hms.appstore.android.util.Log;

public class InAppListener extends Service {
    private static final String LOG_TAG = InAppListener.class.getCanonicalName();
    /**
     * Proxy Service descriptor to find and locate the service in in-app-proxy securely.
     */
    private static final String IAA_PROXY_SERVICE_DESCRIPTOR = "hms.intent.action.bindInAppProxyService";
    private volatile static InAppProxyService inAppProxyService;

    public static boolean isServiceAlive() {
        return inAppProxyService != null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(LOG_TAG, "The InAppListener was created.");
    }

    @Override
    public void onDestroy() {
        Log.d(LOG_TAG, "The InAppListener was destroyed.");
        super.onDestroy();
    }

    @Override
    /**
     * Asynchronous bind call return InAppProxyService instance to client by calling onServiceConnected.
     */
    public IBinder onBind(Intent intent) {
        if (IAA_PROXY_SERVICE_DESCRIPTOR.equals(intent.getAction())) {
            Log.d(LOG_TAG, "Received bind request from client.");

            inAppProxyService = new InAppProxyService(this);
            Log.d(LOG_TAG, "The InAppListener was bound to client.");
            return inAppProxyService;
        }
        Log.d(LOG_TAG, "The InAppListener was unable to bound and returning null.");
        return null;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        if (IAA_PROXY_SERVICE_DESCRIPTOR.equals(intent.getAction())) {
            Log.d(LOG_TAG, "Receiving unbind request from client.");
            inAppProxyService = null;
            Log.d(LOG_TAG, "The InAppListener was unbound to client.");
        }
        return super.onUnbind(intent);
    }
}
