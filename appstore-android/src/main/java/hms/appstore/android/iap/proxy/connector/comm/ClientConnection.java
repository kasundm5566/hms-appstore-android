///*
// * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
// * All Rights Reserved.
// *
// * These materials are unpublished, proprietary, confidential source code of
// * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
// * of hSenid Mobile Solutions (Pvt) Limited.
// *
// * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
// * property rights in these materials.
// */
//
//package hms.appstore.android.iap.proxy.connector.comm;
//
//import hms.appstore.android.util.Log;
//
//import org.apache.http.HttpEntity;
//import org.apache.http.HttpResponse;
//import org.apache.http.HttpVersion;
//import org.apache.http.NoHttpResponseException;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.client.methods.HttpRequestBase;
//import org.apache.http.conn.ConnectTimeoutException;
//import org.apache.http.conn.params.ConnManagerParams;
//import org.apache.http.conn.params.ConnPerRouteBean;
//import org.apache.http.conn.scheme.PlainSocketFactory;
//import org.apache.http.conn.scheme.Scheme;
//import org.apache.http.conn.scheme.SchemeRegistry;
//import org.apache.http.conn.ssl.SSLSocketFactory;
//import org.apache.http.entity.StringEntity;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
//import org.apache.http.params.BasicHttpParams;
//import org.apache.http.params.HttpConnectionParams;
//import org.apache.http.params.HttpParams;
//import org.apache.http.params.HttpProtocolParams;
//import org.apache.http.protocol.BasicHttpContext;
//import org.apache.http.protocol.HTTP;
//import org.apache.http.protocol.HttpContext;
//import org.apache.http.util.EntityUtils;
//
//import java.io.IOException;
//import java.net.SocketTimeoutException;
//import java.security.KeyStore;
//import java.util.Map;
//
//public class ClientConnection {
//    private static final String LOG_TAG = ClientConnection.class.getCanonicalName();
//
//    private static final int TCP_CONNECT_TIMEOUT = 25000;
//    private static final int TCP_SOCKET_TIMEOUT = 120000;
//    private static final int CONN_MGR_CONNECT_TIMEOUT = 120000;
//    private static final int BUFF_SIZE = 1024;
//    private static final int MAX_CON = 2;
//    private static final int MAX_CON_PER_ROUTE = 5;
//
//    static {
//        try {
//            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
//
//            trustStore.load(null, null);
//
//            SSLSocketFactory sf = new TrustAllSSLSocketFactory(trustStore);
//            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
//
//            SchemeRegistry registry = new SchemeRegistry();
//            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
//            registry.register(new Scheme("https", sf, 443));
//
//            HttpParams params = new BasicHttpParams();
//            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
//            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
//            HttpConnectionParams.setConnectionTimeout(params, TCP_CONNECT_TIMEOUT);
//            HttpConnectionParams.setSoTimeout(params, TCP_SOCKET_TIMEOUT);
//            HttpConnectionParams.setSocketBufferSize(params, BUFF_SIZE);
//            HttpConnectionParams.setTcpNoDelay(params, true);
//
//            ConnManagerParams.setMaxTotalConnections(params, MAX_CON);
//            ConnManagerParams.setMaxConnectionsPerRoute(params, new ConnPerRouteBean(MAX_CON_PER_ROUTE));
//
//            ConnManagerParams.setTimeout(params, CONN_MGR_CONNECT_TIMEOUT);
//
//            connectionManager = new ThreadSafeClientConnManager(params, registry);
//        } catch (Exception e) {
//            Log.e(LOG_TAG, "Unable to setup SSL connection factory", e);
//            throw new RuntimeException("Unable to setup SSL connection factory", e);
//        }
//    }
//
//    private static ThreadSafeClientConnManager connectionManager;
//
//    public ClientConnection() {
//
//    }
//
//    public ConnectionResult executePost(String url, StringEntity stringEntity, Map<String, String> headers) {
//        Log.d(LOG_TAG, "Executing Post : " + url);
//
//        HttpPost httppost = new HttpPost(url);
//        httppost.setEntity(stringEntity);
//        if (headers != null && !headers.isEmpty()) {
//            for (Map.Entry<String, String> header : headers.entrySet()) {
//                httppost.setHeader(header.getKey(), header.getValue());
//            }
//        }
//        return execute(httppost);
//    }
//
////    public ConnectionResult executeGet(String url) {
////        Log.d(LOG_TAG, "Executing Get : " + url);
////        HttpRequestBase httpGet = new HttpGet(url);
////        return execute(httpGet);
////    }
//
//    private ConnectionResult execute(HttpRequestBase httpRequest) {
//        Log.d(LOG_TAG, "Execute httpRequest");
//
//        String response = null;
//        String iapServerConnectionError = "";
//        HttpClient httpClient;
//        HttpResponse httpResponse;
//
//        try {
//            httpClient = getNewHttpClient();
//            HttpContext localContext = new BasicHttpContext();
//
//            Log.d(LOG_TAG, "Started executing httpRequest");
//            httpResponse = httpClient.execute(httpRequest, localContext);
//            Log.d(LOG_TAG, "Completed executing httpRequest with status code : "
//                    + httpResponse.getStatusLine().getStatusCode());
//
//            if (httpResponse.getStatusLine().getStatusCode() == 200) {
//                HttpEntity entity = httpResponse.getEntity();
//                if (entity != null) {
//                    response = EntityUtils.toString(entity);
//                } else {
//                    iapServerConnectionError = "Empty response from server";
//                    Log.e(LOG_TAG, "empty_response");
//                }
//
//            } else {
//                iapServerConnectionError = String.valueOf(httpResponse.getStatusLine().getStatusCode());
//                Log.e(LOG_TAG, "Connection failed to " + httpRequest.getURI() + " "
//                        + httpResponse.getStatusLine().getStatusCode());
//            }
//        } catch (java.net.ConnectException e) {
//            iapServerConnectionError = "Unable to connect to the server, Please try again.";
//            Log.e(LOG_TAG, "connect_timeout", e);
//        } catch (SocketTimeoutException e) {
//            iapServerConnectionError = "Network connection error, Please try again.";
//            Log.e(LOG_TAG, "socket_timeout", e);
//        } catch (NoHttpResponseException e) {
//            iapServerConnectionError = "Internal Error, Please try again.";
//            Log.e(LOG_TAG, "Server Failed ", e);
//        } catch (IOException e) {
//            iapServerConnectionError = "Network connection error, Please try again.";
//            Log.e(LOG_TAG, "io_error :", e);
//        } catch (Exception e) {
//            iapServerConnectionError = "Internal Error, Please try again.";
//            Log.e(LOG_TAG, "Unknown Exception :", e);
//        }
//        if (response != null) {
//            return ConnectionResult.buildResult(response);
//        }
//        return ConnectionResult.buildError(iapServerConnectionError);
//    }
//
//    /**
//     * Create a New Client for SSl
//     *
//     * @return new client
//     */
//    private HttpClient getNewHttpClient() throws SocketTimeoutException,
//            ConnectTimeoutException {
//        try {
//            HttpParams params = new BasicHttpParams();
//            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
//            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
//
//            HttpConnectionParams.setConnectionTimeout(params, TCP_CONNECT_TIMEOUT);
//            HttpConnectionParams.setSoTimeout(params, TCP_SOCKET_TIMEOUT);
//            HttpConnectionParams.setSocketBufferSize(params, 1024);
//            HttpConnectionParams.setTcpNoDelay(params, true);
//
//            return new DefaultHttpClient(connectionManager, params);
//        } catch (Exception e) {
//            Log.e(LOG_TAG, "UnKnown Exception", e);
//            return new DefaultHttpClient();
//        }
//    }
//
//}