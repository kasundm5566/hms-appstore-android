/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.iap.proxy.util;

public class InAppKeys {
    public static final String INAPP_SIGNIN = "signInForInapp";
    public static final String STATUS_CODE = "inAppStatusCode";
    public static final String DISPLAY_TEXT = "displayText";
    public static final String PAYMENT_INSTRUMENT_LIST = "paymentInstrumentList";
    public static final String IS_CLIENT_APP_ALIVE = "isAppAlive";
    public static final String CONFIRM_MESSAGE = "confirmMessage";
    public static final String APP_CRASHED_MESSAGE = "crashedMessage";
}
