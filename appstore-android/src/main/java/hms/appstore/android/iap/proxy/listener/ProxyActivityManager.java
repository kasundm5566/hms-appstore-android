/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.iap.proxy.listener;

import android.content.Context;
import android.content.Intent;
import hms.appstore.android.util.Log;
import hms.appstore.android.iap.proxy.protocol.InAppChargingRequest;
import hms.appstore.android.iap.proxy.protocol.InAppRequest;
import hms.appstore.android.iap.proxy.protocol.InAppResponse;
import hms.appstore.android.iap.proxy.ui.InAppConfirmActivity;
import hms.appstore.android.iap.proxy.ui.InAppDoneActivity;
import hms.appstore.android.iap.proxy.ui.InAppPaymentListActivity;
import hms.appstore.android.iap.proxy.util.InAppKeys;
import hms.appstore.android.iap.proxy.util.MessageTemplates;

import java.util.ArrayList;

public class ProxyActivityManager {
    private static final String LOG_TAG = ProxyActivityManager.class.getCanonicalName();

    public static void startConfirmActivity(Context androidContext, InAppRequest inAppRequest) {
        Log.d(LOG_TAG, "Starting InAppConfirmActivity");
        switch (inAppRequest.getServiceType()) {
            case DIRECT_DEBIT:
                InAppChargingRequest inAppChargingRequest = (InAppChargingRequest) inAppRequest;
                Intent inAppConfirmIntent = new Intent(androidContext, InAppConfirmActivity.class);
                /**
                 * Sending request id to Activity to call back InAppProxyService reference
                 * form InAppListener's inAppProxyServiceContainer
                 */
                String message = String.format(MessageTemplates.CONFIRM_MESSAGE_TEMPLATE,
                        inAppChargingRequest.getItemName(),
                        inAppChargingRequest.getApplicationDisplayName(), inAppChargingRequest.getCurrency(),
                        inAppChargingRequest.getAmount());
                inAppConfirmIntent.putExtra(InAppKeys.CONFIRM_MESSAGE, message);
                inAppConfirmIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                inAppConfirmIntent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                androidContext.startActivity(inAppConfirmIntent);
                Log.d(LOG_TAG, "Started InAppConfirmActivity for direct debit with message: " + message);
                break;
            default:
                Log.e(LOG_TAG, "Unsupported request type",null);
                throw new IllegalStateException("Unsupported request type");
        }
    }

    public static void startPaymentListActivity(Context androidContext, InAppRequest inAppRequest,
                                                ArrayList<String> paymentInstrumentList) {
        Log.d(LOG_TAG, "Starting InAppPaymentListActivity");
        switch (inAppRequest.getServiceType()) {
            case DIRECT_DEBIT:
                Intent inAppPaymentIntent = new Intent(androidContext, InAppPaymentListActivity.class);
                inAppPaymentIntent.putExtra(InAppKeys.PAYMENT_INSTRUMENT_LIST, paymentInstrumentList);
                inAppPaymentIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                inAppPaymentIntent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                androidContext.startActivity(inAppPaymentIntent);
                Log.d(LOG_TAG, "Started InAppPaymentListActivity for direct debit with payment list: "
                        + paymentInstrumentList);
                break;
            default:
                Log.e(LOG_TAG, "Unsupported request type",null);
                throw new IllegalStateException("Unsupported request type");
        }
    }

    public static void startDoneActivity(Context androidContext, InAppRequest inAppRequest,
                                         InAppResponse inAppResponse) {
        Log.d(LOG_TAG, "Starting InAppDoneActivity");
        switch (inAppRequest.getServiceType()) {
            case DIRECT_DEBIT:
                Intent inAppDoneIntent = new Intent(androidContext, InAppDoneActivity.class);
                inAppDoneIntent.putExtra(InAppKeys.STATUS_CODE, inAppResponse.getStatusCode());
                inAppDoneIntent.putExtra(InAppKeys.DISPLAY_TEXT, inAppResponse.getDisplayableText());
                inAppDoneIntent.putExtra(InAppKeys.APP_CRASHED_MESSAGE, String.format(MessageTemplates
                        .APP_CRASHED_MESSAGE_TEMPLATE, inAppRequest.getApplicationDisplayName()));
                if (!InAppListener.isServiceAlive()) {
                    //if client app crashed before CaaS message received.
                    inAppDoneIntent.putExtra(InAppKeys.IS_CLIENT_APP_ALIVE, false);
                } else {
                    inAppDoneIntent.putExtra(InAppKeys.IS_CLIENT_APP_ALIVE, true);
                }
                inAppDoneIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                inAppDoneIntent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                androidContext.startActivity(inAppDoneIntent);
                Log.d(LOG_TAG, "Started InAppDoneActivity for direct debit with message: "
                        + inAppResponse.getDisplayableText());
                break;
            default:
                Log.e(LOG_TAG, "Unsupported request type",null);
                throw new IllegalStateException("Unsupported request type");

        }
    }
}
