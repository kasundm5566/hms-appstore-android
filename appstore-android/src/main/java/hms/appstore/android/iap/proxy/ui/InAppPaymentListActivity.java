/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.iap.proxy.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import hms.appstore.android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;
import hms.appstore.android.R;
import hms.appstore.android.iap.proxy.listener.InAppListener;
import hms.appstore.android.iap.proxy.listener.InAppProxyService;
import hms.appstore.android.iap.proxy.util.InAppKeys;
import hms.appstore.android.iap.proxy.util.MessageTemplates;

import java.util.ArrayList;


public class InAppPaymentListActivity extends Activity implements View.OnClickListener {
    private static final String LOG_TAG = InAppPaymentListActivity.class.getCanonicalName();
    TextView infoText;
    EditText paymentPIN;
    RadioGroup radioGroup;
    Button buttonConfirm;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_LEFT_ICON);
        setContentView(R.layout.inapp_payment_list);
        getWindow().setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.header_icon);

        infoText = (TextView) findViewById(R.id.tv_inapp_payment_text);
        infoText.setText(MessageTemplates.PAYMENT_SELECT_MESSAGE);

        paymentPIN = (EditText) findViewById(R.id.et_inapp_paymentlist_pin);
        paymentPIN.setHint(MessageTemplates.PAYMENT_PIN_MESSAGE);
        paymentPIN.setVisibility(View.INVISIBLE);


        ArrayList<String> paymentInstrumentList = getIntent().getExtras().getStringArrayList(InAppKeys
                .PAYMENT_INSTRUMENT_LIST);

        radioGroup = (RadioGroup) findViewById(R.id.rg_inapp_paymentlist);
        for (int i = 0; i < paymentInstrumentList.size(); i++) {
            String paymentInstrumentName = paymentInstrumentList.get(i);
            //instantiate...
            RadioButton radioButton = new RadioButton(this);

            //set the values that would otherwise hardCode in the xml.
            radioButton.setLayoutParams
                    (new RadioGroup.LayoutParams
                            (RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT));
            //label the button...

            radioButton.setText(paymentInstrumentName);
            radioButton.setTextColor(getResources().getColor(R.color.text_color));
            radioButton.setBackgroundColor(getResources().getColor(R.color.background_color_for_app));
            radioButton.setId(100 + i);
            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int selectedId = radioGroup.getCheckedRadioButtonId();
                    // find the radioButton by returned id
                    RadioButton radioButton = (RadioButton) findViewById(selectedId);
                    if (MessageTemplates.M_PAISA.equals(radioButton.getText())) {
                        paymentPIN.setVisibility(View.VISIBLE);
                        paymentPIN.setFocusableInTouchMode(true);
                        paymentPIN.requestFocus();
                    } else {
                        paymentPIN.setVisibility(View.INVISIBLE);
                    }
                }
            });
            //add it to the group.
            radioGroup.addView(radioButton, i);
        }
        buttonConfirm = (Button) findViewById(R.id.bt_inapp_payment_ok);
        buttonConfirm.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        //on Confirm button click
        Animation fadeIn = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_in);
        buttonConfirm.startAnimation(fadeIn);

        if (InAppListener.isServiceAlive()) {
            //ensure the called client activity not killed
            Log.d(LOG_TAG, "User PaymentList Activity selected");
            // get selected radio button from radioGroup
            int selectedId = radioGroup.getCheckedRadioButtonId();
            // find the radioButton by returned id
            RadioButton radioButton = (RadioButton) findViewById(selectedId);
            if (radioButton == null) {
                // user not selected but clicks confirm
                infoText.setTextColor(getResources().getColor(R.color.selected_color_for_titles));
                infoText.startAnimation(fadeIn);
            } else {
                if (MessageTemplates.M_PAISA.equals(radioButton.getText()) && "".equals(paymentPIN.getText().toString())) {
                    //user selected M-PaiSa but not entered the pin number, pressing confirm button.
                    paymentPIN.setHintTextColor(getResources().getColor(R.color.selected_color_for_titles));
                    paymentPIN.startAnimation(fadeIn);
                } else {
                    InAppProxyService.inAppProcessor.onPaymentInstrumentSelected
                            (radioButton.getText().toString(), paymentPIN.getText().toString());
                    finish();
                }
            }
        } else {
            //if the called activity killed
            finish();
            Log.d(LOG_TAG, "Client Activity closed, Stopping InApp Activity");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!InAppListener.isServiceAlive()) {
            //if the called activity killed
            finish();
            Log.d(LOG_TAG, "Client Activity closed, Stopping InApp Activity");
        }
    }

    @TargetApi(Build.VERSION_CODES.ECLAIR)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (InAppListener.isServiceAlive()) {
            //ensure the called client activity not killed
            InAppProxyService.inAppProcessor.onPaymentInstrumentSelected(null, null);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Rect dialogBounds = new Rect();
        getWindow().getDecorView().getHitRect(dialogBounds);

        if (!dialogBounds.contains((int) ev.getX(), (int) ev.getY())) {
            // Tapped outside so we do not do anything.
            return false;
        }
        return super.dispatchTouchEvent(ev);
    }
}