/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.iap.proxy.util;

public class MessageTemplates {
    //Confirmation activity messages
    // Press Continue to confirm the purchase of <itemName> from <appName> for <currency> <amount>
    public static final String CONFIRM_MESSAGE_TEMPLATE = "Press Continue to confirm the purchase of %s from %s" +
            " for %s %s";
    //Payment list selection activity messages
    public static final String M_PAISA = "M-PAiSA";
    public static final String PAYMENT_SELECT_MESSAGE = "Please select your payment method";
    public static final String PAYMENT_PIN_MESSAGE = M_PAISA + " Subscriber PIN?";
    //InApp Done activity messages.
    public static final String SUCCESS_STATUS_CODE = "S1000";

    public static final String APP_CRASHED_MESSAGE_TEMPLATE = "Unfortunately,  %s  is not responding, " +
            "Requested feature may not be enabled %n Please contact Customer Care";
}
