/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.iap.proxy.util;


import hms.appstore.android.iap.proxy.protocol.InAppChargingResponse;
import hms.appstore.android.iap.proxy.protocol.InAppResponse;
import hms.appstore.android.iap.proxy.protocol.ServiceType;
import hms.appstore.android.util.ProfileProperty;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Generate response for errors on in-app-proxy/AppStore.
 */
public class ProxyResponseBuilder {

    private static String getUTCTime() {
        SimpleDateFormat sdf = new SimpleDateFormat(ProfileProperty.ISO_DATETIME_FORMAT);
        return sdf.format(new Date());
    }

    public static InAppResponse buildErrorResponse(String externalTrxId, ServiceType serviceType,
                                                   InAppProxyErrorCode inAppProxyErrorCode) {
        return buildErrorResponse(externalTrxId, serviceType, inAppProxyErrorCode, "", "");
    }

    public static InAppResponse buildErrorResponseWithAppend(String externalTrxId, ServiceType serviceType,
                                                             InAppProxyErrorCode inAppProxyErrorCode, String appendDetail,
                                                             String appendDisplayText) {
        if (appendDetail == null) {
            appendDetail = "";
        }
        if (appendDisplayText == null) {
            appendDisplayText = "";
        }
        return buildErrorResponse(externalTrxId, serviceType, inAppProxyErrorCode, appendDetail, appendDisplayText);
    }

    private static InAppResponse buildErrorResponse(String externalTrxId, ServiceType serviceType,
                                                    InAppProxyErrorCode inAppProxyErrorCode, String appendDetail,
                                                    String appendDisplayText) {
        switch (serviceType) {
            case DIRECT_DEBIT:
                return new InAppChargingResponse(externalTrxId, getUTCTime(), inAppProxyErrorCode.code(),
                        inAppProxyErrorCode.detail() + appendDetail,
                        inAppProxyErrorCode.displayText() + appendDisplayText, null, null);
        }
        return new InAppChargingResponse(externalTrxId, getUTCTime(), inAppProxyErrorCode.code(),
                inAppProxyErrorCode.detail(),
                inAppProxyErrorCode.displayText(), null, null);
    }


}
