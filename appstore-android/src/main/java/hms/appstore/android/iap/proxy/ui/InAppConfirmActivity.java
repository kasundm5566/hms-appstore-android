/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.iap.proxy.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import hms.appstore.android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import hms.appstore.android.R;
import hms.appstore.android.iap.proxy.listener.InAppListener;
import hms.appstore.android.iap.proxy.listener.InAppProxyService;
import hms.appstore.android.iap.proxy.util.InAppKeys;


public class InAppConfirmActivity extends Activity implements View.OnClickListener {
    private static final String LOG_TAG = InAppConfirmActivity.class.getCanonicalName();
    private Button buttonOK;
    private Button buttonCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_LEFT_ICON);
        setContentView(R.layout.inapp_confirm);
        getWindow().setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.header_icon);

        String message = null;
        if (getIntent().hasExtra(InAppKeys.CONFIRM_MESSAGE)) {
            //noinspection ConstantConditions
            message = getIntent().getExtras().getString(InAppKeys.CONFIRM_MESSAGE);
        }

        TextView textView = (TextView) findViewById(R.id.tv_inapp_confirm_text);
        textView.setText(message);

        buttonOK = (Button) findViewById(R.id.bt_inapp_confirm_ok);
        buttonOK.setOnClickListener(this);

        buttonCancel = (Button) findViewById(R.id.bt_inapp_confirm_cancel);
        buttonCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Animation fadeIn = AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_in);

        if (InAppListener.isServiceAlive()) {
            //ensure the called client activity not killed
            if (v.getId() == R.id.bt_inapp_confirm_ok) {
                Log.d(LOG_TAG, "User Confirmation Activity OK pressed");
                if (InAppListener.isServiceAlive()) {
                    //ensure the called client activity not killed
                    InAppProxyService.inAppProcessor.onUserConfirmation(true);
                }
                buttonOK.startAnimation(fadeIn);
                buttonOK.setEnabled(false);
                buttonCancel.setEnabled(false);
            } else if (v.getId() == R.id.bt_inapp_confirm_cancel) {
                Log.d(LOG_TAG, "User Confirmation Activity CANCEL pressed");
                buttonCancel.startAnimation(fadeIn);
                if (InAppListener.isServiceAlive()) {
                    //ensure the called client activity not killed
                    InAppProxyService.inAppProcessor.onUserConfirmation(false);
                }
            }
            finish();
        } else {
            //if the called activity killed
            finish();
            Log.d(LOG_TAG, "Client Activity closed, Stopping InApp Activity");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!InAppListener.isServiceAlive()) {
            //if the called activity killed
            finish();
            Log.d(LOG_TAG, "Client Activity closed, Stopping InApp Activity");
        }
    }

    @TargetApi(Build.VERSION_CODES.ECLAIR)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (InAppListener.isServiceAlive()) {
            //ensure the called client activity not killed
            InAppProxyService.inAppProcessor.onUserConfirmation(false);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Rect dialogBounds = new Rect();
        getWindow().getDecorView().getHitRect(dialogBounds);

        if (!dialogBounds.contains((int) ev.getX(), (int) ev.getY())) {
            // Tapped outside so we do not do anything.
            return false;
        }
        return super.dispatchTouchEvent(ev);
    }
}