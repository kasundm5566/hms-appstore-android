/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.iap.proxy.connector;

import hms.appstore.android.util.Log;
import hms.appstore.android.iap.proxy.protocol.InAppChargingRequest;
import hms.appstore.android.iap.proxy.protocol.InAppChargingResponse;
import hms.appstore.android.iap.proxy.protocol.ServiceType;
import hms.appstore.android.iap.proxy.util.InAppProxyErrorCode;
import hms.appstore.android.iap.proxy.util.ProxyResponseBuilder;
import hms.appstore.android.util.ProfileProperty;
import hms.iap.proto.MessageDecoder;
import hms.iap.proto.MessageEncoder;
import hms.iap.proto.MessageSignature;
import hms.iap.proto.Messages;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.Map;

public class ExternalMessageMapper {
    private static final String LOG_TAG = ExternalMessageMapper.class.getCanonicalName();

    /**
     * Convert InAppRequest to the iap-server's request format and sign with application's public key,
     * convert to base64 String.
     * for charging.
     *
     * @param context              InAppContext
     * @param inAppChargingRequest InAppChargingRequest
     * @return Base64 String
     * @throws NoSuchAlgorithmException
     * @throws SignatureException
     * @throws InvalidKeySpecException
     * @throws InvalidKeyException
     * @throws IOException
     */
    public static String convertChargingRequest(InAppContext context, InAppChargingRequest inAppChargingRequest)
            throws NoSuchAlgorithmException, SignatureException, InvalidKeySpecException,
            InvalidKeyException, IOException {
        Log.d(LOG_TAG, "Converting charging request to server's request format");
        Messages.IapChargingRequest.Builder payload = Messages.IapChargingRequest
                .newBuilder()
                .setAmount(inAppChargingRequest.getAmount())
                .setCurrencyCode(inAppChargingRequest.getCurrency())
                .setItemName(inAppChargingRequest.getItemName())
                .setPaymentInstrument(context.getPaymentInstrumentName())
                .setItemDescription(inAppChargingRequest.getItemDescription());
        JSONObject additionalParams = new JSONObject();
        try {
            for (Map.Entry<String, String> entry : inAppChargingRequest.getAdditionalParams().entrySet()) {
                additionalParams.put(entry.getKey(), entry.getValue());
            }
            if (context.getPin() != null && !"".equals(context.getPin())) {
                additionalParams.put(ProfileProperty.PAYMENT_PIN, context.getPin());
            }
        } catch (JSONException e) {
            Log.d(LOG_TAG, "JSON Exception", e);
        }
        Messages.IapRequest request = Messages.IapRequest.newBuilder()
                .setApplicationId(inAppChargingRequest.getApplicationId())
                .setApplicationName(inAppChargingRequest.getApplicationDisplayName())
                .setExternalTransactionId(inAppChargingRequest.getExternalTrxId())
                .setSessionId(context.getSessionId())
                .setTimeStamp(inAppChargingRequest.getTimeStamp())
                .setAdditionalParams(additionalParams.toString())
                .setChannel(Messages.Channel.Android)
                .setServiceType(Messages.ServiceType.IapDirectDebit)
                .setExtension(Messages.IapChargingRequest.extension, payload.build())
                .build();
        Messages.IapRequest signedRequest;
        signedRequest = MessageSignature.sign(request, inAppChargingRequest.getApplicationKey());
        return MessageEncoder.encodeRequest(signedRequest);
    }

    /**
     * Verify iap-server's base64 encoded signed response and convert it to InAppResponse to send it to client.
     *
     * @param iapResponse IapResponse
     * @return InAppChargingResponse
     * @throws InvalidKeySpecException
     * @throws SignatureException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     * @throws IOException
     */
    public static InAppChargingResponse convertChargingResponse(String iapResponse) throws InvalidKeySpecException,
            SignatureException, NoSuchAlgorithmException, InvalidKeyException, IOException {

        Log.d(LOG_TAG, "Converting charging response to SDK's response format");
        InAppChargingResponse inAppChargingResponse;
        Messages.IapResponse receivedIapResponse = MessageDecoder.decodeResponse(iapResponse);
        Log.d(LOG_TAG, "received status code from iap-server" + receivedIapResponse.getStatusCode());
        Log.d(LOG_TAG, "received status detail from iap-server" + receivedIapResponse.getStatusDetail());

        Messages.IapChargingResponse iapChargingResponse =
                receivedIapResponse.getExtension(Messages.IapChargingResponse.extension);

        if (iapChargingResponse != null && MessageSignature.verify(receivedIapResponse, ProfileProperty.IAP_SERVER_KEY)) {
            Log.d(LOG_TAG, "Converting charging response to SDK's response format");
            //Server signs only if response built with a service type extension.
            inAppChargingResponse = new InAppChargingResponse(receivedIapResponse.getExternalTransactionId(),
                    receivedIapResponse.getTimeStamp(),
                    receivedIapResponse.getStatusCode(),
                    receivedIapResponse.getStatusDetail(),
                    receivedIapResponse.getDisplayableText(),
                    iapChargingResponse.getInternalTransactionId(),
                    iapChargingResponse.getReferenceId());
        } else {
            Log.d(LOG_TAG, "Server response null or unable to verify server");
            inAppChargingResponse = (InAppChargingResponse) ProxyResponseBuilder.buildErrorResponse
                    (receivedIapResponse.getExternalTransactionId(),
                            ServiceType.DIRECT_DEBIT, InAppProxyErrorCode.INTERNAL_ERROR);
        }
        return inAppChargingResponse;
    }
}
