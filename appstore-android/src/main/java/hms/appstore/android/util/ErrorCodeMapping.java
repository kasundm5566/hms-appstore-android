/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.util;

public class ErrorCodeMapping {

    public static boolean isSuccess(String statusCode) {

        boolean isSuccess = false;
        if (statusCode != null && statusCode.substring(0, 1).equalsIgnoreCase("S")) {

            isSuccess = true;
        }else if(statusCode != null && statusCode.substring(0, 1).equalsIgnoreCase("E")){
            isSuccess = false;
        }
        return isSuccess;
    }
}