package hms.appstore.android.util;

import android.content.Context;
import android.widget.EditText;
import android.widget.Toast;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import hms.appstore.android.R;

public class Commons {

    public static String convertDate(String strDate) {
        try {
            DateFormat discoveryDF = new SimpleDateFormat("yyyy-MM-dd'T'HHmmss.SSSZ");
            Date dateObj = discoveryDF.parse(strDate.replace(":", ""));
            DateFormat androidDF = new SimpleDateFormat("dd MMM yyyy");
            strDate = androidDF.format(dateObj);
        } catch (ParseException e) {
            strDate = strDate.substring(0, 10);
            e.printStackTrace();
        }
        return strDate;
    }

    public static String capitalizeFirstLtr(String word) {
        return (word.toUpperCase().substring(0, 1) + word.toLowerCase().substring(1));
    }

    public static boolean isEditTextEmpty(EditText textField, String errorMsg) {
        if (textField.getText().toString().trim().length() == 0) {
            textField.setError(errorMsg);
            return true;
        } else {
            textField.setError(null);
            return false;
        }
    }

    public static void showToastMessage(String results, Context context) {
        if (results != null) {
            Toast aToast = Toast.makeText(context, results, Toast.LENGTH_SHORT);
            hms.appstore.android.util.ToastExpander.showFor(aToast, 5000);
        } else {
            Toast.makeText(context, context.getString(R.string.E5000), Toast.LENGTH_SHORT).show();
        }
    }
}
