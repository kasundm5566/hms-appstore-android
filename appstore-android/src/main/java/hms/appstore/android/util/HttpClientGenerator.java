///*
// * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
// * All Rights Reserved.
// *
// * These materials are unpublished, proprietary, confidential source code of
// * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
// * of hSenid Mobile Solutions (Pvt) Limited.
// *
// * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
// * property rights in these materials.
// */
//
//package hms.appstore.android.util;
//
//import org.apache.http.HttpVersion;
//import org.apache.http.client.HttpClient;
//import org.apache.http.conn.ClientConnectionManager;
//import org.apache.http.conn.scheme.PlainSocketFactory;
//import org.apache.http.conn.scheme.Scheme;
//import org.apache.http.conn.scheme.SchemeRegistry;
//import org.apache.http.conn.ssl.SSLSocketFactory;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
//import org.apache.http.params.BasicHttpParams;
//import org.apache.http.params.HttpConnectionParams;
//import org.apache.http.params.HttpParams;
//import org.apache.http.params.HttpProtocolParams;
//import org.apache.http.protocol.HTTP;
//
//import java.net.HttpURLConnection;
//import java.net.URL;
//import java.security.KeyStore;
//
//public class HttpClientGenerator {
//    public static HttpClient getNewHttpClient() {
//        try {
//            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
//            trustStore.load(null, null);
//
//            SSLSocketFactory sf = new DefaultSSLSocketFactory(trustStore);
//            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
//
//           // URL requestUrl = null;
//            HttpURLConnection urlConnection = null;
//           // requestUrl = new URL(url);
//            //urlConnection = (HttpURLConnection) requestUrl.openConnection();
//
//            urlConnection.setConnectTimeout(Property.TIMEOUT_CONNECTION);
//            urlConnection.setReadTimeout(Property.TIMEOUT_SOCKET);
//            urlConnection.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
//            urlConnection.connect();
//
//
//            HttpParams params = new BasicHttpParams();
//            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
//            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
//
//            // Set the timeout in milliseconds until a connection is established.
//            // The default value is zero, that means the timeout is not used.
//            int timeoutConnection = Property.TIMEOUT_CONNECTION;
//            HttpConnectionParams.setConnectionTimeout(params, timeoutConnection);
//            // Set the default socket timeout (SO_TIMEOUT)
//            // in milliseconds which is the timeout for waiting for data.
//            int timeoutSocket = Property.TIMEOUT_SOCKET;
//            HttpConnectionParams.setSoTimeout(params, timeoutSocket);
//
//            SchemeRegistry registry = new SchemeRegistry();
//            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
//            registry.register(new Scheme("https", sf, 443));
//
//            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);
//
//            return new DefaultHttpClient(ccm, params);
//        } catch (Exception e) {
//            return new DefaultHttpClient();
//        }
//    }
//}
