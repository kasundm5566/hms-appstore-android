package hms.appstore.android.util.manager.analytics;

import android.app.Activity;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import hms.appstore.android.AppstoreApp;

public class AnalyticEventDispatcher {

    private String category;
    private String action;
    private String label;

    public AnalyticEventDispatcher(String category, String action, String label) {
        this.category = category;
        this.action = action;
        this.label = label;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void dispatch(Activity activity) {
        Tracker t = ((AppstoreApp) activity.getApplication()).getTracker(
                AppstoreApp.TrackerName.APP_TRACKER);
        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder()
                .setCategory(this.category)
                .setAction(this.action)
                .setLabel(this.label)
                .build());
    }
}
