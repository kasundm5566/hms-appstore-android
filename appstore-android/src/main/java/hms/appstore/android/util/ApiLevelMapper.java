package hms.appstore.android.util;

import hms.appstore.android.util.Log;
import hms.appstore.android.rest.response.DownloadableBinaryDetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ApiLevelMapper {

    DownloadableBinaryDetails detailsObj;

    private static final String LOG_TAG = ApiLevelMapper.class.getCanonicalName();

    public static void setApiLevel(DownloadableBinaryDetails downloadableBinaryDetails ){

        int apiLevel = -1;

        String version = downloadableBinaryDetails.getOsVersion();
            switch (version) {
                case "5.1":
                case "5.1.1":
                    apiLevel = 22;
                    break;
                case "5.0":
                case "5.0.1":
                case "5.0.2":
                    apiLevel = 21;
                    break;
                case "4.4":
                case "4.4.1":
                case "4.4.2":
                    apiLevel = 19;
                    break;
                case "4.3":
                case "4.3.1":
                    apiLevel = 18;
                    break;
                case "4.2":
                case "4.2.1":
                case "4.2.2":
                    apiLevel = 17;
                    break;
                case "4.1":
                case "4.1.1":
                case "4.1.2":
                    apiLevel = 16;
                    break;
                case "4.0.3":
                case "4.0.4":
                    apiLevel = 15;
                    break;
                case "4.0":
                case "4.0.1":
                case "4.0.2":
                    apiLevel = 14;
                    break;
                case "3.2":
                case "3.2.1":
                case "3.2.2":
                case "3.2.3":
                case "3.2.4":
                case "3.2.5":
                case "3.2.6":
                    apiLevel = 13;
                    break;
                case "3.1":
                case "3.1.1":
                case "3.1.2":
                case "3.1.3":
                case "3.1.4":
                case "3.1.5":
                case "3.1.6":
                case "3.1.7":
                case "3.1.8":
                case "3.1.9":
                    apiLevel = 12;
                    break;
                case "3.0":
                case "3.0.1":
                case "3.0.2":
                case "3.0.3":
                case "3.0.4":
                case "3.0.5":
                case "3.0.6":
                case "3.0.7":
                case "3.0.8":
                case "3.0.9":
                    apiLevel = 11;
                    break;
                case "2.3.3":
                case "2.3.4":
                case "2.3.5":
                case "2.4.6":
                case "2.4.7":
                case "2.3.6":
                case "2.3.7":
                    apiLevel = 10;
                    break;
                case "2.3":
                case "2.3.1":
                case "2.3.2":
                    apiLevel = 9;
                    break;
                case "2.2":
                case "2.2.1":
                case "2.2.2":
                case "2.2.3":
                case "2.2.4":
                case "2.2.5":
                case "2.2.6":
                case "2.2.7":
                case "2.2.8":
                case "2.2.9":
                    apiLevel = 8;
                    break;
                case "2.1":
                case "2.1.1":
                case "2.1.2":
                case "2.1.3":
                case "2.1.4":
                case "2.1.5":
                case "2.1.6":
                case "2.1.7":
                case "2.1.8":
                case "2.1.9":
                    apiLevel = 7;
                    break;
            }
        downloadableBinaryDetails.setApiLevel(apiLevel);
    }

    public static DownloadableBinaryDetails versionToApiLevel(DownloadableBinaryDetails downloadableBinaryDetails){
        setApiLevel(downloadableBinaryDetails);
        Log.d(LOG_TAG," Setting Api Level "+ downloadableBinaryDetails.getApiLevel());
        return downloadableBinaryDetails;
    }

    public static DownloadableBinaryDetails getDownloadableBinaries(int apiLevel,List<DownloadableBinaryDetails> downloadableBinaryDetailsList){
        Log.d(LOG_TAG,"Device API level "+ apiLevel);
        DownloadableBinaryDetails binaryDetails = null;
        for(DownloadableBinaryDetails obj:downloadableBinaryDetailsList){
            if(versionToApiLevel(obj).getApiLevel() == apiLevel){
                Log.d(LOG_TAG,"Match found in api level "+ apiLevel);
                binaryDetails = obj;
            }
        }
        return binaryDetails;
    }


}
