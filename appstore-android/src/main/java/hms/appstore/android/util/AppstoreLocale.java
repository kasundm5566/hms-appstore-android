package hms.appstore.android.util;

import android.content.Context;
import android.graphics.Typeface;

public class AppstoreLocale {

    public static String appLocale = AppstoreLocale.ENGLISH_LOCALE;

    public static final String ENGLISH_LOCALE = "en";
    public static final String BENGALI_LOCALE = "bn";
    public static final String RUSSIAN_LOCALE = "ru";

    public static boolean isDefaultLocale(){
        return appLocale.equals(ENGLISH_LOCALE);
    }

    public static Typeface getLocaleTypeFace(Context context) {
        if(!isDefaultLocale()){
            String fontPath = "font_" + AppstoreLocale.appLocale + ".ttf";
            Typeface typeFace = Typeface.createFromAsset(context.getAssets(), fontPath);
            return typeFace;
        } else {
            return Typeface.DEFAULT;
        }
    }

}
