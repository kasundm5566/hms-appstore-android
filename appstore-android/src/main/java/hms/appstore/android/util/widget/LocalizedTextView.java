package hms.appstore.android.util.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;
import hms.appstore.android.util.AppstoreLocale;

public class LocalizedTextView extends TextView{

    public LocalizedTextView(Context context) {
        super(context);
    }

    public LocalizedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LocalizedTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setTypeface(Typeface tf, int style) {
        this.setTypeface(AppstoreLocale.getLocaleTypeFace(getContext()));
    }
}
