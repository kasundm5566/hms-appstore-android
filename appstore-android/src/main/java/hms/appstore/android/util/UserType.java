package hms.appstore.android.util;

public enum UserType {
    INDIVIDUAL,
    CORPORATE,
}
