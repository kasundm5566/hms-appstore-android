package hms.appstore.android.util;

import android.content.Context;
import android.content.res.Configuration;

public class ApplicationLayoutHelper {

    private static int numberOfColumns = 3;

    public static int getNumberOfColumns(Context context) {

        try {
            if ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {

                if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
                    numberOfColumns = 4;
                else
                    numberOfColumns = 6;

            } else if ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE) {

                if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
                    numberOfColumns = 5;
                else
                    numberOfColumns = 8;

            } else {
                if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
                    numberOfColumns = 3;
                else
                    numberOfColumns = 5;
            }
        } catch (Exception e) {
            e.printStackTrace();
            numberOfColumns = 3;
        }
        return numberOfColumns;
    }
}
