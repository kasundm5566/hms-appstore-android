package hms.appstore.android.util.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.CheckBox;

import hms.appstore.android.util.AppstoreLocale;

public class LocalizedCheckbox extends CheckBox {


    public LocalizedCheckbox(Context context) {
        super(context);
    }

    public LocalizedCheckbox(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LocalizedCheckbox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setTypeface(Typeface tf, int style) {
        this.setTypeface(AppstoreLocale.getLocaleTypeFace(getContext()));
    }
}
