package hms.appstore.android.util.manager.downloadable;

import com.google.gson.Gson;

import java.util.List;
import java.util.Map;

public class DownloadableApplications {

    private Map<String, DownloadableApplication> downloadableAppsMap;

    public Map<String, DownloadableApplication> getDownloadableAppsMap() {
        return downloadableAppsMap;
    }

    public void setDownloadableAppsMap(Map<String, DownloadableApplication> downloadableAppsMap) {
        this.downloadableAppsMap = downloadableAppsMap;
    }

    public String toJsonString(){
        Gson downloadableAppsGson = new Gson();
        return downloadableAppsGson.toJson(this);
    }
}
