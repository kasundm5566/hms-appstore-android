/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import java.util.List;

import hms.appstore.android.R;
import hms.appstore.android.activity.app.AppDescriptionActivity;
import hms.appstore.android.rest.response.Application;

public class DefaultBucketListAdapter extends BaseAdapter{
    View bucketElement = null;
    private LayoutInflater inflater;
    List<Application> elements;

    ImageView appIcon;
    // Get singletone instance of ImageLoader
    Activity ctx;
    private static ImageLoader imageLoader = ImageLoader.getInstance();
    private static ImageLoaderConfiguration imageConfig;
    private static DisplayImageOptions displayImgOpts = new DisplayImageOptions.Builder()
            .cacheInMemory()
            .cacheOnDisc()
            .delayBeforeLoading(500)
            .build();


    Typeface font = null;
    int singleAppLayout = R.layout.single_app;

    public DefaultBucketListAdapter(Activity ctx, List<Application> elements, Typeface font, int layout) {
        //super(ctx, elements);
        imageConfig = new ImageLoaderConfiguration.Builder(ctx)
                .threadPoolSize(5)
                .memoryCache(new UsingFreqLimitedMemoryCache(5 * 1024 * 1024))
                //.imageDownloader(new HttpClientImageDownloader(HttpClientGenerator.getNewHttpClient()))
                .imageDownloader(new BaseImageDownloader(ctx))
                .build();
        this.font = font;
        this.singleAppLayout = layout;
        this.elements = elements;
    }

    public DefaultBucketListAdapter(Activity ctx, List<Application> elements, Typeface font) {
       // super(ctx, elements);
        imageConfig = new ImageLoaderConfiguration.Builder(ctx)
                .threadPoolSize(5)
                .memoryCache(new UsingFreqLimitedMemoryCache(5 * 1024 * 1024))
                //.imageDownloader(new HttpClientImageDownloader(HttpClientGenerator.getNewHttpClient()))
                .imageDownloader(new BaseImageDownloader(ctx))
                .build();
        this.ctx = ctx;
        this.font = font;
        this.elements = elements;
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public DefaultBucketListAdapter(Activity ctx, List<Application> elements, Integer bucketSize, Typeface font) {
       // super(ctx, elements, bucketSize);
        imageConfig = new ImageLoaderConfiguration.Builder(ctx)
                .threadPoolSize(5)
                .memoryCache(new UsingFreqLimitedMemoryCache(5 * 1024 * 1024))
                //.imageDownloader(new HttpClientImageDownloader(HttpClientGenerator.getNewHttpClient()))
                .imageDownloader(new BaseImageDownloader(ctx))
                .build();
        this.font = font;
    }

/*    @Override
    protected View getBucketElement(int position, Application app) {
        final Application currentApp = app;
        bucketElement = View.inflate(ctx, singleAppLayout, null);

        TextView txtAppId = (TextView) bucketElement.findViewById(R.id.single_app_id);
        txtAppId.setText(currentApp.getId());

        TextView txtAppName = (TextView) bucketElement.findViewById(R.id.single_app_name);
        txtAppName.setText(currentApp.getName());

//        TextView txtAppCategory = (TextView)bucketElement.findViewById(R.id.single_app_category);
//        txtAppCategory.setText(currentElement.getDetailMap().get(Tags.TAG_CATEGORY.name()));

        TextView txtAppDescription = (TextView) bucketElement.findViewById(R.id.single_app_description);

        txtAppDescription.setText(currentApp.getDescription());
//        txtAppDescription.setTypeface(font);

        TextView txtAppCost = (TextView) bucketElement.findViewById(R.id.single_app_cost);
        txtAppCost.setText("By : " + currentApp.getDeveloper());

        RatingBar appRatingBar = (RatingBar) bucketElement.findViewById(R.id.ratingBar1);
        appRatingBar.setRating(currentApp.getRating());

        appIcon = (ImageView) bucketElement.findViewById(R.id.icon);
//        appIcon.setImageBitmap(currentElement.getIcon());

        String imageUrl = ProfileProperty.APPSTORE_IMAGE_URL + currentApp.getAppIcon();

        // Initialize ImageLoader with configuration. Do it once.
        imageLoader.init(imageConfig);
        // Load and display image asynchronously
        imageLoader.displayImage(imageUrl, appIcon, displayImgOpts);

        bucketElement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = ((TextView) v.findViewById(R.id.single_app_name)).getText().toString();
                String app_id = ((TextView) v.findViewById(R.id.single_app_id)).getText().toString();

                // Starting new intent
                Intent in = new Intent(ctx, AppDescriptionActivity.class);
                in.putExtra(Tags.TAG_NAME.name(), name);
                in.putExtra(Tags.TAG_APP_ID.name(), app_id);
                in.putExtra("app", currentApp);

                ctx.startActivity(in);
            }
        });
        return bucketElement;
    }*/


    @Override
    public int getCount() {
        return elements.size();
    }

    @Override
    public Application getItem(int position) {
        return elements.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        convertView = inflater.inflate(R.layout.single_app,null);
      //  convertView = inflater.inflate(R.layout.search_page,parent,false);

       final Application currentApp = getItem(position);

        TextView txtAppId = (TextView) convertView.findViewById(R.id.single_app_id);
        txtAppId.setText(currentApp.getId());

        TextView txtAppName = (TextView) convertView.findViewById(R.id.single_app_name);
        txtAppName.setText(currentApp.getName());

        TextView txtAppDescription = (TextView) convertView.findViewById(R.id.single_app_description);
        txtAppDescription.setText(currentApp.getDescription());

        TextView txtAppCost = (TextView) convertView.findViewById(R.id.single_app_cost);
        txtAppCost.setText("By : " + currentApp.getDeveloper());

        RatingBar appRatingBar = (RatingBar) convertView.findViewById(R.id.ratingBar1);
        appRatingBar.setRating(currentApp.getRating());

        appIcon = (ImageView) convertView.findViewById(R.id.icon);
//        appIcon.setImageBitmap(currentElement.getIcon());

        String imageUrl = ProfileProperty.APPSTORE_IMAGE_URL + currentApp.getAppIcon();

        // Initialize ImageLoader with configuration. Do it once.
        imageLoader.init(imageConfig);
        // Load and display image asynchronously
        imageLoader.displayImage(imageUrl, appIcon, displayImgOpts);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = ((TextView) v.findViewById(R.id.single_app_name)).getText().toString();
                String app_id = ((TextView) v.findViewById(R.id.single_app_id)).getText().toString();

                // Starting new intent
                Intent in = new Intent(ctx, AppDescriptionActivity.class);
                in.putExtra(Tags.TAG_NAME.name(), name);
                in.putExtra(Tags.TAG_APP_ID.name(), app_id);
                in.putExtra("app", currentApp);

                ctx.startActivity(in);
            }
        });
        return convertView;
    }
}