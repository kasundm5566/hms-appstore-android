package hms.appstore.android.util.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import hms.appstore.android.util.AppstoreLocale;

public class LocalizedButton extends Button{

    public LocalizedButton(Context context) {
        super(context);
    }

    public LocalizedButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LocalizedButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setTypeface(Typeface tf, int style) {
        this.setTypeface(AppstoreLocale.getLocaleTypeFace(getContext()));
    }
}
