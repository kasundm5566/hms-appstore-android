package hms.appstore.android.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import hms.appstore.android.activity.MainActivity;
import hms.appstore.android.activity.MyAppsActivity;
import hms.appstore.android.activity.app.AppDescriptionActivity;
import hms.appstore.android.activity.app.AppsForCategoryActivity;
import hms.appstore.android.activity.app.PushNotificationActivity;
import hms.appstore.android.activity.app.SearchActivity;

public class Property {
    static {

        Properties properties = new Properties();

        try {
            properties.loadFromXML(Property.class.getClassLoader().getResourceAsStream("res/raw/property.xml"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        ALL_APPS_URL = properties.getProperty("all.apps.url");
        CATEGORY_ALL_APPS_URL = properties.getProperty("category.all.apps.url");
        NEWLY_ADDED_APPS_URL = properties.getProperty("newly.added.apps.url");
        MOSTLY_USED_APPS_URL = properties.getProperty("mostly.used.apps.url");
        TOP_RATED_APPS_URL = properties.getProperty("top.rated.apps.url");
        FEATURED_APPS_URL = properties.getProperty("featured.apps.url");
        FREE_APPS_URL = properties.getProperty("free.apps.url");

        APP_CATEGORIES_URL = properties.getProperty("app.categories.url");
        APP_DETAILS_URL = properties.getProperty("app.details.url");
        RELATED_APPS_URL = properties.getProperty("related.apps.url");
        APP_SEARCH_URL = properties.getProperty("app.search.url");

        CREATE_USER_URL = properties.getProperty("create.user.url");
        ACCOUNT_RECOVERY_URL = properties.getProperty("account.recovery.url");
        CHANGE_PASSWORD_URL = properties.getProperty("change.password.url");
        ACCOUNT_VERIFY_URL = properties.getProperty("account.verify.url");
        REQUEST_VERIFY_CODE_URL = properties.getProperty("request.verify.code.url");
        SIGN_IN_URL = properties.getProperty("sign.in.url");

        MY_SUBSCRIPTION_URL = properties.getProperty("my.subscription.url");
        MY_DOWNLOAD_URL = properties.getProperty("my.download.url");
        MY_UPDATES_URL = properties.getProperty("my.update.url");
        SUBSCRIBE_URL = properties.getProperty("subscribe.url");
        UNSUBSCRIBE_URL = properties.getProperty("unsubscribe.url");
        DOWNLOAD_URL = properties.getProperty("download.url");
        USER_COMMENT_URL = properties.getProperty("user.comment.url");
        ALL_USER_COMMENT_URL = properties.getProperty("all.user.comment.url");
        REPORT_ABUSE_URL = properties.getProperty("report.abuse.url");

        DOWNLOADABLE_APP_COUNT_URL = properties.getProperty("downloadable.app.count.url");
        DOWNLOADABLE_APP_COUNT_WITH_CATEGORY_URL = properties.getProperty("downloadable.app.count.with.category.url");
        DOWNLOADABLE_FREE_APP_COUNT_URL = properties.getProperty("downloadable.free.app.count.url");
        DOWNLOADABLE_FREE_APP_WITH_CATEGORY_COUNT_URL = properties.getProperty("downloadable.free.app.count.with.category.url");
        CHARGE_DOWNLOAD_URL = properties.getProperty("charge.download.url");
        PI_LIST_URL = properties.getProperty("pi.list.url");

        VERSION_UPDATE_URL = properties.getProperty("version.update.url");
        APP_UPDATE_COUNT_URL = properties.getProperty("app.update.count.url");
        AUTO_LOGIN_URL = properties.getProperty("auto.login.url");
        AUTH_PROVIDER_URL = properties.getProperty("auth.provider.url");
        VALIDATE_USER_BY_SESSION_ID_URL = properties.getProperty("validate.user.by.session.id.url");
        VALIDATE_USER_BY_SESSION_ID_URL_SUFFIX = properties.getProperty("validate.user.by.session.id.url.suffix");

        LIST_ITEM_WIDTH = Integer.parseInt(properties.getProperty("list.item.width"));
        LIST_ITEM_LIMIT = Integer.parseInt(properties.getProperty("list.item.limit"));
        RELATED_APPS_LIMIT = Integer.parseInt(properties.getProperty("related.apps.limit"));
        FEATURED_APPS_LIMIT = Integer.parseInt(properties.getProperty("featured.apps.limit"));
        PUSH_MESSAGE_LIMIT = Integer.parseInt(properties.getProperty("push.message.limit"));
        TIMEOUT_CONNECTION = Integer.parseInt(properties.getProperty("connection.timeout"));
        TIMEOUT_SOCKET = Integer.parseInt(properties.getProperty("socket.timeout"));
        SPLASH_DURATION = Integer.parseInt(properties.getProperty("splash.duration"));

        SUCCESS_STATUS = properties.getProperty("success.status");
        ERROR_EMAIL_ALREADY_USED = properties.getProperty("error.email.already.used");
        NO_APPS_FOR_THE_CRITERIA_STATUS = properties.getProperty("no.apps.for.criteria.status");
        INACTIVE_USER_STATUS = properties.getProperty("inactive.user.status");
        INITIAL_TEMP_USER_LOGIN_STATUS = properties.getProperty("temp.user.login.status.initial");
        CHANGED_PASSWORD_TEMP_USER_LOGIN_STATUS = properties.getProperty("temp.user.login.status.changed.password");
        NO_USER_FOR_MSISDN_STATUS = properties.getProperty("no.user.for.msisdn.status");
        MULTIPLE_PI_AVAILABLE_STATUS = properties.getProperty("multiple.pi.available.status");

        //In-App Related Error Codes and Messages
        IAP_INTERNAL_ERROR_STATUS = properties.getProperty("iap.internal.error.status");
        IAP_USER_CANCELLED_STATUS = properties.getProperty("iap.user.cancelled.status");
        IAP_INVALID_KEY_STATUS = properties.getProperty("iap.invalid.key.status");
        IAP_CONNECTION_ERROR_STATUS = properties.getProperty("iap.connection.error.status");
        IAP_NO_PI_STATUS = properties.getProperty("iap.no.pi.status");
        IAP_PI_INACTIVE_STATUS = properties.getProperty("iap.pi.inactive.status");
        IAP_PI_ERROR_STATUS = properties.getProperty("iap.pi.error.status");
        IAP_APPSTORE_BUSY_STATUS = properties.getProperty("iap.appstore.busy.status");

        IAP_INTERNAL_ERROR_DISPLAY_TXT = properties.getProperty("iap.internal.error.display.txt");
        IAP_USER_CANCELLED_DISPLAY_TXT = properties.getProperty("iap.user.cancelled.display.txt");
        IAP_INVALID_KEY_DISPLAY_TXT = properties.getProperty("iap.invalid.key.display.txt");
        IAP_CONNECTION_ERROR_DISPLAY_TXT = properties.getProperty("iap.connection.error.display.txt");
        IAP_NO_PI_DISPLAY_TXT = properties.getProperty("iap.no.pi.display.txt");
        IAP_PI_INACTIVE_DISPLAY_TXT = properties.getProperty("iap.pi.inactive.display.txt");
        IAP_PI_ERROR_DISPLAY_TXT = properties.getProperty("iap.pi.error.display.txt");
        IAP_APPSTORE_BUSY_DISPLAY_TXT = properties.getProperty("iap.appstore.busy.display.txt");

        IAP_INTERNAL_ERROR_DETAIL = properties.getProperty("iap.internal.error.detail");
        IAP_USER_CANCELLED_DETAIL = properties.getProperty("iap.user.cancelled.detail");
        IAP_INVALID_KEY_DETAIL = properties.getProperty("iap.invalid.key.detail");
        IAP_CONNECTION_ERROR_DETAIL = properties.getProperty("iap.connection.error.detail");
        IAP_NO_PI_DETAIL = properties.getProperty("iap.no.pi.detail");
        IAP_PI_INACTIVE_DETAIL = properties.getProperty("iap.pi.inactive.detail");
        IAP_PI_ERROR_DETAIL = properties.getProperty("iap.pi.error.detail");

        //Intent Extra Identifiers
        REDIRECT_KEY = properties.getProperty("redirect.key.extra");
        NOTIFICATION_EXTRA = properties.getProperty("notification.extra");
        MOBILE_NO_EXTRA = properties.getProperty("mobile.no.extra");
        USERNAME_EXTRA = properties.getProperty("username.extra");
        DOWNLOAD_REQ_ID_EXTRA = properties.getProperty("download.req.id.extra");
        DOWNLOAD_REQ_URI_EXTRA = properties.getProperty("download.req.uri.extra");

        PREFS_NAME = properties.getProperty("preference.data.name");
        PREFS_APP_LOCALE = properties.getProperty("preference.locale");
        PREFS_AUTO_INSTALL_DOWNLOADABLES = properties.getProperty("preference.auto.install.downloadables");
        PREFS_DOWNLOADABLE_APP_QUEUE = properties.getProperty("preference.downloadable.app.queue");
        PREFS_LOGGED_USER = properties.getProperty("preference.logged.user");
        PREFS_USERNAME = properties.getProperty("preference.username");
        PREFS_FCM_TOKEN = properties.getProperty("preference.fcm.token");
        PREFS_SESSION_ID = properties.getProperty("preference.session.id");

        PLATFORM = properties.getProperty("platform");
        ANDROID_VERSION_PREFIX = properties.getProperty("android.version.prefix");

        //Mobile-connect Properties
        MOBILE_CONNECT_AUTHORIZATION_PREFIX = properties.getProperty("mobile.connect.authorization.url.id");
        MOBILE_CONNECT_OPERATOR_ID_PREFIX = properties.getProperty("mobile.connect.operator.url.id");
        MOBILE_CONNECT_TOKEN_URL_ID = properties.getProperty("mobile.connect.token.url.id");
        MOBILE_CONNECT_AUTHENTICATION_MAX_AGE = Integer.parseInt(properties.getProperty("mobile.connect.authorization.max.age"));
        MOBILE_CONNECT_AUTHENTICATION_ACR_VALUE = properties.getProperty("mobile.connect.authorization.acr.values");

        MOBILE_CONNECT_TOKEN_LOGIN_URL =properties.getProperty("mobile.connect.token.login");
        MOBILE_CONNECT_OPERATOR_REGEX =properties.getProperty("mobile.connect.operator.regex");

        APP_CATEGORIES_BANNERS = properties.getProperty("app.categories.banners");
        APP_HOME_BANNERS = properties.getProperty("app.home.banners");
        APP_POPULAR_BANNERS = properties.getProperty("app.popular.banners");
        APP_SUBSCRIPTION_BANNERS = properties.getProperty("app.subscription.banners");
        APP_FEATURED_BANNERS = properties.getProperty("app.featured.banners");
        APP_DOWNLOADABLE_BANNERS = properties.getProperty("app.downloadable.banners");
        APP_DEV_BANNERS = properties.getProperty("app.dev.banners");
        APP_MY_APPS_BANNERS = properties.getProperty("app.my.apps.banners");

        MY_ACCOUNT_PROFILE_DETAILS = properties.getProperty("my.account.profile.details");
        MY_ACCOUNT_PROFILE_UPDATE = properties.getProperty("my.account.profile.update");

        POPULAR_APPS_URL = properties.getProperty("popular.apps.url");
        SUBSCRIPTION_APPS_URL = properties.getProperty("subscription.apps.url");
        DOWNLOADABLE_APPS_URL = properties.getProperty("downloadable.apps.url");
        DEV_APPS_URL = properties.getProperty("dev.apps.url");
        DEV_ACCOUNT_PROFILE_DETAIL_URL = properties.getProperty("dev.profile.details.url");
        FEATURED_DOWNLOADABLE_APPS_URL = properties.getProperty("featured.downloadable.apps.url");
        APP_RATE_THIS_URL = properties.getProperty("app.ratet.this.url");
        APP_DEVICE_REGISTER_URL = properties.getProperty("app.device.register.url");
        APP_PUSH_NOTIFICATION_URL = properties.getProperty("app.push.notification.url");
    }

    public static final String IAP_INTERNAL_ERROR_STATUS;
    public static final String IAP_USER_CANCELLED_STATUS;
    public static final String IAP_INVALID_KEY_STATUS;
    public static final String IAP_CONNECTION_ERROR_STATUS;
    public static final String IAP_NO_PI_STATUS;
    public static final String IAP_PI_INACTIVE_STATUS;
    public static final String IAP_PI_ERROR_STATUS;
    public static final String IAP_APPSTORE_BUSY_STATUS;

    public static final String IAP_INTERNAL_ERROR_DISPLAY_TXT;
    public static final String IAP_USER_CANCELLED_DISPLAY_TXT;
    public static final String IAP_INVALID_KEY_DISPLAY_TXT;
    public static final String IAP_CONNECTION_ERROR_DISPLAY_TXT;
    public static final String IAP_NO_PI_DISPLAY_TXT;
    public static final String IAP_PI_INACTIVE_DISPLAY_TXT;
    public static final String IAP_PI_ERROR_DISPLAY_TXT;
    public static final String IAP_APPSTORE_BUSY_DISPLAY_TXT;

    public static final String IAP_INTERNAL_ERROR_DETAIL;
    public static final String IAP_USER_CANCELLED_DETAIL;
    public static final String IAP_INVALID_KEY_DETAIL;
    public static final String IAP_CONNECTION_ERROR_DETAIL;
    public static final String IAP_NO_PI_DETAIL;
    public static final String IAP_PI_INACTIVE_DETAIL;
    public static final String IAP_PI_ERROR_DETAIL;

    public static final String ALL_APPS_URL;
    public static final String NEWLY_ADDED_APPS_URL;
    public static final String CATEGORY_ALL_APPS_URL;
    public static final String MOSTLY_USED_APPS_URL;
    public static final String TOP_RATED_APPS_URL;
    public static final String FEATURED_APPS_URL;
    public static final String FREE_APPS_URL;

    public static final String APP_CATEGORIES_URL;
    public static final String APP_DETAILS_URL;
    public static final String RELATED_APPS_URL;
    public static final String APP_SEARCH_URL;

    public static final String CREATE_USER_URL;
    public static final String ACCOUNT_RECOVERY_URL;
    public static final String CHANGE_PASSWORD_URL;
    public static final String ACCOUNT_VERIFY_URL;
    public static final String REQUEST_VERIFY_CODE_URL;
    public static final String SIGN_IN_URL;

    public static final String MY_SUBSCRIPTION_URL;
    public static final String MY_DOWNLOAD_URL;
    public static final String MY_UPDATES_URL;
    public static final String SUBSCRIBE_URL;
    public static final String UNSUBSCRIBE_URL;
    public static final String DOWNLOAD_URL;
    public static final String USER_COMMENT_URL;
    public static final String ALL_USER_COMMENT_URL;
    public static final String REPORT_ABUSE_URL;

    public static final String DOWNLOADABLE_APP_COUNT_URL;
    public static final String DOWNLOADABLE_APP_COUNT_WITH_CATEGORY_URL;
    public static final String DOWNLOADABLE_FREE_APP_COUNT_URL;
    public static final String DOWNLOADABLE_FREE_APP_WITH_CATEGORY_COUNT_URL;
    public static final String CHARGE_DOWNLOAD_URL;
    public static final String PI_LIST_URL;

    public static final String VERSION_UPDATE_URL;
    public static final String APP_UPDATE_COUNT_URL;
    public static final String AUTO_LOGIN_URL;
    public static final String AUTH_PROVIDER_URL;
    public static final String VALIDATE_USER_BY_SESSION_ID_URL;
    public static final String VALIDATE_USER_BY_SESSION_ID_URL_SUFFIX;

    public static final int LIST_ITEM_WIDTH;
    public static final int LIST_ITEM_LIMIT;
    public static final int RELATED_APPS_LIMIT;
    public static final int FEATURED_APPS_LIMIT;
    public static final int PUSH_MESSAGE_LIMIT;
    public static final int TIMEOUT_CONNECTION;
    public static final int TIMEOUT_SOCKET;
    public static final int SPLASH_DURATION;

    public static final String SUCCESS_STATUS;
    public static final String ERROR_EMAIL_ALREADY_USED;
    public static final String NO_APPS_FOR_THE_CRITERIA_STATUS;
    public static final String INACTIVE_USER_STATUS;
    public static final String INITIAL_TEMP_USER_LOGIN_STATUS;
    public static final String CHANGED_PASSWORD_TEMP_USER_LOGIN_STATUS;
    public static final String NO_USER_FOR_MSISDN_STATUS;
    public static final String MULTIPLE_PI_AVAILABLE_STATUS;

    public static final String REDIRECT_KEY;
    public static final String NOTIFICATION_EXTRA;
    public static final String MOBILE_NO_EXTRA;
    public static final String USERNAME_EXTRA;
    public static final String DOWNLOAD_REQ_ID_EXTRA;
    public static final String DOWNLOAD_REQ_URI_EXTRA;

    public static final String PREFS_NAME;
    public static final String PREFS_APP_LOCALE;
    public static final String PREFS_AUTO_INSTALL_DOWNLOADABLES;
    public static final String PREFS_DOWNLOADABLE_APP_QUEUE;
    public static final String PREFS_LOGGED_USER;
    public static final String PREFS_USERNAME;
    public static final String PLATFORM;
    public static final String ANDROID_VERSION_PREFIX;
    public static final String PREFS_FCM_TOKEN;
    public static final String PREFS_SESSION_ID;

    public static final String MOBILE_CONNECT_AUTHORIZATION_PREFIX;
    public static final String MOBILE_CONNECT_OPERATOR_ID_PREFIX;
    public static final String MOBILE_CONNECT_TOKEN_URL_ID;
    public static final int MOBILE_CONNECT_AUTHENTICATION_MAX_AGE;
    public static final String MOBILE_CONNECT_AUTHENTICATION_ACR_VALUE;

    public static final String MOBILE_CONNECT_TOKEN_LOGIN_URL;
    public static final String MOBILE_CONNECT_OPERATOR_REGEX;

    public static final HashMap<String, Class<?>> REDIRECT_MAP = new HashMap<String, Class<?>>() {{
        put(RedirectKeys.HOME.name(), MainActivity.class);
        put(RedirectKeys.DESCRIPTION.name(), AppDescriptionActivity.class);
        put(RedirectKeys.MY_APPSSTORE.name(), MyAppsActivity.class);
        put(RedirectKeys.CATEGORIES_HOME.name(), AppsForCategoryActivity.class);
        put(RedirectKeys.SEARCH.name(), SearchActivity.class);
        put(RedirectKeys.PUSH_NOTIFICATIONS.name(), PushNotificationActivity.class);
    }};

    public static final HashMap<String, Integer> MYAPPS_FRAG_MAP = new HashMap<String, Integer>() {{
        put(RedirectKeys.MY_SUBSCRIPTIONS.name(), 0);
        put(RedirectKeys.MY_DOWNLOADS.name(), 1);
        put(RedirectKeys.MY_UPDATES.name(), 2);
    }};

    public static final String APP_CATEGORIES_BANNERS;
    public static final String APP_HOME_BANNERS;
    public static final String APP_POPULAR_BANNERS;
    public static final String APP_SUBSCRIPTION_BANNERS;
    public static final String APP_FEATURED_BANNERS;
    public static final String APP_DOWNLOADABLE_BANNERS;
    public static final String APP_DEV_BANNERS;
    public static final String APP_MY_APPS_BANNERS;

    public static final String MY_ACCOUNT_PROFILE_DETAILS;
    public static final String MY_ACCOUNT_PROFILE_UPDATE;

    public static final String POPULAR_APPS_URL;
    public static final String SUBSCRIPTION_APPS_URL;
    public static final String DOWNLOADABLE_APPS_URL;
    public static final String DEV_APPS_URL;
    public static final String DEV_ACCOUNT_PROFILE_DETAIL_URL;
    public static final String FEATURED_DOWNLOADABLE_APPS_URL;
    public static final String APP_RATE_THIS_URL;
    public static final String APP_DEVICE_REGISTER_URL;
    public static final String APP_PUSH_NOTIFICATION_URL;

}
