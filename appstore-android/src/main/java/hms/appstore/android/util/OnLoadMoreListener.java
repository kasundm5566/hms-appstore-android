package hms.appstore.android.util;

public interface OnLoadMoreListener {

    void onLoadMore();
}