package hms.appstore.android.util.manager.downloadable;

public class DownloadableApplication {

    private String appId;
    private String appName;
    private long downloadQueueId;
    private boolean isDownloadComplete;

    public DownloadableApplication(String appId, String appName, long downloadQueueId) {
        this.appId = appId;
        this.appName = appName;
        this.downloadQueueId = downloadQueueId;
        this.isDownloadComplete = false;
    }

    public boolean isDownloadComplete() {
        return isDownloadComplete;
    }

    public void setDownloadComplete(boolean isDownloadComplete) {
        this.isDownloadComplete = isDownloadComplete;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public long getDownloadQueueId() {
        return downloadQueueId;
    }

    public void setDownloadQueueId(long downloadQueueId) {
        this.downloadQueueId = downloadQueueId;
    }
}
