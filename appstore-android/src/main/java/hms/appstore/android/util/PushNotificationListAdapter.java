package hms.appstore.android.util;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import hms.appstore.android.R;
import hms.appstore.android.activity.app.AppDescriptionActivity;
import hms.appstore.android.db.DatabaseHandler;
import hms.appstore.android.util.widget.PushMessage;

public class PushNotificationListAdapter extends BaseAdapter {

    private static final String DATA_APP_ID = "appId";
    Context context;
    ArrayList<PushMessage> notificationArrayList = null;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss Z", Locale.ENGLISH);
    SimpleDateFormat sdfOutput = new SimpleDateFormat("kk:mm:ss dd-MM-yyyy", Locale.ENGLISH);

    public PushNotificationListAdapter(Context context, int resource, ArrayList<PushMessage> notificationList) {
        this.context = context;
        this.notificationArrayList = notificationList;
    }

    @Override
    public int getCount() {
        if (notificationArrayList != null)
            return notificationArrayList.size();
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return notificationArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View listItem = convertView;
        if (listItem == null)
            listItem = LayoutInflater.from(context).inflate(R.layout.list_item_notifications, parent, false);

        final PushMessage pushMessage = notificationArrayList.get(position);

        TextView tvMessage = (TextView) listItem.findViewById(R.id.tvMessage);
        TextView tvDate = (TextView) listItem.findViewById(R.id.tvDate);
        final LinearLayout llListItem = (LinearLayout) listItem.findViewById(R.id.llListItem);

        if (pushMessage.isRead())
            llListItem.setBackgroundColor(ContextCompat.getColor(context, R.color.push_notification_list_item_inactive_color));
        else
            llListItem.setBackgroundColor(ContextCompat.getColor(context, R.color.push_notification_list_item_active_color));

        tvMessage.setText(pushMessage.getMessageContent());

        tvDate.setText(getLocalTime(pushMessage.getDispatchedTime()));

        System.out.println("Notification Message requestId " + pushMessage.getRequestId());

        listItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pushMessage.setRead(true);
                notificationArrayList.set(position, pushMessage);
                DatabaseHandler.getInstance(context).updateMessage(new PushNotification(pushMessage.getRequestId(), true));

                if (!pushMessage.getData().containsKey(DATA_APP_ID)) {
                    LayoutInflater inflater = LayoutInflater.from(context);
                    final View addView = inflater.inflate(R.layout.detailed_notification, null);
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(addView);
                    dialog.setCanceledOnTouchOutside(false);

                    TextView tvMessage = (TextView) addView.findViewById(R.id.tvMessage);
                    TextView tvDate = (TextView) addView.findViewById(R.id.tvDate);
                    tvMessage.setText(pushMessage.getMessageContent());
                    tvDate.setText(getLocalTime(pushMessage.getDispatchedTime()));

                    dialog.setOnKeyListener(new Dialog.OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
                            if (keyCode == KeyEvent.KEYCODE_BACK) {
                                dialog.dismiss();
                                notifyDataSetChanged();
                            }
                            return true;
                        }
                    });

                    dialog.show();
                    llListItem.setBackgroundColor(ContextCompat.getColor(context, R.color.push_notification_list_item_inactive_color));
                } else {
                    Intent in = new Intent(context, AppDescriptionActivity.class);
                    in.putExtra(Tags.TAG_APP_ID.name(), pushMessage.getData().get(DATA_APP_ID));
                    context.startActivity(in);
                    notifyDataSetChanged();
                }
            }
        });
        return listItem;
    }

    private String getLocalTime(String dispatchedTime) {
        String localTime = "";
        try {
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = sdf.parse(dispatchedTime);
            sdfOutput.setTimeZone(TimeZone.getDefault());
            localTime = sdfOutput.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return localTime;
    }

    public ArrayList<PushMessage> getNotificationArrayList() {
        return notificationArrayList;
    }

    public void setNotificationArrayList(ArrayList<PushMessage> notificationArrayList) {
        this.notificationArrayList = notificationArrayList;
    }
}
