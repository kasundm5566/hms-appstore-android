package hms.appstore.android.util.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;

import hms.appstore.android.util.AppstoreLocale;

public class LocalizedRadioButton extends RadioButton {

    public LocalizedRadioButton(Context context) {
        super(context);
    }

    public LocalizedRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LocalizedRadioButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setTypeface(Typeface tf, int style) {
        this.setTypeface(AppstoreLocale.getLocaleTypeFace(getContext()));
    }
}
