package hms.appstore.android.util;

import java.io.IOException;
import java.util.Properties;

public class ProfileProperty {
    static {

        Properties properties = new Properties();

        try {
            properties.loadFromXML(ProfileProperty.class.getClassLoader().getResourceAsStream("res/raw/profile_property.xml"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        REST_HOST = properties.getProperty("rest.host");
        REST_HOST_HTTP = properties.getProperty("rest.host.http");
        APPSTORE_IMAGE_URL = properties.getProperty("appstore.image.url");

        REGISTRATION_LINK = properties.getProperty("registration.link");
        FORGOT_PASSWORD_LINK = properties.getProperty("forgot.password.link");
        OPERATOR_NAME = properties.getProperty("operator.name");
        APK_FOLDER_PATH = properties.getProperty("apk.folder.path");
        ANALYTICS_TRACKING_ID = properties.getProperty("google.analytics.tracking.id");
        DEFAULT_LOCALE = properties.getProperty("default.locale");

        //In-App Server Related Configs
        IAP_SERVER_URL = properties.getProperty("iap.server.url");

        REQUEST_VERSION = properties.getProperty("iap.request.version");
        RESPONSE_VERSION = properties.getProperty("iap.response.version");
        ISO_DATETIME_FORMAT = properties.getProperty("iap.iso.datetime.format");
        IAP_MESSAGE_CONTENT_TYPE = properties.getProperty("iap.message.content.type");
        IAP_MESSAGE_ACCEPT_TYPE = properties.getProperty("iap.message.accept.type");
        PAYMENT_PIN = properties.getProperty("iap.payment.pin");
        IAP_SERVER_KEY = properties.getProperty("iap.server.key");

        MOBILE_CONNECT_DISCOVERY_URL = properties.getProperty("mobile.connect.discovery.url");
        MOBILE_CONNECT_DIALOG_TOKEN_URL = properties.getProperty("mobile.connect.dialog.token.url");
        MOBILE_CONNECT_REDIRECT_URL = properties.getProperty("mobile.connect.redirect.url");
        MOBILE_CONNECT_CLIENT_KEY = properties.getProperty("mobile.connect.client.key");
        MOBILE_CONNECT_CLIENT_SECRET = properties.getProperty("mobile.connect.client.secret");
        MOBILE_CONNECT_DEFAULT_MCC = properties.getProperty("mobile.connect.dailog.mcc");
        MOBILE_CONNECT_DEFAULT_MNC = properties.getProperty("mobile.connect.dailog.mnc");
        MOBILE_CONNECT_OPENID_SCOPES = properties.getProperty("mobile.connect.dailog.openid.scopes");
        MOBILE_CONNECT_UI_LOCALES = properties.getProperty("mobile.connect.ui.locales");
        MOBILE_CONNECT_CLAIMS_LOCALES = properties.getProperty("mobile.connect.clims.locales");
        TEST_MODE_ENABLE = (String) properties.get("test.mode.enable");
        TEST_MODE_REST_CLIENT_HEADERS = properties.getProperty("test.mode.rest.client.headers");
    }

    public static final String REST_HOST;
    public static final String REST_HOST_HTTP;
    public static final String APPSTORE_IMAGE_URL;

    public static final String REGISTRATION_LINK;
    public static final String FORGOT_PASSWORD_LINK;
    public static final String OPERATOR_NAME;
    public static final String APK_FOLDER_PATH;
    public static final String ANALYTICS_TRACKING_ID;
    public static final String DEFAULT_LOCALE;

    public static final String IAP_SERVER_URL;

    public static final String REQUEST_VERSION;
    public static final String RESPONSE_VERSION;
    public static final String ISO_DATETIME_FORMAT;
    public static final String IAP_MESSAGE_CONTENT_TYPE;
    public static final String IAP_MESSAGE_ACCEPT_TYPE;
    public static final String PAYMENT_PIN;
    public static final String IAP_SERVER_KEY;

    public static final String MOBILE_CONNECT_DISCOVERY_URL;
    public static final String MOBILE_CONNECT_DIALOG_TOKEN_URL;
    public static final String MOBILE_CONNECT_REDIRECT_URL;
    public static final String MOBILE_CONNECT_CLIENT_KEY;
    public static final String MOBILE_CONNECT_CLIENT_SECRET;
    public static final String MOBILE_CONNECT_DEFAULT_MCC;
    public static final String MOBILE_CONNECT_DEFAULT_MNC;
    public static final String MOBILE_CONNECT_OPENID_SCOPES;
    public static final String MOBILE_CONNECT_UI_LOCALES;
    public static final String MOBILE_CONNECT_CLAIMS_LOCALES;

    public static final String TEST_MODE_ENABLE;
    public static final String TEST_MODE_REST_CLIENT_HEADERS;


}
