package hms.appstore.android.util;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import java.util.ArrayList;

import hms.appstore.android.R;
import hms.appstore.android.activity.app.AppDescriptionActivity;
import hms.appstore.android.rest.response.Application;

public class GridRecyclerAdapter extends RecyclerView.Adapter<GridRecyclerAdapter.ViewHolder> {

    private ArrayList<Application> appList;
    private LayoutInflater mInflater;
    private Application currentApp;
    private Context context;

    private static ImageLoader imageLoader = ImageLoader.getInstance();
    private static ImageLoaderConfiguration imageConfig;
    private static DisplayImageOptions displayImgOpts = new DisplayImageOptions.Builder()
            .cacheInMemory()
            .cacheOnDisc()
            .delayBeforeLoading(500)
            .build();


    Typeface font = null;
    int singleAppLayout = R.layout.single_app;

    // data is passed into the constructor
    public GridRecyclerAdapter(Context context, ArrayList<Application> appList) {
        this.mInflater = LayoutInflater.from(context);
        this.appList = appList;
        this.context = context;

        imageConfig = new ImageLoaderConfiguration.Builder(context)
                .threadPoolSize(5)
                .memoryCache(new UsingFreqLimitedMemoryCache(5 * 1024 * 1024))
                //.imageDownloader(new HttpClientImageDownloader(HttpClientGenerator.getNewHttpClient()))
                .imageDownloader(new BaseImageDownloader(context))
                .build();
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.single_app, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        currentApp = appList.get(position);

        holder.txtAppId.setText(currentApp.getId());

        holder.txtAppName.setText(currentApp.getName());

        holder.txtAppDescription.setText(currentApp.getDescription());

        holder.txtAppCost.setText(currentApp.getDeveloper());

        holder.appRating.setText(String.valueOf(currentApp.getRating()));

        String imageUrl = ProfileProperty.APPSTORE_IMAGE_URL + currentApp.getAppIcon();

        // Initialize ImageLoader with configuration. Do it once.
        imageLoader.init(imageConfig);
        // Load and display image asynchronously
//        imageLoader.displayImage(imageUrl, holder.appIcon, displayImgOpts);

        Glide.with(context)
                .load(imageUrl)
                .asBitmap()
                .into(AppIconUtil.getRoundedImageTarget(context, holder.appIcon, 20));
    }

    @Override
    public int getItemCount() {
        if (appList != null)
            return appList.size();
        return 0;
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txtAppId, txtAppName, txtAppDescription, txtAppCost, appRating;
        ImageView appIcon;

        ViewHolder(View itemView) {
            super(itemView);
            txtAppId = (TextView) itemView.findViewById(R.id.single_app_id);
            txtAppName = (TextView) itemView.findViewById(R.id.single_app_name);
            txtAppDescription = (TextView) itemView.findViewById(R.id.single_app_description);
            txtAppCost = (TextView) itemView.findViewById(R.id.single_app_cost);
            appRating = (TextView) itemView.findViewById(R.id.single_app_rating);
            appIcon = (ImageView) itemView.findViewById(R.id.icon);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            String name = ((TextView) v.findViewById(R.id.single_app_name)).getText().toString();
            String app_id = ((TextView) v.findViewById(R.id.single_app_id)).getText().toString();

            Intent in = new Intent(context, AppDescriptionActivity.class);
            in.putExtra(Tags.TAG_NAME.name(), name);
            in.putExtra(Tags.TAG_APP_ID.name(), app_id);
            in.putExtra("app", appList.get(getAdapterPosition()));

            context.startActivity(in);
        }
    }
}