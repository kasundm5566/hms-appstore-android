package hms.appstore.android.util;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import hms.appstore.android.R;
import hms.appstore.android.activity.app.AppDescriptionActivity;
import hms.appstore.android.rest.response.Application;

public class PaginGridAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Application> appList;

    private Application currentApp;

    public PaginGridAdapter(Context context, ArrayList<Application> appList) {
        this.context = context;
        this.appList = appList;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.single_app, null);
            viewHolder = new ViewHolder();
            viewHolder.txtAppId = (TextView) convertView.findViewById(R.id.single_app_id);
            viewHolder.txtAppName = (TextView) convertView.findViewById(R.id.single_app_name);
            viewHolder.txtAppDescription = (TextView) convertView.findViewById(R.id.single_app_description);
            viewHolder.txtAppCost = (TextView) convertView.findViewById(R.id.single_app_cost);
            viewHolder.appRating = (TextView) convertView.findViewById(R.id.single_app_rating);
            viewHolder.appIcon = (ImageView) convertView.findViewById(R.id.icon);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        currentApp = appList.get(position);
        viewHolder.txtAppId.setText(currentApp.getId());
        viewHolder.txtAppName.setText(currentApp.getName());
        viewHolder.txtAppDescription.setText(currentApp.getDescription());
        viewHolder.txtAppCost.setText(currentApp.getDeveloper());
        viewHolder.appRating.setText(String.valueOf(currentApp.getRating()));
        String imageUrl = ProfileProperty.APPSTORE_IMAGE_URL + currentApp.getAppIcon();
        Glide.with(context)
                .load(imageUrl)
                .asBitmap()
                .into(AppIconUtil.getRoundedImageTarget(context, viewHolder.appIcon, 20));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = ((TextView) v.findViewById(R.id.single_app_name)).getText().toString();
                String app_id = ((TextView) v.findViewById(R.id.single_app_id)).getText().toString();

                Intent in = new Intent(context, AppDescriptionActivity.class);
                in.putExtra(Tags.TAG_NAME.name(), name);
                in.putExtra(Tags.TAG_APP_ID.name(), app_id);
                in.putExtra("app", appList.get(position));

                context.startActivity(in);
            }
        });

        return convertView;
    }

    private static class ViewHolder {
        TextView txtAppId, txtAppName, txtAppDescription, txtAppCost, appRating;
        ImageView appIcon;
    }

    @Override
    public int getCount() {
        if (appList != null)
            return appList.size();
        else
            return 0;
    }

    @Override
    public Application getItem(int position) {
        return appList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void setAppList(ArrayList<Application> appList) {
        this.appList = appList;
    }
}