package hms.appstore.android.util.manager.downloadable;

import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hms.appstore.android.R;
import hms.appstore.android.activity.app.AppInstallationActivity;
import hms.appstore.android.util.AppTags;
import hms.appstore.android.util.Property;
import hms.appstore.android.util.Tags;

public class DownloadBroadcastReceiver extends BroadcastReceiver {

    private SharedPreferences sharedPrefs;
    private Map<String, DownloadableApplication> appQueue;

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @Override
    public void onReceive(Context context, Intent intent)
    {
        String action = intent.getAction();
        long requestId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

        sharedPrefs = context.getSharedPreferences(Property.PREFS_NAME, 0);
        appQueue = getAppQueue();

        if (appQueue.containsKey(String.valueOf(requestId))) {
            DownloadableApplication app = appQueue.get(String.valueOf(requestId));
            DownloadManager dm = (DownloadManager) context.getSystemService(context.DOWNLOAD_SERVICE);

            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                DownloadManager.Query query = new DownloadManager.Query();
                if (requestId != 0) {
                    query.setFilterById(requestId);
                    Cursor c = dm.query(query);
                    if (c.moveToFirst()) {
                        int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);
                        if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                            try {
                                String uriString = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));

                                Intent activityIntent = new Intent(context, AppInstallationActivity.class);
                                activityIntent.putExtra(Tags.TAG_NAME.name(), app.getAppName());
                                activityIntent.putExtra(Tags.TAG_APP_ID.name(), app.getAppId());
                                activityIntent.putExtra(Property.DOWNLOAD_REQ_ID_EXTRA, app.getDownloadQueueId());
                                activityIntent.putExtra(Property.DOWNLOAD_REQ_URI_EXTRA, uriString);
                                activityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                                updateDownloadableQueueJson(requestId);
                                context.startActivity(activityIntent);
                            } catch (ActivityNotFoundException e) {
                                Toast.makeText(context, context.getString(R.string.app_install_download_failed_msg), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }
            }
        }

    }

    private void updateDownloadableQueueJson(long requestId) {
        DownloadableApplication app = appQueue.remove(String.valueOf(requestId));
        app.setDownloadComplete(true);
        appQueue.put(String.valueOf(requestId), app);

        DownloadableApplications downloadableApplications = new DownloadableApplications();
        downloadableApplications.setDownloadableAppsMap(appQueue);

        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(Property.PREFS_DOWNLOADABLE_APP_QUEUE, downloadableApplications.toJsonString());
        editor.commit();
    }

    private Map<String, DownloadableApplication> getAppQueue(){
        String queueJson = sharedPrefs.getString(Property.PREFS_DOWNLOADABLE_APP_QUEUE, "");
        Map<String, DownloadableApplication> downloadableQueue = new HashMap<>();
        DownloadableApplications downloadableApplications;

        if(!queueJson.equals("")) {
            downloadableApplications = new Gson().fromJson(queueJson, DownloadableApplications.class);
            downloadableQueue = downloadableApplications.getDownloadableAppsMap();
        }
        return downloadableQueue;
    }
}
