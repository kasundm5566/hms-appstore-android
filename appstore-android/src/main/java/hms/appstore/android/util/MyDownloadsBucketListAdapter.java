/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import java.util.List;

import hms.appstore.android.R;
import hms.appstore.android.activity.app.AppDescriptionActivity;
import hms.appstore.android.rest.response.Application;

public class MyDownloadsBucketListAdapter extends BaseAdapter {
    View bucketElement = null;

    ImageView appIcon;
    Context ctx;
    Application app;
    Typeface font = null;
    int singleAppLayout = R.layout.single_app_my_apps;
    List<Application> elements;

    public MyDownloadsBucketListAdapter(Activity ctx, List<Application> elements, Typeface font, int layout) {
        this.font = font;
        this.singleAppLayout = layout;
    }

    public MyDownloadsBucketListAdapter(Activity ctx, List<Application> elements, Typeface font) {
        this.font = font;
        this.ctx = ctx;
        this.elements = elements;
        this.singleAppLayout = R.layout.single_app_my_apps;
    }

    @Override
    public int getCount() {
        return elements.size();
    }

    @Override
    public Application getItem(int position) {
        return elements.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Application currentApp = getItem(position);
        bucketElement = View.inflate(ctx, singleAppLayout, null);

        TextView txtAppId = (TextView) bucketElement.findViewById(R.id.single_app_id);
        txtAppId.setText(currentApp.getId());

        TextView txtAppName = (TextView) bucketElement.findViewById(R.id.single_app_name);
        txtAppName.setText(currentApp.getName());

        TextView txtAppStatus = (TextView) bucketElement.findViewById(R.id.single_app_status);
        txtAppStatus.setText(Commons.capitalizeFirstLtr(currentApp.getDownloadStatus().getStatus()));

        TextView txtAppDeveloper = (TextView) bucketElement.findViewById(R.id.single_app_developer);
        txtAppDeveloper.setText(String.format(txtAppDeveloper.getText().toString(), currentApp.getDeveloper()));

        TextView txtRequestedDate = (TextView) bucketElement.findViewById(R.id.single_app_req_date);
        txtRequestedDate.setText(
                String.format(bucketElement.getContext().getString(R.string.my_apps_requested_date_download),
                        Commons.convertDate(currentApp.getDownloadStatus().getRequestedDate())));

        RatingBar appRatingBar = (RatingBar) bucketElement.findViewById(R.id.ratingBar1);
        appRatingBar.setRating(currentApp.getRating());

        appIcon = (ImageView) bucketElement.findViewById(R.id.icon);

        String imageUrl = ProfileProperty.APPSTORE_IMAGE_URL + currentApp.getAppIcon();
        // Get singletone instance of ImageLoader
        ImageLoader imageLoader = ImageLoader.getInstance();
        // Initialize ImageLoader with configuration. Do it once.
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(ctx)
                .threadPoolSize(5)
                .memoryCache(new UsingFreqLimitedMemoryCache(5 * 1024 * 1024))
                .imageDownloader(new BaseImageDownloader(ctx))
                .build();
        imageLoader.init(config);

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory()
                .cacheOnDisc()
                .build();

        // Load and display image asynchronously
        imageLoader.displayImage(imageUrl, appIcon, options);

        bucketElement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = ((TextView) v.findViewById(R.id.single_app_name)).getText().toString();
                String app_id = ((TextView) v.findViewById(R.id.single_app_id)).getText().toString();

                // Starting new intent
                Intent in = new Intent(ctx.getApplicationContext(), AppDescriptionActivity.class);
                in.putExtra(Tags.TAG_NAME.name(), name);
                in.putExtra(Tags.TAG_APP_ID.name(), app_id);

                ctx.startActivity(in);
            }
        });
        return bucketElement;
    }
}