package hms.appstore.android.util;

public class AppTags {

    public static final String TAG_CATEGORY = "category";
    public static final String TAG_ID = "id";
    public static final String TAG_NAME = "name";
    public static final String TAG_RATING = "rating";
    public static final String TAG_DESCRIPTION = "description";
    public static final String TAG_CREATED_BY = "createdBy";
    public static final String TAG_ICON_PATH = "icon";
    public static final String TAG_CHARGING_DETAIL = "charging";
    public static final String TAG_CHARGING_LABEL = "charging-lbl";
    public static final String TAG_SUPPORTED_OS_VERSIONS = "versions";
    public static final String TAG_NO_APP = "NO_APP_AVAILABLE";
    public static final String TAG_SUBSCRIPTION_COUNT = "SUBSCRIPTION_COUNT";

    public static final String APK_MIME_TYPE = "application/vnd.android.package-archive";
}
