package hms.appstore.android.util.manager.user;

import com.google.gson.Gson;

import hms.appstore.android.util.LoginKeys;

public class LoggedUser {

    private String sessionId;
    private LoginKeys loginMethod;
    private long loggedInTime;

    public LoggedUser(String sessionId, LoginKeys loginMethod, long loggedInTime) {
        this.sessionId = sessionId;
        this.loginMethod = loginMethod;
        this.loggedInTime = loggedInTime;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public long getLoggedInTime() {
        return loggedInTime;
    }

    public void setLoggedInTime(long loggedInTime) {
        this.loggedInTime = loggedInTime;
    }

    public String toJsonString(){
        Gson loggedUser = new Gson();
        return loggedUser.toJson(this);
    }

    public LoginKeys getLoginMethod() {
        return loginMethod;
    }

    public void setLoginMethod(LoginKeys loginMethod) {
        this.loginMethod = loginMethod;
    }
}
