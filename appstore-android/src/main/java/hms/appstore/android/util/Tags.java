/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.util;

public enum Tags {
    TAG_APP_ID,
    TAG_NAME,
    TAG_DESCRIPTION,
    TAG_RATING,
    TAG_CATEGORY,
    TAG_CREATED_BY,
    TAG_APP_ICON,
    TAG_PUSH_NOTIFICATION_ID
}
