/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.util;

import hms.appstore.android.util.Log;
import android.widget.Toast;

public class ToastExpander {

	public static final String TAG = "ToastExpander";
	private static Thread t;
	public static void showFor(final Toast aToast,
			final long durationInMilliseconds) {
		aToast.setDuration(Toast.LENGTH_SHORT);

		 t = new Thread() {
			long timeElapsed = 0l;

			public void run() {
				try {
					while (timeElapsed <= durationInMilliseconds) {
						long start = System.currentTimeMillis();
						aToast.show();
						sleep(1750);
						timeElapsed += System.currentTimeMillis() - start;
					}
				} catch (InterruptedException e) {
                        Log.d(TAG, e.toString());
				}
			}
		};
		t.start();
		}

}


