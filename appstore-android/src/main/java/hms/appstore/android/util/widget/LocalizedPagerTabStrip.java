package hms.appstore.android.util.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.support.v4.view.PagerTabStrip;
import android.util.AttributeSet;
import android.widget.TextView;

import hms.appstore.android.R;
import hms.appstore.android.util.AppstoreLocale;

public class LocalizedPagerTabStrip extends PagerTabStrip {

    public LocalizedPagerTabStrip(Context context) {
        super(context);
    }

    public LocalizedPagerTabStrip(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setDrawFullUnderline(true);
        this.setTabIndicatorColor(context.getResources().getColor(R.color.home_frag_highlighter));
    }

    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        for (int i=0; i<this.getChildCount(); i++) {
            if (this.getChildAt(i) instanceof TextView) {
                ((TextView)this.getChildAt(i)).setTypeface(AppstoreLocale.getLocaleTypeFace(getContext()));
            }
        }

    }

}
