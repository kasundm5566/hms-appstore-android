/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.android.util;

public enum SessionKeys {
    SESSION_ID,
    MOBILE_NUMBER,
    USERNAME,
    PASSWORD,
    LOGIN_METHOD,
    LOGIN_STATUS_CODE,
    ACCESS_TOKEN,
}
