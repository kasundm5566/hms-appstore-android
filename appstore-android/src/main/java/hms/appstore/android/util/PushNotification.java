package hms.appstore.android.util;

public class PushNotification {

    private String id;
    private String message;
    private boolean isRead = false;
    private long date;

    public PushNotification(String id, String message, boolean isRead, long date) {
        this.id = id;
        this.message = message;
        this.isRead = isRead;
        this.date = date;
    }

    public PushNotification(String message, boolean isRead, long date) {
        this.message = message;
        this.isRead = isRead;
        this.date = date;
    }

    public PushNotification(String reqID, boolean isRead) {
        this.id = reqID;
        this.isRead = isRead;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }
}
