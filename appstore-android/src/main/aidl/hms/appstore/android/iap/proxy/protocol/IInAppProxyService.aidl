/* The package where the aidl file is located */
package hms.appstore.android.iap.proxy.protocol;

/* Import Parcelable message */
import hms.appstore.android.iap.proxy.protocol.InAppChargingRequest;
import hms.appstore.android.iap.proxy.protocol.InAppChargingResponse;

/* The name of the remote inAppListener */
interface IInAppProxyService {

   InAppChargingResponse sendRequest(in InAppChargingRequest inAppChargingequest);

}