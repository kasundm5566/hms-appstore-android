/* The package where the aidl file is located */
package hms.appstore.android.iap.proxy.protocol;

/* Declare message as a class which implements the Parcelable interface */
parcelable InAppChargingRequest;