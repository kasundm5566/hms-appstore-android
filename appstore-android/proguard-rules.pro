# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/pasindu/installs/android/android-sdk-linux/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:

-keep class !hms.appstore.android.iap.proxy.** { *; }

-keepclassmembers class hms.appstore.android.iap.proxy.listener.InAppListener {
   public *;
}

-dontwarn org.apache.http.**
-dontwarn com.google.android.gms.**
-dontwarn org.codehaus.jackson.**

-keepattributes Signature
-dontwarn com.squareup.picasso.**
-dontwarn com.google.firebase.messaging.**
-dontwarn okio.**
-dontwarn retrofit2.Platform$Java8