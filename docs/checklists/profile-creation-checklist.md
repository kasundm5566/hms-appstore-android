# Creating a Profile/Flavor in Appstore Android

1. In the build.gradle file, create the new flavor details. Use the "operator" flavor's details and replace it with the new flavor's details.

        new_profile {
            applicationId 'hms.appstore.android.new_profile'
            minSdkVersion 7
            targetSdkVersion 19
            signingConfig signingConfigs.new_profile
        }

2. Create a directory in "appstore-android/src" for the new flavor. Use all simple letters.

        /appstore-android/src/new_profile/

3. Copy the contents from the "operator" flavor directory from src and paste them in the new flavor directory.

        /appstore-android/src/operator/assets -> /appstore-android/src/new_profile/assets
        /appstore-android/src/operator/res -> /appstore-android/src/new_profile/res

3. The "assets" directory will contain the font files for each locale. This consists of font_localeISO.ttf

        /appstore-android/src/new_profile/assets/font_es.ttf -> Spanish Font

4. In the "res" directory, update all the app icons, drawer icons for the new flavor. These located in drawable, drawable-hdpi, drawable-ldpi, drawable-mdpi, drawable-xhdpi, drawable-xxhdpi, drawable-xxxhdpi directories.

        /appstore-android/src/operator/res/drawable
        /appstore-android/src/operator/res/drawable-hdpi
        /appstore-android/src/operator/res/drawable-ldpi
        /appstore-android/src/operator/res/drawable-mdpi
        /appstore-android/src/operator/res/drawable-xhdpi
        /appstore-android/src/operator/res/drawable-xxhdpi
        /appstore-android/src/operator/res/drawable-xxchdpi

5. splash_screen.xml file inside the "layout" directory will have the settings for the Splash Screen. The Splash Screen's background colour is manipulated from splash_screen_bg_under.xml inside the "drawable" directory. Update them as neccassary.

        /appstore-android/src/operator/res/layout/splash_screen.xml -> Splash Screen Settings

        /appstore-android/src/operator/res/drawable/splash_screen_bg_under.xml -> Splash Screen Backgorund Colour Settings

        <gradient
            android:angle="90"
            android:startColor="#2ecc71"
            android:centerColor="#2ecc71"
            android:endColor="#2ecc71"
            android:type="linear" />
        <corners android:radius="0dp"/>

6. Update profile_property.xml in "raw" directory for the following properties; registration.link, forgot.password.link, operator.name, apk.folder.path and default.locale

        /appstore-android/src/operator/res/raw/profile_property.xml

        <entry key="registration.link">http://www.hsenidmobile.com/</entry>
        <entry key="forgot.password.link">http://www.hsenidmobile.com/</entry>
        <entry key="operator.name">new_profile</entry>
        <entry key="apk.folder.path">/new_profile</entry>

**default.locale will determine which locale the app will start in.

7. Update the color scheme for the app in colors.xml from the "values" directory.

        /appstore-android/src/operator/res/values/colours.xml

8. Update the mobile number and password length for the app in config.xml from the "values" directory.

        /appstore-android/src/operator/res/values/config.xml

        <string name="country_mobile_code" translatable="false">948</string>
        <integer name="register_screen_mobile_num_length" translatable="false">10</integer>
        <integer name="user_password_min_length" translatable="false">6</integer>

9. Update the available features for the app in features.xml from the "values" directory.

        /appstore-android/src/operator/res/values/features.xml

10. Update the basic strings for the app in strings.xml from the "values" directory.

        /appstore-android/src/operator/res/values/strings.xml

## Adding a new Locale

1. If you add a new locale, specify the new locale to features.xml and make sure you update that change across all the flavors.

        /appstore-android/src/operator/res/values/features.xml

        <bool name="spanish_language_support">true</bool>

2. Add the Radio Button for the new locale in settings.xml

        /appstore-android/src/main/res/layout/settings.xml

        <hms.appstore.android.util.widget.LocalizedRadioButton
            android:id="@+id/lang_spanish_radio_btn"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:paddingLeft="40dip"
            android:text="@string/language_spanish"
            android:textColor="@color/text_color"
            android:textSize="@dimen/setting_screen_text_size" />

3. Update the radio button accordingly in the SettingsActivity.java

        /appstore-android/src/main/java/hms/appstore/android/activity/app/SettingsActivity.java

        private void initializeUIElements(){
            .
            .
            .
            spanishLangSupportRadioBtn = (LocalizedRadioButton) findViewById(R.id.lang_spanish_radio_btn);
        }

        private void activateLanguageSupportSettings(){
            if(getResources().getBoolean(R.bool.multi_language_support)){
                .
                .
                .

                if(getResources().getBoolean(R.bool.spanish_language_support)){
                    localeMap.put(AppstoreLocale.SPANISH_LOCALE, spanishLangSupportRadioBtn.getId());
                } else {
                    spanishLangSupportRadioBtn.setVisibility(View.GONE);
                }

                .
                .
                .
            } else {
                langSupportArea.setVisibility(View.GONE);
            }
        }

4. Update Locale details in AppstoreLocale.java file.

        /appstore-android/src/main/java/hms/appstore/android/util/AppstoreLocale.java

        public static final String SPANISH_LOCALE = "es";

5. Update strings.xml in all the flavors with the new String "@string/language_spanish"

        <string name="language_spanish">Spanish</string> -> In the English Strings File
        <string name="language_spanish">испанский</string> -> In the Russian Strings File

6. In the operator flavor, locales Russian (ru) and Bengali (bn) has been already added. Copy the contents, paste and rename it with the new locales ISO or create one with the editor in IDEA. If a Locale is unique only to one Flavor, have that particular values directory in that flavor directory only.

        /appstore-android/src/new_profile/res/values-es/

7. To test out the new Locale, launch the app, go to Settings and Switch between Languages.